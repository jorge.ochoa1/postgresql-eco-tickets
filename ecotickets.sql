--
-- PostgreSQL database dump
--

-- Dumped from database version 10.8
-- Dumped by pg_dump version 10.8

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: alfio_check_row_access(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.alfio_check_row_access(integer) RETURNS boolean
    LANGUAGE sql
    AS $_$
    select (not coalesce(current_setting('alfio.checkRowAccess', true), 'false') = 'true') or

    (current_setting('alfio.checkRowAccess', true) = 'true' and ($1)::text = any(string_to_array(coalesce(current_setting('alfio.currentUserOrgs', true), ''),',')))
$_$;


ALTER FUNCTION public.alfio_check_row_access(integer) OWNER TO postgres;

--
-- Name: jsonb_recursive_merge(jsonb, jsonb); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.jsonb_recursive_merge(a jsonb, b jsonb) RETURNS jsonb
    LANGUAGE sql
    AS $$
select
    jsonb_object_agg(
        coalesce(ka, kb),
        case
            when va isnull then vb
            when vb isnull then va
            when jsonb_typeof(va) <> 'object' or jsonb_typeof(vb) <> 'object' then vb
            else jsonb_recursive_merge(va, vb) end
        )
    from jsonb_each(a) e1(ka, va)
    full join jsonb_each(b) e2(kb, vb) on ka = kb
$$;


ALTER FUNCTION public.jsonb_recursive_merge(a jsonb, b jsonb) OWNER TO postgres;

--
-- Name: set_organization_id_fk_from_a_group_id_fk(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.set_organization_id_fk_from_a_group_id_fk() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ begin
    if new.organization_id_fk is null then
      new.organization_id_fk = (select a_group.organization_id_fk from a_group where a_group.id = new.a_group_id_fk);
    end if;
    return new;
end;
$$;


ALTER FUNCTION public.set_organization_id_fk_from_a_group_id_fk() OWNER TO postgres;

--
-- Name: set_organization_id_fk_from_additional_service_id_fk(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.set_organization_id_fk_from_additional_service_id_fk() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ begin
    if new.organization_id_fk is null then
      new.organization_id_fk = (select additional_service.organization_id_fk from additional_service where additional_service.id = new.additional_service_id_fk);
    end if;
    return new;
end;
$$;


ALTER FUNCTION public.set_organization_id_fk_from_additional_service_id_fk() OWNER TO postgres;

--
-- Name: set_organization_id_fk_from_event_id(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.set_organization_id_fk_from_event_id() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ begin
    if new.organization_id_fk is null then
      new.organization_id_fk = (select org_id from event where event.id = new.event_id);
    end if;
    return new;
end;
$$;


ALTER FUNCTION public.set_organization_id_fk_from_event_id() OWNER TO postgres;

--
-- Name: set_organization_id_fk_from_event_id_fk(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.set_organization_id_fk_from_event_id_fk() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ begin
    if new.organization_id_fk is null then
      new.organization_id_fk = (select org_id from event where event.id = new.event_id_fk);
    end if;
    return new;
end;
$$;


ALTER FUNCTION public.set_organization_id_fk_from_event_id_fk() OWNER TO postgres;

--
-- Name: set_organization_id_fk_from_group_member_id_fk(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.set_organization_id_fk_from_group_member_id_fk() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ begin
    if new.organization_id_fk is null then
      new.organization_id_fk = (select group_member.organization_id_fk from group_member where group_member.id = new.group_member_id_fk);
    end if;
    return new;
end;
$$;


ALTER FUNCTION public.set_organization_id_fk_from_group_member_id_fk() OWNER TO postgres;

--
-- Name: set_organization_id_fk_from_reservation_id(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.set_organization_id_fk_from_reservation_id() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ begin
    if new.organization_id_fk is null then
      new.organization_id_fk = (select tickets_reservation.organization_id_fk from tickets_reservation where tickets_reservation.id = new.reservation_id);
    end if;
    return new;
end;
$$;


ALTER FUNCTION public.set_organization_id_fk_from_reservation_id() OWNER TO postgres;

--
-- Name: set_organization_id_fk_from_ticket_category_id(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.set_organization_id_fk_from_ticket_category_id() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ begin
    if new.organization_id_fk is null then
      new.organization_id_fk = (select ticket_category.organization_id_fk from ticket_category where ticket_category.id = new.ticket_category_id);
    end if;
    return new;
end;
$$;


ALTER FUNCTION public.set_organization_id_fk_from_ticket_category_id() OWNER TO postgres;

--
-- Name: set_organization_id_fk_from_ticket_category_id_fk(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.set_organization_id_fk_from_ticket_category_id_fk() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ begin
    if new.organization_id_fk is null then
      new.organization_id_fk = (select ticket_category.organization_id_fk from ticket_category where ticket_category.id = new.ticket_category_id_fk);
    end if;
    return new;
end;
$$;


ALTER FUNCTION public.set_organization_id_fk_from_ticket_category_id_fk() OWNER TO postgres;

--
-- Name: set_organization_id_fk_from_ticket_field_configuration_id_fk(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.set_organization_id_fk_from_ticket_field_configuration_id_fk() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ begin
    if new.organization_id_fk is null then
      new.organization_id_fk = (select ticket_field_configuration.organization_id_fk from ticket_field_configuration where ticket_field_configuration.id = new.ticket_field_configuration_id_fk);
    end if;
    return new;
end;
$$;


ALTER FUNCTION public.set_organization_id_fk_from_ticket_field_configuration_id_fk() OWNER TO postgres;

--
-- Name: trf_check_access_code_allocation(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.trf_check_access_code_allocation() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    r_count numeric;
BEGIN
    IF (NEW.access_code_id_fk is not null) THEN
        -- serialize by locking all tokens for the current category
        PERFORM * from special_price where ticket_category_id = NEW.ticket_category_id for update;
        r_count = (select count(*) from special_price where access_code_id_fk = NEW.access_code_id_fk);
        PERFORM max_usage from promo_code where id = NEW.access_code_id_fk and max_usage is not null and
               max_usage < r_count;
        IF FOUND THEN
            raise EXCEPTION USING MESSAGE = ('Max usage exceeded. Tokens requested: ' || r_count);
        END IF;
    END IF;
    RETURN NULL;
END
$$;


ALTER FUNCTION public.trf_check_access_code_allocation() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: a_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.a_group (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(2048),
    organization_id_fk integer NOT NULL,
    active boolean DEFAULT true
);

ALTER TABLE ONLY public.a_group FORCE ROW LEVEL SECURITY;


ALTER TABLE public.a_group OWNER TO postgres;

--
-- Name: a_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.a_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.a_group_id_seq OWNER TO postgres;

--
-- Name: a_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.a_group_id_seq OWNED BY public.a_group.id;


--
-- Name: additional_service; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.additional_service (
    id integer NOT NULL,
    event_id_fk integer NOT NULL,
    price_cts integer,
    fix_price boolean NOT NULL,
    ordinal integer DEFAULT 0 NOT NULL,
    available_qty integer DEFAULT '-1'::integer NOT NULL,
    max_qty_per_order integer DEFAULT '-1'::integer NOT NULL,
    inception_ts timestamp with time zone,
    expiration_ts timestamp with time zone,
    vat numeric(5,2),
    vat_type character varying(50) NOT NULL,
    src_price_cts integer DEFAULT 0 NOT NULL,
    service_type character varying(255),
    supplement_policy character varying(255),
    organization_id_fk integer NOT NULL
);

ALTER TABLE ONLY public.additional_service FORCE ROW LEVEL SECURITY;


ALTER TABLE public.additional_service OWNER TO postgres;

--
-- Name: additional_service_description; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.additional_service_description (
    id integer NOT NULL,
    additional_service_id_fk integer NOT NULL,
    locale character varying(8) NOT NULL,
    type character varying(16) NOT NULL,
    value character varying(2048) NOT NULL,
    organization_id_fk integer NOT NULL
);

ALTER TABLE ONLY public.additional_service_description FORCE ROW LEVEL SECURITY;


ALTER TABLE public.additional_service_description OWNER TO postgres;

--
-- Name: additional_service_description_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.additional_service_description_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.additional_service_description_id_seq OWNER TO postgres;

--
-- Name: additional_service_description_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.additional_service_description_id_seq OWNED BY public.additional_service_description.id;


--
-- Name: additional_service_field_value; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.additional_service_field_value (
    additional_service_id_fk integer NOT NULL,
    ticket_field_configuration_id_fk integer NOT NULL,
    field_value character varying(2048)
);


ALTER TABLE public.additional_service_field_value OWNER TO postgres;

--
-- Name: additional_service_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.additional_service_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.additional_service_id_seq OWNER TO postgres;

--
-- Name: additional_service_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.additional_service_id_seq OWNED BY public.additional_service.id;


--
-- Name: additional_service_item; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.additional_service_item (
    id integer NOT NULL,
    uuid character(36) NOT NULL,
    creation timestamp with time zone NOT NULL,
    last_modified timestamp with time zone,
    tickets_reservation_uuid character(36),
    additional_service_id_fk integer NOT NULL,
    original_price_cts integer,
    paid_price_cts integer,
    status character varying(20),
    event_id_fk integer NOT NULL,
    src_price_cts integer DEFAULT 0 NOT NULL,
    final_price_cts integer DEFAULT 0 NOT NULL,
    vat_cts integer DEFAULT 0 NOT NULL,
    discount_cts integer DEFAULT 0 NOT NULL,
    organization_id_fk integer NOT NULL,
    currency_code character varying(10)
);

ALTER TABLE ONLY public.additional_service_item FORCE ROW LEVEL SECURITY;


ALTER TABLE public.additional_service_item OWNER TO postgres;

--
-- Name: additional_service_item_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.additional_service_item_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.additional_service_item_id_seq OWNER TO postgres;

--
-- Name: additional_service_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.additional_service_item_id_seq OWNED BY public.additional_service_item.id;


--
-- Name: event; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.event (
    id integer NOT NULL,
    short_name character varying(2048) NOT NULL,
    location character varying(2048) NOT NULL,
    latitude character varying(255),
    longitude character varying(255),
    start_ts timestamp with time zone NOT NULL,
    end_ts timestamp with time zone NOT NULL,
    time_zone character varying(255) NOT NULL,
    regular_price_cts integer NOT NULL,
    currency character varying(3),
    available_seats integer NOT NULL,
    vat_included boolean NOT NULL,
    vat numeric(5,2) NOT NULL,
    allowed_payment_proxies character varying(2048) NOT NULL,
    private_key character varying(2048) NOT NULL,
    org_id integer NOT NULL,
    website_url character varying(2048) DEFAULT ''::character varying NOT NULL,
    website_t_c_url character varying(2048) DEFAULT ''::character varying NOT NULL,
    image_url character varying(2048),
    file_blob_id character(64),
    display_name character varying(255),
    locales integer DEFAULT 7 NOT NULL,
    type character varying(20) DEFAULT 'INTERNAL'::character varying NOT NULL,
    external_url character varying(1024),
    src_price_cts integer DEFAULT 0 NOT NULL,
    vat_status character varying(50) DEFAULT 'NONE'::character varying NOT NULL,
    version character varying(50),
    status character varying(255) DEFAULT 'PUBLIC'::character varying NOT NULL,
    website_p_p_url character varying(2048)
);

ALTER TABLE ONLY public.event FORCE ROW LEVEL SECURITY;


ALTER TABLE public.event OWNER TO postgres;

--
-- Name: additional_service_with_currency; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.additional_service_with_currency AS
 SELECT asv.id,
    asv.event_id_fk,
    asv.price_cts,
    asv.fix_price,
    asv.ordinal,
    asv.available_qty,
    asv.max_qty_per_order,
    asv.inception_ts,
    asv.expiration_ts,
    asv.vat,
    asv.vat_type,
    asv.src_price_cts,
    asv.service_type,
    asv.supplement_policy,
    asv.organization_id_fk,
    e.currency AS currency_code
   FROM public.additional_service asv,
    public.event e
  WHERE (asv.event_id_fk = e.id);


ALTER TABLE public.additional_service_with_currency OWNER TO postgres;

--
-- Name: admin_job_queue; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.admin_job_queue (
    id bigint NOT NULL,
    job_name character varying(255) NOT NULL,
    request_ts timestamp without time zone NOT NULL,
    status character varying(255) NOT NULL,
    execution_ts timestamp without time zone,
    metadata jsonb
);


ALTER TABLE public.admin_job_queue OWNER TO postgres;

--
-- Name: admin_job_queue_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.admin_job_queue_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_job_queue_id_seq OWNER TO postgres;

--
-- Name: admin_job_queue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.admin_job_queue_id_seq OWNED BY public.admin_job_queue.id;


--
-- Name: admin_reservation_request; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.admin_reservation_request (
    id bigint NOT NULL,
    request_id character varying(512) NOT NULL,
    user_id bigint NOT NULL,
    event_id bigint NOT NULL,
    reservation_id character varying(512),
    request_type character varying(64),
    status character varying(64),
    body text,
    failure_code character varying(255),
    organization_id_fk integer NOT NULL
);

ALTER TABLE ONLY public.admin_reservation_request FORCE ROW LEVEL SECURITY;


ALTER TABLE public.admin_reservation_request OWNER TO postgres;

--
-- Name: admin_reservation_request_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.admin_reservation_request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_reservation_request_id_seq OWNER TO postgres;

--
-- Name: admin_reservation_request_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.admin_reservation_request_id_seq OWNED BY public.admin_reservation_request.id;


--
-- Name: admin_reservation_request_stats; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.admin_reservation_request_stats AS
 SELECT admin_reservation_request.request_id,
    admin_reservation_request.user_id,
    admin_reservation_request.event_id,
    sum(
        CASE ((admin_reservation_request.status)::text = 'PENDING'::text)
            WHEN true THEN 1
            ELSE 0
        END) AS count_pending,
    sum(
        CASE ((admin_reservation_request.status)::text = 'SUCCESS'::text)
            WHEN true THEN 1
            ELSE 0
        END) AS count_success,
    sum(
        CASE ((admin_reservation_request.status)::text = 'ERROR'::text)
            WHEN true THEN 1
            ELSE 0
        END) AS count_error
   FROM public.admin_reservation_request
  GROUP BY admin_reservation_request.request_id, admin_reservation_request.event_id, admin_reservation_request.user_id;


ALTER TABLE public.admin_reservation_request_stats OWNER TO postgres;

--
-- Name: auditing; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auditing (
    reservation_id character varying(512),
    user_id integer,
    event_type character varying(128),
    event_time timestamp without time zone NOT NULL,
    entity_type character varying(64),
    entity_id character varying(512),
    modifications text,
    event_id integer,
    organization_id_fk integer
);

ALTER TABLE ONLY public.auditing FORCE ROW LEVEL SECURITY;


ALTER TABLE public.auditing OWNER TO postgres;

--
-- Name: ba_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ba_user (
    id integer NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(2048) NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    email_address character varying(255) NOT NULL,
    enabled boolean DEFAULT true,
    user_type character varying(255) DEFAULT 'INTERNAL'::character varying,
    user_creation_time timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    valid_to timestamp without time zone,
    description character varying(256)
);


ALTER TABLE public.ba_user OWNER TO postgres;

--
-- Name: auditing_user; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.auditing_user AS
 SELECT auditing.reservation_id,
    auditing.user_id,
    auditing.event_type,
    auditing.event_time,
    auditing.entity_type,
    auditing.entity_id,
    auditing.modifications,
    ba_user.id,
    ba_user.username,
    ba_user.password,
    ba_user.first_name,
    ba_user.last_name,
    ba_user.email_address,
    ba_user.enabled
   FROM (public.auditing
     LEFT JOIN public.ba_user ON ((auditing.user_id = ba_user.id)));


ALTER TABLE public.auditing_user OWNER TO postgres;

--
-- Name: authority; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.authority (
    username character varying(255) NOT NULL,
    role character varying(255) NOT NULL
);


ALTER TABLE public.authority OWNER TO postgres;

--
-- Name: b_transaction; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.b_transaction (
    id integer NOT NULL,
    gtw_tx_id character varying(2048) NOT NULL,
    reservation_id character(36) NOT NULL,
    t_timestamp timestamp with time zone NOT NULL,
    price_cts integer NOT NULL,
    currency character varying(255) NOT NULL,
    description character varying(2048) NOT NULL,
    payment_proxy character varying(2048) NOT NULL,
    gtw_payment_id character varying(2048),
    plat_fee integer DEFAULT 0 NOT NULL,
    gtw_fee integer DEFAULT 0 NOT NULL,
    organization_id_fk integer NOT NULL,
    status character(32) DEFAULT 'COMPLETE'::bpchar NOT NULL,
    metadata jsonb
);

ALTER TABLE ONLY public.b_transaction FORCE ROW LEVEL SECURITY;


ALTER TABLE public.b_transaction OWNER TO postgres;

--
-- Name: b_transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.b_transaction_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.b_transaction_id_seq OWNER TO postgres;

--
-- Name: b_transaction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.b_transaction_id_seq OWNED BY public.b_transaction.id;


--
-- Name: ba_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ba_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ba_user_id_seq OWNER TO postgres;

--
-- Name: ba_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ba_user_id_seq OWNED BY public.ba_user.id;


--
-- Name: billing_document; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.billing_document (
    id bigint NOT NULL,
    event_id_fk integer NOT NULL,
    number character varying(255) NOT NULL,
    reservation_id_fk character(36) NOT NULL,
    type character varying(255) NOT NULL,
    model text,
    generation_ts timestamp with time zone NOT NULL,
    external_id character varying(255),
    status character varying(255) NOT NULL,
    organization_id_fk integer NOT NULL
);

ALTER TABLE ONLY public.billing_document FORCE ROW LEVEL SECURITY;


ALTER TABLE public.billing_document OWNER TO postgres;

--
-- Name: billing_document_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.billing_document_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.billing_document_id_seq OWNER TO postgres;

--
-- Name: billing_document_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.billing_document_id_seq OWNED BY public.billing_document.id;


--
-- Name: configuration; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.configuration (
    id integer NOT NULL,
    c_key character varying(255) NOT NULL,
    c_value text NOT NULL,
    description character varying(2048)
);


ALTER TABLE public.configuration OWNER TO postgres;

--
-- Name: configuration_event; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.configuration_event (
    id integer NOT NULL,
    organization_id_fk integer NOT NULL,
    event_id_fk integer NOT NULL,
    c_key character varying(255) NOT NULL,
    c_value text NOT NULL,
    description character varying(2048)
);

ALTER TABLE ONLY public.configuration_event FORCE ROW LEVEL SECURITY;


ALTER TABLE public.configuration_event OWNER TO postgres;

--
-- Name: configuration_event_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.configuration_event_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.configuration_event_id_seq OWNER TO postgres;

--
-- Name: configuration_event_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.configuration_event_id_seq OWNED BY public.configuration_event.id;


--
-- Name: configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.configuration_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.configuration_id_seq OWNER TO postgres;

--
-- Name: configuration_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.configuration_id_seq OWNED BY public.configuration.id;


--
-- Name: configuration_organization; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.configuration_organization (
    id integer NOT NULL,
    organization_id_fk integer NOT NULL,
    c_key character varying(255) NOT NULL,
    c_value text NOT NULL,
    description character varying(2048)
);

ALTER TABLE ONLY public.configuration_organization FORCE ROW LEVEL SECURITY;


ALTER TABLE public.configuration_organization OWNER TO postgres;

--
-- Name: configuration_organization_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.configuration_organization_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.configuration_organization_id_seq OWNER TO postgres;

--
-- Name: configuration_organization_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.configuration_organization_id_seq OWNED BY public.configuration_organization.id;


--
-- Name: configuration_ticket_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.configuration_ticket_category (
    id integer NOT NULL,
    organization_id_fk integer NOT NULL,
    event_id_fk integer NOT NULL,
    ticket_category_id_fk integer NOT NULL,
    c_key character varying(255) NOT NULL,
    c_value text NOT NULL,
    description character varying(2048)
);

ALTER TABLE ONLY public.configuration_ticket_category FORCE ROW LEVEL SECURITY;


ALTER TABLE public.configuration_ticket_category OWNER TO postgres;

--
-- Name: configuration_ticket_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.configuration_ticket_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.configuration_ticket_category_id_seq OWNER TO postgres;

--
-- Name: configuration_ticket_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.configuration_ticket_category_id_seq OWNED BY public.configuration_ticket_category.id;


--
-- Name: dynamic_field_template; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.dynamic_field_template (
    id integer NOT NULL,
    field_name character varying(64) NOT NULL,
    field_type character varying(64) NOT NULL,
    field_restricted_values text,
    field_description text,
    field_maxlength integer,
    field_minlength integer
);


ALTER TABLE public.dynamic_field_template OWNER TO postgres;

--
-- Name: dynamic_field_template_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.dynamic_field_template_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dynamic_field_template_id_seq OWNER TO postgres;

--
-- Name: dynamic_field_template_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.dynamic_field_template_id_seq OWNED BY public.dynamic_field_template.id;


--
-- Name: email_message; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.email_message (
    id integer NOT NULL,
    event_id integer NOT NULL,
    status character varying(255) NOT NULL,
    recipient character varying(255) NOT NULL,
    subject character varying(255) NOT NULL,
    message text NOT NULL,
    attachments text,
    checksum character varying(255) NOT NULL,
    request_ts timestamp with time zone NOT NULL,
    sent_ts timestamp with time zone,
    attempts integer DEFAULT 0 NOT NULL,
    email_cc text,
    organization_id_fk integer NOT NULL,
    reservation_id character(36)
);

ALTER TABLE ONLY public.email_message FORCE ROW LEVEL SECURITY;


ALTER TABLE public.email_message OWNER TO postgres;

--
-- Name: email_message_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.email_message_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.email_message_id_seq OWNER TO postgres;

--
-- Name: email_message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.email_message_id_seq OWNED BY public.email_message.id;


--
-- Name: event_description_text; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.event_description_text (
    event_id_fk integer NOT NULL,
    locale character varying(8) NOT NULL,
    type character varying(16) NOT NULL,
    description character varying(2048) NOT NULL,
    organization_id_fk integer NOT NULL
);

ALTER TABLE ONLY public.event_description_text FORCE ROW LEVEL SECURITY;


ALTER TABLE public.event_description_text OWNER TO postgres;

--
-- Name: event_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.event_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.event_id_seq OWNER TO postgres;

--
-- Name: event_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.event_id_seq OWNED BY public.event.id;


--
-- Name: event_migration; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.event_migration (
    id integer NOT NULL,
    event_id integer NOT NULL,
    current_version character varying(1024) NOT NULL,
    build_ts timestamp without time zone NOT NULL,
    status character varying(255)
);


ALTER TABLE public.event_migration OWNER TO postgres;

--
-- Name: event_migration_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.event_migration_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.event_migration_id_seq OWNER TO postgres;

--
-- Name: event_migration_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.event_migration_id_seq OWNED BY public.event_migration.id;


--
-- Name: ticket; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ticket (
    id integer NOT NULL,
    uuid character(36) NOT NULL,
    creation timestamp with time zone NOT NULL,
    category_id integer,
    event_id integer NOT NULL,
    status character varying(255) NOT NULL,
    original_price_cts integer NOT NULL,
    paid_price_cts integer NOT NULL,
    tickets_reservation_id character(36),
    full_name character varying(255),
    email_address character varying(255),
    special_price_id_fk integer,
    locked_assignment boolean DEFAULT false,
    user_language character varying(20),
    reminder_sent boolean DEFAULT false,
    src_price_cts integer DEFAULT 0 NOT NULL,
    final_price_cts integer DEFAULT 0 NOT NULL,
    vat_cts integer DEFAULT 0 NOT NULL,
    discount_cts integer DEFAULT 0 NOT NULL,
    first_name character varying(255),
    last_name character varying(255),
    ext_reference character varying(255),
    organization_id_fk integer NOT NULL,
    currency_code character varying(10)
);

ALTER TABLE ONLY public.ticket FORCE ROW LEVEL SECURITY;


ALTER TABLE public.ticket OWNER TO postgres;

--
-- Name: ticket_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ticket_category (
    id integer NOT NULL,
    inception timestamp with time zone NOT NULL,
    expiration timestamp with time zone NOT NULL,
    max_tickets integer NOT NULL,
    name character varying(255) NOT NULL,
    price_cts integer NOT NULL,
    access_restricted boolean NOT NULL,
    tc_status character varying(255),
    event_id integer NOT NULL,
    bounded boolean DEFAULT true NOT NULL,
    src_price_cts integer DEFAULT 0 NOT NULL,
    category_code character varying(255),
    valid_checkin_from timestamp with time zone,
    valid_checkin_to timestamp with time zone,
    ticket_validity_start timestamp with time zone,
    ticket_validity_end timestamp with time zone,
    organization_id_fk integer NOT NULL,
    ticket_checkin_strategy character varying(255) DEFAULT 'ONCE_PER_EVENT'::character varying NOT NULL,
    ordinal integer DEFAULT 0 NOT NULL
);

ALTER TABLE ONLY public.ticket_category FORCE ROW LEVEL SECURITY;


ALTER TABLE public.ticket_category OWNER TO postgres;

--
-- Name: tickets_reservation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tickets_reservation (
    id character(36) NOT NULL,
    validity timestamp with time zone NOT NULL,
    status character varying(255) NOT NULL,
    full_name character varying(255),
    email_address character varying(255),
    billing_address character varying(4096),
    confirmation_ts timestamp with time zone,
    payment_method character varying(255),
    offline_payment_reminder_sent boolean DEFAULT false,
    promo_code_id_fk integer,
    latest_reminder_ts timestamp with time zone,
    automatic boolean DEFAULT false NOT NULL,
    user_language character varying(10),
    direct_assignment boolean DEFAULT false NOT NULL,
    first_name character varying(255),
    last_name character varying(255),
    invoice_number character varying(256),
    invoice_model text,
    event_id_fk integer NOT NULL,
    vat_status character varying(50),
    vat_nr character varying(255),
    vat_country character varying(2),
    invoice_requested boolean DEFAULT false,
    used_vat_percent numeric(5,2),
    vat_included boolean,
    creation_ts timestamp with time zone,
    customer_reference character varying(512),
    billing_address_company character varying(512),
    billing_address_line1 character varying(512),
    billing_address_line2 character varying(512),
    billing_address_zip character varying(512),
    billing_address_city character varying(512),
    validated_for_overview boolean,
    skip_vat_nr boolean,
    invoicing_additional_information jsonb,
    registration_ts timestamp with time zone,
    organization_id_fk integer NOT NULL,
    add_company_billing_details boolean,
    src_price_cts integer DEFAULT 0 NOT NULL,
    final_price_cts integer DEFAULT 0 NOT NULL,
    vat_cts integer DEFAULT 0 NOT NULL,
    discount_cts integer DEFAULT 0 NOT NULL,
    currency_code character varying(3)
);

ALTER TABLE ONLY public.tickets_reservation FORCE ROW LEVEL SECURITY;


ALTER TABLE public.tickets_reservation OWNER TO postgres;

--
-- Name: ticket_category_statistics; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.ticket_category_statistics AS
 SELECT res.ticket_category_id,
    res.access_restricted,
    res.max_tickets,
    res.bounded,
    res.is_expired,
    res.event_id,
    res.pending_count,
    res.checked_in_count,
    res.sold_tickets_count,
    res.released_count,
    res.not_sold_tickets,
    res.stuck_count,
    (res.is_expired AND (res.not_sold_tickets > 0)) AS is_containing_orphan_tickets,
    (res.stuck_count > 0) AS is_containing_stuck_tickets
   FROM ( SELECT ticket_cat.id AS ticket_category_id,
            ticket_cat.access_restricted,
            ticket_cat.max_tickets,
            ticket_cat.bounded,
            ticket_cat.is_expired,
            ticket_cat.event_id,
            COALESCE(tickets_stats.pending_count, (0)::bigint) AS pending_count,
            COALESCE(tickets_stats.checked_in_count, (0)::bigint) AS checked_in_count,
            COALESCE(tickets_stats.sold_tickets_count, (0)::bigint) AS sold_tickets_count,
            COALESCE(tickets_stats.released_count, (0)::bigint) AS released_count,
                CASE ticket_cat.bounded
                    WHEN false THEN (0)::bigint
                    ELSE (((ticket_cat.max_tickets - COALESCE(tickets_stats.sold_tickets_count, (0)::bigint)) - COALESCE(tickets_stats.checked_in_count, (0)::bigint)) - COALESCE(tickets_stats.pending_count, (0)::bigint))
                END AS not_sold_tickets,
            COALESCE(stuck_count.stuck_count, (0)::bigint) AS stuck_count
           FROM ((( SELECT ticket_category.max_tickets,
                    ticket_category.bounded,
                    ticket_category.id,
                    ticket_category.event_id,
                    (ticket_category.expiration < now()) AS is_expired,
                    ticket_category.access_restricted
                   FROM public.ticket_category) ticket_cat
             LEFT JOIN ( SELECT sum(
                        CASE ((ticket.status)::text = 'PENDING'::text)
                            WHEN true THEN 1
                            ELSE 0
                        END) AS pending_count,
                    sum(
                        CASE ((ticket.status)::text = 'RELEASED'::text)
                            WHEN true THEN 1
                            ELSE 0
                        END) AS released_count,
                    sum(
                        CASE ((ticket.status)::text = 'CHECKED_IN'::text)
                            WHEN true THEN 1
                            ELSE 0
                        END) AS checked_in_count,
                    sum(
                        CASE ((ticket.status)::text = ANY ((ARRAY['TO_BE_PAID'::character varying, 'ACQUIRED'::character varying])::text[]))
                            WHEN true THEN 1
                            ELSE 0
                        END) AS sold_tickets_count,
                    ticket.category_id
                   FROM (public.ticket
                     JOIN public.ticket_category tc ON ((ticket.category_id = tc.id)))
                  GROUP BY ticket.category_id) tickets_stats ON ((ticket_cat.id = tickets_stats.category_id)))
             LEFT JOIN ( SELECT count(*) AS stuck_count,
                    ticket.category_id
                   FROM (public.ticket
                     JOIN public.tickets_reservation ON ((tickets_reservation.id = ticket.tickets_reservation_id)))
                  WHERE ((tickets_reservation.status)::text = 'STUCK'::text)
                  GROUP BY ticket.category_id) stuck_count ON ((ticket_cat.id = stuck_count.category_id)))) res;


ALTER TABLE public.ticket_category_statistics OWNER TO postgres;

--
-- Name: events_statistics; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.events_statistics AS
 SELECT event.id,
    ( SELECT count(ticket.id) AS count
           FROM public.ticket
          WHERE ((ticket.event_id = event.id) AND ((ticket.status)::text <> ALL ((ARRAY['INVALIDATED'::character varying, 'EXPIRED'::character varying])::text[])))) AS available_seats,
        CASE stats.contains_unbounded_categories
            WHEN true THEN (0)::bigint
            ELSE (( SELECT count(ticket.id) AS count
               FROM public.ticket
              WHERE ((ticket.event_id = event.id) AND ((ticket.status)::text <> ALL ((ARRAY['INVALIDATED'::character varying, 'EXPIRED'::character varying])::text[])))) - stats.allocated_count)
        END AS not_allocated_tickets,
    stats.pending_count AS pending_tickets,
    stats.sold_tickets_count AS sold_tickets,
    ( SELECT (stats.released_count + (count(ticket.id))::numeric)
           FROM public.ticket
          WHERE ((ticket.event_id = event.id) AND ((ticket.status)::text = 'RELEASED'::text) AND (ticket.category_id IS NULL))) AS released_tickets,
    stats.checked_in_count AS checked_in_tickets,
        CASE stats.contains_unbounded_categories
            WHEN true THEN (((((((( SELECT count(ticket.id) AS count
               FROM public.ticket
              WHERE ((ticket.event_id = event.id) AND ((ticket.status)::text <> ALL ((ARRAY['INVALIDATED'::character varying, 'EXPIRED'::character varying])::text[])))) - stats.allocated_count))::numeric - stats.released_count) - stats.sold_tickets_count_unbounded) - stats.checked_in_count_unbounded) - stats.pending_count_unbounded) - (( SELECT count(*) AS count
               FROM public.ticket
              WHERE (((ticket.status)::text = 'RELEASED'::text) AND (ticket.category_id IS NULL) AND (ticket.event_id = event.id))))::numeric)
            ELSE (0)::numeric
        END AS dynamic_allocation,
        CASE stats.contains_unbounded_categories
            WHEN true THEN ((((stats.allocated_count)::numeric - stats.sold_tickets_count_bounded) - stats.checked_in_count_bounded) - stats.pending_count_bounded)
            ELSE ((((stats.allocated_count)::numeric - stats.sold_tickets_count) - stats.checked_in_count) - stats.pending_count)
        END AS not_sold_tickets,
    (stats.is_containing_orphan_tickets_count > 0) AS is_containing_orphan_tickets,
    (stats.is_containing_stuck_tickets_count > 0) AS is_containing_stuck_tickets_count,
    (stats.public_and_valid_count > 0) AS show_public_statistics
   FROM (( SELECT sum(ticket_category_statistics.sold_tickets_count) AS sold_tickets_count,
            sum(ticket_category_statistics.checked_in_count) AS checked_in_count,
            sum(ticket_category_statistics.pending_count) AS pending_count,
            sum(ticket_category_statistics.released_count) AS released_count,
            sum(
                CASE ticket_category_statistics.bounded
                    WHEN true THEN ticket_category_statistics.checked_in_count
                    ELSE (0)::bigint
                END) AS checked_in_count_bounded,
            sum(
                CASE (ticket_category_statistics.bounded = false)
                    WHEN true THEN ticket_category_statistics.checked_in_count
                    ELSE (0)::bigint
                END) AS checked_in_count_unbounded,
            sum(
                CASE ticket_category_statistics.bounded
                    WHEN true THEN ticket_category_statistics.max_tickets
                    ELSE 0
                END) AS allocated_count,
            sum(
                CASE ticket_category_statistics.bounded
                    WHEN true THEN ticket_category_statistics.sold_tickets_count
                    ELSE (0)::bigint
                END) AS sold_tickets_count_bounded,
            sum(
                CASE (ticket_category_statistics.bounded = false)
                    WHEN true THEN ticket_category_statistics.sold_tickets_count
                    ELSE (0)::bigint
                END) AS sold_tickets_count_unbounded,
            sum(
                CASE (ticket_category_statistics.bounded = false)
                    WHEN true THEN ticket_category_statistics.pending_count
                    ELSE (0)::bigint
                END) AS pending_count_unbounded,
            sum(
                CASE ticket_category_statistics.bounded
                    WHEN true THEN ticket_category_statistics.pending_count
                    ELSE (0)::bigint
                END) AS pending_count_bounded,
            (sum(
                CASE ticket_category_statistics.bounded
                    WHEN false THEN 1
                    ELSE 0
                END) > 0) AS contains_unbounded_categories,
            sum(
                CASE ticket_category_statistics.is_containing_orphan_tickets
                    WHEN true THEN 1
                    ELSE 0
                END) AS is_containing_orphan_tickets_count,
            sum(
                CASE ticket_category_statistics.is_containing_stuck_tickets
                    WHEN true THEN 1
                    ELSE 0
                END) AS is_containing_stuck_tickets_count,
            sum(
                CASE ((ticket_category_statistics.access_restricted = false) AND (ticket_category_statistics.is_expired = false))
                    WHEN true THEN 1
                    ELSE 0
                END) AS public_and_valid_count,
            ticket_category_statistics.event_id
           FROM public.ticket_category_statistics
          GROUP BY ticket_category_statistics.event_id) stats
     JOIN public.event ON ((stats.event_id = event.id)));


ALTER TABLE public.events_statistics OWNER TO postgres;

--
-- Name: extension_configuration_metadata; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.extension_configuration_metadata (
    ecm_id integer NOT NULL,
    ecm_name character varying(64) NOT NULL,
    ecm_configuration_level character varying(32) NOT NULL,
    ecm_description character varying(1024),
    ecm_type character varying(20) NOT NULL,
    ecm_mandatory boolean NOT NULL,
    ecm_es_id_fk integer NOT NULL
);


ALTER TABLE public.extension_configuration_metadata OWNER TO postgres;

--
-- Name: extension_configuration_metadata_ecm_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.extension_configuration_metadata_ecm_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extension_configuration_metadata_ecm_id_seq OWNER TO postgres;

--
-- Name: extension_configuration_metadata_ecm_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.extension_configuration_metadata_ecm_id_seq OWNED BY public.extension_configuration_metadata.ecm_id;


--
-- Name: extension_configuration_metadata_value; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.extension_configuration_metadata_value (
    fk_ecm_id integer NOT NULL,
    conf_path character varying(128) NOT NULL,
    conf_value character varying(1024) NOT NULL
);


ALTER TABLE public.extension_configuration_metadata_value OWNER TO postgres;

--
-- Name: extension_event; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.extension_event (
    es_id_fk integer NOT NULL,
    event character varying(128) NOT NULL
);


ALTER TABLE public.extension_event OWNER TO postgres;

--
-- Name: extension_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.extension_log (
    id integer NOT NULL,
    path character varying(128) NOT NULL,
    effective_path character varying(128) NOT NULL,
    name character varying(64) NOT NULL,
    description text NOT NULL,
    type character varying(255),
    event_ts timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.extension_log OWNER TO postgres;

--
-- Name: extension_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.extension_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extension_log_id_seq OWNER TO postgres;

--
-- Name: extension_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.extension_log_id_seq OWNED BY public.extension_log.id;


--
-- Name: extension_support; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.extension_support (
    path character varying(128) NOT NULL,
    name character varying(64) NOT NULL,
    hash character varying(256) NOT NULL,
    enabled boolean NOT NULL,
    async boolean NOT NULL,
    script text NOT NULL,
    es_id integer NOT NULL,
    display_name character varying(255) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.extension_support OWNER TO postgres;

--
-- Name: extension_support_es_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.extension_support_es_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extension_support_es_id_seq OWNER TO postgres;

--
-- Name: extension_support_es_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.extension_support_es_id_seq OWNED BY public.extension_support.es_id;


--
-- Name: file_blob; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.file_blob (
    id character(64) NOT NULL,
    name character varying(255) NOT NULL,
    content_size integer NOT NULL,
    content bytea NOT NULL,
    content_type character varying(255) NOT NULL,
    creation_time timestamp without time zone DEFAULT now() NOT NULL,
    attributes text
);


ALTER TABLE public.file_blob OWNER TO postgres;

--
-- Name: flyway_schema_history; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.flyway_schema_history (
    installed_rank integer NOT NULL,
    version character varying(50),
    description character varying(200) NOT NULL,
    type character varying(20) NOT NULL,
    script character varying(1000) NOT NULL,
    checksum integer,
    installed_by character varying(100) NOT NULL,
    installed_on timestamp without time zone DEFAULT now() NOT NULL,
    execution_time integer NOT NULL,
    success boolean NOT NULL
);


ALTER TABLE public.flyway_schema_history OWNER TO postgres;

--
-- Name: group_active; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.group_active AS
 SELECT a_group.id,
    a_group.name,
    a_group.description,
    a_group.organization_id_fk,
    a_group.active
   FROM public.a_group
  WHERE (a_group.active = true);


ALTER TABLE public.group_active OWNER TO postgres;

--
-- Name: group_link; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.group_link (
    id integer NOT NULL,
    a_group_id_fk integer NOT NULL,
    event_id_fk integer NOT NULL,
    ticket_category_id_fk integer,
    type character varying(255),
    match_type character varying(255),
    max_allocation integer,
    active boolean DEFAULT true NOT NULL,
    organization_id_fk integer NOT NULL
);

ALTER TABLE ONLY public.group_link FORCE ROW LEVEL SECURITY;


ALTER TABLE public.group_link OWNER TO postgres;

--
-- Name: group_link_active; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.group_link_active AS
 SELECT group_link.id,
    group_link.a_group_id_fk,
    group_link.event_id_fk,
    group_link.ticket_category_id_fk,
    group_link.type,
    group_link.match_type,
    group_link.max_allocation,
    group_link.active
   FROM public.group_link
  WHERE (group_link.active = true);


ALTER TABLE public.group_link_active OWNER TO postgres;

--
-- Name: group_link_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.group_link_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.group_link_id_seq OWNER TO postgres;

--
-- Name: group_link_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.group_link_id_seq OWNED BY public.group_link.id;


--
-- Name: group_member; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.group_member (
    id integer NOT NULL,
    a_group_id_fk integer NOT NULL,
    value character varying(255),
    description character varying(2048),
    active boolean DEFAULT true,
    organization_id_fk integer NOT NULL
);

ALTER TABLE ONLY public.group_member FORCE ROW LEVEL SECURITY;


ALTER TABLE public.group_member OWNER TO postgres;

--
-- Name: group_member_active; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.group_member_active AS
 SELECT group_member.id,
    group_member.a_group_id_fk,
    group_member.value,
    group_member.description,
    group_member.active
   FROM public.group_member
  WHERE (group_member.active = true);


ALTER TABLE public.group_member_active OWNER TO postgres;

--
-- Name: group_member_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.group_member_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.group_member_id_seq OWNER TO postgres;

--
-- Name: group_member_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.group_member_id_seq OWNED BY public.group_member.id;


--
-- Name: invoice_sequences; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.invoice_sequences (
    organization_id_fk integer NOT NULL,
    invoice_sequence integer NOT NULL
);

ALTER TABLE ONLY public.invoice_sequences FORCE ROW LEVEL SECURITY;


ALTER TABLE public.invoice_sequences OWNER TO postgres;

--
-- Name: j_user_organization; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.j_user_organization (
    user_id integer NOT NULL,
    org_id integer NOT NULL
);

ALTER TABLE ONLY public.j_user_organization FORCE ROW LEVEL SECURITY;


ALTER TABLE public.j_user_organization OWNER TO postgres;

--
-- Name: latest_ticket_update; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.latest_ticket_update AS
 SELECT (auditing.entity_id)::integer AS ticket_id,
    auditing.event_id,
    max(auditing.event_time) AS last_update
   FROM public.auditing
  WHERE ((auditing.entity_type)::text = 'TICKET'::text)
  GROUP BY (auditing.entity_id)::integer, auditing.event_id;


ALTER TABLE public.latest_ticket_update OWNER TO postgres;

--
-- Name: organization; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.organization (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(2048) NOT NULL,
    email character varying(2048) NOT NULL
);

ALTER TABLE ONLY public.organization FORCE ROW LEVEL SECURITY;


ALTER TABLE public.organization OWNER TO postgres;

--
-- Name: organization_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.organization_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.organization_id_seq OWNER TO postgres;

--
-- Name: organization_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.organization_id_seq OWNED BY public.organization.id;


--
-- Name: promo_code; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.promo_code (
    id integer NOT NULL,
    promo_code character varying(64) NOT NULL,
    event_id_fk integer,
    valid_from timestamp with time zone NOT NULL,
    valid_to timestamp with time zone NOT NULL,
    discount_amount integer NOT NULL,
    discount_type character varying(16) NOT NULL,
    categories character varying(2048),
    organization_id_fk integer NOT NULL,
    max_usage integer,
    description character varying(1024),
    email_reference character varying(256),
    code_type character varying(255) DEFAULT 'DISCOUNT'::character varying NOT NULL,
    hidden_category_id integer,
    CONSTRAINT check_discount_amount CHECK (
CASE
    WHEN ((discount_type)::text = 'FIXED_AMOUNT'::text) THEN (discount_amount >= 0)
    WHEN ((discount_type)::text = 'PERCENTAGE'::text) THEN ((discount_amount >= 0) AND (discount_amount <= 100))
    ELSE NULL::boolean
END),
    CONSTRAINT check_discount_type CHECK ((((discount_type)::text = 'FIXED_AMOUNT'::text) OR ((discount_type)::text = 'PERCENTAGE'::text) OR ((discount_type)::text = 'NONE'::text))),
    CONSTRAINT check_promo_code_min_length CHECK ((char_length((promo_code)::text) >= 7)),
    CONSTRAINT if_access_code_then_hidden_category_not_null CHECK ((((code_type)::text <> 'ACCESS'::text) OR (hidden_category_id IS NOT NULL)))
);

ALTER TABLE ONLY public.promo_code FORCE ROW LEVEL SECURITY;


ALTER TABLE public.promo_code OWNER TO postgres;

--
-- Name: promo_code_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.promo_code_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.promo_code_id_seq OWNER TO postgres;

--
-- Name: promo_code_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.promo_code_id_seq OWNED BY public.promo_code.id;


--
-- Name: qrtz_blob_triggers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.qrtz_blob_triggers (
    sched_name character varying(120) NOT NULL,
    trigger_name character varying(200) NOT NULL,
    trigger_group character varying(200) NOT NULL,
    blob_data bytea
);


ALTER TABLE public.qrtz_blob_triggers OWNER TO postgres;

--
-- Name: qrtz_calendars; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.qrtz_calendars (
    sched_name character varying(120) NOT NULL,
    calendar_name character varying(200) NOT NULL,
    calendar bytea NOT NULL
);


ALTER TABLE public.qrtz_calendars OWNER TO postgres;

--
-- Name: qrtz_cron_triggers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.qrtz_cron_triggers (
    sched_name character varying(120) NOT NULL,
    trigger_name character varying(200) NOT NULL,
    trigger_group character varying(200) NOT NULL,
    cron_expression character varying(120) NOT NULL,
    time_zone_id character varying(80)
);


ALTER TABLE public.qrtz_cron_triggers OWNER TO postgres;

--
-- Name: qrtz_fired_triggers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.qrtz_fired_triggers (
    sched_name character varying(120) NOT NULL,
    entry_id character varying(95) NOT NULL,
    trigger_name character varying(200) NOT NULL,
    trigger_group character varying(200) NOT NULL,
    instance_name character varying(200) NOT NULL,
    fired_time bigint NOT NULL,
    sched_time bigint NOT NULL,
    priority integer NOT NULL,
    state character varying(16) NOT NULL,
    job_name character varying(200),
    job_group character varying(200),
    is_nonconcurrent boolean,
    requests_recovery boolean
);


ALTER TABLE public.qrtz_fired_triggers OWNER TO postgres;

--
-- Name: qrtz_job_details; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.qrtz_job_details (
    sched_name character varying(120) NOT NULL,
    job_name character varying(200) NOT NULL,
    job_group character varying(200) NOT NULL,
    description character varying(250),
    job_class_name character varying(250) NOT NULL,
    is_durable boolean NOT NULL,
    is_nonconcurrent boolean NOT NULL,
    is_update_data boolean NOT NULL,
    requests_recovery boolean NOT NULL,
    job_data bytea
);


ALTER TABLE public.qrtz_job_details OWNER TO postgres;

--
-- Name: qrtz_locks; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.qrtz_locks (
    sched_name character varying(120) NOT NULL,
    lock_name character varying(40) NOT NULL
);


ALTER TABLE public.qrtz_locks OWNER TO postgres;

--
-- Name: qrtz_paused_trigger_grps; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.qrtz_paused_trigger_grps (
    sched_name character varying(120) NOT NULL,
    trigger_group character varying(200) NOT NULL
);


ALTER TABLE public.qrtz_paused_trigger_grps OWNER TO postgres;

--
-- Name: qrtz_scheduler_state; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.qrtz_scheduler_state (
    sched_name character varying(120) NOT NULL,
    instance_name character varying(200) NOT NULL,
    last_checkin_time bigint NOT NULL,
    checkin_interval bigint NOT NULL
);


ALTER TABLE public.qrtz_scheduler_state OWNER TO postgres;

--
-- Name: qrtz_simple_triggers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.qrtz_simple_triggers (
    sched_name character varying(120) NOT NULL,
    trigger_name character varying(200) NOT NULL,
    trigger_group character varying(200) NOT NULL,
    repeat_count bigint NOT NULL,
    repeat_interval bigint NOT NULL,
    times_triggered bigint NOT NULL
);


ALTER TABLE public.qrtz_simple_triggers OWNER TO postgres;

--
-- Name: qrtz_simprop_triggers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.qrtz_simprop_triggers (
    sched_name character varying(120) NOT NULL,
    trigger_name character varying(200) NOT NULL,
    trigger_group character varying(200) NOT NULL,
    str_prop_1 character varying(512),
    str_prop_2 character varying(512),
    str_prop_3 character varying(512),
    int_prop_1 integer,
    int_prop_2 integer,
    long_prop_1 bigint,
    long_prop_2 bigint,
    dec_prop_1 numeric(13,4),
    dec_prop_2 numeric(13,4),
    bool_prop_1 boolean,
    bool_prop_2 boolean
);


ALTER TABLE public.qrtz_simprop_triggers OWNER TO postgres;

--
-- Name: qrtz_triggers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.qrtz_triggers (
    sched_name character varying(120) NOT NULL,
    trigger_name character varying(200) NOT NULL,
    trigger_group character varying(200) NOT NULL,
    job_name character varying(200) NOT NULL,
    job_group character varying(200) NOT NULL,
    description character varying(250),
    next_fire_time bigint,
    prev_fire_time bigint,
    priority integer,
    trigger_state character varying(16) NOT NULL,
    trigger_type character varying(8) NOT NULL,
    start_time bigint NOT NULL,
    end_time bigint,
    calendar_name character varying(200),
    misfire_instr smallint,
    job_data bytea
);


ALTER TABLE public.qrtz_triggers OWNER TO postgres;

--
-- Name: special_price; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.special_price (
    id integer NOT NULL,
    code character varying(64) NOT NULL,
    price_cts integer NOT NULL,
    ticket_category_id integer NOT NULL,
    status character varying(255) NOT NULL,
    session_id character varying(255),
    sent_ts timestamp with time zone,
    recipient_name character varying(255),
    recipient_email character varying(255),
    organization_id_fk integer,
    access_code_id_fk integer
);

ALTER TABLE ONLY public.special_price FORCE ROW LEVEL SECURITY;


ALTER TABLE public.special_price OWNER TO postgres;

--
-- Name: reservation_and_ticket_and_tx; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.reservation_and_ticket_and_tx AS
 SELECT tickets_reservation.id AS tr_id,
    tickets_reservation.validity AS tr_validity,
    tickets_reservation.status AS tr_status,
    tickets_reservation.full_name AS tr_full_name,
    tickets_reservation.first_name AS tr_first_name,
    tickets_reservation.last_name AS tr_last_name,
    tickets_reservation.email_address AS tr_email_address,
    tickets_reservation.billing_address AS tr_billing_address,
    tickets_reservation.confirmation_ts AS tr_confirmation_ts,
    tickets_reservation.latest_reminder_ts AS tr_latest_reminder_ts,
    tickets_reservation.payment_method AS tr_payment_method,
    tickets_reservation.offline_payment_reminder_sent AS tr_offline_payment_reminder_sent,
    tickets_reservation.promo_code_id_fk AS tr_promo_code_id_fk,
    tickets_reservation.automatic AS tr_automatic,
    tickets_reservation.user_language AS tr_user_language,
    tickets_reservation.direct_assignment AS tr_direct_assignment,
    tickets_reservation.invoice_number AS tr_invoice_number,
    tickets_reservation.invoice_model AS tr_invoice_model,
    tickets_reservation.vat_status AS tr_vat_status,
    tickets_reservation.vat_nr AS tr_vat_nr,
    tickets_reservation.vat_country AS tr_vat_country,
    tickets_reservation.invoice_requested AS tr_invoice_requested,
    tickets_reservation.used_vat_percent AS tr_used_vat_percent,
    tickets_reservation.vat_included AS tr_vat_included,
    tickets_reservation.event_id_fk AS tr_event_id,
    tickets_reservation.creation_ts AS tr_creation_ts,
    tickets_reservation.customer_reference AS tr_customer_reference,
    tickets_reservation.billing_address_company AS tr_billing_address_company,
    tickets_reservation.billing_address_line1 AS tr_billing_address_line1,
    tickets_reservation.billing_address_line2 AS tr_billing_address_line2,
    tickets_reservation.billing_address_city AS tr_billing_address_city,
    tickets_reservation.billing_address_zip AS tr_billing_address_zip,
    tickets_reservation.registration_ts AS tr_registration_ts,
    tickets_reservation.invoicing_additional_information AS tr_invoicing_additional_information,
    tickets_reservation.src_price_cts AS tr_src_price_cts,
    tickets_reservation.final_price_cts AS tr_final_price_cts,
    tickets_reservation.vat_cts AS tr_vat_cts,
    tickets_reservation.discount_cts AS tr_discount_cts,
    tickets_reservation.currency_code AS tr_currency_code,
    ticket.id AS t_id,
    ticket.uuid AS t_uuid,
    ticket.creation AS t_creation,
    ticket.category_id AS t_category_id,
    ticket.status AS t_status,
    ticket.event_id AS t_event_id,
    ticket.tickets_reservation_id AS t_tickets_reservation_id,
    ticket.full_name AS t_full_name,
    ticket.first_name AS t_first_name,
    ticket.last_name AS t_last_name,
    ticket.email_address AS t_email_address,
    ticket.locked_assignment AS t_locked_assignment,
    ticket.user_language AS t_user_language,
    ticket.src_price_cts AS t_src_price_cts,
    ticket.final_price_cts AS t_final_price_cts,
    ticket.vat_cts AS t_vat_cts,
    ticket.discount_cts AS t_discount_cts,
    ticket.ext_reference AS t_ext_reference,
    ticket.currency_code AS t_currency_code,
    b_transaction.id AS bt_id,
    b_transaction.gtw_tx_id AS bt_gtw_tx_id,
    b_transaction.gtw_payment_id AS bt_gtw_payment_id,
    b_transaction.reservation_id AS bt_reservation_id,
    b_transaction.t_timestamp AS bt_t_timestamp,
    b_transaction.price_cts AS bt_price_cts,
    b_transaction.currency AS bt_currency,
    b_transaction.description AS bt_description,
    b_transaction.payment_proxy AS bt_payment_proxy,
    b_transaction.gtw_fee AS bt_gtw_fee,
    b_transaction.plat_fee AS bt_plat_fee,
    b_transaction.status AS bt_status,
    b_transaction.metadata AS bt_metadata,
    ( SELECT count(ticket_1.id) AS count
           FROM public.ticket ticket_1
          WHERE (b_transaction.reservation_id = ticket_1.tickets_reservation_id)) AS tickets_count,
    promo_code.promo_code,
    special_price.code AS special_price_token
   FROM ((((public.tickets_reservation
     LEFT JOIN public.ticket ON ((tickets_reservation.id = ticket.tickets_reservation_id)))
     LEFT JOIN public.b_transaction ON (((ticket.tickets_reservation_id = b_transaction.reservation_id) AND (b_transaction.status <> 'INVALID'::bpchar))))
     LEFT JOIN public.promo_code ON ((tickets_reservation.promo_code_id_fk = promo_code.id)))
     LEFT JOIN public.special_price ON ((ticket.special_price_id_fk = special_price.id)));


ALTER TABLE public.reservation_and_ticket_and_tx OWNER TO postgres;

--
-- Name: resource_event; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.resource_event (
    name character varying(255) NOT NULL,
    organization_id_fk integer NOT NULL,
    event_id_fk integer NOT NULL,
    content_size integer NOT NULL,
    content bytea NOT NULL,
    content_type character varying(255) NOT NULL,
    creation_time timestamp without time zone DEFAULT now() NOT NULL,
    attributes text
);

ALTER TABLE ONLY public.resource_event FORCE ROW LEVEL SECURITY;


ALTER TABLE public.resource_event OWNER TO postgres;

--
-- Name: resource_global; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.resource_global (
    name character varying(255) NOT NULL,
    content_size integer NOT NULL,
    content bytea NOT NULL,
    content_type character varying(255) NOT NULL,
    creation_time timestamp without time zone DEFAULT now() NOT NULL,
    attributes text
);


ALTER TABLE public.resource_global OWNER TO postgres;

--
-- Name: resource_organizer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.resource_organizer (
    name character varying(255) NOT NULL,
    organization_id_fk integer NOT NULL,
    content_size integer NOT NULL,
    content bytea NOT NULL,
    content_type character varying(255) NOT NULL,
    creation_time timestamp without time zone DEFAULT now() NOT NULL,
    attributes text
);

ALTER TABLE ONLY public.resource_organizer FORCE ROW LEVEL SECURITY;


ALTER TABLE public.resource_organizer OWNER TO postgres;

--
-- Name: scan_audit; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.scan_audit (
    ticket_uuid character varying(255) NOT NULL,
    event_id_fk integer NOT NULL,
    scan_ts timestamp without time zone NOT NULL,
    username character varying(255) NOT NULL,
    check_in_status character varying(255) NOT NULL,
    operation character varying(255) NOT NULL,
    organization_id_fk integer NOT NULL
);

ALTER TABLE ONLY public.scan_audit FORCE ROW LEVEL SECURITY;


ALTER TABLE public.scan_audit OWNER TO postgres;

--
-- Name: special_price_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.special_price_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.special_price_id_seq OWNER TO postgres;

--
-- Name: special_price_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.special_price_id_seq OWNED BY public.special_price.id;


--
-- Name: sponsor_scan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sponsor_scan (
    id integer NOT NULL,
    creation timestamp with time zone NOT NULL,
    event_id integer NOT NULL,
    ticket_id integer NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    organization_id_fk integer NOT NULL,
    notes text
);

ALTER TABLE ONLY public.sponsor_scan FORCE ROW LEVEL SECURITY;


ALTER TABLE public.sponsor_scan OWNER TO postgres;

--
-- Name: sponsor_scan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sponsor_scan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sponsor_scan_id_seq OWNER TO postgres;

--
-- Name: sponsor_scan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sponsor_scan_id_seq OWNED BY public.sponsor_scan.id;


--
-- Name: spring_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.spring_session (
    session_id character(36) NOT NULL,
    creation_time bigint NOT NULL,
    last_access_time bigint NOT NULL,
    max_inactive_interval integer NOT NULL,
    principal_name character varying(100)
);


ALTER TABLE public.spring_session OWNER TO postgres;

--
-- Name: spring_session_attributes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.spring_session_attributes (
    session_id character(36) NOT NULL,
    attribute_name character varying(200) NOT NULL,
    attribute_bytes bytea NOT NULL
);


ALTER TABLE public.spring_session_attributes OWNER TO postgres;

--
-- Name: ticket_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ticket_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ticket_category_id_seq OWNER TO postgres;

--
-- Name: ticket_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ticket_category_id_seq OWNED BY public.ticket_category.id;


--
-- Name: ticket_category_text; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ticket_category_text (
    ticket_category_id_fk integer NOT NULL,
    locale character varying(8) NOT NULL,
    description character varying(1024) NOT NULL,
    organization_id_fk integer NOT NULL
);

ALTER TABLE ONLY public.ticket_category_text FORCE ROW LEVEL SECURITY;


ALTER TABLE public.ticket_category_text OWNER TO postgres;

--
-- Name: ticket_category_with_currency; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.ticket_category_with_currency AS
 SELECT tc.id,
    tc.inception,
    tc.expiration,
    tc.max_tickets,
    tc.name,
    tc.price_cts,
    tc.access_restricted,
    tc.tc_status,
    tc.event_id,
    tc.bounded,
    tc.src_price_cts,
    tc.category_code,
    tc.valid_checkin_from,
    tc.valid_checkin_to,
    tc.ticket_validity_start,
    tc.ticket_validity_end,
    tc.organization_id_fk,
    tc.ordinal,
    tc.ticket_checkin_strategy,
    e.currency AS currency_code
   FROM public.ticket_category tc,
    public.event e
  WHERE (tc.event_id = e.id);


ALTER TABLE public.ticket_category_with_currency OWNER TO postgres;

--
-- Name: ticket_field_configuration; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ticket_field_configuration (
    id integer NOT NULL,
    event_id_fk integer NOT NULL,
    field_name character varying(64) NOT NULL,
    field_order integer NOT NULL,
    field_type character varying(64) NOT NULL,
    field_restricted_values text,
    field_maxlength integer,
    field_minlength integer,
    field_required boolean NOT NULL,
    context character varying(50) DEFAULT 'ATTENDEE'::character varying NOT NULL,
    additional_service_id integer DEFAULT '-1'::integer NOT NULL,
    ticket_category_ids character varying(1024),
    field_disabled_values character varying(1024),
    organization_id_fk integer NOT NULL,
    field_editable boolean DEFAULT true NOT NULL
);

ALTER TABLE ONLY public.ticket_field_configuration FORCE ROW LEVEL SECURITY;


ALTER TABLE public.ticket_field_configuration OWNER TO postgres;

--
-- Name: ticket_field_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ticket_field_configuration_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ticket_field_configuration_id_seq OWNER TO postgres;

--
-- Name: ticket_field_configuration_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ticket_field_configuration_id_seq OWNED BY public.ticket_field_configuration.id;


--
-- Name: ticket_field_description; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ticket_field_description (
    ticket_field_configuration_id_fk integer NOT NULL,
    field_locale character varying(8) NOT NULL,
    description text NOT NULL,
    organization_id_fk integer NOT NULL
);

ALTER TABLE ONLY public.ticket_field_description FORCE ROW LEVEL SECURITY;


ALTER TABLE public.ticket_field_description OWNER TO postgres;

--
-- Name: ticket_field_value; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ticket_field_value (
    ticket_id_fk integer NOT NULL,
    ticket_field_configuration_id_fk integer NOT NULL,
    field_value character varying(2048),
    organization_id_fk integer NOT NULL
);

ALTER TABLE ONLY public.ticket_field_value FORCE ROW LEVEL SECURITY;


ALTER TABLE public.ticket_field_value OWNER TO postgres;

--
-- Name: ticket_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ticket_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ticket_id_seq OWNER TO postgres;

--
-- Name: ticket_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ticket_id_seq OWNED BY public.ticket.id;


--
-- Name: waiting_queue; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.waiting_queue (
    id integer NOT NULL,
    creation timestamp with time zone NOT NULL,
    event_id integer NOT NULL,
    status character varying(255) NOT NULL,
    full_name character varying(255),
    email_address character varying(255),
    ticket_reservation_id character varying(255),
    user_language character varying(20) NOT NULL,
    subscription_type character varying(20),
    selected_category_id integer,
    first_name character varying(255),
    last_name character varying(255),
    organization_id_fk integer NOT NULL
);

ALTER TABLE ONLY public.waiting_queue FORCE ROW LEVEL SECURITY;


ALTER TABLE public.waiting_queue OWNER TO postgres;

--
-- Name: waiting_queue_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.waiting_queue_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.waiting_queue_id_seq OWNER TO postgres;

--
-- Name: waiting_queue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.waiting_queue_id_seq OWNED BY public.waiting_queue.id;


--
-- Name: whitelisted_ticket; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.whitelisted_ticket (
    group_member_id_fk integer NOT NULL,
    group_link_id_fk integer NOT NULL,
    ticket_id_fk integer NOT NULL,
    requires_unique_value boolean,
    organization_id_fk integer NOT NULL
);

ALTER TABLE ONLY public.whitelisted_ticket FORCE ROW LEVEL SECURITY;


ALTER TABLE public.whitelisted_ticket OWNER TO postgres;

--
-- Name: a_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.a_group ALTER COLUMN id SET DEFAULT nextval('public.a_group_id_seq'::regclass);


--
-- Name: additional_service id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.additional_service ALTER COLUMN id SET DEFAULT nextval('public.additional_service_id_seq'::regclass);


--
-- Name: additional_service_description id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.additional_service_description ALTER COLUMN id SET DEFAULT nextval('public.additional_service_description_id_seq'::regclass);


--
-- Name: additional_service_item id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.additional_service_item ALTER COLUMN id SET DEFAULT nextval('public.additional_service_item_id_seq'::regclass);


--
-- Name: admin_job_queue id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.admin_job_queue ALTER COLUMN id SET DEFAULT nextval('public.admin_job_queue_id_seq'::regclass);


--
-- Name: admin_reservation_request id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.admin_reservation_request ALTER COLUMN id SET DEFAULT nextval('public.admin_reservation_request_id_seq'::regclass);


--
-- Name: b_transaction id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.b_transaction ALTER COLUMN id SET DEFAULT nextval('public.b_transaction_id_seq'::regclass);


--
-- Name: ba_user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ba_user ALTER COLUMN id SET DEFAULT nextval('public.ba_user_id_seq'::regclass);


--
-- Name: billing_document id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.billing_document ALTER COLUMN id SET DEFAULT nextval('public.billing_document_id_seq'::regclass);


--
-- Name: configuration id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.configuration ALTER COLUMN id SET DEFAULT nextval('public.configuration_id_seq'::regclass);


--
-- Name: configuration_event id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.configuration_event ALTER COLUMN id SET DEFAULT nextval('public.configuration_event_id_seq'::regclass);


--
-- Name: configuration_organization id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.configuration_organization ALTER COLUMN id SET DEFAULT nextval('public.configuration_organization_id_seq'::regclass);


--
-- Name: configuration_ticket_category id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.configuration_ticket_category ALTER COLUMN id SET DEFAULT nextval('public.configuration_ticket_category_id_seq'::regclass);


--
-- Name: dynamic_field_template id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dynamic_field_template ALTER COLUMN id SET DEFAULT nextval('public.dynamic_field_template_id_seq'::regclass);


--
-- Name: email_message id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.email_message ALTER COLUMN id SET DEFAULT nextval('public.email_message_id_seq'::regclass);


--
-- Name: event id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.event ALTER COLUMN id SET DEFAULT nextval('public.event_id_seq'::regclass);


--
-- Name: event_migration id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.event_migration ALTER COLUMN id SET DEFAULT nextval('public.event_migration_id_seq'::regclass);


--
-- Name: extension_configuration_metadata ecm_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.extension_configuration_metadata ALTER COLUMN ecm_id SET DEFAULT nextval('public.extension_configuration_metadata_ecm_id_seq'::regclass);


--
-- Name: extension_log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.extension_log ALTER COLUMN id SET DEFAULT nextval('public.extension_log_id_seq'::regclass);


--
-- Name: extension_support es_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.extension_support ALTER COLUMN es_id SET DEFAULT nextval('public.extension_support_es_id_seq'::regclass);


--
-- Name: group_link id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.group_link ALTER COLUMN id SET DEFAULT nextval('public.group_link_id_seq'::regclass);


--
-- Name: group_member id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.group_member ALTER COLUMN id SET DEFAULT nextval('public.group_member_id_seq'::regclass);


--
-- Name: organization id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.organization ALTER COLUMN id SET DEFAULT nextval('public.organization_id_seq'::regclass);


--
-- Name: promo_code id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.promo_code ALTER COLUMN id SET DEFAULT nextval('public.promo_code_id_seq'::regclass);


--
-- Name: special_price id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.special_price ALTER COLUMN id SET DEFAULT nextval('public.special_price_id_seq'::regclass);


--
-- Name: sponsor_scan id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sponsor_scan ALTER COLUMN id SET DEFAULT nextval('public.sponsor_scan_id_seq'::regclass);


--
-- Name: ticket id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket ALTER COLUMN id SET DEFAULT nextval('public.ticket_id_seq'::regclass);


--
-- Name: ticket_category id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_category ALTER COLUMN id SET DEFAULT nextval('public.ticket_category_id_seq'::regclass);


--
-- Name: ticket_field_configuration id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_field_configuration ALTER COLUMN id SET DEFAULT nextval('public.ticket_field_configuration_id_seq'::regclass);


--
-- Name: waiting_queue id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.waiting_queue ALTER COLUMN id SET DEFAULT nextval('public.waiting_queue_id_seq'::regclass);


--
-- Data for Name: a_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.a_group (id, name, description, organization_id_fk, active) FROM stdin;
\.


--
-- Data for Name: additional_service; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.additional_service (id, event_id_fk, price_cts, fix_price, ordinal, available_qty, max_qty_per_order, inception_ts, expiration_ts, vat, vat_type, src_price_cts, service_type, supplement_policy, organization_id_fk) FROM stdin;
3	2	0	t	0	-1	1	2020-03-25 16:00:00-06	2020-04-01 15:00:00-06	\N	NONE	200	DONATION	\N	1
4	1	0	t	0	-1	1	2020-03-30 12:00:00-06	2020-03-31 07:00:00-06	\N	NONE	500	SUPPLEMENT	OPTIONAL_MAX_AMOUNT_PER_RESERVATION	1
1	1	0	t	0	-1	1	2020-02-22 08:00:00-06	2020-03-31 07:00:00-06	\N	NONE	1000	SUPPLEMENT	MANDATORY_ONE_FOR_TICKET	1
5	4	0	t	0	-1	1	2020-03-27 10:00:00-06	2020-04-30 15:00:00-06	\N	INHERITED	500	SUPPLEMENT	OPTIONAL_UNLIMITED_AMOUNT	1
6	3	0	f	0	-1	1	2020-03-27 10:00:00-06	2020-04-30 15:00:00-06	\N	INHERITED	0	DONATION	\N	1
\.


--
-- Data for Name: additional_service_description; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.additional_service_description (id, additional_service_id_fk, locale, type, value, organization_id_fk) FROM stdin;
5	3	es	TITLE	Apoyo a CIFCO	1
6	3	es	DESCRIPTION	Apoyo a CIFCO para mejorar instalaciones	1
7	4	es	TITLE	Mantenimiento	1
8	4	es	DESCRIPTION	Ayuda con mantenimiento de las instalaciones	1
1	1	es	TITLE	Consumible	1
2	1	es	DESCRIPTION	Costo de entrada requerido para consumir alimentos/bebidas dentro de las instalaciones	1
9	5	es	TITLE	Donacion	1
10	5	es	DESCRIPTION	Donación apoyo a comunidades afectadas por pandemia	1
11	6	es	TITLE	Apoyo CIFCO	1
12	6	es	DESCRIPTION	Apoyo para mantenimiento de localidades CIFCO	1
\.


--
-- Data for Name: additional_service_field_value; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.additional_service_field_value (additional_service_id_fk, ticket_field_configuration_id_fk, field_value) FROM stdin;
\.


--
-- Data for Name: additional_service_item; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.additional_service_item (id, uuid, creation, last_modified, tickets_reservation_uuid, additional_service_id_fk, original_price_cts, paid_price_cts, status, event_id_fk, src_price_cts, final_price_cts, vat_cts, discount_cts, organization_id_fk, currency_code) FROM stdin;
1	a82c0ae5-57f7-4170-afc9-00e6734cbd7b	2020-02-22 08:33:17.302-06	\N	75994244-dbf1-49d8-9553-e5e6e6fbb560	1	\N	\N	CANCELLED	1	1000	1000	0	0	1	USD
2	f26f7003-c9a8-4ea6-b0d0-dd47e5c6414e	2020-03-08 20:21:17.954-06	\N	11f71518-4ea4-4a8e-813c-85312308602d	1	\N	\N	TO_BE_PAID	1	1000	1000	0	0	1	USD
3	4ca93ab2-1956-4eaf-b5de-5471123044b6	2020-03-08 20:21:17.967-06	\N	11f71518-4ea4-4a8e-813c-85312308602d	1	\N	\N	TO_BE_PAID	1	1000	1000	0	0	1	USD
4	69323137-7ed3-49d4-8285-39d78df17ab1	2020-03-08 20:21:17.968-06	\N	11f71518-4ea4-4a8e-813c-85312308602d	1	\N	\N	TO_BE_PAID	1	1000	1000	0	0	1	USD
5	c749f9a4-77d1-4348-bb20-7d6919021b03	2020-03-11 23:53:05.887-06	\N	786a6534-999d-4d49-a944-fb0a5fa3de64	1	\N	\N	PENDING	1	1000	1000	0	0	1	USD
6	95fc495e-1f35-4c24-b164-d3e73506755b	2020-03-17 15:30:51.866-06	\N	f687d682-43dd-4ed8-8c85-61211009cb42	1	\N	\N	CANCELLED	1	1000	1000	0	0	1	USD
7	311fb1e1-b99a-44fe-bf92-82d172ffdc04	2020-03-17 15:30:51.93-06	\N	f687d682-43dd-4ed8-8c85-61211009cb42	1	\N	\N	CANCELLED	1	1000	1000	0	0	1	USD
8	efa231ea-e6ff-4b2e-9260-c8e195e9ac35	2020-03-17 21:30:13.835-06	\N	1f110d7e-1658-4169-ab38-7539a9e98f17	1	\N	\N	CANCELLED	1	1000	1000	0	0	1	USD
9	09fdbaf4-a7f9-4a78-8ccc-12df82c5c212	2020-03-20 11:14:34.953-06	\N	8b9483ac-edc3-4b18-a27f-0b71795b5f37	1	\N	\N	CANCELLED	1	1000	1000	0	0	1	USD
10	233c5da0-30f1-4562-99f8-bf737f2f247c	2020-03-20 12:45:26.187-06	\N	6a464eb7-5403-4575-b8a3-3b43d3bfd13a	1	\N	\N	PENDING	1	1000	1000	0	0	1	USD
11	f7a82c94-9fa1-4def-9140-8105ff364160	2020-03-20 12:47:04.209-06	\N	4ba08935-4af5-4fcd-b843-7cebc90ea94f	1	\N	\N	CANCELLED	1	1000	1000	0	0	1	USD
12	eb6473e7-c6b6-4ed0-a69f-34021bcfdfcb	2020-03-20 15:40:35.533-06	\N	8fccf2c3-ebbd-4fe4-9aa3-a47c58c1912c	1	\N	\N	PENDING	1	1000	1000	0	0	1	USD
13	9c031b4f-5fa9-4af9-8aa1-2e3cf55e4148	2020-03-23 15:56:07.763-06	\N	ee9f9273-a879-4918-9bbd-dc1f0ae8e6d1	1	\N	\N	CANCELLED	1	1000	1000	0	0	1	USD
14	f1d3207b-71be-4236-a1e4-2790293fa0a0	2020-03-23 15:56:07.785-06	\N	ee9f9273-a879-4918-9bbd-dc1f0ae8e6d1	1	\N	\N	CANCELLED	1	1000	1000	0	0	1	USD
15	609ee3d9-67b4-4f22-97be-8ce1595e2f1e	2020-03-24 14:13:14.728-06	\N	4f3eb546-bf4f-46cb-b908-60c947a9890f	1	\N	\N	PENDING	1	1000	1000	0	0	1	USD
16	19a305e4-b3db-42cf-b633-f1d632d4bcfd	2020-03-25 08:59:18.555-06	\N	c55f248c-6d51-454d-a1bc-08445b47f7c6	1	\N	\N	CANCELLED	1	1000	1000	0	0	1	USD
17	63c032c4-cadc-4fbd-8924-f3f5311331b2	2020-03-25 09:15:08.068-06	\N	8789f11a-1d08-4ce0-8929-c890915c20f8	1	\N	\N	PENDING	1	1000	1000	0	0	1	USD
18	6de004fd-889e-4cfb-b473-36e256080c50	2020-03-25 15:46:32.121-06	\N	570666f5-10b7-4167-a9a4-3f93f0e5a365	1	\N	\N	CANCELLED	1	1000	1000	0	0	1	USD
19	fc88e6ce-3fe3-4817-a8cd-a2b165718abf	2020-03-26 12:04:11.784-06	\N	5e7b1564-8c99-4db0-8aca-317cf16e4214	1	\N	\N	CANCELLED	1	1000	1000	0	0	1	USD
20	5e050db8-e5dc-44dc-be9f-c97c44550ef8	2020-03-26 12:11:47.617-06	\N	8aae6df2-d154-48e4-9eab-332223aaaeef	1	\N	\N	PENDING	1	1000	1000	0	0	1	USD
21	cad14911-0e6c-4466-83d6-83a98357623b	2020-03-26 12:12:25.683-06	\N	e083f858-5a11-490e-a72a-83f4724c66f9	1	\N	\N	PENDING	1	1000	1000	0	0	1	USD
22	a41447d8-88e0-4346-ae37-a224434fb728	2020-03-26 12:12:25.685-06	\N	e083f858-5a11-490e-a72a-83f4724c66f9	1	\N	\N	PENDING	1	1000	1000	0	0	1	USD
23	3de1b4ac-4ed9-4ade-b34a-b2f5e39b5e6b	2020-03-26 12:12:25.686-06	\N	e083f858-5a11-490e-a72a-83f4724c66f9	1	\N	\N	PENDING	1	1000	1000	0	0	1	USD
24	3a952a05-12da-4a93-b756-f2406bba5a47	2020-03-26 15:23:07.471-06	\N	3f6d4b8a-d018-483a-9f70-0b65c858f1e2	1	\N	\N	PENDING	1	1000	1000	0	0	1	USD
25	11480f09-152b-43ce-a9fb-64eb4b99e2ac	2020-03-26 21:28:31.135-06	\N	a235287a-e194-480c-bc53-d50a84c8fd73	1	\N	\N	PENDING	1	1000	1000	0	0	1	USD
26	ad0f706a-bcd1-4721-8c51-5bbe0f14f88b	2020-03-26 21:28:31.138-06	\N	a235287a-e194-480c-bc53-d50a84c8fd73	1	\N	\N	PENDING	1	1000	1000	0	0	1	USD
27	aef9eed6-1b8a-4acd-b2e8-0342cf4636c4	2020-03-26 21:28:31.139-06	\N	a235287a-e194-480c-bc53-d50a84c8fd73	1	\N	\N	PENDING	1	1000	1000	0	0	1	USD
\.


--
-- Data for Name: admin_job_queue; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.admin_job_queue (id, job_name, request_ts, status, execution_ts, metadata) FROM stdin;
22368	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:11:00	EXECUTED	2020-03-27 00:12:02.426	{}
22369	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:12:00	EXECUTED	2020-03-27 00:13:02.437	{}
22370	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:13:00	EXECUTED	2020-03-27 00:14:02.449	{}
22371	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:14:00	EXECUTED	2020-03-27 00:15:02.466	{}
22372	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:15:00	EXECUTED	2020-03-27 00:16:02.478	{}
22373	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:16:00	EXECUTED	2020-03-27 00:17:02.504	{}
22374	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:17:00	EXECUTED	2020-03-27 00:18:02.532	{}
22690	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 05:10:00	EXECUTED	2020-03-27 05:11:09.741	{}
22375	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:18:00	EXECUTED	2020-03-27 00:19:02.558	{}
22691	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 05:10:00	EXECUTED	2020-03-27 05:11:09.785	{}
22692	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:10:00	EXECUTED	2020-03-27 05:11:09.807	{}
22376	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:19:00	EXECUTED	2020-03-27 00:20:02.587	{}
22827	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:15:00	EXECUTED	2020-03-27 07:16:12.55	{}
22377	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:20:00	EXECUTED	2020-03-27 00:21:02.613	{}
22949	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:09:00	EXECUTED	2020-03-27 09:10:14.89	{}
22516	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:29:00	EXECUTED	2020-03-27 02:30:05.228	{}
22950	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 09:10:00	EXECUTED	2020-03-27 09:11:14.898	{}
23307	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 14:40:00	EXECUTED	2020-03-27 14:41:21.888	{}
22517	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:30:00	EXECUTED	2020-03-27 02:31:05.255	{}
23308	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 14:40:00	EXECUTED	2020-03-27 14:41:21.908	{}
23309	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:40:00	EXECUTED	2020-03-27 14:41:21.917	{}
23310	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:41:00	EXECUTED	2020-03-27 14:42:21.935	{}
23443	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:44:00	EXECUTED	2020-03-27 16:45:24.402	{}
22518	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:31:00	EXECUTED	2020-03-27 02:32:05.273	{}
23444	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:45:00	EXECUTED	2020-03-27 16:46:24.42	{}
23445	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:46:00	EXECUTED	2020-03-27 16:47:24.439	{}
23446	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:47:00	EXECUTED	2020-03-27 16:48:24.458	{}
23447	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:48:00	EXECUTED	2020-03-27 16:49:24.471	{}
23448	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:49:00	EXECUTED	2020-03-27 16:50:24.48	{}
23449	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:50:00	EXECUTED	2020-03-27 16:51:24.491	{}
23450	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:51:00	EXECUTED	2020-03-27 16:52:24.518	{}
23451	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:52:00	EXECUTED	2020-03-27 16:53:24.53	{}
23452	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:53:00	EXECUTED	2020-03-27 16:54:24.556	{}
23453	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:54:00	EXECUTED	2020-03-27 16:55:24.574	{}
23454	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:55:00	EXECUTED	2020-03-27 16:56:24.59	{}
23455	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:56:00	EXECUTED	2020-03-27 16:57:24.607	{}
22034	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:04:00	EXECUTED	2020-03-26 19:04:55.872	{}
22069	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:37:00	EXECUTED	2020-03-26 19:37:56.592	{}
22070	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:38:00	EXECUTED	2020-03-26 19:38:56.624	{}
22071	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:39:00	EXECUTED	2020-03-26 19:39:56.636	{}
22072	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-26 19:40:00	EXECUTED	2020-03-26 19:40:56.643	{}
22073	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-26 19:40:00	EXECUTED	2020-03-26 19:40:56.689	{}
22074	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:40:00	EXECUTED	2020-03-26 19:40:56.711	{}
22075	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:41:00	EXECUTED	2020-03-26 19:41:56.73	{}
22076	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:42:00	EXECUTED	2020-03-26 19:42:56.749	{}
22077	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:43:00	EXECUTED	2020-03-26 19:43:56.759	{}
22078	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:44:00	EXECUTED	2020-03-26 19:44:56.774	{}
22079	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:45:00	EXECUTED	2020-03-26 19:45:56.784	{}
22080	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:46:00	EXECUTED	2020-03-26 19:46:56.794	{}
22081	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:47:00	EXECUTED	2020-03-26 19:47:56.809	{}
22211	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:47:00	EXECUTED	2020-03-26 21:47:59.322	{}
22212	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:48:00	EXECUTED	2020-03-26 21:48:59.333	{}
22213	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:49:00	EXECUTED	2020-03-26 21:49:59.344	{}
22214	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:50:00	EXECUTED	2020-03-26 21:50:59.357	{}
22215	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:51:00	EXECUTED	2020-03-26 21:51:59.367	{}
22216	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:52:00	EXECUTED	2020-03-26 21:52:59.378	{}
22323	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:31:00	EXECUTED	2020-03-26 23:32:01.607	{}
22519	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:32:00	EXECUTED	2020-03-27 02:33:05.287	{}
22520	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:33:00	EXECUTED	2020-03-27 02:34:05.305	{}
22521	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:34:00	EXECUTED	2020-03-27 02:35:05.333	{}
22522	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:35:00	EXECUTED	2020-03-27 02:36:05.344	{}
22523	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:36:00	EXECUTED	2020-03-27 02:37:05.372	{}
22621	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:06:00	EXECUTED	2020-03-27 04:07:06.843	{}
22622	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:07:00	EXECUTED	2020-03-27 04:08:06.86	{}
22623	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:08:00	EXECUTED	2020-03-27 04:09:06.883	{}
22693	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:11:00	EXECUTED	2020-03-27 05:12:09.83	{}
22694	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:12:00	EXECUTED	2020-03-27 05:13:09.888	{}
23456	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:57:00	EXECUTED	2020-03-27 16:58:24.631	{}
22828	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:16:00	EXECUTED	2020-03-27 07:17:12.571	{}
22829	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:17:00	EXECUTED	2020-03-27 07:18:12.585	{}
22830	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:18:00	EXECUTED	2020-03-27 07:19:12.608	{}
22831	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:19:00	EXECUTED	2020-03-27 07:20:12.632	{}
22832	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:20:00	EXECUTED	2020-03-27 07:21:12.649	{}
23311	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:42:00	EXECUTED	2020-03-27 14:43:21.954	{}
22833	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:21:00	EXECUTED	2020-03-27 07:22:12.658	{}
23457	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:58:00	EXECUTED	2020-03-27 16:59:24.644	{}
22834	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:22:00	EXECUTED	2020-03-27 07:23:12.67	{}
23312	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:43:00	EXECUTED	2020-03-27 14:44:21.974	{}
22835	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:23:00	EXECUTED	2020-03-27 07:24:12.681	{}
23313	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:44:00	EXECUTED	2020-03-27 14:45:21.99	{}
22836	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:24:00	EXECUTED	2020-03-27 07:25:12.692	{}
22837	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:25:00	EXECUTED	2020-03-27 07:26:12.703	{}
23314	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:45:00	EXECUTED	2020-03-27 14:46:22.008	{}
23458	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:59:00	EXECUTED	2020-03-27 17:00:24.652	{}
23459	SEND_OFFLINE_PAYMENT_TO_ORGANIZER	2020-03-27 17:00:00	EXECUTED	2020-03-27 17:00:24.653	{}
22838	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:26:00	EXECUTED	2020-03-27 07:27:12.713	{}
23460	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:00:00	EXECUTED	2020-03-27 17:01:24.663	{}
23461	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:01:00	EXECUTED	2020-03-27 17:02:24.674	{}
22035	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:05:00	EXECUTED	2020-03-26 19:05:55.898	{}
22036	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:06:00	EXECUTED	2020-03-26 19:06:55.929	{}
22082	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:48:00	EXECUTED	2020-03-26 19:48:56.833	{}
22083	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:49:00	EXECUTED	2020-03-26 19:49:56.859	{}
22084	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:50:00	EXECUTED	2020-03-26 19:50:56.878	{}
22085	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:51:00	EXECUTED	2020-03-26 19:51:56.905	{}
22086	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:52:00	EXECUTED	2020-03-26 19:52:56.917	{}
22087	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:53:00	EXECUTED	2020-03-26 19:53:56.928	{}
22088	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:54:00	EXECUTED	2020-03-26 19:54:56.94	{}
22089	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:55:00	EXECUTED	2020-03-26 19:55:56.966	{}
22090	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:56:00	EXECUTED	2020-03-26 19:56:56.978	{}
22091	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:57:00	EXECUTED	2020-03-26 19:57:57.005	{}
22092	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:58:00	EXECUTED	2020-03-26 19:58:57.016	{}
22093	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:59:00	EXECUTED	2020-03-26 19:59:57.025	{}
22094	SEND_OFFLINE_PAYMENT_TO_ORGANIZER	2020-03-26 20:00:00	EXECUTED	2020-03-26 20:00:57.029	{}
22095	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:00:00	EXECUTED	2020-03-26 20:00:57.037	{}
22096	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:01:00	EXECUTED	2020-03-26 20:01:57.05	{}
22839	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:27:00	EXECUTED	2020-03-27 07:28:12.725	{}
22097	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:02:00	EXECUTED	2020-03-26 20:02:57.067	{}
22840	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:28:00	EXECUTED	2020-03-27 07:29:12.741	{}
22217	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:53:00	EXECUTED	2020-03-26 21:53:59.399	{}
22841	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:29:00	EXECUTED	2020-03-27 07:30:12.767	{}
22218	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:54:00	EXECUTED	2020-03-26 21:54:59.411	{}
22842	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:30:00	EXECUTED	2020-03-27 07:31:12.779	{}
22219	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:55:00	EXECUTED	2020-03-26 21:55:59.424	{}
22220	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:56:00	EXECUTED	2020-03-26 21:56:59.437	{}
22221	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:57:00	EXECUTED	2020-03-26 21:57:59.449	{}
22222	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:58:00	EXECUTED	2020-03-26 21:58:59.46	{}
22223	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:59:00	EXECUTED	2020-03-26 21:59:59.471	{}
22378	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:21:00	EXECUTED	2020-03-27 00:22:02.632	{}
22843	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:31:00	EXECUTED	2020-03-27 07:32:12.79	{}
22844	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:32:00	EXECUTED	2020-03-27 07:33:12.81	{}
22845	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:33:00	EXECUTED	2020-03-27 07:34:12.836	{}
22952	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:10:00	EXECUTED	2020-03-27 09:11:14.934	{}
22695	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:13:00	EXECUTED	2020-03-27 05:14:09.901	{}
22696	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:14:00	EXECUTED	2020-03-27 05:15:09.916	{}
22697	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:15:00	EXECUTED	2020-03-27 05:16:09.927	{}
22698	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:16:00	EXECUTED	2020-03-27 05:17:09.955	{}
22699	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:17:00	EXECUTED	2020-03-27 05:18:09.997	{}
22846	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:34:00	EXECUTED	2020-03-27 07:35:12.856	{}
22847	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:35:00	EXECUTED	2020-03-27 07:36:12.889	{}
22951	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 09:10:00	EXECUTED	2020-03-27 09:11:14.924	{}
22953	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:11:00	EXECUTED	2020-03-27 09:12:14.952	{}
22954	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:12:00	EXECUTED	2020-03-27 09:13:14.963	{}
22992	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:48:00	EXECUTED	2020-03-27 09:49:15.731	{}
22993	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:49:00	EXECUTED	2020-03-27 09:50:15.76	{}
22994	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:50:00	EXECUTED	2020-03-27 09:51:15.772	{}
22995	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:51:00	EXECUTED	2020-03-27 09:52:15.799	{}
22996	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:52:00	EXECUTED	2020-03-27 09:53:15.809	{}
22997	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:53:00	EXECUTED	2020-03-27 09:54:15.821	{}
22998	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:54:00	EXECUTED	2020-03-27 09:55:15.831	{}
22999	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:55:00	EXECUTED	2020-03-27 09:56:15.856	{}
23000	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:56:00	EXECUTED	2020-03-27 09:57:15.866	{}
23001	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:57:00	EXECUTED	2020-03-27 09:58:15.877	{}
23002	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:58:00	EXECUTED	2020-03-27 09:59:15.886	{}
23003	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:59:00	EXECUTED	2020-03-27 10:00:15.911	{}
23004	SEND_OFFLINE_PAYMENT_TO_ORGANIZER	2020-03-27 10:00:00	EXECUTED	2020-03-27 10:00:15.914	{}
23005	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:00:00	EXECUTED	2020-03-27 10:01:15.927	{}
23118	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:44:00	EXECUTED	2020-03-27 11:45:18.179	{}
23119	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:45:00	EXECUTED	2020-03-27 11:46:18.192	{}
23120	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:46:00	EXECUTED	2020-03-27 11:47:18.205	{}
23121	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:47:00	EXECUTED	2020-03-27 11:48:18.215	{}
23122	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:48:00	EXECUTED	2020-03-27 11:49:18.224	{}
23123	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:49:00	EXECUTED	2020-03-27 11:50:18.253	{}
23124	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:50:00	EXECUTED	2020-03-27 11:51:18.264	{}
23125	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:51:00	EXECUTED	2020-03-27 11:52:18.273	{}
23126	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:52:00	EXECUTED	2020-03-27 11:53:18.292	{}
23127	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:53:00	EXECUTED	2020-03-27 11:54:18.304	{}
23128	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:54:00	EXECUTED	2020-03-27 11:55:18.319	{}
23129	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:55:00	EXECUTED	2020-03-27 11:56:18.329	{}
23130	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:56:00	EXECUTED	2020-03-27 11:57:18.356	{}
23131	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:57:00	EXECUTED	2020-03-27 11:58:18.372	{}
23132	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:58:00	EXECUTED	2020-03-27 11:59:18.383	{}
23133	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:59:00	EXECUTED	2020-03-27 12:00:18.41	{}
23134	SEND_OFFLINE_PAYMENT_TO_ORGANIZER	2020-03-27 12:00:00	EXECUTED	2020-03-27 12:00:18.476	{}
23315	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:46:00	EXECUTED	2020-03-27 14:47:22.039	{}
23135	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:00:00	EXECUTED	2020-03-27 12:01:18.528	{}
23316	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:47:00	EXECUTED	2020-03-27 14:48:22.052	{}
23251	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:47:00	EXECUTED	2020-03-27 13:48:20.85	{}
23317	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:48:00	EXECUTED	2020-03-27 14:49:22.074	{}
23252	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:48:00	EXECUTED	2020-03-27 13:49:20.884	{}
23318	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:49:00	EXECUTED	2020-03-27 14:50:22.098	{}
23253	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:49:00	EXECUTED	2020-03-27 13:50:20.928	{}
23319	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:50:00	EXECUTED	2020-03-27 14:51:22.127	{}
23254	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:50:00	EXECUTED	2020-03-27 13:51:20.945	{}
23320	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:51:00	EXECUTED	2020-03-27 14:52:22.157	{}
23255	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:51:00	EXECUTED	2020-03-27 13:52:20.965	{}
23321	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:52:00	EXECUTED	2020-03-27 14:53:22.17	{}
23256	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:52:00	EXECUTED	2020-03-27 13:53:20.977	{}
23322	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:53:00	EXECUTED	2020-03-27 14:54:22.192	{}
23257	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:53:00	EXECUTED	2020-03-27 13:54:21.005	{}
23258	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:54:00	EXECUTED	2020-03-27 13:55:21.016	{}
23259	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:55:00	EXECUTED	2020-03-27 13:56:21.03	{}
22037	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:07:00	EXECUTED	2020-03-26 19:07:55.949	{}
22038	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:08:00	EXECUTED	2020-03-26 19:08:55.964	{}
22039	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:09:00	EXECUTED	2020-03-26 19:09:55.982	{}
22040	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-26 19:10:00	EXECUTED	2020-03-26 19:10:55.991	{}
22041	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-26 19:10:00	EXECUTED	2020-03-26 19:10:56.037	{}
22042	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:10:00	EXECUTED	2020-03-26 19:10:56.056	{}
22379	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:22:00	EXECUTED	2020-03-27 00:23:02.664	{}
23260	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:56:00	EXECUTED	2020-03-27 13:57:21.05	{}
23261	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:57:00	EXECUTED	2020-03-27 13:58:21.061	{}
23262	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:58:00	EXECUTED	2020-03-27 13:59:21.075	{}
22700	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:18:00	EXECUTED	2020-03-27 05:19:10.008	{}
22380	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:23:00	EXECUTED	2020-03-27 00:24:02.687	{}
22381	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:24:00	EXECUTED	2020-03-27 00:25:02.706	{}
22701	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:19:00	EXECUTED	2020-03-27 05:20:10.017	{}
22382	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:25:00	EXECUTED	2020-03-27 00:26:02.734	{}
22702	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:20:00	EXECUTED	2020-03-27 05:21:10.045	{}
22383	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:26:00	EXECUTED	2020-03-27 00:27:02.754	{}
22384	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:27:00	EXECUTED	2020-03-27 00:28:02.764	{}
23006	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:01:00	EXECUTED	2020-03-27 10:02:15.954	{}
22703	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:21:00	EXECUTED	2020-03-27 05:22:10.057	{}
22704	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:22:00	EXECUTED	2020-03-27 05:23:10.073	{}
22524	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:37:00	EXECUTED	2020-03-27 02:38:05.387	{}
23007	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:02:00	EXECUTED	2020-03-27 10:03:16.004	{}
22705	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:23:00	EXECUTED	2020-03-27 05:24:10.085	{}
23136	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:01:00	EXECUTED	2020-03-27 12:02:18.542	{}
22525	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:38:00	EXECUTED	2020-03-27 02:39:05.396	{}
23137	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:02:00	EXECUTED	2020-03-27 12:03:18.571	{}
23138	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:03:00	EXECUTED	2020-03-27 12:04:18.589	{}
23139	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:04:00	EXECUTED	2020-03-27 12:05:18.604	{}
23140	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:05:00	EXECUTED	2020-03-27 12:06:18.615	{}
23141	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:06:00	EXECUTED	2020-03-27 12:07:18.628	{}
23323	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:54:00	EXECUTED	2020-03-27 14:55:22.202	{}
23142	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:07:00	EXECUTED	2020-03-27 12:08:18.65	{}
23143	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:08:00	EXECUTED	2020-03-27 12:09:18.661	{}
23144	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:09:00	EXECUTED	2020-03-27 12:10:18.672	{}
23263	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:59:00	EXECUTED	2020-03-27 14:00:21.097	{}
23324	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:55:00	EXECUTED	2020-03-27 14:56:22.215	{}
23325	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:56:00	EXECUTED	2020-03-27 14:57:22.27	{}
23326	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:57:00	EXECUTED	2020-03-27 14:58:22.282	{}
23327	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:58:00	EXECUTED	2020-03-27 14:59:22.304	{}
23462	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:02:00	EXECUTED	2020-03-27 17:03:24.693	{}
23463	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:03:00	EXECUTED	2020-03-27 17:04:24.707	{}
23464	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:04:00	EXECUTED	2020-03-27 17:05:24.72	{}
23465	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:05:00	EXECUTED	2020-03-27 17:06:24.733	{}
23466	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:06:00	EXECUTED	2020-03-27 17:07:24.745	{}
23467	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:07:00	EXECUTED	2020-03-27 17:08:24.759	{}
23468	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:08:00	EXECUTED	2020-03-27 17:09:24.768	{}
23469	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:09:00	EXECUTED	2020-03-27 17:10:24.792	{}
22098	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:03:00	EXECUTED	2020-03-26 20:03:57.12	{}
22224	SEND_OFFLINE_PAYMENT_TO_ORGANIZER	2020-03-26 22:00:00	EXECUTED	2020-03-26 22:00:59.478	{}
22225	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:00:00	EXECUTED	2020-03-26 22:00:59.503	{}
22324	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:32:00	EXECUTED	2020-03-26 23:33:01.625	{}
22325	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:33:00	EXECUTED	2020-03-26 23:34:01.645	{}
22326	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:34:00	EXECUTED	2020-03-26 23:35:01.661	{}
22327	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:35:00	EXECUTED	2020-03-26 23:36:01.671	{}
22328	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:36:00	EXECUTED	2020-03-26 23:37:01.682	{}
22329	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:37:00	EXECUTED	2020-03-26 23:38:01.691	{}
22330	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:38:00	EXECUTED	2020-03-26 23:39:01.699	{}
22331	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:39:00	EXECUTED	2020-03-26 23:40:01.707	{}
22333	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-26 23:40:00	EXECUTED	2020-03-26 23:41:01.729	{}
22334	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:40:00	EXECUTED	2020-03-26 23:41:01.736	{}
22526	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:39:00	EXECUTED	2020-03-27 02:40:05.407	{}
22528	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 02:40:00	EXECUTED	2020-03-27 02:41:05.429	{}
22529	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:40:00	EXECUTED	2020-03-27 02:41:05.438	{}
23145	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 12:10:00	EXECUTED	2020-03-27 12:11:18.675	{}
22226	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:01:00	EXECUTED	2020-03-26 22:01:59.524	{}
23146	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 12:10:00	EXECUTED	2020-03-27 12:11:18.694	{}
22099	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:04:00	EXECUTED	2020-03-26 20:04:57.168	{}
23264	SEND_OFFLINE_PAYMENT_TO_ORGANIZER	2020-03-27 14:00:00	EXECUTED	2020-03-27 14:00:21.098	{}
23265	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:00:00	EXECUTED	2020-03-27 14:01:21.125	{}
23266	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:01:00	EXECUTED	2020-03-27 14:02:21.139	{}
22100	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:05:00	EXECUTED	2020-03-26 20:05:57.182	{}
22043	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:11:00	EXECUTED	2020-03-26 19:11:56.066	{}
22385	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:28:00	EXECUTED	2020-03-27 00:29:02.784	{}
22044	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:12:00	EXECUTED	2020-03-26 19:12:56.079	{}
22045	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:13:00	EXECUTED	2020-03-26 19:13:56.089	{}
22046	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:14:00	EXECUTED	2020-03-26 19:14:56.1	{}
22047	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:15:00	EXECUTED	2020-03-26 19:15:56.126	{}
22048	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:16:00	EXECUTED	2020-03-26 19:16:56.137	{}
22049	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:17:00	EXECUTED	2020-03-26 19:17:56.154	{}
22050	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:18:00	EXECUTED	2020-03-26 19:18:56.175	{}
22101	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:06:00	EXECUTED	2020-03-26 20:06:57.217	{}
22332	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-26 23:40:00	EXECUTED	2020-03-26 23:41:01.712	{}
22335	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:41:00	EXECUTED	2020-03-26 23:42:01.744	{}
22336	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:42:00	EXECUTED	2020-03-26 23:43:01.754	{}
22337	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:43:00	EXECUTED	2020-03-26 23:44:01.766	{}
22386	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:29:00	EXECUTED	2020-03-27 00:30:02.801	{}
22527	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 02:40:00	EXECUTED	2020-03-27 02:41:05.412	{}
22530	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:41:00	EXECUTED	2020-03-27 02:42:05.449	{}
22624	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:09:00	EXECUTED	2020-03-27 04:10:06.903	{}
22625	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 04:10:00	EXECUTED	2020-03-27 04:11:06.907	{}
22626	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 04:10:00	EXECUTED	2020-03-27 04:11:06.927	{}
22627	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:10:00	EXECUTED	2020-03-27 04:11:06.938	{}
22706	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:24:00	EXECUTED	2020-03-27 05:25:10.111	{}
22707	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:25:00	EXECUTED	2020-03-27 05:26:10.138	{}
22708	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:26:00	EXECUTED	2020-03-27 05:27:10.155	{}
22709	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:27:00	EXECUTED	2020-03-27 05:28:10.165	{}
22710	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:28:00	EXECUTED	2020-03-27 05:29:10.178	{}
22711	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:29:00	EXECUTED	2020-03-27 05:30:10.195	{}
22712	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:30:00	EXECUTED	2020-03-27 05:31:10.206	{}
22713	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:31:00	EXECUTED	2020-03-27 05:32:10.225	{}
22714	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:32:00	EXECUTED	2020-03-27 05:33:10.251	{}
22715	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:33:00	EXECUTED	2020-03-27 05:34:10.261	{}
22716	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:34:00	EXECUTED	2020-03-27 05:35:10.274	{}
22717	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:35:00	EXECUTED	2020-03-27 05:36:10.293	{}
22848	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:36:00	EXECUTED	2020-03-27 07:37:12.909	{}
22849	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:37:00	EXECUTED	2020-03-27 07:38:12.94	{}
22850	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:38:00	EXECUTED	2020-03-27 07:39:12.967	{}
22851	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:39:00	EXECUTED	2020-03-27 07:40:12.981	{}
22955	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:13:00	EXECUTED	2020-03-27 09:14:15.011	{}
22956	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:14:00	EXECUTED	2020-03-27 09:15:15.034	{}
23008	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:03:00	EXECUTED	2020-03-27 10:04:16.02	{}
23009	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:04:00	EXECUTED	2020-03-27 10:05:16.075	{}
23147	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:10:00	EXECUTED	2020-03-27 12:11:18.702	{}
23148	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:11:00	EXECUTED	2020-03-27 12:12:18.728	{}
23149	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:12:00	EXECUTED	2020-03-27 12:13:18.741	{}
23150	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:13:00	EXECUTED	2020-03-27 12:14:18.802	{}
23267	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:02:00	EXECUTED	2020-03-27 14:03:21.149	{}
23268	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:03:00	EXECUTED	2020-03-27 14:04:21.162	{}
23269	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:04:00	EXECUTED	2020-03-27 14:05:21.181	{}
23270	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:05:00	EXECUTED	2020-03-27 14:06:21.194	{}
23271	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:06:00	EXECUTED	2020-03-27 14:07:21.22	{}
23272	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:07:00	EXECUTED	2020-03-27 14:08:21.232	{}
23273	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:08:00	EXECUTED	2020-03-27 14:09:21.244	{}
23274	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:09:00	EXECUTED	2020-03-27 14:10:21.254	{}
23276	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 14:10:00	EXECUTED	2020-03-27 14:11:21.277	{}
22718	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:36:00	EXECUTED	2020-03-27 05:37:10.305	{}
23328	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:59:00	EXECUTED	2020-03-27 15:00:22.317	{}
23329	SEND_OFFLINE_PAYMENT_TO_ORGANIZER	2020-03-27 15:00:00	EXECUTED	2020-03-27 15:00:22.319	{}
22531	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:42:00	EXECUTED	2020-03-27 02:43:05.464	{}
23330	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:00:00	EXECUTED	2020-03-27 15:01:22.333	{}
22852	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 07:40:00	EXECUTED	2020-03-27 07:41:12.991	{}
22719	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:37:00	EXECUTED	2020-03-27 05:38:10.317	{}
23331	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:01:00	EXECUTED	2020-03-27 15:02:22.347	{}
23332	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:02:00	EXECUTED	2020-03-27 15:03:22.358	{}
23333	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:03:00	EXECUTED	2020-03-27 15:04:22.377	{}
22853	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 07:40:00	EXECUTED	2020-03-27 07:41:13.045	{}
23334	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:04:00	EXECUTED	2020-03-27 15:05:22.405	{}
22854	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:40:00	EXECUTED	2020-03-27 07:41:13.059	{}
23335	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:05:00	EXECUTED	2020-03-27 15:06:22.418	{}
22855	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:41:00	EXECUTED	2020-03-27 07:42:13.091	{}
23336	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:06:00	EXECUTED	2020-03-27 15:07:22.427	{}
23338	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:08:00	EXECUTED	2020-03-27 15:09:22.45	{}
23339	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:09:00	EXECUTED	2020-03-27 15:10:22.461	{}
23340	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 15:10:00	EXECUTED	2020-03-27 15:11:22.465	{}
23470	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 17:10:00	EXECUTED	2020-03-27 17:11:24.797	{}
22102	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:07:00	EXECUTED	2020-03-26 20:07:57.237	{}
22103	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:08:00	EXECUTED	2020-03-26 20:08:57.247	{}
22104	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:09:00	EXECUTED	2020-03-26 20:09:57.258	{}
22387	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:30:00	EXECUTED	2020-03-27 00:31:02.823	{}
22532	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:43:00	EXECUTED	2020-03-27 02:44:05.481	{}
22533	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:44:00	EXECUTED	2020-03-27 02:45:05.493	{}
22051	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:19:00	EXECUTED	2020-03-26 19:19:56.191	{}
22534	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:45:00	EXECUTED	2020-03-27 02:46:05.505	{}
22052	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:20:00	EXECUTED	2020-03-26 19:20:56.236	{}
22535	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:46:00	EXECUTED	2020-03-27 02:47:05.517	{}
22536	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:47:00	EXECUTED	2020-03-27 02:48:05.535	{}
22537	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:48:00	EXECUTED	2020-03-27 02:49:05.588	{}
22628	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:11:00	EXECUTED	2020-03-27 04:12:06.98	{}
22053	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:21:00	EXECUTED	2020-03-26 19:21:56.253	{}
22629	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:12:00	EXECUTED	2020-03-27 04:13:07.006	{}
22630	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:13:00	EXECUTED	2020-03-27 04:14:07.019	{}
22631	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:14:00	EXECUTED	2020-03-27 04:15:07.036	{}
22054	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:22:00	EXECUTED	2020-03-26 19:22:56.265	{}
22632	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:15:00	EXECUTED	2020-03-27 04:16:07.048	{}
22633	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:16:00	EXECUTED	2020-03-27 04:17:07.059	{}
22634	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:17:00	EXECUTED	2020-03-27 04:18:07.11	{}
22055	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:23:00	EXECUTED	2020-03-26 19:23:56.277	{}
22856	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:42:00	EXECUTED	2020-03-27 07:43:13.103	{}
22857	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:43:00	EXECUTED	2020-03-27 07:44:13.13	{}
22858	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:44:00	EXECUTED	2020-03-27 07:45:13.15	{}
22056	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:24:00	EXECUTED	2020-03-26 19:24:56.289	{}
22957	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:15:00	EXECUTED	2020-03-27 09:16:15.079	{}
22958	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:16:00	EXECUTED	2020-03-27 09:17:15.1	{}
22959	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:17:00	EXECUTED	2020-03-27 09:18:15.128	{}
22960	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:18:00	EXECUTED	2020-03-27 09:19:15.139	{}
22961	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:19:00	EXECUTED	2020-03-27 09:20:15.153	{}
22962	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:20:00	EXECUTED	2020-03-27 09:21:15.163	{}
22963	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:21:00	EXECUTED	2020-03-27 09:22:15.177	{}
23337	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:07:00	EXECUTED	2020-03-27 15:08:22.437	{}
23341	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 15:10:00	EXECUTED	2020-03-27 15:11:22.482	{}
23342	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:10:00	EXECUTED	2020-03-27 15:11:22.491	{}
23471	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 17:10:00	EXECUTED	2020-03-27 17:11:24.816	{}
23472	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:10:00	EXECUTED	2020-03-27 17:11:24.837	{}
22057	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:25:00	EXECUTED	2020-03-26 19:25:56.315	{}
22058	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:26:00	EXECUTED	2020-03-26 19:26:56.342	{}
22059	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:27:00	EXECUTED	2020-03-26 19:27:56.354	{}
22060	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:28:00	EXECUTED	2020-03-26 19:28:56.364	{}
22061	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:29:00	EXECUTED	2020-03-26 19:29:56.375	{}
22062	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:30:00	EXECUTED	2020-03-26 19:30:56.401	{}
22063	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:31:00	EXECUTED	2020-03-26 19:31:56.421	{}
22064	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:32:00	EXECUTED	2020-03-26 19:32:56.447	{}
22065	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:33:00	EXECUTED	2020-03-26 19:33:56.501	{}
23154	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:17:00	EXECUTED	2020-03-27 12:18:18.896	{}
23343	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:11:00	EXECUTED	2020-03-27 15:12:22.518	{}
23010	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:05:00	EXECUTED	2020-03-27 10:06:16.109	{}
22966	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:24:00	EXECUTED	2020-03-27 09:25:15.225	{}
23473	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:11:00	EXECUTED	2020-03-27 17:12:24.86	{}
23011	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:06:00	EXECUTED	2020-03-27 10:07:16.142	{}
22967	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:25:00	EXECUTED	2020-03-27 09:26:15.236	{}
23474	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:12:00	EXECUTED	2020-03-27 17:13:24.88	{}
23012	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:07:00	EXECUTED	2020-03-27 10:08:16.155	{}
22968	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:26:00	EXECUTED	2020-03-27 09:27:15.253	{}
23475	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:13:00	EXECUTED	2020-03-27 17:14:24.891	{}
23013	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:08:00	EXECUTED	2020-03-27 10:09:16.171	{}
22066	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:34:00	EXECUTED	2020-03-26 19:34:56.524	{}
22067	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:35:00	EXECUTED	2020-03-26 19:35:56.538	{}
22068	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:36:00	EXECUTED	2020-03-26 19:36:56.556	{}
22105	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-26 20:10:00	EXECUTED	2020-03-26 20:10:57.262	{}
22106	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-26 20:10:00	EXECUTED	2020-03-26 20:10:57.28	{}
22107	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:10:00	EXECUTED	2020-03-26 20:10:57.288	{}
22108	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:11:00	EXECUTED	2020-03-26 20:11:57.3	{}
22227	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:02:00	EXECUTED	2020-03-26 22:02:59.569	{}
22338	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:44:00	EXECUTED	2020-03-26 23:45:01.785	{}
22339	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:45:00	EXECUTED	2020-03-26 23:46:01.799	{}
22340	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:46:00	EXECUTED	2020-03-26 23:47:01.81	{}
22341	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:47:00	EXECUTED	2020-03-26 23:48:01.821	{}
22342	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:48:00	EXECUTED	2020-03-26 23:49:01.832	{}
22343	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:49:00	EXECUTED	2020-03-26 23:50:01.842	{}
22344	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:50:00	EXECUTED	2020-03-26 23:51:01.87	{}
22388	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:31:00	EXECUTED	2020-03-27 00:32:02.868	{}
22389	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:32:00	EXECUTED	2020-03-27 00:33:02.901	{}
22390	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:33:00	EXECUTED	2020-03-27 00:34:02.919	{}
22391	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:34:00	EXECUTED	2020-03-27 00:35:02.933	{}
22392	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:35:00	EXECUTED	2020-03-27 00:36:02.944	{}
22393	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:36:00	EXECUTED	2020-03-27 00:37:02.962	{}
22394	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:37:00	EXECUTED	2020-03-27 00:38:02.99	{}
22395	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:38:00	EXECUTED	2020-03-27 00:39:03.002	{}
22396	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:39:00	EXECUTED	2020-03-27 00:40:03.029	{}
22397	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 00:40:00	EXECUTED	2020-03-27 00:41:03.034	{}
22398	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 00:40:00	EXECUTED	2020-03-27 00:41:03.051	{}
22399	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:40:00	EXECUTED	2020-03-27 00:41:03.059	{}
22400	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:41:00	EXECUTED	2020-03-27 00:42:03.084	{}
22401	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:42:00	EXECUTED	2020-03-27 00:43:03.096	{}
22402	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:43:00	EXECUTED	2020-03-27 00:44:03.114	{}
22538	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:49:00	EXECUTED	2020-03-27 02:50:05.599	{}
22635	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:18:00	EXECUTED	2020-03-27 04:19:07.125	{}
22720	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:38:00	EXECUTED	2020-03-27 05:39:10.366	{}
22721	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:39:00	EXECUTED	2020-03-27 05:40:10.389	{}
22722	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 05:40:00	EXECUTED	2020-03-27 05:41:10.395	{}
22723	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 05:40:00	EXECUTED	2020-03-27 05:41:10.424	{}
22724	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:40:00	EXECUTED	2020-03-27 05:41:10.444	{}
22859	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:45:00	EXECUTED	2020-03-27 07:46:13.166	{}
22964	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:22:00	EXECUTED	2020-03-27 09:23:15.188	{}
22965	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:23:00	EXECUTED	2020-03-27 09:24:15.204	{}
22969	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:27:00	EXECUTED	2020-03-27 09:28:15.27	{}
22970	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:28:00	EXECUTED	2020-03-27 09:29:15.281	{}
22971	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:29:00	EXECUTED	2020-03-27 09:30:15.291	{}
22972	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:30:00	EXECUTED	2020-03-27 09:31:15.317	{}
22973	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:31:00	EXECUTED	2020-03-27 09:32:15.328	{}
22974	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:32:00	EXECUTED	2020-03-27 09:33:15.38	{}
23151	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:14:00	EXECUTED	2020-03-27 12:15:18.815	{}
23152	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:15:00	EXECUTED	2020-03-27 12:16:18.828	{}
23153	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:16:00	EXECUTED	2020-03-27 12:17:18.851	{}
23275	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 14:10:00	EXECUTED	2020-03-27 14:11:21.259	{}
23277	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:10:00	EXECUTED	2020-03-27 14:11:21.285	{}
23278	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:11:00	EXECUTED	2020-03-27 14:12:21.311	{}
23476	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:14:00	EXECUTED	2020-03-27 17:15:24.919	{}
23477	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:15:00	EXECUTED	2020-03-27 17:16:24.93	{}
23478	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:16:00	EXECUTED	2020-03-27 17:17:24.94	{}
23479	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:17:00	EXECUTED	2020-03-27 17:18:24.959	{}
22725	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:41:00	EXECUTED	2020-03-27 05:42:10.476	{}
23014	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:09:00	EXECUTED	2020-03-27 10:10:16.185	{}
22726	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:42:00	EXECUTED	2020-03-27 05:43:10.494	{}
22109	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:12:00	EXECUTED	2020-03-26 20:12:57.313	{}
22727	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:43:00	EXECUTED	2020-03-27 05:44:10.521	{}
22728	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:44:00	EXECUTED	2020-03-27 05:45:10.535	{}
22110	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:13:00	EXECUTED	2020-03-26 20:13:57.325	{}
22729	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:45:00	EXECUTED	2020-03-27 05:46:10.562	{}
23015	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 10:10:00	EXECUTED	2020-03-27 10:11:16.201	{}
23016	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 10:10:00	EXECUTED	2020-03-27 10:11:16.246	{}
22860	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:46:00	EXECUTED	2020-03-27 07:47:13.191	{}
22111	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:14:00	EXECUTED	2020-03-26 20:14:57.337	{}
23017	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:10:00	EXECUTED	2020-03-27 10:11:16.262	{}
23018	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:11:00	EXECUTED	2020-03-27 10:12:16.273	{}
23155	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:18:00	EXECUTED	2020-03-27 12:19:18.909	{}
22112	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:15:00	EXECUTED	2020-03-26 20:15:57.351	{}
23156	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:19:00	EXECUTED	2020-03-27 12:20:18.938	{}
23157	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:20:00	EXECUTED	2020-03-27 12:21:18.951	{}
23158	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:21:00	EXECUTED	2020-03-27 12:22:18.981	{}
23159	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:22:00	EXECUTED	2020-03-27 12:23:19.01	{}
23160	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:23:00	EXECUTED	2020-03-27 12:24:19.036	{}
23161	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:24:00	EXECUTED	2020-03-27 12:25:19.064	{}
23344	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:12:00	EXECUTED	2020-03-27 15:13:22.528	{}
23162	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:25:00	EXECUTED	2020-03-27 12:26:19.077	{}
23163	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:26:00	EXECUTED	2020-03-27 12:27:19.092	{}
23345	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:13:00	EXECUTED	2020-03-27 15:14:22.552	{}
23164	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:27:00	EXECUTED	2020-03-27 12:28:19.12	{}
23346	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:14:00	EXECUTED	2020-03-27 15:15:22.578	{}
23165	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:28:00	EXECUTED	2020-03-27 12:29:19.132	{}
23480	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:18:00	EXECUTED	2020-03-27 17:19:24.969	{}
23166	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:29:00	EXECUTED	2020-03-27 12:30:19.156	{}
23481	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:19:00	EXECUTED	2020-03-27 17:20:24.997	{}
23279	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:12:00	EXECUTED	2020-03-27 14:13:21.343	{}
23482	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:20:00	EXECUTED	2020-03-27 17:21:25.025	{}
23280	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:13:00	EXECUTED	2020-03-27 14:14:21.373	{}
23281	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:14:00	EXECUTED	2020-03-27 14:15:21.384	{}
23282	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:15:00	EXECUTED	2020-03-27 14:16:21.397	{}
22113	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:16:00	EXECUTED	2020-03-26 20:16:57.362	{}
22861	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:47:00	EXECUTED	2020-03-27 07:48:13.206	{}
22114	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:17:00	EXECUTED	2020-03-26 20:17:57.373	{}
22862	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:48:00	EXECUTED	2020-03-27 07:49:13.216	{}
22863	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:49:00	EXECUTED	2020-03-27 07:50:13.243	{}
22228	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:03:00	EXECUTED	2020-03-26 22:03:59.645	{}
22229	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:04:00	EXECUTED	2020-03-26 22:04:59.675	{}
22230	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:05:00	EXECUTED	2020-03-26 22:05:59.695	{}
22231	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:06:00	EXECUTED	2020-03-26 22:06:59.712	{}
22232	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:07:00	EXECUTED	2020-03-26 22:07:59.728	{}
22345	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:51:00	EXECUTED	2020-03-26 23:52:01.91	{}
22233	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:08:00	EXECUTED	2020-03-26 22:08:59.748	{}
22234	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:09:00	EXECUTED	2020-03-26 22:09:59.769	{}
22235	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-26 22:10:00	EXECUTED	2020-03-26 22:10:59.774	{}
22236	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-26 22:10:00	EXECUTED	2020-03-26 22:10:59.791	{}
22237	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:10:00	EXECUTED	2020-03-26 22:10:59.799	{}
22238	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:11:00	EXECUTED	2020-03-26 22:11:59.815	{}
22239	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:12:00	EXECUTED	2020-03-26 22:12:59.826	{}
22240	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:13:00	EXECUTED	2020-03-26 22:13:59.835	{}
22241	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:14:00	EXECUTED	2020-03-26 22:14:59.847	{}
22242	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:15:00	EXECUTED	2020-03-26 22:15:59.857	{}
22346	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:52:00	EXECUTED	2020-03-26 23:53:01.943	{}
22347	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:53:00	EXECUTED	2020-03-26 23:54:01.955	{}
22348	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:54:00	EXECUTED	2020-03-26 23:55:01.975	{}
23347	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:15:00	EXECUTED	2020-03-27 15:16:22.633	{}
23348	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:16:00	EXECUTED	2020-03-27 15:17:22.648	{}
23485	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:23:00	EXECUTED	2020-03-27 17:24:25.071	{}
23349	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:17:00	EXECUTED	2020-03-27 15:18:22.659	{}
23350	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:18:00	EXECUTED	2020-03-27 15:19:22.671	{}
23351	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:19:00	EXECUTED	2020-03-27 15:20:22.684	{}
23483	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:21:00	EXECUTED	2020-03-27 17:22:25.049	{}
23484	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:22:00	EXECUTED	2020-03-27 17:23:25.062	{}
23486	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:24:00	EXECUTED	2020-03-27 17:25:25.099	{}
23487	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:25:00	EXECUTED	2020-03-27 17:26:25.111	{}
23488	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:26:00	EXECUTED	2020-03-27 17:27:25.13	{}
22403	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:44:00	EXECUTED	2020-03-27 00:45:03.144	{}
22115	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:18:00	EXECUTED	2020-03-26 20:18:57.411	{}
22116	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:19:00	EXECUTED	2020-03-26 20:19:57.436	{}
22117	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:20:00	EXECUTED	2020-03-26 20:20:57.446	{}
22118	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:21:00	EXECUTED	2020-03-26 20:21:57.459	{}
22404	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:45:00	EXECUTED	2020-03-27 00:46:03.188	{}
22119	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:22:00	EXECUTED	2020-03-26 20:22:57.469	{}
22243	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:16:00	EXECUTED	2020-03-26 22:16:59.88	{}
22120	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:23:00	EXECUTED	2020-03-26 20:23:57.495	{}
22121	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:24:00	EXECUTED	2020-03-26 20:24:57.504	{}
22122	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:25:00	EXECUTED	2020-03-26 20:25:57.53	{}
22405	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:46:00	EXECUTED	2020-03-27 00:47:03.198	{}
22244	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:17:00	EXECUTED	2020-03-26 22:17:59.894	{}
22123	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:26:00	EXECUTED	2020-03-26 20:26:57.55	{}
22406	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:47:00	EXECUTED	2020-03-27 00:48:03.215	{}
22245	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:18:00	EXECUTED	2020-03-26 22:18:59.906	{}
22407	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:48:00	EXECUTED	2020-03-27 00:49:03.238	{}
22246	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:19:00	EXECUTED	2020-03-26 22:19:59.917	{}
22408	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:49:00	EXECUTED	2020-03-27 00:50:03.255	{}
22247	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:20:00	EXECUTED	2020-03-26 22:20:59.929	{}
22409	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:50:00	EXECUTED	2020-03-27 00:51:03.274	{}
22350	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:56:00	EXECUTED	2020-03-26 23:57:02.067	{}
22539	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:50:00	EXECUTED	2020-03-27 02:51:05.614	{}
22351	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:57:00	EXECUTED	2020-03-26 23:58:02.081	{}
22540	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:51:00	EXECUTED	2020-03-27 02:52:05.647	{}
22730	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:46:00	EXECUTED	2020-03-27 05:47:10.609	{}
22541	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:52:00	EXECUTED	2020-03-27 02:53:05.677	{}
22864	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:50:00	EXECUTED	2020-03-27 07:51:13.257	{}
22865	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:51:00	EXECUTED	2020-03-27 07:52:13.269	{}
22866	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:52:00	EXECUTED	2020-03-27 07:53:13.281	{}
22867	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:53:00	EXECUTED	2020-03-27 07:54:13.295	{}
22124	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:27:00	EXECUTED	2020-03-26 20:27:57.564	{}
22125	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:28:00	EXECUTED	2020-03-26 20:28:57.582	{}
22126	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:29:00	EXECUTED	2020-03-26 20:29:57.609	{}
22127	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:30:00	EXECUTED	2020-03-26 20:30:57.619	{}
22128	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:31:00	EXECUTED	2020-03-26 20:31:57.636	{}
22129	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:32:00	EXECUTED	2020-03-26 20:32:57.655	{}
22248	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:21:00	EXECUTED	2020-03-26 22:21:59.955	{}
22349	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:55:00	EXECUTED	2020-03-26 23:56:01.995	{}
22352	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:58:00	EXECUTED	2020-03-26 23:59:02.11	{}
22353	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:59:00	EXECUTED	2020-03-27 00:00:02.124	{}
22354	SEND_OFFLINE_PAYMENT_TO_ORGANIZER	2020-03-27 00:00:00	EXECUTED	2020-03-27 00:00:02.125	{}
22868	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:54:00	EXECUTED	2020-03-27 07:55:13.318	{}
22975	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:33:00	EXECUTED	2020-03-27 09:34:15.41	{}
23019	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:12:00	EXECUTED	2020-03-27 10:13:16.323	{}
22732	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:48:00	EXECUTED	2020-03-27 05:49:10.661	{}
22641	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:24:00	EXECUTED	2020-03-27 04:25:07.286	{}
22256	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:29:00	EXECUTED	2020-03-26 22:30:00.105	{}
23020	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:13:00	EXECUTED	2020-03-27 10:14:16.337	{}
22731	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:47:00	EXECUTED	2020-03-27 05:48:10.638	{}
22417	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:58:00	EXECUTED	2020-03-27 00:59:03.367	{}
22257	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:30:00	EXECUTED	2020-03-26 22:31:00.115	{}
23283	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:16:00	EXECUTED	2020-03-27 14:17:21.442	{}
22130	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:33:00	EXECUTED	2020-03-26 20:33:57.69	{}
22249	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:22:00	EXECUTED	2020-03-26 22:23:00.006	{}
22250	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:23:00	EXECUTED	2020-03-26 22:24:00.043	{}
22251	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:24:00	EXECUTED	2020-03-26 22:25:00.053	{}
22252	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:25:00	EXECUTED	2020-03-26 22:26:00.062	{}
22253	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:26:00	EXECUTED	2020-03-26 22:27:00.073	{}
22410	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:51:00	EXECUTED	2020-03-27 00:52:03.299	{}
22254	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:27:00	EXECUTED	2020-03-26 22:28:00.083	{}
22411	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:52:00	EXECUTED	2020-03-27 00:53:03.308	{}
22255	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:28:00	EXECUTED	2020-03-26 22:29:00.093	{}
22355	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:00:00	EXECUTED	2020-03-27 00:01:02.174	{}
22412	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:53:00	EXECUTED	2020-03-27 00:54:03.318	{}
22413	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:54:00	EXECUTED	2020-03-27 00:55:03.328	{}
22414	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:55:00	EXECUTED	2020-03-27 00:56:03.337	{}
22415	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:56:00	EXECUTED	2020-03-27 00:57:03.346	{}
22416	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:57:00	EXECUTED	2020-03-27 00:58:03.356	{}
22418	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:59:00	EXECUTED	2020-03-27 01:00:03.387	{}
22542	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:53:00	EXECUTED	2020-03-27 02:54:05.705	{}
22543	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:54:00	EXECUTED	2020-03-27 02:55:05.716	{}
22544	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:55:00	EXECUTED	2020-03-27 02:56:05.729	{}
22733	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:49:00	EXECUTED	2020-03-27 05:50:10.679	{}
22545	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:56:00	EXECUTED	2020-03-27 02:57:05.757	{}
22546	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:57:00	EXECUTED	2020-03-27 02:58:05.791	{}
22734	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:50:00	EXECUTED	2020-03-27 05:51:10.689	{}
22636	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:19:00	EXECUTED	2020-03-27 04:20:07.174	{}
22735	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:51:00	EXECUTED	2020-03-27 05:52:10.701	{}
22637	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:20:00	EXECUTED	2020-03-27 04:21:07.208	{}
22736	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:52:00	EXECUTED	2020-03-27 05:53:10.727	{}
22638	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:21:00	EXECUTED	2020-03-27 04:22:07.239	{}
22737	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:53:00	EXECUTED	2020-03-27 05:54:10.743	{}
22642	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:25:00	EXECUTED	2020-03-27 04:26:07.313	{}
22738	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:54:00	EXECUTED	2020-03-27 05:55:10.758	{}
22643	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:26:00	EXECUTED	2020-03-27 04:27:07.329	{}
22739	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:55:00	EXECUTED	2020-03-27 05:56:10.769	{}
22644	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:27:00	EXECUTED	2020-03-27 04:28:07.339	{}
22740	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:56:00	EXECUTED	2020-03-27 05:57:10.796	{}
22645	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:28:00	EXECUTED	2020-03-27 04:29:07.367	{}
22741	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:57:00	EXECUTED	2020-03-27 05:58:10.818	{}
23021	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:14:00	EXECUTED	2020-03-27 10:15:16.362	{}
23167	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:30:00	EXECUTED	2020-03-27 12:31:19.18	{}
22742	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:58:00	EXECUTED	2020-03-27 05:59:10.831	{}
23284	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:17:00	EXECUTED	2020-03-27 14:18:21.476	{}
23285	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:18:00	EXECUTED	2020-03-27 14:19:21.49	{}
22639	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:22:00	EXECUTED	2020-03-27 04:23:07.251	{}
22640	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:23:00	EXECUTED	2020-03-27 04:24:07.274	{}
22646	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:29:00	EXECUTED	2020-03-27 04:30:07.385	{}
22647	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:30:00	EXECUTED	2020-03-27 04:31:07.403	{}
22648	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:31:00	EXECUTED	2020-03-27 04:32:07.414	{}
22649	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:32:00	EXECUTED	2020-03-27 04:33:07.424	{}
22650	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:33:00	EXECUTED	2020-03-27 04:34:07.434	{}
22651	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:34:00	EXECUTED	2020-03-27 04:35:07.446	{}
22652	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:35:00	EXECUTED	2020-03-27 04:36:07.455	{}
23286	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:19:00	EXECUTED	2020-03-27 14:20:21.517	{}
23287	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:20:00	EXECUTED	2020-03-27 14:21:21.528	{}
23288	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:21:00	EXECUTED	2020-03-27 14:22:21.545	{}
23022	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:15:00	EXECUTED	2020-03-27 10:16:16.395	{}
23352	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:20:00	EXECUTED	2020-03-27 15:21:22.696	{}
23023	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:16:00	EXECUTED	2020-03-27 10:17:16.413	{}
23024	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:17:00	EXECUTED	2020-03-27 10:18:16.442	{}
23353	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:21:00	EXECUTED	2020-03-27 15:22:22.707	{}
23025	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:18:00	EXECUTED	2020-03-27 10:19:16.452	{}
23354	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:22:00	EXECUTED	2020-03-27 15:23:22.721	{}
23026	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:19:00	EXECUTED	2020-03-27 10:20:16.478	{}
23027	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:20:00	EXECUTED	2020-03-27 10:21:16.506	{}
23355	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:23:00	EXECUTED	2020-03-27 15:24:22.736	{}
23028	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:21:00	EXECUTED	2020-03-27 10:22:16.533	{}
23356	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:24:00	EXECUTED	2020-03-27 15:25:22.747	{}
23489	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:27:00	EXECUTED	2020-03-27 17:28:25.16	{}
23357	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:25:00	EXECUTED	2020-03-27 15:26:22.757	{}
23358	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:26:00	EXECUTED	2020-03-27 15:27:22.766	{}
22131	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:34:00	EXECUTED	2020-03-26 20:34:57.738	{}
23359	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:27:00	EXECUTED	2020-03-27 15:28:22.785	{}
22132	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:35:00	EXECUTED	2020-03-26 20:35:57.753	{}
22258	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:31:00	EXECUTED	2020-03-26 22:32:00.131	{}
22356	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:01:00	EXECUTED	2020-03-27 00:02:02.192	{}
22357	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:02:00	EXECUTED	2020-03-27 00:03:02.204	{}
22358	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:03:00	EXECUTED	2020-03-27 00:04:02.22	{}
22359	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:04:00	EXECUTED	2020-03-27 00:05:02.245	{}
22360	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:05:00	EXECUTED	2020-03-27 00:06:02.259	{}
22361	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:06:00	EXECUTED	2020-03-27 00:07:02.271	{}
22362	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:07:00	EXECUTED	2020-03-27 00:08:02.283	{}
22419	SEND_OFFLINE_PAYMENT_TO_ORGANIZER	2020-03-27 01:00:00	EXECUTED	2020-03-27 01:00:03.389	{}
22363	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:08:00	EXECUTED	2020-03-27 00:09:02.339	{}
22420	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:00:00	EXECUTED	2020-03-27 01:01:03.402	{}
22421	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:01:00	EXECUTED	2020-03-27 01:02:03.412	{}
22422	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:02:00	EXECUTED	2020-03-27 01:03:03.424	{}
22423	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:03:00	EXECUTED	2020-03-27 01:04:03.433	{}
22424	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:04:00	EXECUTED	2020-03-27 01:05:03.445	{}
22425	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:05:00	EXECUTED	2020-03-27 01:06:03.454	{}
22426	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:06:00	EXECUTED	2020-03-27 01:07:03.465	{}
22427	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:07:00	EXECUTED	2020-03-27 01:08:03.474	{}
23029	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:22:00	EXECUTED	2020-03-27 10:23:16.545	{}
22428	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:08:00	EXECUTED	2020-03-27 01:09:03.485	{}
22429	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:09:00	EXECUTED	2020-03-27 01:10:03.496	{}
22430	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 01:10:00	EXECUTED	2020-03-27 01:11:03.501	{}
22431	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 01:10:00	EXECUTED	2020-03-27 01:11:03.519	{}
22432	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:10:00	EXECUTED	2020-03-27 01:11:03.526	{}
22433	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:11:00	EXECUTED	2020-03-27 01:12:03.55	{}
22434	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:12:00	EXECUTED	2020-03-27 01:13:03.56	{}
22547	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:58:00	EXECUTED	2020-03-27 02:59:05.836	{}
22548	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:59:00	EXECUTED	2020-03-27 03:00:05.853	{}
22549	SEND_OFFLINE_PAYMENT_TO_ORGANIZER	2020-03-27 03:00:00	EXECUTED	2020-03-27 03:00:05.855	{}
22550	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:00:00	EXECUTED	2020-03-27 03:01:05.875	{}
22551	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:01:00	EXECUTED	2020-03-27 03:02:05.89	{}
22552	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:02:00	EXECUTED	2020-03-27 03:03:05.9	{}
23030	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:23:00	EXECUTED	2020-03-27 10:24:16.557	{}
23031	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:24:00	EXECUTED	2020-03-27 10:25:16.569	{}
23032	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:25:00	EXECUTED	2020-03-27 10:26:16.578	{}
23033	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:26:00	EXECUTED	2020-03-27 10:27:16.588	{}
23034	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:27:00	EXECUTED	2020-03-27 10:28:16.61	{}
23035	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:28:00	EXECUTED	2020-03-27 10:29:16.619	{}
23036	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:29:00	EXECUTED	2020-03-27 10:30:16.641	{}
23360	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:28:00	EXECUTED	2020-03-27 15:29:22.819	{}
23168	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:31:00	EXECUTED	2020-03-27 12:32:19.195	{}
23490	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:28:00	EXECUTED	2020-03-27 17:29:25.189	{}
23169	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:32:00	EXECUTED	2020-03-27 12:33:19.208	{}
23491	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:29:00	EXECUTED	2020-03-27 17:30:25.204	{}
23492	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:30:00	EXECUTED	2020-03-27 17:31:25.212	{}
23493	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:31:00	EXECUTED	2020-03-27 17:32:25.221	{}
23494	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:32:00	EXECUTED	2020-03-27 17:33:25.232	{}
23495	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:33:00	EXECUTED	2020-03-27 17:34:25.247	{}
23496	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:34:00	EXECUTED	2020-03-27 17:35:25.258	{}
22133	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:36:00	EXECUTED	2020-03-26 20:36:57.778	{}
22134	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:37:00	EXECUTED	2020-03-26 20:37:57.795	{}
22135	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:38:00	EXECUTED	2020-03-26 20:38:57.813	{}
22136	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:39:00	EXECUTED	2020-03-26 20:39:57.823	{}
22137	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-26 20:40:00	EXECUTED	2020-03-26 20:40:57.828	{}
22138	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-26 20:40:00	EXECUTED	2020-03-26 20:40:57.921	{}
22139	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:40:00	EXECUTED	2020-03-26 20:40:57.932	{}
22140	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:41:00	EXECUTED	2020-03-26 20:41:57.958	{}
22141	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:42:00	EXECUTED	2020-03-26 20:42:57.97	{}
22142	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:43:00	EXECUTED	2020-03-26 20:43:57.982	{}
22143	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:44:00	EXECUTED	2020-03-26 20:44:57.993	{}
22144	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:45:00	EXECUTED	2020-03-26 20:45:58.005	{}
22145	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:46:00	EXECUTED	2020-03-26 20:46:58.031	{}
22259	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:32:00	EXECUTED	2020-03-26 22:33:00.151	{}
22364	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:09:00	EXECUTED	2020-03-27 00:10:02.373	{}
22365	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 00:10:00	EXECUTED	2020-03-27 00:11:02.378	{}
22366	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 00:10:00	EXECUTED	2020-03-27 00:11:02.401	{}
22367	CHECK_OFFLINE_PAYMENTS	2020-03-27 00:10:00	EXECUTED	2020-03-27 00:11:02.412	{}
22435	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:13:00	EXECUTED	2020-03-27 01:14:03.582	{}
22436	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:14:00	EXECUTED	2020-03-27 01:15:03.624	{}
22437	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:15:00	EXECUTED	2020-03-27 01:16:03.655	{}
22438	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:16:00	EXECUTED	2020-03-27 01:17:03.666	{}
22439	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:17:00	EXECUTED	2020-03-27 01:18:03.678	{}
22440	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:18:00	EXECUTED	2020-03-27 01:19:03.686	{}
22441	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:19:00	EXECUTED	2020-03-27 01:20:03.697	{}
22442	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:20:00	EXECUTED	2020-03-27 01:21:03.725	{}
22553	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:03:00	EXECUTED	2020-03-27 03:04:05.913	{}
22554	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:04:00	EXECUTED	2020-03-27 03:05:05.928	{}
22555	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:05:00	EXECUTED	2020-03-27 03:06:05.94	{}
22556	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:06:00	EXECUTED	2020-03-27 03:07:05.95	{}
22557	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:07:00	EXECUTED	2020-03-27 03:08:05.959	{}
22558	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:08:00	EXECUTED	2020-03-27 03:09:05.969	{}
22559	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:09:00	EXECUTED	2020-03-27 03:10:05.98	{}
22743	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:59:00	EXECUTED	2020-03-27 06:00:10.852	{}
22563	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:11:00	EXECUTED	2020-03-27 03:12:06.022	{}
22560	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 03:10:00	EXECUTED	2020-03-27 03:11:05.985	{}
22561	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 03:10:00	EXECUTED	2020-03-27 03:11:06.002	{}
22562	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:10:00	EXECUTED	2020-03-27 03:11:06.01	{}
22744	SEND_OFFLINE_PAYMENT_TO_ORGANIZER	2020-03-27 06:00:00	EXECUTED	2020-03-27 06:00:10.854	{}
22653	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:36:00	EXECUTED	2020-03-27 04:37:07.504	{}
22654	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:37:00	EXECUTED	2020-03-27 04:38:07.518	{}
22655	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:38:00	EXECUTED	2020-03-27 04:39:07.557	{}
22745	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:00:00	EXECUTED	2020-03-27 06:01:10.881	{}
22869	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:55:00	EXECUTED	2020-03-27 07:56:13.33	{}
22870	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:56:00	EXECUTED	2020-03-27 07:57:13.342	{}
22871	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:57:00	EXECUTED	2020-03-27 07:58:13.366	{}
22872	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:58:00	EXECUTED	2020-03-27 07:59:13.379	{}
22873	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:59:00	EXECUTED	2020-03-27 08:00:13.406	{}
22874	SEND_OFFLINE_PAYMENT_TO_ORGANIZER	2020-03-27 08:00:00	EXECUTED	2020-03-27 08:00:13.408	{}
22875	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:00:00	EXECUTED	2020-03-27 08:01:13.421	{}
22876	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:01:00	EXECUTED	2020-03-27 08:02:13.447	{}
22877	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:02:00	EXECUTED	2020-03-27 08:03:13.458	{}
22878	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:03:00	EXECUTED	2020-03-27 08:04:13.502	{}
22976	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:34:00	EXECUTED	2020-03-27 09:35:15.427	{}
23037	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:30:00	EXECUTED	2020-03-27 10:31:16.651	{}
23038	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:31:00	EXECUTED	2020-03-27 10:32:16.661	{}
23170	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:33:00	EXECUTED	2020-03-27 12:34:19.219	{}
23171	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:34:00	EXECUTED	2020-03-27 12:35:19.247	{}
23172	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:35:00	EXECUTED	2020-03-27 12:36:19.274	{}
23173	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:36:00	EXECUTED	2020-03-27 12:37:19.287	{}
23174	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:37:00	EXECUTED	2020-03-27 12:38:19.313	{}
23175	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:38:00	EXECUTED	2020-03-27 12:39:19.324	{}
23497	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:35:00	EXECUTED	2020-03-27 17:36:25.268	{}
23361	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:29:00	EXECUTED	2020-03-27 15:30:22.832	{}
23039	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:32:00	EXECUTED	2020-03-27 10:33:16.688	{}
23290	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:23:00	EXECUTED	2020-03-27 14:24:21.57	{}
23501	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:39:00	EXECUTED	2020-03-27 17:40:25.315	{}
22146	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:47:00	EXECUTED	2020-03-26 20:47:58.054	{}
23362	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:30:00	EXECUTED	2020-03-27 15:31:22.842	{}
23291	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:24:00	EXECUTED	2020-03-27 14:25:21.593	{}
23040	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:33:00	EXECUTED	2020-03-27 10:34:16.701	{}
23363	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:31:00	EXECUTED	2020-03-27 15:32:22.868	{}
22147	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:48:00	EXECUTED	2020-03-26 20:48:58.086	{}
22148	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:49:00	EXECUTED	2020-03-26 20:49:58.099	{}
23292	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:25:00	EXECUTED	2020-03-27 14:26:21.611	{}
22149	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:50:00	EXECUTED	2020-03-26 20:50:58.117	{}
22443	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:21:00	EXECUTED	2020-03-27 01:22:03.759	{}
22150	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:51:00	EXECUTED	2020-03-26 20:51:58.13	{}
22444	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:22:00	EXECUTED	2020-03-27 01:23:03.771	{}
22151	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:52:00	EXECUTED	2020-03-26 20:52:58.141	{}
22445	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:23:00	EXECUTED	2020-03-27 01:24:03.798	{}
22152	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:53:00	EXECUTED	2020-03-26 20:53:58.168	{}
22746	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:01:00	EXECUTED	2020-03-27 06:02:10.894	{}
22153	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:54:00	EXECUTED	2020-03-26 20:54:58.179	{}
22747	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:02:00	EXECUTED	2020-03-27 06:03:10.912	{}
22748	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:03:00	EXECUTED	2020-03-27 06:04:10.924	{}
22749	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:04:00	EXECUTED	2020-03-27 06:05:10.941	{}
22750	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:05:00	EXECUTED	2020-03-27 06:06:10.983	{}
22751	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:06:00	EXECUTED	2020-03-27 06:07:10.995	{}
22752	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:07:00	EXECUTED	2020-03-27 06:08:11.038	{}
22879	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:04:00	EXECUTED	2020-03-27 08:05:13.516	{}
22880	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:05:00	EXECUTED	2020-03-27 08:06:13.557	{}
22977	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:35:00	EXECUTED	2020-03-27 09:36:15.464	{}
22978	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:36:00	EXECUTED	2020-03-27 09:37:15.475	{}
22979	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:37:00	EXECUTED	2020-03-27 09:38:15.487	{}
22980	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:38:00	EXECUTED	2020-03-27 09:39:15.505	{}
23041	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:34:00	EXECUTED	2020-03-27 10:35:16.756	{}
23176	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:39:00	EXECUTED	2020-03-27 12:40:19.341	{}
23177	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 12:40:00	EXECUTED	2020-03-27 12:41:19.347	{}
23178	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 12:40:00	EXECUTED	2020-03-27 12:41:19.373	{}
23179	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:40:00	EXECUTED	2020-03-27 12:41:19.385	{}
23180	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:41:00	EXECUTED	2020-03-27 12:42:19.408	{}
23181	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:42:00	EXECUTED	2020-03-27 12:43:19.461	{}
23289	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:22:00	EXECUTED	2020-03-27 14:23:21.556	{}
23293	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:26:00	EXECUTED	2020-03-27 14:27:21.621	{}
23294	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:27:00	EXECUTED	2020-03-27 14:28:21.645	{}
23295	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:28:00	EXECUTED	2020-03-27 14:29:21.666	{}
23364	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:32:00	EXECUTED	2020-03-27 15:33:22.879	{}
23365	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:33:00	EXECUTED	2020-03-27 15:34:22.89	{}
23366	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:34:00	EXECUTED	2020-03-27 15:35:22.901	{}
23367	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:35:00	EXECUTED	2020-03-27 15:36:22.911	{}
23368	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:36:00	EXECUTED	2020-03-27 15:37:22.921	{}
23369	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:37:00	EXECUTED	2020-03-27 15:38:22.933	{}
23370	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:38:00	EXECUTED	2020-03-27 15:39:22.945	{}
23371	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:39:00	EXECUTED	2020-03-27 15:40:22.957	{}
23372	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 15:40:00	EXECUTED	2020-03-27 15:41:22.96	{}
23373	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 15:40:00	EXECUTED	2020-03-27 15:41:22.978	{}
23374	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:40:00	EXECUTED	2020-03-27 15:41:22.994	{}
23498	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:36:00	EXECUTED	2020-03-27 17:37:25.28	{}
23499	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:37:00	EXECUTED	2020-03-27 17:38:25.29	{}
23500	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:38:00	EXECUTED	2020-03-27 17:39:25.299	{}
22154	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:55:00	EXECUTED	2020-03-26 20:55:58.193	{}
22155	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:56:00	EXECUTED	2020-03-26 20:56:58.204	{}
23042	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:35:00	EXECUTED	2020-03-27 10:36:16.77	{}
22753	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:08:00	EXECUTED	2020-03-27 06:09:11.057	{}
23296	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:29:00	EXECUTED	2020-03-27 14:30:21.678	{}
22656	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:39:00	EXECUTED	2020-03-27 04:40:07.577	{}
23043	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:36:00	EXECUTED	2020-03-27 10:37:16.782	{}
22982	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 09:40:00	EXECUTED	2020-03-27 09:41:15.525	{}
22881	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:06:00	EXECUTED	2020-03-27 08:07:13.573	{}
21958	CHECK_OFFLINE_PAYMENTS	2020-03-26 17:54:00	EXECUTED	2020-03-26 17:54:54.524	{}
22446	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:24:00	EXECUTED	2020-03-27 01:25:03.827	{}
21959	CHECK_OFFLINE_PAYMENTS	2020-03-26 17:55:00	EXECUTED	2020-03-26 17:55:54.534	{}
22983	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 09:40:00	EXECUTED	2020-03-27 09:41:15.572	{}
21960	CHECK_OFFLINE_PAYMENTS	2020-03-26 17:56:00	EXECUTED	2020-03-26 17:56:54.545	{}
21961	CHECK_OFFLINE_PAYMENTS	2020-03-26 17:57:00	EXECUTED	2020-03-26 17:57:54.586	{}
21962	CHECK_OFFLINE_PAYMENTS	2020-03-26 17:58:00	EXECUTED	2020-03-26 17:58:54.597	{}
21963	CHECK_OFFLINE_PAYMENTS	2020-03-26 17:59:00	EXECUTED	2020-03-26 17:59:54.607	{}
21964	SEND_OFFLINE_PAYMENT_TO_ORGANIZER	2020-03-26 18:00:00	EXECUTED	2020-03-26 18:00:54.612	{}
21965	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:00:00	EXECUTED	2020-03-26 18:00:54.621	{}
22157	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:58:00	EXECUTED	2020-03-26 20:58:58.225	{}
21966	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:01:00	EXECUTED	2020-03-26 18:01:54.631	{}
22447	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:25:00	EXECUTED	2020-03-27 01:26:03.845	{}
21967	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:02:00	EXECUTED	2020-03-26 18:02:54.671	{}
21968	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:03:00	EXECUTED	2020-03-26 18:03:54.681	{}
22158	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:59:00	EXECUTED	2020-03-26 20:59:58.235	{}
22448	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:26:00	EXECUTED	2020-03-27 01:27:03.859	{}
21969	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:04:00	EXECUTED	2020-03-26 18:04:54.691	{}
21970	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:05:00	EXECUTED	2020-03-26 18:05:54.701	{}
22449	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:27:00	EXECUTED	2020-03-27 01:28:03.876	{}
22159	SEND_OFFLINE_PAYMENT_TO_ORGANIZER	2020-03-26 21:00:00	EXECUTED	2020-03-26 21:00:58.237	{}
22450	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:28:00	EXECUTED	2020-03-27 01:29:03.889	{}
22160	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:00:00	EXECUTED	2020-03-26 21:00:58.244	{}
22161	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:01:00	EXECUTED	2020-03-26 21:01:58.253	{}
22564	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:12:00	EXECUTED	2020-03-27 03:13:06.039	{}
22262	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:35:00	EXECUTED	2020-03-26 22:36:00.221	{}
22565	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:13:00	EXECUTED	2020-03-27 03:14:06.052	{}
22566	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:14:00	EXECUTED	2020-03-27 03:15:06.063	{}
22567	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:15:00	EXECUTED	2020-03-27 03:16:06.076	{}
22754	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:09:00	EXECUTED	2020-03-27 06:10:11.087	{}
22568	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:16:00	EXECUTED	2020-03-27 03:17:06.086	{}
22882	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:07:00	EXECUTED	2020-03-27 08:08:13.585	{}
22569	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:17:00	EXECUTED	2020-03-27 03:18:06.097	{}
22883	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:08:00	EXECUTED	2020-03-27 08:09:13.597	{}
22884	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:09:00	EXECUTED	2020-03-27 08:10:13.645	{}
22570	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:18:00	EXECUTED	2020-03-27 03:19:06.115	{}
22981	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:39:00	EXECUTED	2020-03-27 09:40:15.518	{}
21971	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:06:00	EXECUTED	2020-03-26 18:06:54.712	{}
22156	CHECK_OFFLINE_PAYMENTS	2020-03-26 20:57:00	EXECUTED	2020-03-26 20:57:58.214	{}
23044	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:37:00	EXECUTED	2020-03-27 10:38:16.811	{}
22260	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:33:00	EXECUTED	2020-03-26 22:34:00.164	{}
22261	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:34:00	EXECUTED	2020-03-26 22:35:00.21	{}
22263	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:36:00	EXECUTED	2020-03-26 22:37:00.249	{}
22264	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:37:00	EXECUTED	2020-03-26 22:38:00.261	{}
22265	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:38:00	EXECUTED	2020-03-26 22:39:00.273	{}
22984	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:40:00	EXECUTED	2020-03-27 09:41:15.593	{}
22657	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 04:40:00	EXECUTED	2020-03-27 04:41:07.591	{}
22985	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:41:00	EXECUTED	2020-03-27 09:42:15.609	{}
22658	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 04:40:00	EXECUTED	2020-03-27 04:41:07.635	{}
22659	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:40:00	EXECUTED	2020-03-27 04:41:07.657	{}
22986	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:42:00	EXECUTED	2020-03-27 09:43:15.637	{}
23182	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:43:00	EXECUTED	2020-03-27 12:44:19.508	{}
23297	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:30:00	EXECUTED	2020-03-27 14:31:21.728	{}
23298	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:31:00	EXECUTED	2020-03-27 14:32:21.745	{}
23299	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:32:00	EXECUTED	2020-03-27 14:33:21.759	{}
23300	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:33:00	EXECUTED	2020-03-27 14:34:21.771	{}
23301	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:34:00	EXECUTED	2020-03-27 14:35:21.781	{}
23302	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:35:00	EXECUTED	2020-03-27 14:36:21.793	{}
23303	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:36:00	EXECUTED	2020-03-27 14:37:21.82	{}
22266	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:39:00	EXECUTED	2020-03-26 22:40:00.295	{}
22267	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-26 22:40:00	EXECUTED	2020-03-26 22:41:00.3	{}
22268	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-26 22:40:00	EXECUTED	2020-03-26 22:41:00.316	{}
22269	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:40:00	EXECUTED	2020-03-26 22:41:00.324	{}
22270	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:41:00	EXECUTED	2020-03-26 22:42:00.346	{}
22271	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:42:00	EXECUTED	2020-03-26 22:43:00.355	{}
22272	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:43:00	EXECUTED	2020-03-26 22:44:00.377	{}
22885	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 08:10:00	EXECUTED	2020-03-27 08:11:13.65	{}
23306	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:39:00	EXECUTED	2020-03-27 14:40:21.884	{}
23304	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:37:00	EXECUTED	2020-03-27 14:38:21.833	{}
22162	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:02:00	EXECUTED	2020-03-26 21:02:58.271	{}
22886	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 08:10:00	EXECUTED	2020-03-27 08:11:13.678	{}
23505	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:41:00	EXECUTED	2020-03-27 17:42:25.354	{}
23184	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:45:00	EXECUTED	2020-03-27 12:46:19.554	{}
23045	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:38:00	EXECUTED	2020-03-27 10:39:16.827	{}
23375	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:41:00	EXECUTED	2020-03-27 15:42:23.02	{}
22274	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:45:00	EXECUTED	2020-03-26 22:46:00.408	{}
22887	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:10:00	EXECUTED	2020-03-27 08:11:13.696	{}
22273	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:44:00	EXECUTED	2020-03-26 22:45:00.39	{}
22451	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:29:00	EXECUTED	2020-03-27 01:30:03.909	{}
22452	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:30:00	EXECUTED	2020-03-27 01:31:03.932	{}
22453	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:31:00	EXECUTED	2020-03-27 01:32:03.962	{}
22454	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:32:00	EXECUTED	2020-03-27 01:33:03.991	{}
22571	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:19:00	EXECUTED	2020-03-27 03:20:06.133	{}
22660	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:41:00	EXECUTED	2020-03-27 04:42:07.67	{}
22755	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 06:10:00	EXECUTED	2020-03-27 06:11:11.093	{}
22756	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 06:10:00	EXECUTED	2020-03-27 06:11:11.111	{}
22757	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:10:00	EXECUTED	2020-03-27 06:11:11.126	{}
22758	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:11:00	EXECUTED	2020-03-27 06:12:11.15	{}
22759	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:12:00	EXECUTED	2020-03-27 06:13:11.16	{}
22760	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:13:00	EXECUTED	2020-03-27 06:14:11.172	{}
22761	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:14:00	EXECUTED	2020-03-27 06:15:11.205	{}
22762	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:15:00	EXECUTED	2020-03-27 06:16:11.218	{}
22888	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:11:00	EXECUTED	2020-03-27 08:12:13.708	{}
22889	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:12:00	EXECUTED	2020-03-27 08:13:13.726	{}
22890	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:13:00	EXECUTED	2020-03-27 08:14:13.738	{}
23046	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:39:00	EXECUTED	2020-03-27 10:40:16.841	{}
22987	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:43:00	EXECUTED	2020-03-27 09:44:15.655	{}
22988	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:44:00	EXECUTED	2020-03-27 09:45:15.667	{}
23047	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 10:40:00	EXECUTED	2020-03-27 10:41:16.85	{}
23048	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 10:40:00	EXECUTED	2020-03-27 10:41:16.887	{}
23049	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:40:00	EXECUTED	2020-03-27 10:41:16.902	{}
23050	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:41:00	EXECUTED	2020-03-27 10:42:16.924	{}
23376	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:42:00	EXECUTED	2020-03-27 15:43:23.055	{}
23305	CHECK_OFFLINE_PAYMENTS	2020-03-27 14:38:00	EXECUTED	2020-03-27 14:39:21.863	{}
23377	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:43:00	EXECUTED	2020-03-27 15:44:23.085	{}
23378	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:44:00	EXECUTED	2020-03-27 15:45:23.104	{}
23502	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 17:40:00	EXECUTED	2020-03-27 17:41:25.319	{}
23503	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 17:40:00	EXECUTED	2020-03-27 17:41:25.335	{}
23504	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:40:00	EXECUTED	2020-03-27 17:41:25.345	{}
23506	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:42:00	EXECUTED	2020-03-27 17:43:25.365	{}
23507	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:43:00	EXECUTED	2020-03-27 17:44:25.377	{}
23508	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:44:00	EXECUTED	2020-03-27 17:45:25.393	{}
23509	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:45:00	EXECUTED	2020-03-27 17:46:25.402	{}
23510	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:46:00	EXECUTED	2020-03-27 17:47:25.422	{}
23511	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:47:00	EXECUTED	2020-03-27 17:48:25.477	{}
21972	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:07:00	EXECUTED	2020-03-26 18:07:54.745	{}
21973	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:08:00	EXECUTED	2020-03-26 18:08:54.758	{}
21974	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:09:00	EXECUTED	2020-03-26 18:09:54.771	{}
23183	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:44:00	EXECUTED	2020-03-27 12:45:19.54	{}
21975	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-26 18:10:00	EXECUTED	2020-03-26 18:10:54.776	{}
23185	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:46:00	EXECUTED	2020-03-27 12:47:19.582	{}
23186	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:47:00	EXECUTED	2020-03-27 12:48:19.634	{}
21976	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-26 18:10:00	EXECUTED	2020-03-26 18:10:54.79	{}
21977	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:10:00	EXECUTED	2020-03-26 18:10:54.799	{}
21978	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:11:00	EXECUTED	2020-03-26 18:11:54.809	{}
22163	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:03:00	EXECUTED	2020-03-26 21:03:58.294	{}
23513	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:49:00	EXECUTED	2020-03-27 17:50:25.501	{}
21979	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:12:00	EXECUTED	2020-03-26 18:12:54.819	{}
22164	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:04:00	EXECUTED	2020-03-26 21:04:58.308	{}
21980	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:13:00	EXECUTED	2020-03-26 18:13:54.846	{}
22275	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:46:00	EXECUTED	2020-03-26 22:47:00.421	{}
22455	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:33:00	EXECUTED	2020-03-27 01:34:04.002	{}
22276	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:47:00	EXECUTED	2020-03-26 22:48:00.432	{}
22277	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:48:00	EXECUTED	2020-03-26 22:49:00.459	{}
22278	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:49:00	EXECUTED	2020-03-26 22:50:00.474	{}
22279	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:50:00	EXECUTED	2020-03-26 22:51:00.493	{}
22280	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:51:00	EXECUTED	2020-03-26 22:52:00.504	{}
22456	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:34:00	EXECUTED	2020-03-27 01:35:04.03	{}
22457	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:35:00	EXECUTED	2020-03-27 01:36:04.043	{}
22458	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:36:00	EXECUTED	2020-03-27 01:37:04.097	{}
22459	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:37:00	EXECUTED	2020-03-27 01:38:04.116	{}
22460	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:38:00	EXECUTED	2020-03-27 01:39:04.164	{}
22461	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:39:00	EXECUTED	2020-03-27 01:40:04.181	{}
22462	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 01:40:00	EXECUTED	2020-03-27 01:41:04.185	{}
22463	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 01:40:00	EXECUTED	2020-03-27 01:41:04.203	{}
22464	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:40:00	EXECUTED	2020-03-27 01:41:04.211	{}
22465	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:41:00	EXECUTED	2020-03-27 01:42:04.221	{}
22466	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:42:00	EXECUTED	2020-03-27 01:43:04.249	{}
22572	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:20:00	EXECUTED	2020-03-27 03:21:06.145	{}
22573	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:21:00	EXECUTED	2020-03-27 03:22:06.155	{}
22574	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:22:00	EXECUTED	2020-03-27 03:23:06.164	{}
22575	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:23:00	EXECUTED	2020-03-27 03:24:06.175	{}
22576	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:24:00	EXECUTED	2020-03-27 03:25:06.186	{}
22577	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:25:00	EXECUTED	2020-03-27 03:26:06.198	{}
22578	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:26:00	EXECUTED	2020-03-27 03:27:06.208	{}
22579	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:27:00	EXECUTED	2020-03-27 03:28:06.218	{}
22580	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:28:00	EXECUTED	2020-03-27 03:29:06.284	{}
22763	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:16:00	EXECUTED	2020-03-27 06:17:11.236	{}
22581	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:29:00	EXECUTED	2020-03-27 03:30:06.294	{}
22891	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:14:00	EXECUTED	2020-03-27 08:15:13.768	{}
22892	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:15:00	EXECUTED	2020-03-27 08:16:13.787	{}
22989	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:45:00	EXECUTED	2020-03-27 09:46:15.685	{}
22990	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:46:00	EXECUTED	2020-03-27 09:47:15.697	{}
22991	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:47:00	EXECUTED	2020-03-27 09:48:15.72	{}
23051	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:42:00	EXECUTED	2020-03-27 10:43:16.941	{}
23052	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:43:00	EXECUTED	2020-03-27 10:44:16.953	{}
23187	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:48:00	EXECUTED	2020-03-27 12:49:19.646	{}
23188	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:49:00	EXECUTED	2020-03-27 12:50:19.661	{}
23189	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:50:00	EXECUTED	2020-03-27 12:51:19.676	{}
23190	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:51:00	EXECUTED	2020-03-27 12:52:19.686	{}
23191	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:52:00	EXECUTED	2020-03-27 12:53:19.698	{}
23192	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:53:00	EXECUTED	2020-03-27 12:54:19.71	{}
23193	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:54:00	EXECUTED	2020-03-27 12:55:19.722	{}
23194	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:55:00	EXECUTED	2020-03-27 12:56:19.748	{}
23195	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:56:00	EXECUTED	2020-03-27 12:57:19.76	{}
23196	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:57:00	EXECUTED	2020-03-27 12:58:19.773	{}
23197	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:58:00	EXECUTED	2020-03-27 12:59:19.784	{}
23198	CHECK_OFFLINE_PAYMENTS	2020-03-27 12:59:00	EXECUTED	2020-03-27 13:00:19.83	{}
23199	SEND_OFFLINE_PAYMENT_TO_ORGANIZER	2020-03-27 13:00:00	EXECUTED	2020-03-27 13:00:19.833	{}
23379	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:45:00	EXECUTED	2020-03-27 15:46:23.118	{}
23380	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:46:00	EXECUTED	2020-03-27 15:47:23.129	{}
23381	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:47:00	EXECUTED	2020-03-27 15:48:23.142	{}
23382	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:48:00	EXECUTED	2020-03-27 15:49:23.154	{}
23383	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:49:00	EXECUTED	2020-03-27 15:50:23.163	{}
23384	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:50:00	EXECUTED	2020-03-27 15:51:23.173	{}
23385	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:51:00	EXECUTED	2020-03-27 15:52:23.186	{}
23386	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:52:00	EXECUTED	2020-03-27 15:53:23.195	{}
23387	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:53:00	EXECUTED	2020-03-27 15:54:23.207	{}
23388	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:54:00	EXECUTED	2020-03-27 15:55:23.22	{}
23389	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:55:00	EXECUTED	2020-03-27 15:56:23.23	{}
23390	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:56:00	EXECUTED	2020-03-27 15:57:23.238	{}
23391	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:57:00	EXECUTED	2020-03-27 15:58:23.278	{}
23512	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:48:00	EXECUTED	2020-03-27 17:49:25.489	{}
23514	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:50:00	EXECUTED	2020-03-27 17:51:25.51	{}
22582	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:30:00	EXECUTED	2020-03-27 03:31:06.303	{}
23515	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:51:00	EXECUTED	2020-03-27 17:52:25.522	{}
23516	CHECK_OFFLINE_PAYMENTS	2020-03-27 17:52:00	SCHEDULED	\N	{}
23403	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:08:00	EXECUTED	2020-03-27 16:09:23.457	{}
22165	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:05:00	EXECUTED	2020-03-26 21:05:58.363	{}
23392	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:58:00	EXECUTED	2020-03-27 15:59:23.308	{}
23404	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:09:00	EXECUTED	2020-03-27 16:10:23.472	{}
22166	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:06:00	EXECUTED	2020-03-26 21:06:58.375	{}
22167	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:07:00	EXECUTED	2020-03-26 21:07:58.388	{}
22467	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:43:00	EXECUTED	2020-03-27 01:44:04.283	{}
22168	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:08:00	EXECUTED	2020-03-26 21:08:58.398	{}
22468	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:44:00	EXECUTED	2020-03-27 01:45:04.295	{}
22469	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:45:00	EXECUTED	2020-03-27 01:46:04.308	{}
22470	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:46:00	EXECUTED	2020-03-27 01:47:04.32	{}
22471	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:47:00	EXECUTED	2020-03-27 01:48:04.333	{}
22472	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:48:00	EXECUTED	2020-03-27 01:49:04.345	{}
22473	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:49:00	EXECUTED	2020-03-27 01:50:04.373	{}
22583	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:31:00	EXECUTED	2020-03-27 03:32:06.315	{}
22584	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:32:00	EXECUTED	2020-03-27 03:33:06.324	{}
22764	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:17:00	EXECUTED	2020-03-27 06:18:11.249	{}
22585	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:33:00	EXECUTED	2020-03-27 03:34:06.334	{}
22586	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:34:00	EXECUTED	2020-03-27 03:35:06.345	{}
22587	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:35:00	EXECUTED	2020-03-27 03:36:06.354	{}
22588	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:36:00	EXECUTED	2020-03-27 03:37:06.367	{}
22661	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:42:00	EXECUTED	2020-03-27 04:43:07.745	{}
22765	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:18:00	EXECUTED	2020-03-27 06:19:11.261	{}
22662	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:43:00	EXECUTED	2020-03-27 04:44:07.76	{}
22766	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:19:00	EXECUTED	2020-03-27 06:20:11.273	{}
22663	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:44:00	EXECUTED	2020-03-27 04:45:07.775	{}
22767	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:20:00	EXECUTED	2020-03-27 06:21:11.29	{}
23053	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:44:00	EXECUTED	2020-03-27 10:45:17	{}
22768	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:21:00	EXECUTED	2020-03-27 06:22:11.304	{}
22664	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:45:00	EXECUTED	2020-03-27 04:46:07.794	{}
22769	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:22:00	EXECUTED	2020-03-27 06:23:11.331	{}
23054	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:45:00	EXECUTED	2020-03-27 10:46:17.02	{}
22665	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:46:00	EXECUTED	2020-03-27 04:47:07.836	{}
22770	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:23:00	EXECUTED	2020-03-27 06:24:11.358	{}
23055	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:46:00	EXECUTED	2020-03-27 10:47:17.036	{}
22771	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:24:00	EXECUTED	2020-03-27 06:25:11.372	{}
22772	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:25:00	EXECUTED	2020-03-27 06:26:11.383	{}
22773	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:26:00	EXECUTED	2020-03-27 06:27:11.398	{}
23056	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:47:00	EXECUTED	2020-03-27 10:48:17.048	{}
22774	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:27:00	EXECUTED	2020-03-27 06:28:11.409	{}
23057	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:48:00	EXECUTED	2020-03-27 10:49:17.057	{}
22775	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:28:00	EXECUTED	2020-03-27 06:29:11.424	{}
23058	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:49:00	EXECUTED	2020-03-27 10:50:17.07	{}
23393	CHECK_OFFLINE_PAYMENTS	2020-03-27 15:59:00	EXECUTED	2020-03-27 16:00:23.329	{}
23059	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:50:00	EXECUTED	2020-03-27 10:51:17.097	{}
23394	SEND_OFFLINE_PAYMENT_TO_ORGANIZER	2020-03-27 16:00:00	EXECUTED	2020-03-27 16:00:23.332	{}
23060	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:51:00	EXECUTED	2020-03-27 10:52:17.109	{}
23395	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:00:00	EXECUTED	2020-03-27 16:01:23.343	{}
23061	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:52:00	EXECUTED	2020-03-27 10:53:17.138	{}
23396	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:01:00	EXECUTED	2020-03-27 16:02:23.372	{}
23062	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:53:00	EXECUTED	2020-03-27 10:54:17.151	{}
23397	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:02:00	EXECUTED	2020-03-27 16:03:23.392	{}
23398	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:03:00	EXECUTED	2020-03-27 16:04:23.402	{}
23399	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:04:00	EXECUTED	2020-03-27 16:05:23.416	{}
23400	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:05:00	EXECUTED	2020-03-27 16:06:23.426	{}
23401	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:06:00	EXECUTED	2020-03-27 16:07:23.436	{}
23402	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:07:00	EXECUTED	2020-03-27 16:08:23.447	{}
21981	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:14:00	EXECUTED	2020-03-26 18:14:54.864	{}
21982	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:15:00	EXECUTED	2020-03-26 18:15:54.874	{}
22776	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:29:00	EXECUTED	2020-03-27 06:30:11.436	{}
22777	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:30:00	EXECUTED	2020-03-27 06:31:11.462	{}
22778	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:31:00	EXECUTED	2020-03-27 06:32:11.474	{}
22779	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:32:00	EXECUTED	2020-03-27 06:33:11.5	{}
22780	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:33:00	EXECUTED	2020-03-27 06:34:11.518	{}
22781	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:34:00	EXECUTED	2020-03-27 06:35:11.546	{}
22893	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:16:00	EXECUTED	2020-03-27 08:17:13.801	{}
22169	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:09:00	EXECUTED	2020-03-26 21:09:58.426	{}
21983	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:16:00	EXECUTED	2020-03-26 18:16:54.893	{}
22170	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-26 21:10:00	EXECUTED	2020-03-26 21:10:58.433	{}
22171	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-26 21:10:00	EXECUTED	2020-03-26 21:10:58.462	{}
21984	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:17:00	EXECUTED	2020-03-26 18:17:54.904	{}
22172	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:10:00	EXECUTED	2020-03-26 21:10:58.477	{}
22173	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:11:00	EXECUTED	2020-03-26 21:11:58.496	{}
21987	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:20:00	EXECUTED	2020-03-26 18:20:54.938	{}
22283	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:54:00	EXECUTED	2020-03-26 22:55:00.61	{}
22174	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:12:00	EXECUTED	2020-03-26 21:12:58.516	{}
22474	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:50:00	EXECUTED	2020-03-27 01:51:04.391	{}
22175	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:13:00	EXECUTED	2020-03-26 21:13:58.584	{}
22284	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:55:00	EXECUTED	2020-03-26 22:56:00.621	{}
22281	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:52:00	EXECUTED	2020-03-26 22:53:00.55	{}
22589	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:37:00	EXECUTED	2020-03-27 03:38:06.383	{}
22282	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:53:00	EXECUTED	2020-03-26 22:54:00.582	{}
22285	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:56:00	EXECUTED	2020-03-26 22:57:00.649	{}
22590	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:38:00	EXECUTED	2020-03-27 03:39:06.394	{}
22286	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:57:00	EXECUTED	2020-03-26 22:58:00.66	{}
22287	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:58:00	EXECUTED	2020-03-26 22:59:00.674	{}
22288	CHECK_OFFLINE_PAYMENTS	2020-03-26 22:59:00	EXECUTED	2020-03-26 23:00:00.699	{}
22666	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:47:00	EXECUTED	2020-03-27 04:48:09.127	{}
22289	SEND_OFFLINE_PAYMENT_TO_ORGANIZER	2020-03-26 23:00:00	EXECUTED	2020-03-26 23:00:00.703	{}
22290	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:00:00	EXECUTED	2020-03-26 23:01:00.714	{}
22782	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:35:00	EXECUTED	2020-03-27 06:36:11.595	{}
22783	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:36:00	EXECUTED	2020-03-27 06:37:11.631	{}
22894	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:17:00	EXECUTED	2020-03-27 08:18:13.812	{}
22895	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:18:00	EXECUTED	2020-03-27 08:19:13.839	{}
22896	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:19:00	EXECUTED	2020-03-27 08:20:13.85	{}
22897	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:20:00	EXECUTED	2020-03-27 08:21:13.875	{}
22898	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:21:00	EXECUTED	2020-03-27 08:22:13.886	{}
22899	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:22:00	EXECUTED	2020-03-27 08:23:13.908	{}
22900	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:23:00	EXECUTED	2020-03-27 08:24:13.918	{}
22901	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:24:00	EXECUTED	2020-03-27 08:25:13.946	{}
22902	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:25:00	EXECUTED	2020-03-27 08:26:13.971	{}
23063	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:54:00	EXECUTED	2020-03-27 10:55:17.164	{}
22903	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:26:00	EXECUTED	2020-03-27 08:27:14	{}
22904	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:27:00	EXECUTED	2020-03-27 08:28:14.011	{}
23064	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:55:00	EXECUTED	2020-03-27 10:56:17.175	{}
22905	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:28:00	EXECUTED	2020-03-27 08:29:14.023	{}
22906	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:29:00	EXECUTED	2020-03-27 08:30:14.05	{}
23065	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:56:00	EXECUTED	2020-03-27 10:57:17.185	{}
22907	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:30:00	EXECUTED	2020-03-27 08:31:14.062	{}
22908	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:31:00	EXECUTED	2020-03-27 08:32:14.073	{}
22909	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:32:00	EXECUTED	2020-03-27 08:33:14.091	{}
23066	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:57:00	EXECUTED	2020-03-27 10:58:17.197	{}
23067	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:58:00	EXECUTED	2020-03-27 10:59:17.208	{}
23068	CHECK_OFFLINE_PAYMENTS	2020-03-27 10:59:00	EXECUTED	2020-03-27 11:00:17.219	{}
23069	SEND_OFFLINE_PAYMENT_TO_ORGANIZER	2020-03-27 11:00:00	EXECUTED	2020-03-27 11:00:17.223	{}
23070	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:00:00	EXECUTED	2020-03-27 11:01:17.234	{}
23071	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:01:00	EXECUTED	2020-03-27 11:02:17.246	{}
23200	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:00:00	EXECUTED	2020-03-27 13:01:19.867	{}
23201	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:01:00	EXECUTED	2020-03-27 13:02:19.881	{}
23202	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:02:00	EXECUTED	2020-03-27 13:03:19.901	{}
23203	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:03:00	EXECUTED	2020-03-27 13:04:19.912	{}
23204	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:04:00	EXECUTED	2020-03-27 13:05:19.938	{}
23205	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:05:00	EXECUTED	2020-03-27 13:06:19.965	{}
23206	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:06:00	EXECUTED	2020-03-27 13:07:19.989	{}
23405	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 16:10:00	EXECUTED	2020-03-27 16:11:23.477	{}
23406	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 16:10:00	EXECUTED	2020-03-27 16:11:23.495	{}
23407	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:10:00	EXECUTED	2020-03-27 16:11:23.512	{}
21985	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:18:00	EXECUTED	2020-03-26 18:18:54.916	{}
21986	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:19:00	EXECUTED	2020-03-26 18:19:54.928	{}
23207	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:07:00	EXECUTED	2020-03-27 13:08:20	{}
23208	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:08:00	EXECUTED	2020-03-27 13:09:20.009	{}
23209	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:09:00	EXECUTED	2020-03-27 13:10:20.026	{}
23210	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 13:10:00	EXECUTED	2020-03-27 13:11:20.032	{}
23211	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 13:10:00	EXECUTED	2020-03-27 13:11:20.074	{}
23212	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:10:00	EXECUTED	2020-03-27 13:11:20.091	{}
23213	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:11:00	EXECUTED	2020-03-27 13:12:20.102	{}
23214	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:12:00	EXECUTED	2020-03-27 13:13:20.149	{}
23408	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:11:00	EXECUTED	2020-03-27 16:12:23.524	{}
21988	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:21:00	EXECUTED	2020-03-26 18:21:54.995	{}
22176	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:14:00	EXECUTED	2020-03-26 21:14:58.613	{}
21989	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:22:00	EXECUTED	2020-03-26 18:22:55.028	{}
21990	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:23:00	EXECUTED	2020-03-26 18:23:55.04	{}
22177	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:15:00	EXECUTED	2020-03-26 21:15:58.648	{}
21991	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:24:00	EXECUTED	2020-03-26 18:24:55.068	{}
22291	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:01:00	EXECUTED	2020-03-26 23:02:00.738	{}
22475	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:51:00	EXECUTED	2020-03-27 01:52:04.408	{}
22476	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:52:00	EXECUTED	2020-03-27 01:53:04.418	{}
22477	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:53:00	EXECUTED	2020-03-27 01:54:04.435	{}
22478	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:54:00	EXECUTED	2020-03-27 01:55:04.445	{}
22479	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:55:00	EXECUTED	2020-03-27 01:56:04.474	{}
22480	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:56:00	EXECUTED	2020-03-27 01:57:04.501	{}
22481	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:57:00	EXECUTED	2020-03-27 01:58:04.527	{}
22482	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:58:00	EXECUTED	2020-03-27 01:59:04.548	{}
22784	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:37:00	EXECUTED	2020-03-27 06:38:11.667	{}
22591	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:39:00	EXECUTED	2020-03-27 03:40:06.412	{}
22667	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:48:00	EXECUTED	2020-03-27 04:49:09.147	{}
22785	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:38:00	EXECUTED	2020-03-27 06:39:11.682	{}
23072	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:02:00	EXECUTED	2020-03-27 11:03:17.264	{}
22668	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:49:00	EXECUTED	2020-03-27 04:50:09.166	{}
22786	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:39:00	EXECUTED	2020-03-27 06:40:11.699	{}
22910	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:33:00	EXECUTED	2020-03-27 08:34:14.145	{}
22911	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:34:00	EXECUTED	2020-03-27 08:35:14.158	{}
22669	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:50:00	EXECUTED	2020-03-27 04:51:09.221	{}
23073	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:03:00	EXECUTED	2020-03-27 11:04:17.297	{}
22670	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:51:00	EXECUTED	2020-03-27 04:52:09.28	{}
23215	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:13:00	EXECUTED	2020-03-27 13:14:20.168	{}
23216	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:14:00	EXECUTED	2020-03-27 13:15:20.18	{}
22671	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:52:00	EXECUTED	2020-03-27 04:53:09.291	{}
23217	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:15:00	EXECUTED	2020-03-27 13:16:20.194	{}
22672	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:53:00	EXECUTED	2020-03-27 04:54:09.32	{}
23218	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:16:00	EXECUTED	2020-03-27 13:17:20.211	{}
23409	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:12:00	EXECUTED	2020-03-27 16:13:23.536	{}
23410	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:13:00	EXECUTED	2020-03-27 16:14:23.555	{}
22673	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:54:00	EXECUTED	2020-03-27 04:55:09.333	{}
22674	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:55:00	EXECUTED	2020-03-27 04:56:09.378	{}
22675	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:56:00	EXECUTED	2020-03-27 04:57:09.436	{}
22676	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:57:00	EXECUTED	2020-03-27 04:58:09.48	{}
21992	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:25:00	EXECUTED	2020-03-26 18:25:55.154	{}
21993	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:26:00	EXECUTED	2020-03-26 18:26:55.165	{}
21994	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:27:00	EXECUTED	2020-03-26 18:27:55.176	{}
21995	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:28:00	EXECUTED	2020-03-26 18:28:55.185	{}
21996	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:29:00	EXECUTED	2020-03-26 18:29:55.195	{}
21997	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:30:00	EXECUTED	2020-03-26 18:30:55.223	{}
21998	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:31:00	EXECUTED	2020-03-26 18:31:55.233	{}
21999	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:32:00	EXECUTED	2020-03-26 18:32:55.245	{}
22000	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:33:00	EXECUTED	2020-03-26 18:33:55.264	{}
22677	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:58:00	EXECUTED	2020-03-27 04:59:09.546	{}
22001	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:34:00	EXECUTED	2020-03-26 18:34:55.277	{}
22678	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:59:00	EXECUTED	2020-03-27 05:00:09.574	{}
22679	SEND_OFFLINE_PAYMENT_TO_ORGANIZER	2020-03-27 05:00:00	EXECUTED	2020-03-27 05:00:09.576	{}
22680	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:00:00	EXECUTED	2020-03-27 05:01:09.586	{}
22681	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:01:00	EXECUTED	2020-03-27 05:02:09.598	{}
22682	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:02:00	EXECUTED	2020-03-27 05:03:09.62	{}
22683	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:03:00	EXECUTED	2020-03-27 05:04:09.63	{}
22684	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:04:00	EXECUTED	2020-03-27 05:05:09.65	{}
22685	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:05:00	EXECUTED	2020-03-27 05:06:09.662	{}
22178	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:16:00	EXECUTED	2020-03-26 21:16:58.704	{}
23420	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:23:00	EXECUTED	2020-03-27 16:24:23.716	{}
22179	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:17:00	EXECUTED	2020-03-26 21:17:58.735	{}
22180	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:18:00	EXECUTED	2020-03-26 21:18:58.747	{}
22181	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:19:00	EXECUTED	2020-03-26 21:19:58.757	{}
22182	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:20:00	EXECUTED	2020-03-26 21:20:58.791	{}
22183	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:21:00	EXECUTED	2020-03-26 21:21:58.804	{}
22184	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:22:00	EXECUTED	2020-03-26 21:22:58.818	{}
22185	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:23:00	EXECUTED	2020-03-26 21:23:58.836	{}
22483	CHECK_OFFLINE_PAYMENTS	2020-03-27 01:59:00	EXECUTED	2020-03-27 02:00:04.576	{}
22484	SEND_OFFLINE_PAYMENT_TO_ORGANIZER	2020-03-27 02:00:00	EXECUTED	2020-03-27 02:00:04.579	{}
22485	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:00:00	EXECUTED	2020-03-27 02:01:04.593	{}
22486	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:01:00	EXECUTED	2020-03-27 02:02:04.606	{}
22487	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:02:00	EXECUTED	2020-03-27 02:03:04.617	{}
22488	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:03:00	EXECUTED	2020-03-27 02:04:04.645	{}
22489	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:04:00	EXECUTED	2020-03-27 02:05:04.665	{}
22490	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:05:00	EXECUTED	2020-03-27 02:06:04.677	{}
22491	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:06:00	EXECUTED	2020-03-27 02:07:04.69	{}
22492	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:07:00	EXECUTED	2020-03-27 02:08:04.702	{}
22493	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:08:00	EXECUTED	2020-03-27 02:09:04.718	{}
22787	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 06:40:00	EXECUTED	2020-03-27 06:41:11.707	{}
22788	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 06:40:00	EXECUTED	2020-03-27 06:41:11.754	{}
22789	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:40:00	EXECUTED	2020-03-27 06:41:11.788	{}
22790	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:41:00	EXECUTED	2020-03-27 06:42:11.814	{}
22791	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:42:00	EXECUTED	2020-03-27 06:43:11.844	{}
22792	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:43:00	EXECUTED	2020-03-27 06:44:11.873	{}
22793	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:44:00	EXECUTED	2020-03-27 06:45:11.888	{}
22794	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:45:00	EXECUTED	2020-03-27 06:46:11.92	{}
22912	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:35:00	EXECUTED	2020-03-27 08:36:14.182	{}
22913	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:36:00	EXECUTED	2020-03-27 08:37:14.195	{}
22914	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:37:00	EXECUTED	2020-03-27 08:38:14.208	{}
22915	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:38:00	EXECUTED	2020-03-27 08:39:14.235	{}
23074	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:04:00	EXECUTED	2020-03-27 11:05:17.313	{}
23075	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:05:00	EXECUTED	2020-03-27 11:06:17.338	{}
23219	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:17:00	EXECUTED	2020-03-27 13:18:20.237	{}
23220	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:18:00	EXECUTED	2020-03-27 13:19:20.246	{}
23221	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:19:00	EXECUTED	2020-03-27 13:20:20.286	{}
23222	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:20:00	EXECUTED	2020-03-27 13:21:20.298	{}
23223	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:21:00	EXECUTED	2020-03-27 13:22:20.309	{}
23224	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:22:00	EXECUTED	2020-03-27 13:23:20.32	{}
23225	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:23:00	EXECUTED	2020-03-27 13:24:20.332	{}
23226	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:24:00	EXECUTED	2020-03-27 13:25:20.344	{}
23227	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:25:00	EXECUTED	2020-03-27 13:26:20.372	{}
23228	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:26:00	EXECUTED	2020-03-27 13:27:20.384	{}
23229	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:27:00	EXECUTED	2020-03-27 13:28:20.41	{}
23230	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:28:00	EXECUTED	2020-03-27 13:29:20.42	{}
23411	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:14:00	EXECUTED	2020-03-27 16:15:23.578	{}
23412	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:15:00	EXECUTED	2020-03-27 16:16:23.591	{}
23413	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:16:00	EXECUTED	2020-03-27 16:17:23.62	{}
23414	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:17:00	EXECUTED	2020-03-27 16:18:23.637	{}
23415	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:18:00	EXECUTED	2020-03-27 16:19:23.647	{}
23416	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:19:00	EXECUTED	2020-03-27 16:20:23.659	{}
23417	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:20:00	EXECUTED	2020-03-27 16:21:23.671	{}
23418	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:21:00	EXECUTED	2020-03-27 16:22:23.694	{}
23419	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:22:00	EXECUTED	2020-03-27 16:23:23.704	{}
22186	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:24:00	EXECUTED	2020-03-26 21:24:58.892	{}
22595	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:41:00	EXECUTED	2020-03-27 03:42:06.458	{}
22494	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:09:00	EXECUTED	2020-03-27 02:10:04.731	{}
22795	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:46:00	EXECUTED	2020-03-27 06:47:11.94	{}
22187	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:25:00	EXECUTED	2020-03-26 21:25:58.904	{}
22188	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:26:00	EXECUTED	2020-03-26 21:26:58.93	{}
22686	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:06:00	EXECUTED	2020-03-27 05:07:09.682	{}
22495	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 02:10:00	EXECUTED	2020-03-27 02:11:04.74	{}
22796	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:47:00	EXECUTED	2020-03-27 06:48:11.966	{}
22496	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 02:10:00	EXECUTED	2020-03-27 02:11:04.757	{}
22497	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:10:00	EXECUTED	2020-03-27 02:11:04.764	{}
22498	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:11:00	EXECUTED	2020-03-27 02:12:04.798	{}
22499	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:12:00	EXECUTED	2020-03-27 02:13:04.824	{}
22687	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:07:00	EXECUTED	2020-03-27 05:08:09.696	{}
22592	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 03:40:00	EXECUTED	2020-03-27 03:41:06.417	{}
22797	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:48:00	EXECUTED	2020-03-27 06:49:11.983	{}
22593	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 03:40:00	EXECUTED	2020-03-27 03:41:06.432	{}
22594	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:40:00	EXECUTED	2020-03-27 03:41:06.442	{}
22688	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:08:00	EXECUTED	2020-03-27 05:09:09.717	{}
22798	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:49:00	EXECUTED	2020-03-27 06:50:12.011	{}
22916	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:39:00	EXECUTED	2020-03-27 08:40:14.258	{}
22917	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 08:40:00	EXECUTED	2020-03-27 08:41:14.262	{}
22918	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 08:40:00	EXECUTED	2020-03-27 08:41:14.28	{}
22919	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:40:00	EXECUTED	2020-03-27 08:41:14.291	{}
22920	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:41:00	EXECUTED	2020-03-27 08:42:14.319	{}
23076	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:06:00	EXECUTED	2020-03-27 11:07:17.352	{}
22921	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:42:00	EXECUTED	2020-03-27 08:43:14.331	{}
23077	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:07:00	EXECUTED	2020-03-27 11:08:17.364	{}
23078	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:08:00	EXECUTED	2020-03-27 11:09:17.378	{}
23079	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:09:00	EXECUTED	2020-03-27 11:10:17.388	{}
23080	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 11:10:00	EXECUTED	2020-03-27 11:11:17.396	{}
23081	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 11:10:00	EXECUTED	2020-03-27 11:11:17.432	{}
23082	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:10:00	EXECUTED	2020-03-27 11:11:17.447	{}
23083	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:11:00	EXECUTED	2020-03-27 11:12:17.457	{}
23084	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:12:00	EXECUTED	2020-03-27 11:13:17.484	{}
23085	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:13:00	EXECUTED	2020-03-27 11:14:17.495	{}
23231	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:29:00	EXECUTED	2020-03-27 13:30:20.475	{}
23421	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:24:00	EXECUTED	2020-03-27 16:25:23.743	{}
23422	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:25:00	EXECUTED	2020-03-27 16:26:23.764	{}
23423	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:26:00	EXECUTED	2020-03-27 16:27:23.786	{}
22296	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:06:00	EXECUTED	2020-03-26 23:07:00.819	{}
22500	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:13:00	EXECUTED	2020-03-27 02:14:04.843	{}
22297	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:07:00	EXECUTED	2020-03-26 23:08:00.835	{}
22501	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:14:00	EXECUTED	2020-03-27 02:15:04.858	{}
22298	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:08:00	EXECUTED	2020-03-26 23:09:00.853	{}
22502	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:15:00	EXECUTED	2020-03-27 02:16:04.869	{}
22299	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:09:00	EXECUTED	2020-03-26 23:10:00.87	{}
22503	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:16:00	EXECUTED	2020-03-27 02:17:04.88	{}
22799	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:50:00	EXECUTED	2020-03-27 06:51:12.04	{}
22504	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:17:00	EXECUTED	2020-03-27 02:18:04.927	{}
22505	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:18:00	EXECUTED	2020-03-27 02:19:04.953	{}
22800	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:51:00	EXECUTED	2020-03-27 06:52:12.067	{}
22689	CHECK_OFFLINE_PAYMENTS	2020-03-27 05:09:00	EXECUTED	2020-03-27 05:10:09.731	{}
22801	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:52:00	EXECUTED	2020-03-27 06:53:12.08	{}
23086	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:14:00	EXECUTED	2020-03-27 11:15:17.51	{}
22802	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:53:00	EXECUTED	2020-03-27 06:54:12.106	{}
22803	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:54:00	EXECUTED	2020-03-27 06:55:12.115	{}
22804	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:55:00	EXECUTED	2020-03-27 06:56:12.139	{}
23087	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:15:00	EXECUTED	2020-03-27 11:16:17.534	{}
22805	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:56:00	EXECUTED	2020-03-27 06:57:12.151	{}
22806	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:57:00	EXECUTED	2020-03-27 06:58:12.163	{}
23088	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:16:00	EXECUTED	2020-03-27 11:17:17.563	{}
22807	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:58:00	EXECUTED	2020-03-27 06:59:12.173	{}
22808	CHECK_OFFLINE_PAYMENTS	2020-03-27 06:59:00	EXECUTED	2020-03-27 07:00:12.183	{}
23089	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:17:00	EXECUTED	2020-03-27 11:18:17.591	{}
22809	SEND_OFFLINE_PAYMENT_TO_ORGANIZER	2020-03-27 07:00:00	EXECUTED	2020-03-27 07:00:12.184	{}
23090	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:18:00	EXECUTED	2020-03-27 11:19:17.603	{}
22810	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:00:00	EXECUTED	2020-03-27 07:01:12.198	{}
23091	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:19:00	EXECUTED	2020-03-27 11:20:17.62	{}
23424	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:27:00	EXECUTED	2020-03-27 16:28:23.808	{}
22811	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:01:00	EXECUTED	2020-03-27 07:02:12.21	{}
23232	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:30:00	EXECUTED	2020-03-27 13:31:20.489	{}
23092	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:20:00	EXECUTED	2020-03-27 11:21:17.629	{}
23233	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:31:00	EXECUTED	2020-03-27 13:32:20.502	{}
22002	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:35:00	EXECUTED	2020-03-26 18:35:55.313	{}
22003	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:36:00	EXECUTED	2020-03-26 18:36:55.339	{}
22004	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:37:00	EXECUTED	2020-03-26 18:37:55.393	{}
22189	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:27:00	EXECUTED	2020-03-26 21:27:58.942	{}
22190	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:28:00	EXECUTED	2020-03-26 21:28:58.96	{}
22191	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:29:00	EXECUTED	2020-03-26 21:29:58.97	{}
22192	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:30:00	EXECUTED	2020-03-26 21:30:58.98	{}
22193	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:31:00	EXECUTED	2020-03-26 21:31:58.99	{}
22292	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:02:00	EXECUTED	2020-03-26 23:03:00.76	{}
22293	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:03:00	EXECUTED	2020-03-26 23:04:00.775	{}
22294	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:04:00	EXECUTED	2020-03-26 23:05:00.787	{}
22295	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:05:00	EXECUTED	2020-03-26 23:06:00.804	{}
22300	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-26 23:10:00	EXECUTED	2020-03-26 23:11:01.037	{}
22301	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-26 23:10:00	EXECUTED	2020-03-26 23:11:01.073	{}
22302	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:10:00	EXECUTED	2020-03-26 23:11:01.087	{}
22303	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:11:00	EXECUTED	2020-03-26 23:12:01.098	{}
22304	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:12:00	EXECUTED	2020-03-26 23:13:01.109	{}
22305	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:13:00	EXECUTED	2020-03-26 23:14:01.12	{}
22306	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:14:00	EXECUTED	2020-03-26 23:15:01.145	{}
22596	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:42:00	EXECUTED	2020-03-27 03:43:06.471	{}
22597	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:43:00	EXECUTED	2020-03-27 03:44:06.481	{}
22598	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:44:00	EXECUTED	2020-03-27 03:45:06.494	{}
22599	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:45:00	EXECUTED	2020-03-27 03:46:06.504	{}
22600	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:46:00	EXECUTED	2020-03-27 03:47:06.513	{}
22601	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:47:00	EXECUTED	2020-03-27 03:48:06.526	{}
22812	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:02:00	EXECUTED	2020-03-27 07:03:12.221	{}
22813	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:03:00	EXECUTED	2020-03-27 07:04:12.234	{}
22814	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:04:00	EXECUTED	2020-03-27 07:05:12.266	{}
22922	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:43:00	EXECUTED	2020-03-27 08:44:14.384	{}
22923	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:44:00	EXECUTED	2020-03-27 08:45:14.397	{}
23093	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:21:00	EXECUTED	2020-03-27 11:22:17.647	{}
23234	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:32:00	EXECUTED	2020-03-27 13:33:20.513	{}
23235	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:33:00	EXECUTED	2020-03-27 13:34:20.53	{}
22010	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:41:00	EXECUTED	2020-03-26 18:41:55.477	{}
22194	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:32:00	EXECUTED	2020-03-26 21:32:59.007	{}
22311	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:19:00	EXECUTED	2020-03-26 23:20:01.269	{}
22307	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:15:00	EXECUTED	2020-03-26 23:16:01.177	{}
22506	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:19:00	EXECUTED	2020-03-27 02:20:04.972	{}
22312	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:20:00	EXECUTED	2020-03-26 23:21:01.28	{}
22602	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:48:00	EXECUTED	2020-03-27 03:49:06.544	{}
22313	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:21:00	EXECUTED	2020-03-26 23:22:01.378	{}
22815	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:05:00	EXECUTED	2020-03-27 07:06:12.281	{}
22816	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:06:00	EXECUTED	2020-03-27 07:07:12.298	{}
22924	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:45:00	EXECUTED	2020-03-27 08:46:14.421	{}
22925	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:46:00	EXECUTED	2020-03-27 08:47:14.434	{}
22926	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:47:00	EXECUTED	2020-03-27 08:48:14.446	{}
22927	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:48:00	EXECUTED	2020-03-27 08:49:14.467	{}
22928	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:49:00	EXECUTED	2020-03-27 08:50:14.491	{}
22937	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:58:00	EXECUTED	2020-03-27 08:59:14.658	{}
23094	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:22:00	EXECUTED	2020-03-27 11:23:17.665	{}
22929	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:50:00	EXECUTED	2020-03-27 08:51:14.503	{}
22930	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:51:00	EXECUTED	2020-03-27 08:52:14.529	{}
23095	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:23:00	EXECUTED	2020-03-27 11:24:17.678	{}
22931	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:52:00	EXECUTED	2020-03-27 08:53:14.555	{}
22938	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:59:00	EXECUTED	2020-03-27 09:00:14.669	{}
22939	SEND_OFFLINE_PAYMENT_TO_ORGANIZER	2020-03-27 09:00:00	EXECUTED	2020-03-27 09:00:14.67	{}
23096	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:24:00	EXECUTED	2020-03-27 11:25:17.691	{}
22940	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:00:00	EXECUTED	2020-03-27 09:01:14.681	{}
23097	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:25:00	EXECUTED	2020-03-27 11:26:17.718	{}
23238	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:36:00	EXECUTED	2020-03-27 13:37:20.609	{}
23425	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:28:00	EXECUTED	2020-03-27 16:29:23.822	{}
23426	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:29:00	EXECUTED	2020-03-27 16:30:23.863	{}
23427	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:30:00	EXECUTED	2020-03-27 16:31:23.876	{}
23428	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:31:00	EXECUTED	2020-03-27 16:32:23.897	{}
23429	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:32:00	EXECUTED	2020-03-27 16:33:23.917	{}
23430	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:33:00	EXECUTED	2020-03-27 16:34:23.93	{}
23431	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:34:00	EXECUTED	2020-03-27 16:35:23.982	{}
23432	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:35:00	EXECUTED	2020-03-27 16:36:23.996	{}
23433	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:36:00	EXECUTED	2020-03-27 16:37:24.048	{}
23434	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:37:00	EXECUTED	2020-03-27 16:38:24.063	{}
22005	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:38:00	EXECUTED	2020-03-26 18:38:55.406	{}
22006	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:39:00	EXECUTED	2020-03-26 18:39:55.416	{}
22007	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-26 18:40:00	EXECUTED	2020-03-26 18:40:55.421	{}
22008	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-26 18:40:00	EXECUTED	2020-03-26 18:40:55.44	{}
22009	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:40:00	EXECUTED	2020-03-26 18:40:55.449	{}
22011	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:42:00	EXECUTED	2020-03-26 18:42:55.487	{}
22012	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:43:00	EXECUTED	2020-03-26 18:43:55.495	{}
22013	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:44:00	EXECUTED	2020-03-26 18:44:55.506	{}
22014	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:45:00	EXECUTED	2020-03-26 18:45:55.518	{}
22015	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:46:00	EXECUTED	2020-03-26 18:46:55.528	{}
22016	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:47:00	EXECUTED	2020-03-26 18:47:55.539	{}
22017	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:48:00	EXECUTED	2020-03-26 18:48:55.55	{}
22308	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:16:00	EXECUTED	2020-03-26 23:17:01.197	{}
22309	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:17:00	EXECUTED	2020-03-26 23:18:01.224	{}
22310	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:18:00	EXECUTED	2020-03-26 23:19:01.251	{}
22932	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:53:00	EXECUTED	2020-03-27 08:54:14.566	{}
22933	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:54:00	EXECUTED	2020-03-27 08:55:14.592	{}
22934	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:55:00	EXECUTED	2020-03-27 08:56:14.604	{}
22935	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:56:00	EXECUTED	2020-03-27 08:57:14.632	{}
22936	CHECK_OFFLINE_PAYMENTS	2020-03-27 08:57:00	EXECUTED	2020-03-27 08:58:14.643	{}
22941	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:01:00	EXECUTED	2020-03-27 09:02:14.701	{}
22942	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:02:00	EXECUTED	2020-03-27 09:03:14.715	{}
23098	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:26:00	EXECUTED	2020-03-27 11:27:17.737	{}
23099	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:27:00	EXECUTED	2020-03-27 11:28:17.751	{}
23100	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:28:00	EXECUTED	2020-03-27 11:29:17.762	{}
23101	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:29:00	EXECUTED	2020-03-27 11:30:17.774	{}
23102	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:30:00	EXECUTED	2020-03-27 11:31:17.798	{}
23236	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:34:00	EXECUTED	2020-03-27 13:35:20.557	{}
23237	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:35:00	EXECUTED	2020-03-27 13:36:20.581	{}
23239	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:37:00	EXECUTED	2020-03-27 13:38:20.62	{}
23240	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:38:00	EXECUTED	2020-03-27 13:39:20.628	{}
23241	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:39:00	EXECUTED	2020-03-27 13:40:20.639	{}
23242	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 13:40:00	EXECUTED	2020-03-27 13:41:20.647	{}
23243	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 13:40:00	EXECUTED	2020-03-27 13:41:20.671	{}
23244	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:40:00	EXECUTED	2020-03-27 13:41:20.679	{}
23245	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:41:00	EXECUTED	2020-03-27 13:42:20.702	{}
23435	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:38:00	EXECUTED	2020-03-27 16:39:24.08	{}
23436	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:39:00	EXECUTED	2020-03-27 16:40:24.118	{}
22198	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:36:00	EXECUTED	2020-03-26 21:36:59.106	{}
22507	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:20:00	EXECUTED	2020-03-27 02:21:04.988	{}
22195	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:33:00	EXECUTED	2020-03-26 21:33:59.029	{}
22508	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:21:00	EXECUTED	2020-03-27 02:22:05.022	{}
22509	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:22:00	EXECUTED	2020-03-27 02:23:05.035	{}
22510	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:23:00	EXECUTED	2020-03-27 02:24:05.061	{}
22196	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:34:00	EXECUTED	2020-03-26 21:34:59.062	{}
22511	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:24:00	EXECUTED	2020-03-27 02:25:05.093	{}
22817	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:07:00	EXECUTED	2020-03-27 07:08:12.323	{}
22512	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:25:00	EXECUTED	2020-03-27 02:26:05.118	{}
23103	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:31:00	EXECUTED	2020-03-27 11:32:17.818	{}
22513	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:26:00	EXECUTED	2020-03-27 02:27:05.148	{}
22818	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:08:00	EXECUTED	2020-03-27 07:09:12.336	{}
22514	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:27:00	EXECUTED	2020-03-27 02:28:05.16	{}
23104	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:32:00	EXECUTED	2020-03-27 11:33:17.83	{}
22603	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:49:00	EXECUTED	2020-03-27 03:50:06.564	{}
22943	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:03:00	EXECUTED	2020-03-27 09:04:14.735	{}
22604	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:50:00	EXECUTED	2020-03-27 03:51:06.578	{}
23105	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:33:00	EXECUTED	2020-03-27 11:34:17.86	{}
23246	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:42:00	EXECUTED	2020-03-27 13:43:20.73	{}
22944	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:04:00	EXECUTED	2020-03-27 09:05:14.769	{}
23437	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 16:40:00	EXECUTED	2020-03-27 16:41:24.126	{}
23438	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 16:40:00	EXECUTED	2020-03-27 16:41:24.18	{}
23439	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:40:00	EXECUTED	2020-03-27 16:41:24.197	{}
22605	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:51:00	EXECUTED	2020-03-27 03:52:06.587	{}
22197	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:35:00	EXECUTED	2020-03-26 21:35:59.076	{}
22606	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:52:00	EXECUTED	2020-03-27 03:53:06.598	{}
22018	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:49:00	EXECUTED	2020-03-26 18:49:55.604	{}
22019	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:50:00	EXECUTED	2020-03-26 18:50:55.617	{}
22020	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:51:00	EXECUTED	2020-03-26 18:51:55.641	{}
22199	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:37:00	EXECUTED	2020-03-26 21:37:59.12	{}
22200	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:38:00	EXECUTED	2020-03-26 21:38:59.13	{}
22201	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:39:00	EXECUTED	2020-03-26 21:39:59.155	{}
22202	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-26 21:40:00	EXECUTED	2020-03-26 21:40:59.161	{}
22203	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-26 21:40:00	EXECUTED	2020-03-26 21:40:59.192	{}
22204	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:40:00	EXECUTED	2020-03-26 21:40:59.203	{}
22205	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:41:00	EXECUTED	2020-03-26 21:41:59.215	{}
22206	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:42:00	EXECUTED	2020-03-26 21:42:59.226	{}
22207	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:43:00	EXECUTED	2020-03-26 21:43:59.255	{}
22208	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:44:00	EXECUTED	2020-03-26 21:44:59.266	{}
22607	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:53:00	EXECUTED	2020-03-27 03:54:06.611	{}
22608	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:54:00	EXECUTED	2020-03-27 03:55:06.639	{}
22819	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:09:00	EXECUTED	2020-03-27 07:10:12.35	{}
22319	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:27:00	EXECUTED	2020-03-26 23:28:01.501	{}
22209	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:45:00	EXECUTED	2020-03-26 21:45:59.276	{}
22515	CHECK_OFFLINE_PAYMENTS	2020-03-27 02:28:00	EXECUTED	2020-03-27 02:29:05.206	{}
23106	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:34:00	EXECUTED	2020-03-27 11:35:17.902	{}
22609	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:55:00	EXECUTED	2020-03-27 03:56:06.65	{}
23107	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:35:00	EXECUTED	2020-03-27 11:36:17.934	{}
22610	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:56:00	EXECUTED	2020-03-27 03:57:06.66	{}
23108	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:36:00	EXECUTED	2020-03-27 11:37:17.965	{}
23109	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:37:00	EXECUTED	2020-03-27 11:38:17.993	{}
22320	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:28:00	EXECUTED	2020-03-26 23:29:01.527	{}
22611	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:57:00	EXECUTED	2020-03-27 03:58:06.672	{}
23110	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:38:00	EXECUTED	2020-03-27 11:39:18.004	{}
23111	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:39:00	EXECUTED	2020-03-27 11:40:18.023	{}
23112	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 11:40:00	EXECUTED	2020-03-27 11:41:18.027	{}
23113	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 11:40:00	EXECUTED	2020-03-27 11:41:18.044	{}
23114	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:40:00	EXECUTED	2020-03-27 11:41:18.052	{}
23115	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:41:00	EXECUTED	2020-03-27 11:42:18.062	{}
23116	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:42:00	EXECUTED	2020-03-27 11:43:18.077	{}
23117	CHECK_OFFLINE_PAYMENTS	2020-03-27 11:43:00	EXECUTED	2020-03-27 11:44:18.163	{}
23249	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:45:00	EXECUTED	2020-03-27 13:46:20.774	{}
23440	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:41:00	EXECUTED	2020-03-27 16:42:24.209	{}
22021	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:52:00	EXECUTED	2020-03-26 18:52:55.661	{}
22022	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:53:00	EXECUTED	2020-03-26 18:53:55.694	{}
22023	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:54:00	EXECUTED	2020-03-26 18:54:55.724	{}
22024	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:55:00	EXECUTED	2020-03-26 18:55:55.735	{}
22025	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:56:00	EXECUTED	2020-03-26 18:56:55.751	{}
22026	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:57:00	EXECUTED	2020-03-26 18:57:55.761	{}
22027	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:58:00	EXECUTED	2020-03-26 18:58:55.774	{}
22028	CHECK_OFFLINE_PAYMENTS	2020-03-26 18:59:00	EXECUTED	2020-03-26 18:59:55.794	{}
22029	SEND_OFFLINE_PAYMENT_TO_ORGANIZER	2020-03-26 19:00:00	EXECUTED	2020-03-26 19:00:55.799	{}
22030	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:00:00	EXECUTED	2020-03-26 19:00:55.813	{}
22031	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:01:00	EXECUTED	2020-03-26 19:01:55.83	{}
22032	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:02:00	EXECUTED	2020-03-26 19:02:55.841	{}
22033	CHECK_OFFLINE_PAYMENTS	2020-03-26 19:03:00	EXECUTED	2020-03-26 19:03:55.853	{}
22210	CHECK_OFFLINE_PAYMENTS	2020-03-26 21:46:00	EXECUTED	2020-03-26 21:46:59.298	{}
22314	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:22:00	EXECUTED	2020-03-26 23:23:01.4	{}
22315	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:23:00	EXECUTED	2020-03-26 23:24:01.41	{}
22316	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:24:00	EXECUTED	2020-03-26 23:25:01.429	{}
22317	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:25:00	EXECUTED	2020-03-26 23:26:01.464	{}
22318	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:26:00	EXECUTED	2020-03-26 23:27:01.49	{}
22321	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:29:00	EXECUTED	2020-03-26 23:30:01.554	{}
22322	CHECK_OFFLINE_PAYMENTS	2020-03-26 23:30:00	EXECUTED	2020-03-26 23:31:01.585	{}
22612	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:58:00	EXECUTED	2020-03-27 03:59:06.7	{}
22613	CHECK_OFFLINE_PAYMENTS	2020-03-27 03:59:00	EXECUTED	2020-03-27 04:00:06.714	{}
22614	SEND_OFFLINE_PAYMENT_TO_ORGANIZER	2020-03-27 04:00:00	EXECUTED	2020-03-27 04:00:06.716	{}
22615	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:00:00	EXECUTED	2020-03-27 04:01:06.728	{}
22616	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:01:00	EXECUTED	2020-03-27 04:02:06.739	{}
22617	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:02:00	EXECUTED	2020-03-27 04:03:06.75	{}
22618	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:03:00	EXECUTED	2020-03-27 04:04:06.766	{}
22619	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:04:00	EXECUTED	2020-03-27 04:05:06.78	{}
22620	CHECK_OFFLINE_PAYMENTS	2020-03-27 04:05:00	EXECUTED	2020-03-27 04:06:06.794	{}
22820	SEND_OFFLINE_PAYMENT_REMINDER	2020-03-27 07:10:00	EXECUTED	2020-03-27 07:11:12.359	{}
22821	SEND_TICKET_ASSIGNMENT_REMINDER	2020-03-27 07:10:00	EXECUTED	2020-03-27 07:11:12.406	{}
22822	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:10:00	EXECUTED	2020-03-27 07:11:12.434	{}
22823	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:11:00	EXECUTED	2020-03-27 07:12:12.447	{}
22824	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:12:00	EXECUTED	2020-03-27 07:13:12.475	{}
22825	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:13:00	EXECUTED	2020-03-27 07:14:12.495	{}
22826	CHECK_OFFLINE_PAYMENTS	2020-03-27 07:14:00	EXECUTED	2020-03-27 07:15:12.522	{}
22945	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:05:00	EXECUTED	2020-03-27 09:06:14.79	{}
22946	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:06:00	EXECUTED	2020-03-27 09:07:14.821	{}
22947	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:07:00	EXECUTED	2020-03-27 09:08:14.834	{}
22948	CHECK_OFFLINE_PAYMENTS	2020-03-27 09:08:00	EXECUTED	2020-03-27 09:09:14.855	{}
23247	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:43:00	EXECUTED	2020-03-27 13:44:20.743	{}
23248	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:44:00	EXECUTED	2020-03-27 13:45:20.762	{}
23250	CHECK_OFFLINE_PAYMENTS	2020-03-27 13:46:00	EXECUTED	2020-03-27 13:47:20.829	{}
23441	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:42:00	EXECUTED	2020-03-27 16:43:24.347	{}
23442	CHECK_OFFLINE_PAYMENTS	2020-03-27 16:43:00	EXECUTED	2020-03-27 16:44:24.371	{}
\.


--
-- Data for Name: admin_reservation_request; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.admin_reservation_request (id, request_id, user_id, event_id, reservation_id, request_type, status, body, failure_code, organization_id_fk) FROM stdin;
\.


--
-- Data for Name: auditing; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auditing (reservation_id, user_id, event_type, event_time, entity_type, entity_id, modifications, event_id, organization_id_fk) FROM stdin;
56f440a3-03c5-403c-b73a-80cc575cfe89	\N	UPDATE_TICKET	2020-02-21 11:33:28.184	TICKET	1	[{"newValue":"ACQUIRED","propertyName":"/status","state":"CHANGED","oldValue":"PENDING"}]	1	1
56f440a3-03c5-403c-b73a-80cc575cfe89	\N	RESERVATION_COMPLETE	2020-02-21 11:33:28.209	RESERVATION	56f440a3-03c5-403c-b73a-80cc575cfe89	\N	1	1
56f440a3-03c5-403c-b73a-80cc575cfe89	3	MANUAL_CHECK_IN	2020-02-21 11:33:41.989	TICKET	1	\N	1	1
56f440a3-03c5-403c-b73a-80cc575cfe89	3	REVERT_CHECK_IN	2020-02-21 11:33:50.775	TICKET	1	\N	1	1
56f440a3-03c5-403c-b73a-80cc575cfe89	2	BILLING_DOCUMENT_GENERATED	2020-02-21 11:35:06.777	RESERVATION	56f440a3-03c5-403c-b73a-80cc575cfe89	[{"documentId":1}]	1	1
56f440a3-03c5-403c-b73a-80cc575cfe89	2	CANCEL_TICKET	2020-02-21 11:35:06.785	TICKET	1	\N	1	1
56f440a3-03c5-403c-b73a-80cc575cfe89	2	CANCEL_RESERVATION	2020-02-21 11:35:06.822	RESERVATION	56f440a3-03c5-403c-b73a-80cc575cfe89	\N	1	1
bf6e63df-da4d-4735-964b-46ab6bb6d6f3	\N	RESERVATION_CREATE	2020-02-21 11:42:04.365	RESERVATION	bf6e63df-da4d-4735-964b-46ab6bb6d6f3	\N	1	1
bf6e63df-da4d-4735-964b-46ab6bb6d6f3	\N	UPDATE_TICKET	2020-02-21 11:42:51.87	TICKET	1	[{"newValue":"TO_BE_PAID","propertyName":"/status","state":"CHANGED","oldValue":"PENDING"}]	1	1
bf6e63df-da4d-4735-964b-46ab6bb6d6f3	\N	UPDATE_TICKET	2020-02-21 11:42:51.873	TICKET	2	[{"newValue":"TO_BE_PAID","propertyName":"/status","state":"CHANGED","oldValue":"PENDING"}]	1	1
bf6e63df-da4d-4735-964b-46ab6bb6d6f3	\N	RESERVATION_COMPLETE	2020-02-21 11:42:51.878	RESERVATION	bf6e63df-da4d-4735-964b-46ab6bb6d6f3	\N	1	1
bf6e63df-da4d-4735-964b-46ab6bb6d6f3	\N	TERMS_CONDITION_ACCEPTED	2020-02-21 11:42:51.878	RESERVATION	bf6e63df-da4d-4735-964b-46ab6bb6d6f3	[{"termsAndConditionsUrl":"https://svconcert.com/epica/terms"}]	1	1
bf6e63df-da4d-4735-964b-46ab6bb6d6f3	\N	PRIVACY_POLICY_ACCEPTED	2020-02-21 11:42:51.878	RESERVATION	bf6e63df-da4d-4735-964b-46ab6bb6d6f3	[{"privacyPolicyUrl":"https://svconcert.com/epica/terms/privacy"}]	1	1
bf6e63df-da4d-4735-964b-46ab6bb6d6f3	1	CANCEL_TICKET	2020-02-21 11:43:48.783	TICKET	1	\N	1	1
bf6e63df-da4d-4735-964b-46ab6bb6d6f3	1	CANCEL_TICKET	2020-02-21 11:43:48.783	TICKET	2	\N	1	1
bf6e63df-da4d-4735-964b-46ab6bb6d6f3	1	CANCEL_RESERVATION	2020-02-21 11:43:48.796	RESERVATION	bf6e63df-da4d-4735-964b-46ab6bb6d6f3	\N	1	1
1a71e32b-35c3-4089-b249-6e7528ef1a26	\N	RESERVATION_CREATE	2020-02-21 11:46:22.183	RESERVATION	1a71e32b-35c3-4089-b249-6e7528ef1a26	\N	1	1
1a71e32b-35c3-4089-b249-6e7528ef1a26	\N	UPDATE_TICKET	2020-02-21 11:46:38.8	TICKET	1	[{"newValue":true,"propertyName":"/assigned","state":"CHANGED","oldValue":false},{"newValue":"jandor.ac@gmail.com","propertyName":"/email","state":"CHANGED","oldValue":""},{"newValue":"Alejandro","propertyName":"/firstName","state":"CHANGED","oldValue":null},{"newValue":"Alejandro Castro","propertyName":"/fullName","state":"CHANGED","oldValue":""},{"newValue":"Castro","propertyName":"/lastName","state":"CHANGED","oldValue":null},{"newValue":"036361094","propertyName":"/{DUI}","state":"ADDED","oldValue":null}]	1	1
1a71e32b-35c3-4089-b249-6e7528ef1a26	\N	UPDATE_TICKET	2020-02-21 11:46:43.293	TICKET	1	[{"newValue":"03636109","propertyName":"/{DUI}","state":"CHANGED","oldValue":"036361094"}]	1	1
1a71e32b-35c3-4089-b249-6e7528ef1a26	\N	UPDATE_TICKET	2020-02-21 11:46:47.865	TICKET	1	[{"newValue":"0363610","propertyName":"/{DUI}","state":"CHANGED","oldValue":"03636109"}]	1	1
1a71e32b-35c3-4089-b249-6e7528ef1a26	\N	UPDATE_TICKET	2020-02-21 11:46:49.451	TICKET	1	[{"newValue":"036361","propertyName":"/{DUI}","state":"CHANGED","oldValue":"0363610"}]	1	1
1a71e32b-35c3-4089-b249-6e7528ef1a26	\N	UPDATE_TICKET	2020-02-21 11:46:50.538	TICKET	1	[{"newValue":"03636","propertyName":"/{DUI}","state":"CHANGED","oldValue":"036361"}]	1	1
1a71e32b-35c3-4089-b249-6e7528ef1a26	\N	UPDATE_TICKET	2020-02-21 11:46:51.392	TICKET	1	[{"newValue":"0363","propertyName":"/{DUI}","state":"CHANGED","oldValue":"03636"}]	1	1
1a71e32b-35c3-4089-b249-6e7528ef1a26	\N	UPDATE_TICKET	2020-02-21 11:46:52.151	TICKET	1	[{"newValue":"036","propertyName":"/{DUI}","state":"CHANGED","oldValue":"0363"}]	1	1
1a71e32b-35c3-4089-b249-6e7528ef1a26	\N	UPDATE_TICKET	2020-02-21 11:46:52.866	TICKET	1	[{"newValue":"03","propertyName":"/{DUI}","state":"CHANGED","oldValue":"036"}]	1	1
1a71e32b-35c3-4089-b249-6e7528ef1a26	\N	UPDATE_TICKET	2020-02-21 11:46:53.553	TICKET	1	[{"newValue":"0","propertyName":"/{DUI}","state":"CHANGED","oldValue":"03"}]	1	1
1a71e32b-35c3-4089-b249-6e7528ef1a26	\N	UPDATE_TICKET	2020-02-21 11:48:01.645	TICKET	1	[{"newValue":"036361094","propertyName":"/{DUI}","state":"ADDED","oldValue":null}]	1	1
1a71e32b-35c3-4089-b249-6e7528ef1a26	\N	RESERVATION_COMPLETE	2020-02-21 11:55:39.571	RESERVATION	1a71e32b-35c3-4089-b249-6e7528ef1a26	\N	1	1
1a71e32b-35c3-4089-b249-6e7528ef1a26	\N	TERMS_CONDITION_ACCEPTED	2020-02-21 11:55:39.571	RESERVATION	1a71e32b-35c3-4089-b249-6e7528ef1a26	[{"termsAndConditionsUrl":"https://svconcert.com/epica/terms"}]	1	1
1a71e32b-35c3-4089-b249-6e7528ef1a26	\N	PRIVACY_POLICY_ACCEPTED	2020-02-21 11:55:39.571	RESERVATION	1a71e32b-35c3-4089-b249-6e7528ef1a26	[{"privacyPolicyUrl":"https://svconcert.com/epica/terms/privacy"}]	1	1
1a71e32b-35c3-4089-b249-6e7528ef1a26	1	RESERVATION_OFFLINE_PAYMENT_CONFIRMED	2020-02-21 11:56:28.879	RESERVATION	1a71e32b-35c3-4089-b249-6e7528ef1a26	\N	1	1
1a71e32b-35c3-4089-b249-6e7528ef1a26	\N	UPDATE_TICKET	2020-02-21 11:56:28.881	TICKET	1	[{"newValue":"ACQUIRED","propertyName":"/status","state":"CHANGED","oldValue":"PENDING"}]	1	1
1a71e32b-35c3-4089-b249-6e7528ef1a26	1	BILLING_DOCUMENT_GENERATED	2020-02-21 11:56:28.976	RESERVATION	1a71e32b-35c3-4089-b249-6e7528ef1a26	[{"documentId":2}]	1	1
1a71e32b-35c3-4089-b249-6e7528ef1a26	1	BILLING_DOCUMENT_GENERATED	2020-02-21 11:56:57.596	RESERVATION	1a71e32b-35c3-4089-b249-6e7528ef1a26	[{"documentId":3}]	1	1
75994244-dbf1-49d8-9553-e5e6e6fbb560	\N	RESERVATION_CREATE	2020-02-22 08:33:17.309	RESERVATION	75994244-dbf1-49d8-9553-e5e6e6fbb560	\N	1	1
75994244-dbf1-49d8-9553-e5e6e6fbb560	\N	CANCEL_RESERVATION	2020-02-22 08:33:28.226	RESERVATION	75994244-dbf1-49d8-9553-e5e6e6fbb560	\N	1	1
75994244-dbf1-49d8-9553-e5e6e6fbb560	\N	CANCEL_RESERVATION	2020-02-22 08:33:28.235	RESERVATION	75994244-dbf1-49d8-9553-e5e6e6fbb560	\N	1	1
fa31ec0a-3f43-4a2c-834c-e0afdeebcce3	\N	UPDATE_TICKET	2020-02-22 08:52:45.218	TICKET	2	[{"newValue":"ACQUIRED","propertyName":"/status","state":"CHANGED","oldValue":"PENDING"}]	1	1
fa31ec0a-3f43-4a2c-834c-e0afdeebcce3	\N	RESERVATION_COMPLETE	2020-02-22 08:52:45.245	RESERVATION	fa31ec0a-3f43-4a2c-834c-e0afdeebcce3	\N	1	1
fa31ec0a-3f43-4a2c-834c-e0afdeebcce3	3	MANUAL_CHECK_IN	2020-02-22 08:53:03.526	TICKET	2	\N	1	1
fa31ec0a-3f43-4a2c-834c-e0afdeebcce3	3	REVERT_CHECK_IN	2020-02-22 08:53:16.734	TICKET	2	\N	1	1
fa31ec0a-3f43-4a2c-834c-e0afdeebcce3	2	MANUAL_CHECK_IN	2020-02-22 09:09:05.082	TICKET	2	\N	1	1
1a71e32b-35c3-4089-b249-6e7528ef1a26	2	MANUAL_CHECK_IN	2020-02-22 09:09:06.996	TICKET	1	\N	1	1
1a71e32b-35c3-4089-b249-6e7528ef1a26	2	REVERT_CHECK_IN	2020-02-22 09:13:17.557	TICKET	1	\N	1	1
fa31ec0a-3f43-4a2c-834c-e0afdeebcce3	2	REVERT_CHECK_IN	2020-02-22 09:13:18.838	TICKET	2	\N	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	RESERVATION_CREATE	2020-03-08 20:21:17.983	RESERVATION	11f71518-4ea4-4a8e-813c-85312308602d	\N	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	UPDATE_TICKET	2020-03-08 20:22:17.109	TICKET	5	[{"newValue":true,"propertyName":"/assigned","state":"CHANGED","oldValue":false},{"newValue":"pablo.aguilar@itproject41.com","propertyName":"/email","state":"CHANGED","oldValue":""},{"newValue":"Pablo","propertyName":"/firstName","state":"CHANGED","oldValue":null},{"newValue":"Pablo Aguilar","propertyName":"/fullName","state":"CHANGED","oldValue":""},{"newValue":"Aguilar","propertyName":"/lastName","state":"CHANGED","oldValue":null},{"newValue":"03048744","propertyName":"/{DUI}","state":"ADDED","oldValue":null}]	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	UPDATE_TICKET	2020-03-08 20:22:17.137	TICKET	3	[{"newValue":"Paul","propertyName":"/firstName","state":"CHANGED","oldValue":null},{"newValue":"Paul Lopez","propertyName":"/fullName","state":"CHANGED","oldValue":""},{"newValue":"Lopez","propertyName":"/lastName","state":"CHANGED","oldValue":null}]	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	UPDATE_TICKET	2020-03-08 20:22:17.155	TICKET	4	[{"newValue":"null null","propertyName":"/fullName","state":"CHANGED","oldValue":""}]	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	UPDATE_TICKET	2020-03-08 20:23:15.722	TICKET	5	[{"newValue":"033048744","propertyName":"/{DUI}","state":"CHANGED","oldValue":"03048744"}]	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	UPDATE_TICKET	2020-03-08 20:23:15.746	TICKET	3	[{"newValue":true,"propertyName":"/assigned","state":"CHANGED","oldValue":false},{"newValue":"paul.lopez@@itproject41.com","propertyName":"/email","state":"CHANGED","oldValue":""},{"newValue":"034507678","propertyName":"/{DUI}","state":"ADDED","oldValue":null}]	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	UPDATE_TICKET	2020-03-08 20:23:15.763	TICKET	4	[{"newValue":true,"propertyName":"/assigned","state":"CHANGED","oldValue":false},{"newValue":"alejandro.castro@itproject41.com","propertyName":"/email","state":"CHANGED","oldValue":""},{"newValue":"Alejandro","propertyName":"/firstName","state":"CHANGED","oldValue":null},{"newValue":"Alejandro Castro","propertyName":"/fullName","state":"CHANGED","oldValue":"null null"},{"newValue":"Castro","propertyName":"/lastName","state":"CHANGED","oldValue":null},{"newValue":"036361094","propertyName":"/{DUI}","state":"ADDED","oldValue":null}]	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	UPDATE_TICKET	2020-03-08 20:23:25.63	TICKET	3	[{"newValue":"TO_BE_PAID","propertyName":"/status","state":"CHANGED","oldValue":"PENDING"}]	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	UPDATE_TICKET	2020-03-08 20:23:25.635	TICKET	4	[{"newValue":"TO_BE_PAID","propertyName":"/status","state":"CHANGED","oldValue":"PENDING"}]	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	UPDATE_TICKET	2020-03-08 20:23:25.637	TICKET	5	[{"newValue":"TO_BE_PAID","propertyName":"/status","state":"CHANGED","oldValue":"PENDING"}]	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	RESERVATION_COMPLETE	2020-03-08 20:23:26.304	RESERVATION	11f71518-4ea4-4a8e-813c-85312308602d	\N	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	TERMS_CONDITION_ACCEPTED	2020-03-08 20:23:26.304	RESERVATION	11f71518-4ea4-4a8e-813c-85312308602d	[{"termsAndConditionsUrl":"https://svconcert.com/epica/terms"}]	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	PRIVACY_POLICY_ACCEPTED	2020-03-08 20:23:26.304	RESERVATION	11f71518-4ea4-4a8e-813c-85312308602d	[{"privacyPolicyUrl":"https://svconcert.com/epica/terms/privacy"}]	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	UPDATE_TICKET	2020-03-08 20:26:13.234	TICKET	5	[]	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	UPDATE_TICKET	2020-03-08 20:27:08.964	TICKET	5	[{"newValue":"jorge.ochoa@itproject41.com","propertyName":"/email","state":"CHANGED","oldValue":"pablo.aguilar@itproject41.com"},{"newValue":"Jorge","propertyName":"/firstName","state":"CHANGED","oldValue":"Pablo"},{"newValue":"Jorge Ochoa","propertyName":"/fullName","state":"CHANGED","oldValue":"Pablo Aguilar"},{"newValue":"Ochoa","propertyName":"/lastName","state":"CHANGED","oldValue":"Aguilar"}]	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	UPDATE_TICKET	2020-03-08 20:31:11.485	TICKET	5	[{"newValue":"032256792","propertyName":"/{DUI}","state":"CHANGED","oldValue":"033048744"}]	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	UPDATE_TICKET	2020-03-08 20:31:38.575	TICKET	4	[]	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	UPDATE_TICKET	2020-03-08 21:17:22.7	TICKET	5	[{"newValue":"jandor.ac@gmail.com","propertyName":"/email","state":"CHANGED","oldValue":"jorge.ochoa@itproject41.com"},{"newValue":"036361094","propertyName":"/{DUI}","state":"CHANGED","oldValue":"032256792"}]	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	UPDATE_TICKET	2020-03-08 21:20:07.059	TICKET	3	[{"newValue":"alejandro.castro@itproject41.com","propertyName":"/email","state":"CHANGED","oldValue":"paul.lopez@@itproject41.com"},{"newValue":"Alejandro","propertyName":"/firstName","state":"CHANGED","oldValue":"Paul"},{"newValue":"Alejandro Castro","propertyName":"/fullName","state":"CHANGED","oldValue":"Paul Lopez"},{"newValue":"Castro","propertyName":"/lastName","state":"CHANGED","oldValue":"Lopez"},{"newValue":"036361094","propertyName":"/{DUI}","state":"CHANGED","oldValue":"034507678"}]	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	UPDATE_TICKET	2020-03-08 21:25:19.181	TICKET	3	[{"newValue":"pablo.aguilar@itproject41.com","propertyName":"/email","state":"CHANGED","oldValue":"alejandro.castro@itproject41.com"},{"newValue":"Pablo","propertyName":"/firstName","state":"CHANGED","oldValue":"Alejandro"},{"newValue":"Pablo Aguilar","propertyName":"/fullName","state":"CHANGED","oldValue":"Alejandro Castro"},{"newValue":"Aguilar","propertyName":"/lastName","state":"CHANGED","oldValue":"Castro"}]	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	UPDATE_TICKET	2020-03-08 21:29:32.407	TICKET	3	[{"newValue":"jose.perez@correo.com","propertyName":"/email","state":"CHANGED","oldValue":"pablo.aguilar@itproject41.com"},{"newValue":"Jose","propertyName":"/firstName","state":"CHANGED","oldValue":"Pablo"},{"newValue":"Jose Perez","propertyName":"/fullName","state":"CHANGED","oldValue":"Pablo Aguilar"},{"newValue":"Perez","propertyName":"/lastName","state":"CHANGED","oldValue":"Aguilar"},{"newValue":"123456478","propertyName":"/{DUI}","state":"CHANGED","oldValue":"036361094"}]	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	UPDATE_TICKET	2020-03-08 21:30:32.525	TICKET	3	[]	1	1
786a6534-999d-4d49-a944-fb0a5fa3de64	\N	RESERVATION_CREATE	2020-03-11 23:53:05.919	RESERVATION	786a6534-999d-4d49-a944-fb0a5fa3de64	\N	1	1
786a6534-999d-4d49-a944-fb0a5fa3de64	\N	UPDATE_TICKET	2020-03-11 23:53:18.44	TICKET	6	[{"newValue":"null null","propertyName":"/fullName","state":"CHANGED","oldValue":""}]	1	1
f687d682-43dd-4ed8-8c85-61211009cb42	\N	RESERVATION_CREATE	2020-03-17 15:30:51.942	RESERVATION	f687d682-43dd-4ed8-8c85-61211009cb42	\N	1	1
f687d682-43dd-4ed8-8c85-61211009cb42	\N	UPDATE_TICKET	2020-03-17 15:30:58.829	TICKET	6	[{"newValue":"null null","propertyName":"/fullName","state":"CHANGED","oldValue":""}]	1	1
f687d682-43dd-4ed8-8c85-61211009cb42	\N	UPDATE_TICKET	2020-03-17 15:30:58.847	TICKET	7	[{"newValue":"null null","propertyName":"/fullName","state":"CHANGED","oldValue":""}]	1	1
f687d682-43dd-4ed8-8c85-61211009cb42	\N	CANCEL_RESERVATION	2020-03-17 15:33:20.012	RESERVATION	f687d682-43dd-4ed8-8c85-61211009cb42	\N	1	1
f687d682-43dd-4ed8-8c85-61211009cb42	\N	CANCEL_RESERVATION	2020-03-17 15:33:20.036	RESERVATION	f687d682-43dd-4ed8-8c85-61211009cb42	\N	1	1
1f110d7e-1658-4169-ab38-7539a9e98f17	\N	RESERVATION_CREATE	2020-03-17 21:30:13.844	RESERVATION	1f110d7e-1658-4169-ab38-7539a9e98f17	\N	1	1
1f110d7e-1658-4169-ab38-7539a9e98f17	\N	UPDATE_TICKET	2020-03-17 21:30:45.87	TICKET	6	[{"newValue":"null null","propertyName":"/fullName","state":"CHANGED","oldValue":""}]	1	1
1f110d7e-1658-4169-ab38-7539a9e98f17	\N	CANCEL_RESERVATION	2020-03-17 21:31:02.814	RESERVATION	1f110d7e-1658-4169-ab38-7539a9e98f17	\N	1	1
1f110d7e-1658-4169-ab38-7539a9e98f17	\N	CANCEL_RESERVATION	2020-03-17 21:31:02.822	RESERVATION	1f110d7e-1658-4169-ab38-7539a9e98f17	\N	1	1
8b9483ac-edc3-4b18-a27f-0b71795b5f37	\N	RESERVATION_CREATE	2020-03-20 11:14:35.01	RESERVATION	8b9483ac-edc3-4b18-a27f-0b71795b5f37	\N	1	1
8b9483ac-edc3-4b18-a27f-0b71795b5f37	\N	CANCEL_RESERVATION	2020-03-20 11:16:08.146	RESERVATION	8b9483ac-edc3-4b18-a27f-0b71795b5f37	\N	1	1
8b9483ac-edc3-4b18-a27f-0b71795b5f37	\N	CANCEL_RESERVATION	2020-03-20 11:16:08.188	RESERVATION	8b9483ac-edc3-4b18-a27f-0b71795b5f37	\N	1	1
6a464eb7-5403-4575-b8a3-3b43d3bfd13a	\N	RESERVATION_CREATE	2020-03-20 12:45:26.195	RESERVATION	6a464eb7-5403-4575-b8a3-3b43d3bfd13a	\N	1	1
6a464eb7-5403-4575-b8a3-3b43d3bfd13a	\N	UPDATE_TICKET	2020-03-20 12:45:33.487	TICKET	6	[{"newValue":"null null","propertyName":"/fullName","state":"CHANGED","oldValue":""}]	1	1
2e9bece9-9594-4112-9af5-ed008479cdff	\N	RESERVATION_CREATE	2020-03-20 12:45:44.483	RESERVATION	2e9bece9-9594-4112-9af5-ed008479cdff	\N	2	1
2e9bece9-9594-4112-9af5-ed008479cdff	\N	CANCEL_RESERVATION	2020-03-20 12:46:46.538	RESERVATION	2e9bece9-9594-4112-9af5-ed008479cdff	\N	2	1
2e9bece9-9594-4112-9af5-ed008479cdff	\N	CANCEL_RESERVATION	2020-03-20 12:46:46.547	RESERVATION	2e9bece9-9594-4112-9af5-ed008479cdff	\N	2	1
4ba08935-4af5-4fcd-b843-7cebc90ea94f	\N	RESERVATION_CREATE	2020-03-20 12:47:04.216	RESERVATION	4ba08935-4af5-4fcd-b843-7cebc90ea94f	\N	1	1
4ba08935-4af5-4fcd-b843-7cebc90ea94f	\N	UPDATE_TICKET	2020-03-20 12:47:30.021	TICKET	7	[{"newValue":true,"propertyName":"/assigned","state":"CHANGED","oldValue":false},{"newValue":"alejandro.castro@itproject41.com","propertyName":"/email","state":"CHANGED","oldValue":""},{"newValue":"Alejandro","propertyName":"/firstName","state":"CHANGED","oldValue":null},{"newValue":"Alejandro Castro","propertyName":"/fullName","state":"CHANGED","oldValue":""},{"newValue":"Castro","propertyName":"/lastName","state":"CHANGED","oldValue":null},{"newValue":"036361094","propertyName":"/{DUI}","state":"ADDED","oldValue":null}]	1	1
4ba08935-4af5-4fcd-b843-7cebc90ea94f	\N	CANCEL_RESERVATION	2020-03-20 12:50:26.927	RESERVATION	4ba08935-4af5-4fcd-b843-7cebc90ea94f	\N	1	1
4ba08935-4af5-4fcd-b843-7cebc90ea94f	\N	CANCEL_RESERVATION	2020-03-20 12:50:26.941	RESERVATION	4ba08935-4af5-4fcd-b843-7cebc90ea94f	\N	1	1
8fccf2c3-ebbd-4fe4-9aa3-a47c58c1912c	\N	RESERVATION_CREATE	2020-03-20 15:40:35.541	RESERVATION	8fccf2c3-ebbd-4fe4-9aa3-a47c58c1912c	\N	1	1
ee9f9273-a879-4918-9bbd-dc1f0ae8e6d1	\N	RESERVATION_CREATE	2020-03-23 15:56:07.837	RESERVATION	ee9f9273-a879-4918-9bbd-dc1f0ae8e6d1	\N	1	1
ee9f9273-a879-4918-9bbd-dc1f0ae8e6d1	\N	CANCEL_RESERVATION	2020-03-23 15:56:18.288	RESERVATION	ee9f9273-a879-4918-9bbd-dc1f0ae8e6d1	\N	1	1
ee9f9273-a879-4918-9bbd-dc1f0ae8e6d1	\N	CANCEL_RESERVATION	2020-03-23 15:56:18.314	RESERVATION	ee9f9273-a879-4918-9bbd-dc1f0ae8e6d1	\N	1	1
4f3eb546-bf4f-46cb-b908-60c947a9890f	\N	RESERVATION_CREATE	2020-03-24 14:13:14.985	RESERVATION	4f3eb546-bf4f-46cb-b908-60c947a9890f	\N	1	1
4f3eb546-bf4f-46cb-b908-60c947a9890f	\N	UPDATE_TICKET	2020-03-24 14:17:28.293	TICKET	6	[{"newValue":true,"propertyName":"/assigned","state":"CHANGED","oldValue":false},{"newValue":"jandor.ac@gmail.com","propertyName":"/email","state":"CHANGED","oldValue":""},{"newValue":"alejandro","propertyName":"/firstName","state":"CHANGED","oldValue":null},{"newValue":"alejandro castro","propertyName":"/fullName","state":"CHANGED","oldValue":""},{"newValue":"castro","propertyName":"/lastName","state":"CHANGED","oldValue":null}]	1	1
4f3eb546-bf4f-46cb-b908-60c947a9890f	\N	UPDATE_TICKET	2020-03-24 14:17:37.629	TICKET	6	[{"newValue":"Alejandro","propertyName":"/firstName","state":"CHANGED","oldValue":"alejandro"},{"newValue":"Alejandro Castro","propertyName":"/fullName","state":"CHANGED","oldValue":"alejandro castro"},{"newValue":"Castro","propertyName":"/lastName","state":"CHANGED","oldValue":"castro"},{"newValue":"036361094","propertyName":"/{DUI}","state":"ADDED","oldValue":null}]	1	1
4f3eb546-bf4f-46cb-b908-60c947a9890f	\N	UPDATE_TICKET	2020-03-24 14:18:28.24	TICKET	6	[]	1	1
c55f248c-6d51-454d-a1bc-08445b47f7c6	\N	RESERVATION_CREATE	2020-03-25 08:59:18.652	RESERVATION	c55f248c-6d51-454d-a1bc-08445b47f7c6	\N	1	1
c55f248c-6d51-454d-a1bc-08445b47f7c6	\N	UPDATE_TICKET	2020-03-25 09:00:20.513	TICKET	6	[{"newValue":true,"propertyName":"/assigned","state":"CHANGED","oldValue":false},{"newValue":"jandor.ac@gmail.com","propertyName":"/email","state":"CHANGED","oldValue":""},{"newValue":"Alrejandro","propertyName":"/firstName","state":"CHANGED","oldValue":null},{"newValue":"Alrejandro Casrto","propertyName":"/fullName","state":"CHANGED","oldValue":""},{"newValue":"Casrto","propertyName":"/lastName","state":"CHANGED","oldValue":null},{"newValue":"036361094","propertyName":"/{DUI}","state":"ADDED","oldValue":null}]	1	1
c55f248c-6d51-454d-a1bc-08445b47f7c6	\N	CANCEL_RESERVATION	2020-03-25 09:01:22.466	RESERVATION	c55f248c-6d51-454d-a1bc-08445b47f7c6	\N	1	1
c55f248c-6d51-454d-a1bc-08445b47f7c6	\N	CANCEL_RESERVATION	2020-03-25 09:01:22.483	RESERVATION	c55f248c-6d51-454d-a1bc-08445b47f7c6	\N	1	1
8789f11a-1d08-4ce0-8929-c890915c20f8	\N	RESERVATION_CREATE	2020-03-25 09:15:08.073	RESERVATION	8789f11a-1d08-4ce0-8929-c890915c20f8	\N	1	1
1a71e32b-35c3-4089-b249-6e7528ef1a26	1	MANUAL_CHECK_IN	2020-03-25 09:32:41.892	TICKET	1	\N	1	1
1a71e32b-35c3-4089-b249-6e7528ef1a26	1	REVERT_CHECK_IN	2020-03-25 09:32:50.504	TICKET	1	\N	1	1
570666f5-10b7-4167-a9a4-3f93f0e5a365	\N	RESERVATION_CREATE	2020-03-25 15:46:32.597	RESERVATION	570666f5-10b7-4167-a9a4-3f93f0e5a365	\N	1	1
570666f5-10b7-4167-a9a4-3f93f0e5a365	\N	CANCEL_RESERVATION	2020-03-25 15:46:37.776	RESERVATION	570666f5-10b7-4167-a9a4-3f93f0e5a365	\N	1	1
570666f5-10b7-4167-a9a4-3f93f0e5a365	\N	CANCEL_RESERVATION	2020-03-25 15:46:37.9	RESERVATION	570666f5-10b7-4167-a9a4-3f93f0e5a365	\N	1	1
00449ca6-fd93-4a4c-857d-09f49315fccf	\N	RESERVATION_CREATE	2020-03-25 16:11:15.996	RESERVATION	00449ca6-fd93-4a4c-857d-09f49315fccf	\N	4	1
00449ca6-fd93-4a4c-857d-09f49315fccf	\N	UPDATE_TICKET	2020-03-25 16:11:28.319	TICKET	350	[{"newValue":true,"propertyName":"/assigned","state":"CHANGED","oldValue":false},{"newValue":"jandor.ac@gmail.com","propertyName":"/email","state":"CHANGED","oldValue":""},{"newValue":"Alejandro","propertyName":"/firstName","state":"CHANGED","oldValue":null},{"newValue":"Alejandro Castro","propertyName":"/fullName","state":"CHANGED","oldValue":""},{"newValue":"Castro","propertyName":"/lastName","state":"CHANGED","oldValue":null}]	4	1
00449ca6-fd93-4a4c-857d-09f49315fccf	\N	UPDATE_TICKET	2020-03-25 16:11:36.936	TICKET	350	[{"newValue":"TO_BE_PAID","propertyName":"/status","state":"CHANGED","oldValue":"PENDING"}]	4	1
00449ca6-fd93-4a4c-857d-09f49315fccf	\N	RESERVATION_COMPLETE	2020-03-25 16:11:40.217	RESERVATION	00449ca6-fd93-4a4c-857d-09f49315fccf	\N	4	1
00449ca6-fd93-4a4c-857d-09f49315fccf	\N	TERMS_CONDITION_ACCEPTED	2020-03-25 16:11:40.217	RESERVATION	00449ca6-fd93-4a4c-857d-09f49315fccf	[{"termsAndConditionsUrl":"https://omam/terms"}]	4	1
00449ca6-fd93-4a4c-857d-09f49315fccf	\N	PRIVACY_POLICY_ACCEPTED	2020-03-25 16:11:40.217	RESERVATION	00449ca6-fd93-4a4c-857d-09f49315fccf	[{"privacyPolicyUrl":"https://omam/privacy"}]	4	1
2d1bc77d-b298-4709-97b9-f1bdd5f5a1b0	\N	RESERVATION_CREATE	2020-03-25 16:39:21.972	RESERVATION	2d1bc77d-b298-4709-97b9-f1bdd5f5a1b0	\N	2	1
2d1bc77d-b298-4709-97b9-f1bdd5f5a1b0	\N	CANCEL_RESERVATION	2020-03-25 16:39:24.922	RESERVATION	2d1bc77d-b298-4709-97b9-f1bdd5f5a1b0	\N	2	1
2d1bc77d-b298-4709-97b9-f1bdd5f5a1b0	\N	CANCEL_RESERVATION	2020-03-25 16:39:24.932	RESERVATION	2d1bc77d-b298-4709-97b9-f1bdd5f5a1b0	\N	2	1
5e7b1564-8c99-4db0-8aca-317cf16e4214	\N	RESERVATION_CREATE	2020-03-26 12:04:12.118	RESERVATION	5e7b1564-8c99-4db0-8aca-317cf16e4214	\N	1	1
298dfd44-90cc-43bf-876e-95233d835484	\N	RESERVATION_CREATE	2020-03-26 12:04:39.169	RESERVATION	298dfd44-90cc-43bf-876e-95233d835484	\N	2	1
298dfd44-90cc-43bf-876e-95233d835484	\N	CANCEL_RESERVATION	2020-03-26 12:04:45.68	RESERVATION	298dfd44-90cc-43bf-876e-95233d835484	\N	2	1
298dfd44-90cc-43bf-876e-95233d835484	\N	CANCEL_RESERVATION	2020-03-26 12:04:45.775	RESERVATION	298dfd44-90cc-43bf-876e-95233d835484	\N	2	1
5e7b1564-8c99-4db0-8aca-317cf16e4214	\N	CANCEL_RESERVATION	2020-03-26 12:05:18.932	RESERVATION	5e7b1564-8c99-4db0-8aca-317cf16e4214	\N	1	1
5e7b1564-8c99-4db0-8aca-317cf16e4214	\N	CANCEL_RESERVATION	2020-03-26 12:05:18.935	RESERVATION	5e7b1564-8c99-4db0-8aca-317cf16e4214	\N	1	1
1a71e32b-35c3-4089-b249-6e7528ef1a26	\N	UPDATE_TICKET_CATEGORY	2020-03-26 12:08:37.344921	TICKET	1	\N	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	UPDATE_TICKET_CATEGORY	2020-03-26 12:08:37.344921	TICKET	3	\N	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	UPDATE_TICKET_CATEGORY	2020-03-26 12:08:37.344921	TICKET	5	\N	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	UPDATE_TICKET_CATEGORY	2020-03-26 12:08:37.344921	TICKET	4	\N	1	1
fa31ec0a-3f43-4a2c-834c-e0afdeebcce3	\N	UPDATE_TICKET_CATEGORY	2020-03-26 12:08:37.344921	TICKET	2	\N	1	1
1a71e32b-35c3-4089-b249-6e7528ef1a26	\N	UPDATE_TICKET_CATEGORY	2020-03-26 12:09:10.010501	TICKET	1	\N	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	UPDATE_TICKET_CATEGORY	2020-03-26 12:09:10.010501	TICKET	3	\N	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	UPDATE_TICKET_CATEGORY	2020-03-26 12:09:10.010501	TICKET	5	\N	1	1
11f71518-4ea4-4a8e-813c-85312308602d	\N	UPDATE_TICKET_CATEGORY	2020-03-26 12:09:10.010501	TICKET	4	\N	1	1
fa31ec0a-3f43-4a2c-834c-e0afdeebcce3	\N	UPDATE_TICKET_CATEGORY	2020-03-26 12:09:10.010501	TICKET	2	\N	1	1
8aae6df2-d154-48e4-9eab-332223aaaeef	\N	RESERVATION_CREATE	2020-03-26 12:11:47.622	RESERVATION	8aae6df2-d154-48e4-9eab-332223aaaeef	\N	1	1
e083f858-5a11-490e-a72a-83f4724c66f9	\N	RESERVATION_CREATE	2020-03-26 12:12:25.69	RESERVATION	e083f858-5a11-490e-a72a-83f4724c66f9	\N	1	1
3f6d4b8a-d018-483a-9f70-0b65c858f1e2	\N	RESERVATION_CREATE	2020-03-26 15:23:07.481	RESERVATION	3f6d4b8a-d018-483a-9f70-0b65c858f1e2	\N	1	1
a235287a-e194-480c-bc53-d50a84c8fd73	\N	RESERVATION_CREATE	2020-03-26 21:28:31.146	RESERVATION	a235287a-e194-480c-bc53-d50a84c8fd73	\N	1	1
a235287a-e194-480c-bc53-d50a84c8fd73	\N	UPDATE_TICKET_CATEGORY	2020-03-26 21:39:49.57864	TICKET	6	\N	1	1
\.


--
-- Data for Name: authority; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.authority (username, role) FROM stdin;
admin	ROLE_ADMIN
pablo.aguilar	ROLE_OWNER
eivynd.mendoza	ROLE_SUPERVISOR
\.


--
-- Data for Name: b_transaction; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.b_transaction (id, gtw_tx_id, reservation_id, t_timestamp, price_cts, currency, description, payment_proxy, gtw_payment_id, plat_fee, gtw_fee, organization_id_fk, status, metadata) FROM stdin;
1	OFFLINE-1582307788873	1a71e32b-35c3-4089-b249-6e7528ef1a26	2020-02-21 11:56:28.877-06	1500	USD		OFFLINE	\N	0	0	1	COMPLETE                        	{}
\.


--
-- Data for Name: ba_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ba_user (id, username, password, first_name, last_name, email_address, enabled, user_type, user_creation_time, valid_to, description) FROM stdin;
2	pablo.aguilar	$2a$10$3s1FLmgZGEu.DVlE5M7l5.oIkf0B/3zYEBOdqBbRtwPXMlVT9qHVO	Pablo	Aguilar	pablo.aguilar@svconcert.com	t	INTERNAL	2020-02-21 11:18:02.913151	\N	\N
3	eivynd.mendoza	$2a$10$p8JsQhlbtHQjuv4DT4cdx.IlVzCrt9VsMzFHFUgbFXOVqPxdM8VgW	Eivynd	Mendoza	eivynd.mendoza@svconcert.com	t	INTERNAL	2020-02-21 11:19:04.329544	\N	\N
1	admin	$2a$10$7tbAvvI2u1GF/AUEVj9FVuvo4scbYXlshRJtV6K2N/4WzRvCBaQc2	The	Administrator	admin@localhost	t	INTERNAL	2020-02-21 11:10:35.695247	\N	\N
\.


--
-- Data for Name: billing_document; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.billing_document (id, event_id_fk, number, reservation_id_fk, type, model, generation_ts, external_id, status, organization_id_fk) FROM stdin;
1	1	3d271dd7-4bb6-4258-8a68-b3487deb7699	56f440a3-03c5-403c-b73a-80cc575cfe89	RECEIPT	{"confirmationDate":"2020-02-21T11:33:28.212-06:00","vatNr":"","invoicingAdditionalInfo":{"italianEInvoicing":null,"empty":true},"tickets":[{"category":{"id":1,"utcInception":"2020-03-15T17:00:00Z","utcExpiration":"2020-03-31T13:00:00Z","maxTickets":0,"name":"Simphonyc Concert","accessRestricted":false,"status":"ACTIVE","eventId":1,"bounded":false,"srcPriceCts":1500,"code":"3p1c@","validCheckInFrom":null,"validCheckInTo":null,"ticketValidityStart":null,"ticketValidityEnd":null,"currencyCode":"USD","ordinal":1,"ticketCheckInStrategy":"ONCE_PER_EVENT","price":15.00,"free":false},"id":1,"fullName":"Alejandro Castro","currencyCode":"USD","status":"ACQUIRED","formattedFinalPrice":"15.00","extReference":null,"creation":"2020-02-21T17:26:20.814Z","formattedNetPrice":"15.00","checkedIn":false,"categoryName":"Simphonyc Concert","srcPriceCts":1500,"firstName":"Alejandro","email":"alejandro.castro@correo.com","lastName":"Castro","userLanguage":"es","vatCts":0,"finalPriceCts":1500,"discountCts":0,"uuid":"62695386-d742-4080-bb78-2a60526cc03d","categoryId":1,"assigned":true,"eventId":1,"lockedAssignment":true,"ticketsReservationId":"56f440a3-03c5-403c-b73a-80cc575cfe89"}],"reservationShortID":"56F440A3","hasCustomerReference":false,"ticketReservation":{"id":"56f440a3-03c5-403c-b73a-80cc575cfe89","validity":"2020-02-23T05:59:00.000+0000","status":"COMPLETE","fullName":"Alejandro Castro","firstName":"Alejandro","lastName":"Castro","email":"alejandro.castro@correo.com","billingAddress":null,"confirmationTimestamp":"2020-02-21T17:33:28.196Z","latestReminder":null,"paymentMethod":"ADMIN","reminderSent":false,"promoCodeDiscountId":null,"automatic":false,"userLanguage":"es","directAssignmentRequested":false,"invoiceNumber":null,"vatStatus":null,"vatNr":null,"vatCountryCode":null,"invoiceRequested":false,"usedVatPercent":0.00,"vatIncluded":false,"creationTimestamp":"2020-02-21T17:33:28.14Z","customerReference":null,"registrationTimestamp":"2020-02-21T17:33:28.212Z","srcPriceCts":0,"finalPriceCts":0,"vatCts":0,"discountCts":0,"currencyCode":"USD","cancelled":false,"hasInvoiceNumber":false,"pendingOfflinePayment":false,"hasBeenPaid":false,"hasVatNumber":false,"stuck":false,"lineSplittedBillingAddress":[],"hasBillingAddress":false,"hasInvoiceOrReceiptDocument":false,"finalPrice":0},"hasRefund":false,"hasVat":false,"orderSummary":{"originalTotalPrice":{"priceWithVAT":1500,"vat":0,"discount":0,"discountAppliedCount":0,"currencyCode":"USD"},"summary":[{"name":"Simphonyc Concert","price":"15.00","priceBeforeVat":"15.00","amount":1,"subTotal":"15.00","subTotalBeforeVat":"15.00","originalSubTotal":1500,"type":"TICKET","descriptionForPayment":"1 x Simphonyc Concert"}],"free":false,"totalPrice":"15.00","totalVAT":"0","waitingForPayment":false,"deferredPayment":false,"cashPayment":false,"vatPercentage":"0","vatStatus":null,"refundedAmount":null,"singleTicketOrder":true,"descriptionForPayment":"1 x Simphonyc Concert","priceInCents":1500,"ticketAmount":1,"displayVat":true,"totalNetPrice":"15.00","notYetPaid":false,"vatExempt":false},"locale":"es","hasInvoiceAddress":false,"isOfflinePayment":false,"euBusiness":false,"paymentReason":"svconcert-epica 56F440A3","organization":{"id":1,"name":"SVConcert","description":"This is a test organization","email":"info@svconcert.com"},"now":"2020-02-21T11:35:06.7571902-06:00","hasBankAccountNr":false,"event":{"id":1,"organizationId":1,"type":"INTERNAL","shortName":"svconcert-epica","displayName":"EPICA Central America Tour","websiteUrl":"https://svconcert.com/epica","externalUrl":null,"termsAndConditionsUrl":"https://svconcert.com/epica/terms","privacyPolicyUrl":"https://svconcert.com/epica/terms/privacy","imageUrl":null,"fileBlobId":"5eda22f96956834c4ba6dd17fca559427718daf266779d91d0de8dd5b04a7ca2","location":"Centro de Ferias y Convenciones (CIFCO), pabello #8","latitude":null,"longitude":null,"begin":"2020-03-31T07:00:00-06:00","end":"2020-03-31T12:00:00-06:00","currency":"USD","vatIncluded":false,"allowedPaymentProxies":["ON_SITE"],"timeZone":"America/El_Salvador","locales":256,"srcPriceCts":1500,"vatStatus":"NOT_INCLUDED","version":"202.2.0.0.16","status":"PUBLIC","sameDay":true,"imageIsPresent":true,"regularPrice":15.00,"contentLanguages":[{"locale":"es","value":256,"language":"es","displayLanguage":"español"}],"useFirstAndLastName":true,"multiplePaymentMethods":false,"beginTimeZoneOffset":-21600,"endTimeZoneOffset":-21600,"firstPaymentMethod":"ON_SITE","fileBlobIdIsPresent":true,"freeOfCharge":false,"free":false,"internal":true,"privacyPolicyLinkOrNull":"https://svconcert.com/epica/terms/privacy"},"reservationUrl":"ticket/event/svconcert-epica/reservation/56f440a3-03c5-403c-b73a-80cc575cfe89?lang=es","publicId":"56f440a3-03c5-403c-b73a-80cc575cfe89","expirationDate":"2020-02-22T23:59:00-06:00","hasBankAccountOnwer":false}	2020-02-21 11:35:06.772-06	\N	VALID	1
2	1	6d62e44e-d94b-4475-bf1f-e223827972d6	1a71e32b-35c3-4089-b249-6e7528ef1a26	RECEIPT	{"confirmationDate":"2020-02-21T11:55:39.572-06:00","vatNr":"","invoicingAdditionalInfo":{"italianEInvoicing":null,"empty":true},"tickets":[{"category":{"id":1,"utcInception":"2020-02-21T17:00:00Z","utcExpiration":"2020-03-31T13:00:00Z","maxTickets":0,"name":"Simphonyc Concert","accessRestricted":false,"status":"ACTIVE","eventId":1,"bounded":false,"srcPriceCts":1500,"code":"3p1c@","validCheckInFrom":null,"validCheckInTo":null,"ticketValidityStart":null,"ticketValidityEnd":null,"currencyCode":"USD","ordinal":1,"ticketCheckInStrategy":"ONCE_PER_EVENT","price":15.00,"free":false},"id":1,"fullName":"Alejandro Castro","currencyCode":"USD","status":"ACQUIRED","formattedFinalPrice":"15.00","extReference":null,"creation":"2020-02-21T17:26:20.814Z","formattedNetPrice":"15.00","checkedIn":false,"categoryName":"Simphonyc Concert","srcPriceCts":1500,"firstName":"Alejandro","email":"jandor.ac@gmail.com","lastName":"Castro","userLanguage":"es","vatCts":0,"finalPriceCts":1500,"discountCts":0,"uuid":"1d18980c-974e-4554-b452-44bb6079e7d3","categoryId":1,"assigned":true,"eventId":1,"lockedAssignment":false,"ticketsReservationId":"1a71e32b-35c3-4089-b249-6e7528ef1a26"}],"reservationShortID":"1A71E32B","hasCustomerReference":false,"ticketReservation":{"id":"1a71e32b-35c3-4089-b249-6e7528ef1a26","validity":"2020-02-26T14:00:00.000+0000","status":"COMPLETE","fullName":"Alejandro Castro","firstName":"Alejandro","lastName":"Castro","email":"jandor.ac@gmail.com","billingAddress":"Alejandro Castro","confirmationTimestamp":"2020-02-21T17:56:28.883Z","latestReminder":null,"paymentMethod":"OFFLINE","reminderSent":false,"promoCodeDiscountId":null,"automatic":false,"userLanguage":"es","directAssignmentRequested":false,"invoiceNumber":null,"vatStatus":"NOT_INCLUDED","vatNr":null,"vatCountryCode":null,"invoiceRequested":false,"usedVatPercent":0.00,"vatIncluded":false,"creationTimestamp":"2020-02-21T17:46:22.169Z","customerReference":null,"registrationTimestamp":"2020-02-21T17:55:39.572Z","srcPriceCts":1500,"finalPriceCts":1500,"vatCts":0,"discountCts":0,"currencyCode":"USD","cancelled":false,"hasInvoiceNumber":false,"pendingOfflinePayment":false,"hasBeenPaid":true,"hasVatNumber":false,"stuck":false,"lineSplittedBillingAddress":["Alejandro Castro"],"hasBillingAddress":true,"hasInvoiceOrReceiptDocument":false,"finalPrice":15.00},"hasRefund":false,"hasVat":false,"orderSummary":{"originalTotalPrice":{"priceWithVAT":1500,"vat":0,"discount":0,"discountAppliedCount":0,"currencyCode":"USD"},"summary":[{"name":"Simphonyc Concert","price":"15.00","priceBeforeVat":"15.00","amount":1,"subTotal":"15.00","subTotalBeforeVat":"15.00","originalSubTotal":1500,"type":"TICKET","descriptionForPayment":"1 x Simphonyc Concert"}],"free":false,"totalPrice":"15.00","totalVAT":"0","waitingForPayment":false,"deferredPayment":false,"cashPayment":false,"vatPercentage":"0","vatStatus":"NOT_INCLUDED","refundedAmount":null,"singleTicketOrder":true,"descriptionForPayment":"1 x Simphonyc Concert","priceInCents":1500,"ticketAmount":1,"displayVat":true,"totalNetPrice":"15.00","notYetPaid":false,"vatExempt":false},"locale":"es","hasInvoiceAddress":false,"isOfflinePayment":false,"euBusiness":false,"paymentReason":"svconcert-epica 1A71E32B","organization":{"id":1,"name":"SVConcert","description":"This is a test organization","email":"info@svconcert.com"},"now":"2020-02-21T11:56:28.9692774-06:00","hasBankAccountNr":false,"event":{"id":1,"organizationId":1,"type":"INTERNAL","shortName":"svconcert-epica","displayName":"EPICA Central America Tour","websiteUrl":"https://svconcert.com/epica","externalUrl":null,"termsAndConditionsUrl":"https://svconcert.com/epica/terms","privacyPolicyUrl":"https://svconcert.com/epica/terms/privacy","imageUrl":null,"fileBlobId":"5eda22f96956834c4ba6dd17fca559427718daf266779d91d0de8dd5b04a7ca2","location":"Centro de Ferias y Convenciones (CIFCO), pabello #8","latitude":null,"longitude":null,"begin":"2020-03-31T07:00:00-06:00","end":"2020-03-31T12:00:00-06:00","currency":"USD","vatIncluded":false,"allowedPaymentProxies":["ON_SITE","OFFLINE"],"timeZone":"America/El_Salvador","locales":256,"srcPriceCts":1500,"vatStatus":"NOT_INCLUDED","version":"202.2.0.0.16","status":"PUBLIC","sameDay":true,"imageIsPresent":true,"regularPrice":15.00,"contentLanguages":[{"locale":"es","value":256,"language":"es","displayLanguage":"español"}],"useFirstAndLastName":true,"multiplePaymentMethods":true,"beginTimeZoneOffset":-21600,"endTimeZoneOffset":-21600,"firstPaymentMethod":"ON_SITE","fileBlobIdIsPresent":true,"freeOfCharge":false,"free":false,"internal":true,"privacyPolicyLinkOrNull":"https://svconcert.com/epica/terms/privacy"},"reservationUrl":"ticket/event/svconcert-epica/reservation/1a71e32b-35c3-4089-b249-6e7528ef1a26?lang=es","publicId":"1a71e32b-35c3-4089-b249-6e7528ef1a26","expirationDate":"2020-02-26T08:00:00-06:00","hasBankAccountOnwer":false}	2020-02-21 11:56:28.972-06	\N	VALID	1
3	1	7c339cff-ff3f-4dc1-ae22-8449480ea93f	1a71e32b-35c3-4089-b249-6e7528ef1a26	RECEIPT	{"confirmationDate":"2020-02-21T11:55:39.572-06:00","vatNr":"","invoicingAdditionalInfo":{"italianEInvoicing":null,"empty":true},"tickets":[{"category":{"id":1,"utcInception":"2020-02-21T17:00:00Z","utcExpiration":"2020-03-31T13:00:00Z","maxTickets":0,"name":"Simphonyc Concert","accessRestricted":false,"status":"ACTIVE","eventId":1,"bounded":false,"srcPriceCts":1500,"code":"3p1c@","validCheckInFrom":null,"validCheckInTo":null,"ticketValidityStart":null,"ticketValidityEnd":null,"currencyCode":"USD","ordinal":1,"ticketCheckInStrategy":"ONCE_PER_EVENT","price":15.00,"free":false},"id":1,"fullName":"Alejandro Castro","currencyCode":"USD","status":"ACQUIRED","formattedFinalPrice":"15.00","extReference":null,"creation":"2020-02-21T17:26:20.814Z","formattedNetPrice":"15.00","checkedIn":false,"categoryName":"Simphonyc Concert","srcPriceCts":1500,"firstName":"Alejandro","email":"jandor.ac@gmail.com","lastName":"Castro","userLanguage":"es","vatCts":0,"finalPriceCts":1500,"discountCts":0,"uuid":"1d18980c-974e-4554-b452-44bb6079e7d3","categoryId":1,"assigned":true,"eventId":1,"lockedAssignment":false,"ticketsReservationId":"1a71e32b-35c3-4089-b249-6e7528ef1a26"}],"reservationShortID":"1A71E32B","hasCustomerReference":false,"ticketReservation":{"id":"1a71e32b-35c3-4089-b249-6e7528ef1a26","validity":"2020-02-26T14:00:00.000+0000","status":"COMPLETE","fullName":"Alejandro Castro","firstName":"Alejandro","lastName":"Castro","email":"jandor.ac@gmail.com","billingAddress":"Alejandro Castro","confirmationTimestamp":"2020-02-21T17:56:28.883Z","latestReminder":null,"paymentMethod":"OFFLINE","reminderSent":false,"promoCodeDiscountId":null,"automatic":false,"userLanguage":"es","directAssignmentRequested":false,"invoiceNumber":null,"vatStatus":"NOT_INCLUDED","vatNr":null,"vatCountryCode":null,"invoiceRequested":false,"usedVatPercent":0.00,"vatIncluded":false,"creationTimestamp":"2020-02-21T17:46:22.169Z","customerReference":null,"registrationTimestamp":"2020-02-21T17:55:39.572Z","srcPriceCts":1500,"finalPriceCts":1500,"vatCts":0,"discountCts":0,"currencyCode":"USD","cancelled":false,"hasInvoiceNumber":false,"pendingOfflinePayment":false,"hasBeenPaid":true,"hasVatNumber":false,"stuck":false,"lineSplittedBillingAddress":["Alejandro Castro"],"hasBillingAddress":true,"hasInvoiceOrReceiptDocument":true,"finalPrice":15.00},"hasRefund":false,"hasVat":false,"orderSummary":{"originalTotalPrice":{"priceWithVAT":1500,"vat":0,"discount":0,"discountAppliedCount":0,"currencyCode":"USD"},"summary":[{"name":"Simphonyc Concert","price":"15.00","priceBeforeVat":"15.00","amount":1,"subTotal":"15.00","subTotalBeforeVat":"15.00","originalSubTotal":1500,"type":"TICKET","descriptionForPayment":"1 x Simphonyc Concert"}],"free":false,"totalPrice":"15.00","totalVAT":"0","waitingForPayment":false,"deferredPayment":false,"cashPayment":false,"vatPercentage":"0","vatStatus":"NOT_INCLUDED","refundedAmount":null,"singleTicketOrder":true,"descriptionForPayment":"1 x Simphonyc Concert","priceInCents":1500,"ticketAmount":1,"displayVat":true,"totalNetPrice":"15.00","notYetPaid":false,"vatExempt":false},"locale":"es","hasInvoiceAddress":false,"isOfflinePayment":false,"euBusiness":false,"paymentReason":"svconcert-epica 1A71E32B","organization":{"id":1,"name":"SVConcert","description":"This is a test organization","email":"info@svconcert.com"},"now":"2020-02-21T11:56:57.5912311-06:00","hasBankAccountNr":false,"event":{"id":1,"organizationId":1,"type":"INTERNAL","shortName":"svconcert-epica","displayName":"EPICA Central America Tour","websiteUrl":"https://svconcert.com/epica","externalUrl":null,"termsAndConditionsUrl":"https://svconcert.com/epica/terms","privacyPolicyUrl":"https://svconcert.com/epica/terms/privacy","imageUrl":null,"fileBlobId":"5eda22f96956834c4ba6dd17fca559427718daf266779d91d0de8dd5b04a7ca2","location":"Centro de Ferias y Convenciones (CIFCO), pabello #8","latitude":null,"longitude":null,"begin":"2020-03-31T07:00:00-06:00","end":"2020-03-31T12:00:00-06:00","currency":"USD","vatIncluded":false,"allowedPaymentProxies":["ON_SITE","OFFLINE"],"timeZone":"America/El_Salvador","locales":256,"srcPriceCts":1500,"vatStatus":"NOT_INCLUDED","version":"202.2.0.0.16","status":"PUBLIC","sameDay":true,"imageIsPresent":true,"regularPrice":15.00,"contentLanguages":[{"locale":"es","value":256,"language":"es","displayLanguage":"español"}],"useFirstAndLastName":true,"multiplePaymentMethods":true,"beginTimeZoneOffset":-21600,"endTimeZoneOffset":-21600,"firstPaymentMethod":"ON_SITE","fileBlobIdIsPresent":true,"freeOfCharge":false,"free":false,"internal":true,"privacyPolicyLinkOrNull":"https://svconcert.com/epica/terms/privacy"},"reservationUrl":"ticket/event/svconcert-epica/reservation/1a71e32b-35c3-4089-b249-6e7528ef1a26?lang=es","publicId":"1a71e32b-35c3-4089-b249-6e7528ef1a26","expirationDate":"2020-02-26T08:00:00-06:00","hasBankAccountOnwer":false}	2020-02-21 11:56:57.593-06	\N	VALID	1
\.


--
-- Data for Name: configuration; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.configuration (id, c_key, c_value, description) FROM stdin;
4	EU_COUNTRIES_LIST	BE,EL,LT,PT,BG,ES,LU,RO,CZ,FR,HU,SI,DK,HR,MT,SK,DE,IT,NL,FI,EE,CY,AT,SE,IE,LV,PL,GB	
9	INIT_COMPLETED	true	init succeeded
8	STRIPE_CC_ENABLED	true	Stripe Credit Cards Enabled
5	BANK_TRANSFER_ENABLED	true	Bank Transfer Enabled
7	ON_SITE_ENABLED	true	On Site cash payment Enabled
6	PAYPAL_ENABLED	true	PayPal Enabled
12	MAILER_TYPE	disabled	Mailer type (if not set, default will be disabled)
13	SUPPORTED_LANGUAGES	256	supported languages
14	BASE_URL	ticket	Base application url
2	SPECIAL_PRICE_CODE_LENGTH	6	Length of special price code
1	MAX_AMOUNT_OF_TICKETS_BY_RESERVATION	5	Max amount of tickets
11	GOOGLE_ANALYTICS_ANONYMOUS_MODE	true	Run Google Analytics without cookies and scrambling the client IP address (default true)
15	MAPS_PROVIDER	NONE	Select the maps provider (None, Google, Here)
10	MAPS_CLIENT_API_KEY	<Google Maps Client Key>	Google maps' client api key
\.


--
-- Data for Name: configuration_event; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.configuration_event (id, organization_id_fk, event_id_fk, c_key, c_value, description) FROM stdin;
23	1	4	DISPLAY_TICKETS_LEFT_INDICATOR	true	Display how many tickets are left for the event (default false)
20	1	4	ENABLE_WAITING_QUEUE	true	Enable waiting list in case of sold-out (default: false)
21	1	4	ENABLE_PRE_REGISTRATION	true	Enable pre-registration (default false)
22	1	4	LABEL_LAYOUT	{\n  "qrCode": {\n    "additionalInfo": [],\n    "infoSeparator": "::"\n  },\n  "content": {\n    "firstRow": "firstName",\n    "secondRow": "lastName",\n    "additionalRows": [],\n    "checkbox": false\n  },\n  "general": {\n    "printPartialID": true\n  }\n}	Label layout
1	1	1	CHECK_IN_COLOR_CONFIGURATION	{"defaultColorName":"success","configurations":[{"colorName":"dark","categories":[1]},{"colorName":"success","categories":[5]}]}	\N
16	1	3	DISPLAY_TICKETS_LEFT_INDICATOR	true	Display how many tickets are left for the event (default false)
17	1	3	LABEL_LAYOUT	{\n  "qrCode": {\n    "additionalInfo": [],\n    "infoSeparator": "::"\n  },\n  "content": {\n    "firstRow": "firstName",\n    "secondRow": "lastName",\n    "additionalRows": [],\n    "checkbox": false\n  },\n  "general": {\n    "printPartialID": true\n  }\n}	Label layout
9	1	1	MAILER_TYPE	disabled	Mailer type (if not set, default will be disabled)
5	1	1	DISPLAY_TICKETS_LEFT_INDICATOR	true	Display how many tickets are left for the event (default false)
7	1	1	STRIPE_CC_ENABLED	false	Stripe enabled
8	1	1	STRIPE_ENABLE_SCA	false	Enable Strong Customer Authentication
18	1	1	ENABLE_WAITING_QUEUE	true	Enable waiting list in case of sold-out (default: false)
19	1	1	ENABLE_PRE_REGISTRATION	true	Enable pre-registration (default false)
2	1	1	BANK_TRANSFER_ENABLED	true	Bank transfer enabled
3	1	1	PLATFORM_MODE_ENABLED	true	Enable Platform mode
4	1	1	ON_SITE_ENABLED	true	On site enabled
6	1	1	LABEL_LAYOUT	{\n  "qrCode": {\n    "additionalInfo": ["dui"],\n    "infoSeparator": "::"\n  },\n  "content": {\n    "firstRow": "firstName",\n    "secondRow": "lastName",\n    "additionalRows": [],\n    "checkbox": false\n  },\n  "general": {\n    "printPartialID": true\n  }\n}	Label layout
\.


--
-- Data for Name: configuration_organization; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.configuration_organization (id, organization_id_fk, c_key, c_value, description) FROM stdin;
1	1	ON_SITE_ENABLED	true	On site enabled
\.


--
-- Data for Name: configuration_ticket_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.configuration_ticket_category (id, organization_id_fk, event_id_fk, ticket_category_id_fk, c_key, c_value, description) FROM stdin;
\.


--
-- Data for Name: dynamic_field_template; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.dynamic_field_template (id, field_name, field_type, field_restricted_values, field_description, field_maxlength, field_minlength) FROM stdin;
1	jobTitle	input:text	\N	{"en":{"label": "Job title"},"it":{"label": "Ruolo"}, "de":{"label": "Berufsbezeichnung"}, "nl":{"label": "Functie"}}	255	0
2	phoneNumber	input:tel	\N	{"en":{"label": "Phone number"},"it":{"label": "Numero di telefono"}, "de":{"label": "Telefonnummer"}, "nl":{"label": "Telefoon nummer"}}	255	0
3	company	input:text	\N	{"en":{"label": "Company"},"it":{"label": "Societ\\u00E0"}, "de":{"label": "Firma"}, "nl":{"label": "Bedrijf"}}	255	0
4	address	textarea	\N	{"en":{"label": "Address"},"it":{"label": "Indirizzo"}, "de":{"label": "Adresse"}, "nl":{"label": "Adres"}}	450	0
5	country	country	\N	{"en":{"label": "Country"},"it":{"label": "Nazione"}, "de":{"label": "Land"}, "nl":{"label": "Land"}}	255	0
6	notes	textarea	\N	{"en":{"label": "Notes", "placeholder" : "Should you have any special need (such as but not limited to diet or accessibility requirements) please let us know"},"it":{"label": "Note", "placeholder" : "Hai qualche esigenza particolare (ad esempio per quanto riguarda cibo o accessibilit\\u00E0)? Per favore, faccelo sapere!"}, "de":{"label": "Bemerkungen", "placeholder" : "Gerne nehmen wir auf Ihre Bed\\u00FCrfnisse R\\u00FCcksicht, falls du aussergew\\u00F6hnliche Essensgewohnheiten oder eingeschr\\u00E4nkte Zutrittsm\\u00F6glichkeiten hast, informiere uns bitte."}, "nl":{"label": "Notities", "placeholder" : "Heeft uw een speciale behoefte (bijvoorbeeld mindervalidetoegang) laat het ons weten"}}	1024	0
7	gender	select	["F", "M", "X"]	{"en":{"label": "I am", "restrictedValues": {"F" : "a woman", "M" : "a man", "X" : "Other"}},"it":{"label": "Sono", "restrictedValues": {"F" : "una donna", "M" : "un uomo", "X" : "Altro"}}, "de":{"label": "Ich bin", "restrictedValues": {"F" : "Frau", "M" : "Herr", "X" : "Andere"}}, "nl":{"label": "Ik ben een", "restrictedValues": {"F" : "Vrouw", "M" : "Man", "X" : "Overig"}}}	\N	\N
8	tShirtSize	select	["SMALL", "MEDIUM", "LARGE", "X-LARGE", "2X-LARGE"]	{"en":{"label": "T-Shirt size", "restrictedValues": {"SMALL" : "Small", "MEDIUM" : "Medium", "LARGE" : "Large", "X-LARGE" : "X-Large", "2X-LARGE" : "2X-Large" }},"it":{"label": "Misura T-Shirt", "restrictedValues": {"SMALL" : "Small", "MEDIUM" : "Medium", "LARGE" : "Large", "X-LARGE" : "X-Large", "2X-LARGE" : "2X-Large" }}, "de":{"label": "T-Shirt-Gr\\u00F6\\u00DFe", "restrictedValues": {"SMALL" : "Small", "MEDIUM" : "Medium", "LARGE" : "Large", "X-LARGE" : "X-Large", "2X-LARGE" : "2X-Large" }}, "nl":{"label": "T-Shirt maat", "restrictedValues": {"SMALL" : "Small", "MEDIUM" : "Medium", "LARGE" : "Large", "X-LARGE" : "X-Large", "2X-LARGE" : "2X-Large" }}}	\N	\N
\.


--
-- Data for Name: email_message; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.email_message (id, event_id, status, recipient, subject, message, attachments, checksum, request_ts, sent_ts, attempts, email_cc, organization_id_fk, reservation_id) FROM stdin;
2	1	SENT	info@svconcert.com	Reservation complete BF6E63DF	Alejandro Castro<alejandro.castro@correo.com> has completed the reservation BF6E63DF for event EPICA Central America Tour\r\n\r\nCategory: Simphonyc Concert, Quantity: 2, Subtotal: 30.00 USD, Payment Method: ON_SITE\r\n\r\nVAT 0.00%: 0 USD\r\n\r\nTotal: 30.00 USD \r\n\r\n\r\n\r\nReservation id: bf6e63df-da4d-4735-964b-46ab6bb6d6f3.\r\n\r\n\r\nTicket identifier:\r\n - a62c7416-7b55-4566-a7a4-01e606c1aa62\r\n - b677288a-c5b9-480f-8ce9-17ac2b31ef3e\r\n	\N	218d09e4d00f651ced7e77c73d006a5ebd85d099e0955052f2777463e219599d	2020-02-21 11:42:52.07-06	2020-02-21 11:42:55.467-06	0	[]	1	\N
3	1	SENT	jandor.ac@gmail.com	Su reserva n. 1A71E32B para el evento EPICA Central America Tour	Hola Alejandro Castro,\r\n\r\n\r\nSu pedido para el evento EPICA Central America Tour ha sido realizado. Por favor, siga las instrucciones de pago en ticket/event/svconcert-epica/reservation/1a71e32b-35c3-4089-b249-6e7528ef1a26?lang=es\r\n\r\n#### Resumen de reserva ####\r\n\r\nCategoría: Simphonyc Concert, Cantidad: 1, Subtotal: 15.00 USD\r\n\r\nIVA del 0.00%: 0 USD\r\nTotal 15.00 USD\r\n\r\n\r\nATENCIÓN: Debe pagar la cantidad de 15.00 USD para asistir al evento\r\n\r\n\r\n\r\nInfo. del pedido: 1a71e32b-35c3-4089-b249-6e7528ef1a26\r\n\r\n#### Info. del evento ####\r\n\r\nComo recordatorio, el evento tendrá lugar en:\r\n\r\n Centro de Ferias y Convenciones (CIFCO), pabello #8 \r\n\r\nel:\r\n\r\n martes 31 marzo 2020  de  07:00 a  12:00 (CST)\r\n\r\n\r\nSaludos,\r\n\r\nSVConcert <info@svconcert.com>	\N	fc1c814aa749fc166ca4f2f0bcdbf558724e8cfc5dcc959c1ca5617a1e04ec2b	2020-02-21 11:55:39.597-06	2020-02-21 11:55:40.461-06	0	[]	1	1a71e32b-35c3-4089-b249-6e7528ef1a26
1	1	SENT	alejandro.castro@correo.com	Su reserva n. BF6E63DF para el evento EPICA Central America Tour	Hola Alejandro Castro,\r\n\r\nSu reserva para el evento EPICA Central America Tour ha sido completada. Puede modificarla o revisarla en ticket/event/svconcert-epica/reservation/bf6e63df-da4d-4735-964b-46ab6bb6d6f3?lang=es\r\n\r\n\r\n#### Resumen de reserva ####\r\n\r\nCategoría: Simphonyc Concert, Cantidad: 2, Subtotal: 30.00 USD\r\n\r\nIVA del 0.00%: 0 USD\r\nTotal 30.00 USD\r\n\r\n\r\nATENCIÓN: Debe pagar la cantidad de 30.00 USD para asistir al evento\r\n\r\n\r\n\r\nInfo. del pedido: bf6e63df-da4d-4735-964b-46ab6bb6d6f3\r\n\r\n#### Info. del evento ####\r\n\r\nComo recordatorio, el evento tendrá lugar en:\r\n\r\n Centro de Ferias y Convenciones (CIFCO), pabello #8 \r\n\r\nel:\r\n\r\n martes 31 marzo 2020  de  07:00 a  12:00 (CST)\r\n\r\n\r\nSaludos,\r\n\r\nSVConcert <info@svconcert.com>	\N	5f259d619b0fc129a297221a87d54394b1e9923fd48d55c8a62704fe3407f742	2020-02-21 11:42:52.048-06	2020-02-21 11:42:55.471-06	0	[]	1	bf6e63df-da4d-4735-964b-46ab6bb6d6f3
4	1	SENT	info@svconcert.com	Reservation complete 1A71E32B	Alejandro Castro<jandor.ac@gmail.com> has completed the reservation 1A71E32B for event EPICA Central America Tour\r\n\r\nCategory: Simphonyc Concert, Quantity: 1, Subtotal: 15.00 USD, Payment Method: OFFLINE\r\n\r\nVAT 0.00%: 0 USD\r\n\r\nTotal: 15.00 USD \r\n\r\n\r\n\r\nReservation id: 1a71e32b-35c3-4089-b249-6e7528ef1a26.\r\n\r\n\r\nTicket identifier:\r\n - 1d18980c-974e-4554-b452-44bb6079e7d3\r\n	\N	1c0004182fc14b4bcc3d186a72d6d8fd2af3122b2c4d9d91ec4cfa6adc1925c3	2020-02-21 11:55:39.615-06	2020-02-21 11:55:40.457-06	0	[]	1	\N
11	1	SENT	info@svconcert.com	Reservation complete 11F71518	Carlos Castro<jandor.ac@gmail.com> has completed the reservation 11F71518 for event EPICA Central America Tour\r\n\r\nCategory: Simphonyc Concert, Quantity: 3, Subtotal: 45.00 USD, Payment Method: ON_SITECategory: General, Quantity: 3, Subtotal: 30.00 USD, Payment Method: ON_SITE\r\n\r\nVAT 0.00%: 0 USD\r\n\r\nTotal: 75.00 USD \r\n\r\n\r\n\r\nReservation id: 11f71518-4ea4-4a8e-813c-85312308602d.\r\n\r\n\r\nTicket identifier:\r\n - 264b44ea-820f-46de-b5f2-5c3fb89db02e\r\n - 311f332c-f834-4c5d-940d-f2a5a9bfe6a2\r\n - 789a94c5-6718-4565-8ebf-d3ce368b3a7f\r\n	\N	355020094a7208f6c3bb7414a87c26fdfa7eb8fe2974d4789f306a8e0ac75882	2020-03-08 20:23:26.441-06	2020-03-08 20:23:31.02-06	0	[]	1	\N
6	1	SENT	jandor.ac@gmail.com	Su reserva n. 1A71E32B para el evento EPICA Central America Tour	Hola Alejandro Castro,\r\n\r\nSu reserva para el evento EPICA Central America Tour ha sido completada. Puede modificarla o revisarla en ticket/event/svconcert-epica/reservation/1a71e32b-35c3-4089-b249-6e7528ef1a26?lang=es\r\n\r\n\r\n#### Resumen de reserva ####\r\n\r\nCategoría: Simphonyc Concert, Cantidad: 1, Subtotal: 15.00 USD\r\n\r\nIVA del 0.00%: 0 USD\r\nTotal 15.00 USD\r\n\r\n\r\n\r\n\r\n\r\n\r\nInfo. del pedido: 1a71e32b-35c3-4089-b249-6e7528ef1a26\r\n\r\n#### Info. del evento ####\r\n\r\nComo recordatorio, el evento tendrá lugar en:\r\n\r\n Centro de Ferias y Convenciones (CIFCO), pabello #8 \r\n\r\nel:\r\n\r\n martes 31 marzo 2020  de  07:00 a  12:00 (CST)\r\n\r\n\r\nSaludos,\r\n\r\nSVConcert <info@svconcert.com>	[{"filename":"receipt.pdf","contentType":"application/pdf","identifier":"RECEIPT_PDF","model":"{\\"eventId\\":\\"1\\",\\"reservationId\\":\\"1a71e32b-35c3-4089-b249-6e7528ef1a26\\",\\"language\\":\\"\\\\\\"es\\\\\\"\\",\\"reservationEmailModel\\":\\"{\\\\\\"confirmationDate\\\\\\":\\\\\\"2020-02-21T11:55:39.572-06:00\\\\\\",\\\\\\"vatNr\\\\\\":\\\\\\"\\\\\\",\\\\\\"invoicingAdditionalInfo\\\\\\":{\\\\\\"italianEInvoicing\\\\\\":null,\\\\\\"empty\\\\\\":true},\\\\\\"tickets\\\\\\":[{\\\\\\"category\\\\\\":{\\\\\\"id\\\\\\":1,\\\\\\"utcInception\\\\\\":\\\\\\"2020-02-21T17:00:00Z\\\\\\",\\\\\\"utcExpiration\\\\\\":\\\\\\"2020-03-31T13:00:00Z\\\\\\",\\\\\\"maxTickets\\\\\\":0,\\\\\\"name\\\\\\":\\\\\\"Simphonyc Concert\\\\\\",\\\\\\"accessRestricted\\\\\\":false,\\\\\\"status\\\\\\":\\\\\\"ACTIVE\\\\\\",\\\\\\"eventId\\\\\\":1,\\\\\\"bounded\\\\\\":false,\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"code\\\\\\":\\\\\\"3p1c@\\\\\\",\\\\\\"validCheckInFrom\\\\\\":null,\\\\\\"validCheckInTo\\\\\\":null,\\\\\\"ticketValidityStart\\\\\\":null,\\\\\\"ticketValidityEnd\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"ordinal\\\\\\":1,\\\\\\"ticketCheckInStrategy\\\\\\":\\\\\\"ONCE_PER_EVENT\\\\\\",\\\\\\"price\\\\\\":15.0,\\\\\\"free\\\\\\":false},\\\\\\"id\\\\\\":1,\\\\\\"fullName\\\\\\":\\\\\\"Alejandro Castro\\\\\\",\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"status\\\\\\":\\\\\\"ACQUIRED\\\\\\",\\\\\\"formattedFinalPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"extReference\\\\\\":null,\\\\\\"creation\\\\\\":\\\\\\"2020-02-21T17:26:20.814Z\\\\\\",\\\\\\"formattedNetPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"checkedIn\\\\\\":false,\\\\\\"categoryName\\\\\\":\\\\\\"Simphonyc Concert\\\\\\",\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"firstName\\\\\\":\\\\\\"Alejandro\\\\\\",\\\\\\"email\\\\\\":\\\\\\"jandor.ac@gmail.com\\\\\\",\\\\\\"lastName\\\\\\":\\\\\\"Castro\\\\\\",\\\\\\"userLanguage\\\\\\":\\\\\\"es\\\\\\",\\\\\\"vatCts\\\\\\":0,\\\\\\"finalPriceCts\\\\\\":1500,\\\\\\"discountCts\\\\\\":0,\\\\\\"uuid\\\\\\":\\\\\\"1d18980c-974e-4554-b452-44bb6079e7d3\\\\\\",\\\\\\"categoryId\\\\\\":1,\\\\\\"assigned\\\\\\":true,\\\\\\"eventId\\\\\\":1,\\\\\\"lockedAssignment\\\\\\":false,\\\\\\"ticketsReservationId\\\\\\":\\\\\\"1a71e32b-35c3-4089-b249-6e7528ef1a26\\\\\\"}],\\\\\\"reservationShortID\\\\\\":\\\\\\"1A71E32B\\\\\\",\\\\\\"hasCustomerReference\\\\\\":false,\\\\\\"ticketReservation\\\\\\":{\\\\\\"id\\\\\\":\\\\\\"1a71e32b-35c3-4089-b249-6e7528ef1a26\\\\\\",\\\\\\"validity\\\\\\":\\\\\\"2020-02-26T14:00:00.000+0000\\\\\\",\\\\\\"status\\\\\\":\\\\\\"COMPLETE\\\\\\",\\\\\\"fullName\\\\\\":\\\\\\"Alejandro Castro\\\\\\",\\\\\\"firstName\\\\\\":\\\\\\"Alejandro\\\\\\",\\\\\\"lastName\\\\\\":\\\\\\"Castro\\\\\\",\\\\\\"email\\\\\\":\\\\\\"jandor.ac@gmail.com\\\\\\",\\\\\\"billingAddress\\\\\\":\\\\\\"Alejandro Castro\\\\\\",\\\\\\"confirmationTimestamp\\\\\\":\\\\\\"2020-02-21T17:56:28.883Z\\\\\\",\\\\\\"latestReminder\\\\\\":null,\\\\\\"paymentMethod\\\\\\":\\\\\\"OFFLINE\\\\\\",\\\\\\"reminderSent\\\\\\":false,\\\\\\"promoCodeDiscountId\\\\\\":null,\\\\\\"automatic\\\\\\":false,\\\\\\"userLanguage\\\\\\":\\\\\\"es\\\\\\",\\\\\\"directAssignmentRequested\\\\\\":false,\\\\\\"invoiceNumber\\\\\\":null,\\\\\\"vatStatus\\\\\\":\\\\\\"NOT_INCLUDED\\\\\\",\\\\\\"vatNr\\\\\\":null,\\\\\\"vatCountryCode\\\\\\":null,\\\\\\"invoiceRequested\\\\\\":false,\\\\\\"usedVatPercent\\\\\\":0.0,\\\\\\"vatIncluded\\\\\\":false,\\\\\\"creationTimestamp\\\\\\":\\\\\\"2020-02-21T17:46:22.169Z\\\\\\",\\\\\\"customerReference\\\\\\":null,\\\\\\"registrationTimestamp\\\\\\":\\\\\\"2020-02-21T17:55:39.572Z\\\\\\",\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"finalPriceCts\\\\\\":1500,\\\\\\"vatCts\\\\\\":0,\\\\\\"discountCts\\\\\\":0,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"cancelled\\\\\\":false,\\\\\\"hasInvoiceNumber\\\\\\":false,\\\\\\"pendingOfflinePayment\\\\\\":false,\\\\\\"hasBeenPaid\\\\\\":true,\\\\\\"hasVatNumber\\\\\\":false,\\\\\\"stuck\\\\\\":false,\\\\\\"lineSplittedBillingAddress\\\\\\":[\\\\\\"Alejandro Castro\\\\\\"],\\\\\\"hasBillingAddress\\\\\\":true,\\\\\\"hasInvoiceOrReceiptDocument\\\\\\":false,\\\\\\"finalPrice\\\\\\":15.0},\\\\\\"hasRefund\\\\\\":false,\\\\\\"hasVat\\\\\\":false,\\\\\\"orderSummary\\\\\\":{\\\\\\"originalTotalPrice\\\\\\":{\\\\\\"priceWithVAT\\\\\\":1500,\\\\\\"vat\\\\\\":0,\\\\\\"discount\\\\\\":0,\\\\\\"discountAppliedCount\\\\\\":0,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\"},\\\\\\"summary\\\\\\":[{\\\\\\"name\\\\\\":\\\\\\"Simphonyc Concert\\\\\\",\\\\\\"price\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"priceBeforeVat\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"amount\\\\\\":1,\\\\\\"subTotal\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"subTotalBeforeVat\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"originalSubTotal\\\\\\":1500,\\\\\\"type\\\\\\":\\\\\\"TICKET\\\\\\",\\\\\\"descriptionForPayment\\\\\\":\\\\\\"1 x Simphonyc Concert\\\\\\"}],\\\\\\"free\\\\\\":false,\\\\\\"totalPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"totalVAT\\\\\\":\\\\\\"0\\\\\\",\\\\\\"waitingForPayment\\\\\\":false,\\\\\\"deferredPayment\\\\\\":false,\\\\\\"cashPayment\\\\\\":false,\\\\\\"vatPercentage\\\\\\":\\\\\\"0\\\\\\",\\\\\\"vatStatus\\\\\\":\\\\\\"NOT_INCLUDED\\\\\\",\\\\\\"refundedAmount\\\\\\":null,\\\\\\"singleTicketOrder\\\\\\":true,\\\\\\"descriptionForPayment\\\\\\":\\\\\\"1 x Simphonyc Concert\\\\\\",\\\\\\"priceInCents\\\\\\":1500,\\\\\\"ticketAmount\\\\\\":1,\\\\\\"displayVat\\\\\\":true,\\\\\\"totalNetPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"notYetPaid\\\\\\":false,\\\\\\"vatExempt\\\\\\":false},\\\\\\"locale\\\\\\":\\\\\\"es\\\\\\",\\\\\\"hasInvoiceAddress\\\\\\":false,\\\\\\"isOfflinePayment\\\\\\":false,\\\\\\"euBusiness\\\\\\":false,\\\\\\"paymentReason\\\\\\":\\\\\\"svconcert-epica 1A71E32B\\\\\\",\\\\\\"organization\\\\\\":{\\\\\\"id\\\\\\":1,\\\\\\"name\\\\\\":\\\\\\"SVConcert\\\\\\",\\\\\\"description\\\\\\":\\\\\\"This is a test organization\\\\\\",\\\\\\"email\\\\\\":\\\\\\"info@svconcert.com\\\\\\"},\\\\\\"now\\\\\\":\\\\\\"2020-02-21T11:56:28.9692774-06:00\\\\\\",\\\\\\"hasBankAccountNr\\\\\\":false,\\\\\\"event\\\\\\":{\\\\\\"id\\\\\\":1,\\\\\\"organizationId\\\\\\":1,\\\\\\"type\\\\\\":\\\\\\"INTERNAL\\\\\\",\\\\\\"shortName\\\\\\":\\\\\\"svconcert-epica\\\\\\",\\\\\\"displayName\\\\\\":\\\\\\"EPICA Central America Tour\\\\\\",\\\\\\"websiteUrl\\\\\\":\\\\\\"https://svconcert.com/epica\\\\\\",\\\\\\"externalUrl\\\\\\":null,\\\\\\"termsAndConditionsUrl\\\\\\":\\\\\\"https://svconcert.com/epica/terms\\\\\\",\\\\\\"privacyPolicyUrl\\\\\\":\\\\\\"https://svconcert.com/epica/terms/privacy\\\\\\",\\\\\\"imageUrl\\\\\\":null,\\\\\\"fileBlobId\\\\\\":\\\\\\"5eda22f96956834c4ba6dd17fca559427718daf266779d91d0de8dd5b04a7ca2\\\\\\",\\\\\\"location\\\\\\":\\\\\\"Centro de Ferias y Convenciones (CIFCO), pabello #8\\\\\\",\\\\\\"latitude\\\\\\":null,\\\\\\"longitude\\\\\\":null,\\\\\\"begin\\\\\\":\\\\\\"2020-03-31T07:00:00-06:00\\\\\\",\\\\\\"end\\\\\\":\\\\\\"2020-03-31T12:00:00-06:00\\\\\\",\\\\\\"currency\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"vatIncluded\\\\\\":false,\\\\\\"allowedPaymentProxies\\\\\\":[\\\\\\"ON_SITE\\\\\\",\\\\\\"OFFLINE\\\\\\"],\\\\\\"timeZone\\\\\\":\\\\\\"America/El_Salvador\\\\\\",\\\\\\"locales\\\\\\":256,\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"vatStatus\\\\\\":\\\\\\"NOT_INCLUDED\\\\\\",\\\\\\"version\\\\\\":\\\\\\"202.2.0.0.16\\\\\\",\\\\\\"status\\\\\\":\\\\\\"PUBLIC\\\\\\",\\\\\\"sameDay\\\\\\":true,\\\\\\"imageIsPresent\\\\\\":true,\\\\\\"regularPrice\\\\\\":15.0,\\\\\\"contentLanguages\\\\\\":[{\\\\\\"locale\\\\\\":\\\\\\"es\\\\\\",\\\\\\"value\\\\\\":256,\\\\\\"language\\\\\\":\\\\\\"es\\\\\\",\\\\\\"displayLanguage\\\\\\":\\\\\\"español\\\\\\"}],\\\\\\"useFirstAndLastName\\\\\\":true,\\\\\\"multiplePaymentMethods\\\\\\":true,\\\\\\"beginTimeZoneOffset\\\\\\":-21600,\\\\\\"endTimeZoneOffset\\\\\\":-21600,\\\\\\"firstPaymentMethod\\\\\\":\\\\\\"ON_SITE\\\\\\",\\\\\\"fileBlobIdIsPresent\\\\\\":true,\\\\\\"freeOfCharge\\\\\\":false,\\\\\\"free\\\\\\":false,\\\\\\"internal\\\\\\":true,\\\\\\"privacyPolicyLinkOrNull\\\\\\":\\\\\\"https://svconcert.com/epica/terms/privacy\\\\\\"},\\\\\\"reservationUrl\\\\\\":\\\\\\"ticket/event/svconcert-epica/reservation/1a71e32b-35c3-4089-b249-6e7528ef1a26?lang\\u003des\\\\\\",\\\\\\"publicId\\\\\\":\\\\\\"1a71e32b-35c3-4089-b249-6e7528ef1a26\\\\\\",\\\\\\"expirationDate\\\\\\":\\\\\\"2020-02-26T08:00:00-06:00\\\\\\",\\\\\\"hasBankAccountOnwer\\\\\\":false}\\"}"}]	ad6992968c05944e8baebcb40c6b53895f3b309ae46ee6e780a619e9f304d3f6	2020-02-21 11:56:29.002-06	2020-02-21 11:56:41.66-06	0	[]	1	1a71e32b-35c3-4089-b249-6e7528ef1a26
10	1	SENT	jandor.ac@gmail.com	Su reserva n. 11F71518 para el evento EPICA Central America Tour	Hola Carlos Castro,\r\n\r\nSu reserva para el evento EPICA Central America Tour ha sido completada. Puede modificarla o revisarla en ticket/event/svconcert-epica/reservation/11f71518-4ea4-4a8e-813c-85312308602d?lang=es\r\n\r\n\r\n#### Resumen de reserva ####\r\n\r\nCategoría: Simphonyc Concert, Cantidad: 3, Subtotal: 45.00 USD\r\nCategoría: General, Cantidad: 3, Subtotal: 30.00 USD\r\n\r\nIVA del 0.00%: 0 USD\r\nTotal 75.00 USD\r\n\r\n\r\nATENCIÓN: Debe pagar la cantidad de 75.00 USD para asistir al evento\r\n\r\n\r\n\r\nInfo. del pedido: 11f71518-4ea4-4a8e-813c-85312308602d\r\n\r\n#### Info. del evento ####\r\n\r\nComo recordatorio, el evento tendrá lugar en:\r\n\r\n Centro de Ferias y Convenciones (CIFCO), pabello #8 \r\n\r\nel:\r\n\r\n martes 31 marzo 2020  de  07:00 a  12:00 (CST)\r\n\r\n\r\nSaludos,\r\n\r\nSVConcert <info@svconcert.com>	\N	40d1559ef07d2165ef8b056544d8260016cf5e40860b60fc4d5e7cb1254a07cb	2020-03-08 21:18:31.585-06	2020-03-08 21:24:16.215-06	9	[]	1	11f71518-4ea4-4a8e-813c-85312308602d
8	1	SENT	paul.lopez@@itproject41.com	Su entrada para el evento EPICA Central America Tour	Hola Paul Lopez,\r\n\r\nAdjunta a este email encontrará la entrada para el evento: EPICA Central America Tour\r\n\r\nPuede modificar/revisar los detalles de sus entradas en ticket/event/svconcert-epica/ticket/311f332c-f834-4c5d-940d-f2a5a9bfe6a2/update?lang=es\r\n\r\n#### Info. del evento ####\r\n\r\nComo recordatorio, el evento tendrá lugar en:\r\n\r\n Centro de Ferias y Convenciones (CIFCO), pabello #8\r\n\r\nel:\r\n\r\n martes 31 marzo 2020  de  07:00 a  12:00 (CST)\r\n\r\n\r\nPuede guardar el evento en su calendario de Google, haga click en el siguiente enlace: https://www.google.com/calendar/event?action=TEMPLATE&dates=20200331T070000/20200331T120000&ctz=America/El_Salvador&text=EPICA%20Central%20America%20Tour&location=Centro%20de%20Ferias%20y%20Convenciones%20(CIFCO),%20pabello%20%238&detail\r\n\r\nSaludos,\r\n\r\nSVConcert <info@svconcert.com>\r\n\r\nGenerado con orgullo por Alf.io, el sistema de reserva de entradas open source.	[{"filename":"ticket-311f332c-f834-4c5d-940d-f2a5a9bfe6a2.pdf","contentType":"application/pdf","identifier":"TICKET_PDF","model":"{\\"organizationId\\":\\"1\\",\\"ticket\\":\\"{\\\\\\"id\\\\\\":3,\\\\\\"uuid\\\\\\":\\\\\\"311f332c-f834-4c5d-940d-f2a5a9bfe6a2\\\\\\",\\\\\\"creation\\\\\\":\\\\\\"2020-02-21T17:26:20.814Z\\\\\\",\\\\\\"categoryId\\\\\\":1,\\\\\\"status\\\\\\":\\\\\\"TO_BE_PAID\\\\\\",\\\\\\"eventId\\\\\\":1,\\\\\\"ticketsReservationId\\\\\\":\\\\\\"11f71518-4ea4-4a8e-813c-85312308602d\\\\\\",\\\\\\"fullName\\\\\\":\\\\\\"Paul Lopez\\\\\\",\\\\\\"firstName\\\\\\":\\\\\\"Paul\\\\\\",\\\\\\"lastName\\\\\\":\\\\\\"Lopez\\\\\\",\\\\\\"email\\\\\\":\\\\\\"paul.lopez@@itproject41.com\\\\\\",\\\\\\"lockedAssignment\\\\\\":false,\\\\\\"userLanguage\\\\\\":\\\\\\"es\\\\\\",\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"finalPriceCts\\\\\\":1500,\\\\\\"vatCts\\\\\\":0,\\\\\\"discountCts\\\\\\":0,\\\\\\"extReference\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"formattedNetPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"checkedIn\\\\\\":false,\\\\\\"formattedFinalPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"assigned\\\\\\":true}\\",\\"reservationId\\":\\"11f71518-4ea4-4a8e-813c-85312308602d\\",\\"ticketCategory\\":\\"{\\\\\\"id\\\\\\":1,\\\\\\"utcInception\\\\\\":\\\\\\"2020-02-21T17:00:00Z\\\\\\",\\\\\\"utcExpiration\\\\\\":\\\\\\"2020-03-31T13:00:00Z\\\\\\",\\\\\\"maxTickets\\\\\\":0,\\\\\\"name\\\\\\":\\\\\\"Simphonyc Concert\\\\\\",\\\\\\"accessRestricted\\\\\\":false,\\\\\\"status\\\\\\":\\\\\\"ACTIVE\\\\\\",\\\\\\"eventId\\\\\\":1,\\\\\\"bounded\\\\\\":false,\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"code\\\\\\":\\\\\\"3p1c@\\\\\\",\\\\\\"validCheckInFrom\\\\\\":null,\\\\\\"validCheckInTo\\\\\\":null,\\\\\\"ticketValidityStart\\\\\\":null,\\\\\\"ticketValidityEnd\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"ordinal\\\\\\":1,\\\\\\"ticketCheckInStrategy\\\\\\":\\\\\\"ONCE_PER_EVENT\\\\\\",\\\\\\"price\\\\\\":15.00,\\\\\\"free\\\\\\":false}\\"}"}]	a9be91d6c96e6a9ee93c71a574fe41f184baec0a2ce74886cb1ec65d5a7b952d	2020-03-08 20:23:26.281-06	2020-03-08 20:23:34.764-06	0	\N	1	11f71518-4ea4-4a8e-813c-85312308602d
9	1	SENT	alejandro.castro@itproject41.com	Su entrada para el evento EPICA Central America Tour	Hola Alejandro Castro,\r\n\r\nAdjunta a este email encontrará la entrada para el evento: EPICA Central America Tour\r\n\r\nPuede modificar/revisar los detalles de sus entradas en ticket/event/svconcert-epica/ticket/789a94c5-6718-4565-8ebf-d3ce368b3a7f/update?lang=es\r\n\r\n#### Info. del evento ####\r\n\r\nComo recordatorio, el evento tendrá lugar en:\r\n\r\n Centro de Ferias y Convenciones (CIFCO), pabello #8\r\n\r\nel:\r\n\r\n martes 31 marzo 2020  de  07:00 a  12:00 (CST)\r\n\r\n\r\nPuede guardar el evento en su calendario de Google, haga click en el siguiente enlace: https://www.google.com/calendar/event?action=TEMPLATE&dates=20200331T070000/20200331T120000&ctz=America/El_Salvador&text=EPICA%20Central%20America%20Tour&location=Centro%20de%20Ferias%20y%20Convenciones%20(CIFCO),%20pabello%20%238&detail\r\n\r\nSaludos,\r\n\r\nSVConcert <info@svconcert.com>\r\n\r\nGenerado con orgullo por Alf.io, el sistema de reserva de entradas open source.	[{"filename":"ticket-789a94c5-6718-4565-8ebf-d3ce368b3a7f.pdf","contentType":"application/pdf","identifier":"TICKET_PDF","model":"{\\"organizationId\\":\\"1\\",\\"ticket\\":\\"{\\\\\\"id\\\\\\":4,\\\\\\"uuid\\\\\\":\\\\\\"789a94c5-6718-4565-8ebf-d3ce368b3a7f\\\\\\",\\\\\\"creation\\\\\\":\\\\\\"2020-02-21T17:26:20.814Z\\\\\\",\\\\\\"categoryId\\\\\\":1,\\\\\\"status\\\\\\":\\\\\\"TO_BE_PAID\\\\\\",\\\\\\"eventId\\\\\\":1,\\\\\\"ticketsReservationId\\\\\\":\\\\\\"11f71518-4ea4-4a8e-813c-85312308602d\\\\\\",\\\\\\"fullName\\\\\\":\\\\\\"Alejandro Castro\\\\\\",\\\\\\"firstName\\\\\\":\\\\\\"Alejandro\\\\\\",\\\\\\"lastName\\\\\\":\\\\\\"Castro\\\\\\",\\\\\\"email\\\\\\":\\\\\\"alejandro.castro@itproject41.com\\\\\\",\\\\\\"lockedAssignment\\\\\\":false,\\\\\\"userLanguage\\\\\\":\\\\\\"es\\\\\\",\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"finalPriceCts\\\\\\":1500,\\\\\\"vatCts\\\\\\":0,\\\\\\"discountCts\\\\\\":0,\\\\\\"extReference\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"formattedNetPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"checkedIn\\\\\\":false,\\\\\\"formattedFinalPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"assigned\\\\\\":true}\\",\\"reservationId\\":\\"11f71518-4ea4-4a8e-813c-85312308602d\\",\\"ticketCategory\\":\\"{\\\\\\"id\\\\\\":1,\\\\\\"utcInception\\\\\\":\\\\\\"2020-02-21T17:00:00Z\\\\\\",\\\\\\"utcExpiration\\\\\\":\\\\\\"2020-03-31T13:00:00Z\\\\\\",\\\\\\"maxTickets\\\\\\":0,\\\\\\"name\\\\\\":\\\\\\"Simphonyc Concert\\\\\\",\\\\\\"accessRestricted\\\\\\":false,\\\\\\"status\\\\\\":\\\\\\"ACTIVE\\\\\\",\\\\\\"eventId\\\\\\":1,\\\\\\"bounded\\\\\\":false,\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"code\\\\\\":\\\\\\"3p1c@\\\\\\",\\\\\\"validCheckInFrom\\\\\\":null,\\\\\\"validCheckInTo\\\\\\":null,\\\\\\"ticketValidityStart\\\\\\":null,\\\\\\"ticketValidityEnd\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"ordinal\\\\\\":1,\\\\\\"ticketCheckInStrategy\\\\\\":\\\\\\"ONCE_PER_EVENT\\\\\\",\\\\\\"price\\\\\\":15.00,\\\\\\"free\\\\\\":false}\\"}"}]	fe8c2fb797a569137f9b9d45cc1fa6e1c24db2b0d7a4631cd265d866af23f149	2020-03-08 21:17:51.696-06	2020-03-08 21:17:56.296-06	9	\N	1	11f71518-4ea4-4a8e-813c-85312308602d
5	1	SENT	jandor.ac@gmail.com	Su entrada para el evento EPICA Central America Tour	Hola Alejandro Castro,\r\n\r\nAdjunta a este email encontrará la entrada para el evento: EPICA Central America Tour\r\n\r\nPuede modificar/revisar los detalles de sus entradas en ticket/event/svconcert-epica/ticket/1d18980c-974e-4554-b452-44bb6079e7d3/update?lang=es\r\n\r\n#### Info. del evento ####\r\n\r\nComo recordatorio, el evento tendrá lugar en:\r\n\r\n Centro de Ferias y Convenciones (CIFCO), pabello #8\r\n\r\nel:\r\n\r\n martes 31 marzo 2020  de  07:00 a  12:00 (CST)\r\n\r\n\r\nPuede guardar el evento en su calendario de Google, haga click en el siguiente enlace: https://www.google.com/calendar/event?action=TEMPLATE&dates=20200331T070000/20200331T120000&ctz=America/El_Salvador&text=EPICA%20Central%20America%20Tour&location=Centro%20de%20Ferias%20y%20Convenciones%20(CIFCO),%20pabello%20%238&detail\r\n\r\nSaludos,\r\n\r\nSVConcert <info@svconcert.com>\r\n\r\nGenerado con orgullo por Alf.io, el sistema de reserva de entradas open source.	[{"filename":"ticket-1d18980c-974e-4554-b452-44bb6079e7d3.pdf","contentType":"application/pdf","identifier":"TICKET_PDF","model":"{\\"organizationId\\":\\"1\\",\\"ticket\\":\\"{\\\\\\"id\\\\\\":1,\\\\\\"uuid\\\\\\":\\\\\\"1d18980c-974e-4554-b452-44bb6079e7d3\\\\\\",\\\\\\"creation\\\\\\":\\\\\\"2020-02-21T17:26:20.814Z\\\\\\",\\\\\\"categoryId\\\\\\":1,\\\\\\"status\\\\\\":\\\\\\"ACQUIRED\\\\\\",\\\\\\"eventId\\\\\\":1,\\\\\\"ticketsReservationId\\\\\\":\\\\\\"1a71e32b-35c3-4089-b249-6e7528ef1a26\\\\\\",\\\\\\"fullName\\\\\\":\\\\\\"Alejandro Castro\\\\\\",\\\\\\"firstName\\\\\\":\\\\\\"Alejandro\\\\\\",\\\\\\"lastName\\\\\\":\\\\\\"Castro\\\\\\",\\\\\\"email\\\\\\":\\\\\\"jandor.ac@gmail.com\\\\\\",\\\\\\"lockedAssignment\\\\\\":false,\\\\\\"userLanguage\\\\\\":\\\\\\"es\\\\\\",\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"finalPriceCts\\\\\\":1500,\\\\\\"vatCts\\\\\\":0,\\\\\\"discountCts\\\\\\":0,\\\\\\"extReference\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"formattedFinalPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"formattedNetPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"checkedIn\\\\\\":false,\\\\\\"assigned\\\\\\":true}\\",\\"reservationId\\":\\"1a71e32b-35c3-4089-b249-6e7528ef1a26\\",\\"ticketCategory\\":\\"{\\\\\\"id\\\\\\":1,\\\\\\"utcInception\\\\\\":\\\\\\"2020-02-21T17:00:00Z\\\\\\",\\\\\\"utcExpiration\\\\\\":\\\\\\"2020-03-31T13:00:00Z\\\\\\",\\\\\\"maxTickets\\\\\\":0,\\\\\\"name\\\\\\":\\\\\\"Simphonyc Concert\\\\\\",\\\\\\"accessRestricted\\\\\\":false,\\\\\\"status\\\\\\":\\\\\\"ACTIVE\\\\\\",\\\\\\"eventId\\\\\\":1,\\\\\\"bounded\\\\\\":false,\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"code\\\\\\":\\\\\\"3p1c@\\\\\\",\\\\\\"validCheckInFrom\\\\\\":null,\\\\\\"validCheckInTo\\\\\\":null,\\\\\\"ticketValidityStart\\\\\\":null,\\\\\\"ticketValidityEnd\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"ordinal\\\\\\":1,\\\\\\"ticketCheckInStrategy\\\\\\":\\\\\\"ONCE_PER_EVENT\\\\\\",\\\\\\"price\\\\\\":15.00,\\\\\\"free\\\\\\":false}\\"}"}]	07cfb82380517f3add480e97c1aa37f6a55bceebb701c4bbd0b6b2a40db5aa5c	2020-02-21 11:56:28.956-06	2020-02-21 11:56:41.958-06	0	\N	1	1a71e32b-35c3-4089-b249-6e7528ef1a26
7	1	SENT	pablo.aguilar@itproject41.com	Su entrada para el evento EPICA Central America Tour	Hola Pablo Aguilar,\r\n\r\nAdjunta a este email encontrará la entrada para el evento: EPICA Central America Tour\r\n\r\nPuede modificar/revisar los detalles de sus entradas en ticket/event/svconcert-epica/ticket/264b44ea-820f-46de-b5f2-5c3fb89db02e/update?lang=es\r\n\r\n#### Info. del evento ####\r\n\r\nComo recordatorio, el evento tendrá lugar en:\r\n\r\n Centro de Ferias y Convenciones (CIFCO), pabello #8\r\n\r\nel:\r\n\r\n martes 31 marzo 2020  de  07:00 a  12:00 (CST)\r\n\r\n\r\nPuede guardar el evento en su calendario de Google, haga click en el siguiente enlace: https://www.google.com/calendar/event?action=TEMPLATE&dates=20200331T070000/20200331T120000&ctz=America/El_Salvador&text=EPICA%20Central%20America%20Tour&location=Centro%20de%20Ferias%20y%20Convenciones%20(CIFCO),%20pabello%20%238&detail\r\n\r\nSaludos,\r\n\r\nSVConcert <info@svconcert.com>\r\n\r\nGenerado con orgullo por Alf.io, el sistema de reserva de entradas open source.	[{"filename":"ticket-264b44ea-820f-46de-b5f2-5c3fb89db02e.pdf","contentType":"application/pdf","identifier":"TICKET_PDF","model":"{\\"organizationId\\":\\"1\\",\\"ticket\\":\\"{\\\\\\"id\\\\\\":5,\\\\\\"uuid\\\\\\":\\\\\\"264b44ea-820f-46de-b5f2-5c3fb89db02e\\\\\\",\\\\\\"creation\\\\\\":\\\\\\"2020-02-21T17:26:20.814Z\\\\\\",\\\\\\"categoryId\\\\\\":1,\\\\\\"status\\\\\\":\\\\\\"TO_BE_PAID\\\\\\",\\\\\\"eventId\\\\\\":1,\\\\\\"ticketsReservationId\\\\\\":\\\\\\"11f71518-4ea4-4a8e-813c-85312308602d\\\\\\",\\\\\\"fullName\\\\\\":\\\\\\"Pablo Aguilar\\\\\\",\\\\\\"firstName\\\\\\":\\\\\\"Pablo\\\\\\",\\\\\\"lastName\\\\\\":\\\\\\"Aguilar\\\\\\",\\\\\\"email\\\\\\":\\\\\\"pablo.aguilar@itproject41.com\\\\\\",\\\\\\"lockedAssignment\\\\\\":false,\\\\\\"userLanguage\\\\\\":\\\\\\"es\\\\\\",\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"finalPriceCts\\\\\\":1500,\\\\\\"vatCts\\\\\\":0,\\\\\\"discountCts\\\\\\":0,\\\\\\"extReference\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"formattedNetPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"checkedIn\\\\\\":false,\\\\\\"formattedFinalPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"assigned\\\\\\":true}\\",\\"reservationId\\":\\"11f71518-4ea4-4a8e-813c-85312308602d\\",\\"ticketCategory\\":\\"{\\\\\\"id\\\\\\":1,\\\\\\"utcInception\\\\\\":\\\\\\"2020-02-21T17:00:00Z\\\\\\",\\\\\\"utcExpiration\\\\\\":\\\\\\"2020-03-31T13:00:00Z\\\\\\",\\\\\\"maxTickets\\\\\\":0,\\\\\\"name\\\\\\":\\\\\\"Simphonyc Concert\\\\\\",\\\\\\"accessRestricted\\\\\\":false,\\\\\\"status\\\\\\":\\\\\\"ACTIVE\\\\\\",\\\\\\"eventId\\\\\\":1,\\\\\\"bounded\\\\\\":false,\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"code\\\\\\":\\\\\\"3p1c@\\\\\\",\\\\\\"validCheckInFrom\\\\\\":null,\\\\\\"validCheckInTo\\\\\\":null,\\\\\\"ticketValidityStart\\\\\\":null,\\\\\\"ticketValidityEnd\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"ordinal\\\\\\":1,\\\\\\"ticketCheckInStrategy\\\\\\":\\\\\\"ONCE_PER_EVENT\\\\\\",\\\\\\"price\\\\\\":15.00,\\\\\\"free\\\\\\":false}\\"}"}]	670cfee495eb8959dc7d5f68c7dd698ce741bf66e0e3ab7b9a8b1c068b883a61	2020-03-08 20:23:26.217-06	2020-03-08 20:23:34.883-06	0	\N	1	11f71518-4ea4-4a8e-813c-85312308602d
22	4	SENT	jandor.ac@gmail.com	Su reserva n. 00449CA6 para el evento of monster and man - Little Talks	Hola Alejandro Castro,\r\n\r\nSu reserva para el evento of monster and man - Little Talks ha sido completada. Puede modificarla o revisarla en ticket/event/omam/reservation/00449ca6-fd93-4a4c-857d-09f49315fccf?lang=es\r\n\r\n\r\n#### Resumen de reserva ####\r\n\r\nCategoría: Indie Rock, Cantidad: 1, Subtotal: 15.00 USD\r\n\r\nIVA del 0.00%: 0 USD\r\nTotal 15.00 USD\r\n\r\n\r\nATENCIÓN: Debe pagar la cantidad de 15.00 USD para asistir al evento\r\n\r\n\r\n\r\nInfo. del pedido: 00449ca6-fd93-4a4c-857d-09f49315fccf\r\n\r\n#### Info. del evento ####\r\n\r\nComo recordatorio, el evento tendrá lugar en:\r\n\r\n CIFCO, Pabello #15, San Salvador \r\n\r\nel:\r\n\r\n jueves 30 abril 2020  de  15:00 a  16:00 (CST)\r\n\r\n\r\nSaludos,\r\n\r\nSVConcert <info@svconcert.com>	\N	0df3debbd9eebc9978a82e4f7bb583349e59859f659264c43a9e78d509dbfef0	2020-03-25 16:11:40.431-06	2020-03-25 16:11:44.46-06	0	[]	1	00449ca6-fd93-4a4c-857d-09f49315fccf
12	1	SENT	jorge.ochoa@itproject41.com	Su entrada para el evento EPICA Central America Tour	Hola Jorge Ochoa,\r\n\r\nAdjunta a este email encontrará la entrada para el evento: EPICA Central America Tour\r\n\r\nPuede modificar/revisar los detalles de sus entradas en ticket/event/svconcert-epica/ticket/264b44ea-820f-46de-b5f2-5c3fb89db02e/update?lang=es\r\n\r\n#### Info. del evento ####\r\n\r\nComo recordatorio, el evento tendrá lugar en:\r\n\r\n Centro de Ferias y Convenciones (CIFCO), pabello #8\r\n\r\nel:\r\n\r\n martes 31 marzo 2020  de  07:00 a  12:00 (CST)\r\n\r\n\r\nPuede guardar el evento en su calendario de Google, haga click en el siguiente enlace: https://www.google.com/calendar/event?action=TEMPLATE&dates=20200331T070000/20200331T120000&ctz=America/El_Salvador&text=EPICA%20Central%20America%20Tour&location=Centro%20de%20Ferias%20y%20Convenciones%20(CIFCO),%20pabello%20%238&detail\r\n\r\nSaludos,\r\n\r\nSVConcert <info@svconcert.com>\r\n\r\nGenerado con orgullo por Alf.io, el sistema de reserva de entradas open source.	[{"filename":"ticket-264b44ea-820f-46de-b5f2-5c3fb89db02e.pdf","contentType":"application/pdf","identifier":"TICKET_PDF","model":"{\\"organizationId\\":\\"1\\",\\"ticket\\":\\"{\\\\\\"id\\\\\\":5,\\\\\\"uuid\\\\\\":\\\\\\"264b44ea-820f-46de-b5f2-5c3fb89db02e\\\\\\",\\\\\\"creation\\\\\\":\\\\\\"2020-02-21T17:26:20.814Z\\\\\\",\\\\\\"categoryId\\\\\\":1,\\\\\\"status\\\\\\":\\\\\\"TO_BE_PAID\\\\\\",\\\\\\"eventId\\\\\\":1,\\\\\\"ticketsReservationId\\\\\\":\\\\\\"11f71518-4ea4-4a8e-813c-85312308602d\\\\\\",\\\\\\"fullName\\\\\\":\\\\\\"Jorge Ochoa\\\\\\",\\\\\\"firstName\\\\\\":\\\\\\"Jorge\\\\\\",\\\\\\"lastName\\\\\\":\\\\\\"Ochoa\\\\\\",\\\\\\"email\\\\\\":\\\\\\"jorge.ochoa@itproject41.com\\\\\\",\\\\\\"lockedAssignment\\\\\\":false,\\\\\\"userLanguage\\\\\\":\\\\\\"es\\\\\\",\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"finalPriceCts\\\\\\":1500,\\\\\\"vatCts\\\\\\":0,\\\\\\"discountCts\\\\\\":0,\\\\\\"extReference\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"formattedNetPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"checkedIn\\\\\\":false,\\\\\\"formattedFinalPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"assigned\\\\\\":true}\\",\\"reservationId\\":\\"11f71518-4ea4-4a8e-813c-85312308602d\\",\\"ticketCategory\\":\\"{\\\\\\"id\\\\\\":1,\\\\\\"utcInception\\\\\\":\\\\\\"2020-02-21T17:00:00Z\\\\\\",\\\\\\"utcExpiration\\\\\\":\\\\\\"2020-03-31T13:00:00Z\\\\\\",\\\\\\"maxTickets\\\\\\":0,\\\\\\"name\\\\\\":\\\\\\"Simphonyc Concert\\\\\\",\\\\\\"accessRestricted\\\\\\":false,\\\\\\"status\\\\\\":\\\\\\"ACTIVE\\\\\\",\\\\\\"eventId\\\\\\":1,\\\\\\"bounded\\\\\\":false,\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"code\\\\\\":\\\\\\"3p1c@\\\\\\",\\\\\\"validCheckInFrom\\\\\\":null,\\\\\\"validCheckInTo\\\\\\":null,\\\\\\"ticketValidityStart\\\\\\":null,\\\\\\"ticketValidityEnd\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"ordinal\\\\\\":1,\\\\\\"ticketCheckInStrategy\\\\\\":\\\\\\"ONCE_PER_EVENT\\\\\\",\\\\\\"price\\\\\\":15.00,\\\\\\"free\\\\\\":false}\\"}"}]	3124d804e24042b99c882320bbf1542442c016b9960a493a7e94785dda87dc28	2020-03-08 21:17:31.8-06	2020-03-08 21:17:36.258-06	9	\N	1	11f71518-4ea4-4a8e-813c-85312308602d
14	1	SENT	alejandro.castro@itproject41.com	Su entrada para el evento EPICA Central America Tour	Hola Alejandro Castro,\r\n\r\nAdjunta a este email encontrará la entrada para el evento: EPICA Central America Tour\r\n\r\nPuede modificar/revisar los detalles de sus entradas en ticket/event/svconcert-epica/ticket/789a94c5-6718-4565-8ebf-d3ce368b3a7f/update?lang=es\r\n\r\n#### Info. del evento ####\r\n\r\nComo recordatorio, el evento tendrá lugar en:\r\n\r\n Centro de Ferias y Convenciones (CIFCO), pabello #8\r\n\r\nel:\r\n\r\n martes 31 marzo 2020  de  07:00 a  12:00 (CST)\r\n\r\n\r\nPuede guardar el evento en su calendario de Google, haga click en el siguiente enlace: https://www.google.com/calendar/event?action=TEMPLATE&dates=20200331T070000/20200331T120000&ctz=America/El_Salvador&text=EPICA%20Central%20America%20Tour&location=Centro%20de%20Ferias%20y%20Convenciones%20(CIFCO),%20pabello%20%238&detail\r\n\r\nSaludos,\r\n\r\nSVConcert <info@svconcert.com>\r\n\r\nGenerado con orgullo por Alf.io, el sistema de reserva de entradas open source.	[{"filename":"ticket-789a94c5-6718-4565-8ebf-d3ce368b3a7f.pdf","contentType":"application/pdf","identifier":"TICKET_PDF","model":"{\\"organizationId\\":\\"1\\",\\"ticket\\":\\"{\\\\\\"id\\\\\\":4,\\\\\\"uuid\\\\\\":\\\\\\"789a94c5-6718-4565-8ebf-d3ce368b3a7f\\\\\\",\\\\\\"creation\\\\\\":\\\\\\"2020-02-21T17:26:20.814Z\\\\\\",\\\\\\"categoryId\\\\\\":1,\\\\\\"status\\\\\\":\\\\\\"TO_BE_PAID\\\\\\",\\\\\\"eventId\\\\\\":1,\\\\\\"ticketsReservationId\\\\\\":\\\\\\"11f71518-4ea4-4a8e-813c-85312308602d\\\\\\",\\\\\\"fullName\\\\\\":\\\\\\"Alejandro Castro\\\\\\",\\\\\\"firstName\\\\\\":\\\\\\"Alejandro\\\\\\",\\\\\\"lastName\\\\\\":\\\\\\"Castro\\\\\\",\\\\\\"email\\\\\\":\\\\\\"alejandro.castro@itproject41.com\\\\\\",\\\\\\"lockedAssignment\\\\\\":false,\\\\\\"userLanguage\\\\\\":\\\\\\"es\\\\\\",\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"finalPriceCts\\\\\\":1500,\\\\\\"vatCts\\\\\\":0,\\\\\\"discountCts\\\\\\":0,\\\\\\"extReference\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"formattedFinalPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"assigned\\\\\\":true,\\\\\\"formattedNetPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"checkedIn\\\\\\":false}\\",\\"reservationId\\":\\"11f71518-4ea4-4a8e-813c-85312308602d\\",\\"ticketCategory\\":\\"{\\\\\\"id\\\\\\":1,\\\\\\"utcInception\\\\\\":\\\\\\"2020-02-21T17:00:00Z\\\\\\",\\\\\\"utcExpiration\\\\\\":\\\\\\"2020-03-31T13:00:00Z\\\\\\",\\\\\\"maxTickets\\\\\\":0,\\\\\\"name\\\\\\":\\\\\\"Simphonyc Concert\\\\\\",\\\\\\"accessRestricted\\\\\\":false,\\\\\\"status\\\\\\":\\\\\\"ACTIVE\\\\\\",\\\\\\"eventId\\\\\\":1,\\\\\\"bounded\\\\\\":false,\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"code\\\\\\":\\\\\\"3p1c@\\\\\\",\\\\\\"validCheckInFrom\\\\\\":null,\\\\\\"validCheckInTo\\\\\\":null,\\\\\\"ticketValidityStart\\\\\\":null,\\\\\\"ticketValidityEnd\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"ordinal\\\\\\":1,\\\\\\"ticketCheckInStrategy\\\\\\":\\\\\\"ONCE_PER_EVENT\\\\\\",\\\\\\"free\\\\\\":false,\\\\\\"price\\\\\\":15.00}\\"}"}]	3f815a9765b3a2dbc21fc66e074dca42f7e88ae3d9e3a6b20becb1c7d8b81b45	2020-03-08 21:20:16.442-06	2020-03-08 21:20:21.259-06	5	\N	1	11f71518-4ea4-4a8e-813c-85312308602d
13	1	SENT	jorge.ochoa@itproject41.com	Su entrada para el evento EPICA Central America Tour	Hola Jorge Ochoa,\r\n\r\nAdjunta a este email encontrará la entrada para el evento: EPICA Central America Tour\r\n\r\nPuede modificar/revisar los detalles de sus entradas en ticket/event/svconcert-epica/ticket/264b44ea-820f-46de-b5f2-5c3fb89db02e/update?lang=es\r\n\r\n#### Info. del evento ####\r\n\r\nComo recordatorio, el evento tendrá lugar en:\r\n\r\n Centro de Ferias y Convenciones (CIFCO), pabello #8\r\n\r\nel:\r\n\r\n martes 31 marzo 2020  de  07:00 a  12:00 (CST)\r\n\r\n\r\nPuede guardar el evento en su calendario de Google, haga click en el siguiente enlace: https://www.google.com/calendar/event?action=TEMPLATE&dates=20200331T070000/20200331T120000&ctz=America/El_Salvador&text=EPICA%20Central%20America%20Tour&location=Centro%20de%20Ferias%20y%20Convenciones%20(CIFCO),%20pabello%20%238&detail\r\n\r\nSaludos,\r\n\r\nSVConcert <info@svconcert.com>\r\n\r\nGenerado con orgullo por Alf.io, el sistema de reserva de entradas open source.	[{"filename":"ticket-264b44ea-820f-46de-b5f2-5c3fb89db02e.pdf","contentType":"application/pdf","identifier":"TICKET_PDF","model":"{\\"organizationId\\":\\"1\\",\\"ticket\\":\\"{\\\\\\"id\\\\\\":5,\\\\\\"uuid\\\\\\":\\\\\\"264b44ea-820f-46de-b5f2-5c3fb89db02e\\\\\\",\\\\\\"creation\\\\\\":\\\\\\"2020-02-21T17:26:20.814Z\\\\\\",\\\\\\"categoryId\\\\\\":1,\\\\\\"status\\\\\\":\\\\\\"TO_BE_PAID\\\\\\",\\\\\\"eventId\\\\\\":1,\\\\\\"ticketsReservationId\\\\\\":\\\\\\"11f71518-4ea4-4a8e-813c-85312308602d\\\\\\",\\\\\\"fullName\\\\\\":\\\\\\"Jorge Ochoa\\\\\\",\\\\\\"firstName\\\\\\":\\\\\\"Jorge\\\\\\",\\\\\\"lastName\\\\\\":\\\\\\"Ochoa\\\\\\",\\\\\\"email\\\\\\":\\\\\\"jorge.ochoa@itproject41.com\\\\\\",\\\\\\"lockedAssignment\\\\\\":false,\\\\\\"userLanguage\\\\\\":\\\\\\"es\\\\\\",\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"finalPriceCts\\\\\\":1500,\\\\\\"vatCts\\\\\\":0,\\\\\\"discountCts\\\\\\":0,\\\\\\"extReference\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"formattedFinalPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"assigned\\\\\\":true,\\\\\\"formattedNetPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"checkedIn\\\\\\":false}\\",\\"reservationId\\":\\"11f71518-4ea4-4a8e-813c-85312308602d\\",\\"ticketCategory\\":\\"{\\\\\\"id\\\\\\":1,\\\\\\"utcInception\\\\\\":\\\\\\"2020-02-21T17:00:00Z\\\\\\",\\\\\\"utcExpiration\\\\\\":\\\\\\"2020-03-31T13:00:00Z\\\\\\",\\\\\\"maxTickets\\\\\\":0,\\\\\\"name\\\\\\":\\\\\\"Simphonyc Concert\\\\\\",\\\\\\"accessRestricted\\\\\\":false,\\\\\\"status\\\\\\":\\\\\\"ACTIVE\\\\\\",\\\\\\"eventId\\\\\\":1,\\\\\\"bounded\\\\\\":false,\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"code\\\\\\":\\\\\\"3p1c@\\\\\\",\\\\\\"validCheckInFrom\\\\\\":null,\\\\\\"validCheckInTo\\\\\\":null,\\\\\\"ticketValidityStart\\\\\\":null,\\\\\\"ticketValidityEnd\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"ordinal\\\\\\":1,\\\\\\"ticketCheckInStrategy\\\\\\":\\\\\\"ONCE_PER_EVENT\\\\\\",\\\\\\"free\\\\\\":false,\\\\\\"price\\\\\\":15.00}\\"}"}]	e479b04f6003f6a3e8eb09e80cd4216edc0c08342e38ac15e34ce10ac39edf03	2020-03-08 21:16:36.636-06	2020-03-08 21:16:46.262-06	7	\N	1	11f71518-4ea4-4a8e-813c-85312308602d
15	1	SENT	jandor.ac@gmail.com	Su entrada para el evento EPICA Central America Tour	Hola Jorge Ochoa,\r\n\r\nAdjunta a este email encontrará la entrada para el evento: EPICA Central America Tour\r\n\r\nPuede modificar/revisar los detalles de sus entradas en ticket/event/svconcert-epica/ticket/264b44ea-820f-46de-b5f2-5c3fb89db02e/update?lang=es\r\n\r\n#### Info. del evento ####\r\n\r\nComo recordatorio, el evento tendrá lugar en:\r\n\r\n Centro de Ferias y Convenciones (CIFCO), pabello #8\r\n\r\nel:\r\n\r\n martes 31 marzo 2020  de  07:00 a  12:00 (CST)\r\n\r\n\r\nPuede guardar el evento en su calendario de Google, haga click en el siguiente enlace: https://www.google.com/calendar/event?action=TEMPLATE&dates=20200331T070000/20200331T120000&ctz=America/El_Salvador&text=EPICA%20Central%20America%20Tour&location=Centro%20de%20Ferias%20y%20Convenciones%20(CIFCO),%20pabello%20%238&detail\r\n\r\nSaludos,\r\n\r\nSVConcert <info@svconcert.com>\r\n\r\nGenerado con orgullo por Alf.io, el sistema de reserva de entradas open source.	[{"filename":"ticket-264b44ea-820f-46de-b5f2-5c3fb89db02e.pdf","contentType":"application/pdf","identifier":"TICKET_PDF","model":"{\\"organizationId\\":\\"1\\",\\"ticket\\":\\"{\\\\\\"id\\\\\\":5,\\\\\\"uuid\\\\\\":\\\\\\"264b44ea-820f-46de-b5f2-5c3fb89db02e\\\\\\",\\\\\\"creation\\\\\\":\\\\\\"2020-02-21T17:26:20.814Z\\\\\\",\\\\\\"categoryId\\\\\\":1,\\\\\\"status\\\\\\":\\\\\\"TO_BE_PAID\\\\\\",\\\\\\"eventId\\\\\\":1,\\\\\\"ticketsReservationId\\\\\\":\\\\\\"11f71518-4ea4-4a8e-813c-85312308602d\\\\\\",\\\\\\"fullName\\\\\\":\\\\\\"Jorge Ochoa\\\\\\",\\\\\\"firstName\\\\\\":\\\\\\"Jorge\\\\\\",\\\\\\"lastName\\\\\\":\\\\\\"Ochoa\\\\\\",\\\\\\"email\\\\\\":\\\\\\"jandor.ac@gmail.com\\\\\\",\\\\\\"lockedAssignment\\\\\\":false,\\\\\\"userLanguage\\\\\\":\\\\\\"es\\\\\\",\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"finalPriceCts\\\\\\":1500,\\\\\\"vatCts\\\\\\":0,\\\\\\"discountCts\\\\\\":0,\\\\\\"extReference\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"formattedFinalPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"assigned\\\\\\":true,\\\\\\"formattedNetPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"checkedIn\\\\\\":false}\\",\\"reservationId\\":\\"11f71518-4ea4-4a8e-813c-85312308602d\\",\\"ticketCategory\\":\\"{\\\\\\"id\\\\\\":1,\\\\\\"utcInception\\\\\\":\\\\\\"2020-02-21T17:00:00Z\\\\\\",\\\\\\"utcExpiration\\\\\\":\\\\\\"2020-03-31T13:00:00Z\\\\\\",\\\\\\"maxTickets\\\\\\":0,\\\\\\"name\\\\\\":\\\\\\"Simphonyc Concert\\\\\\",\\\\\\"accessRestricted\\\\\\":false,\\\\\\"status\\\\\\":\\\\\\"ACTIVE\\\\\\",\\\\\\"eventId\\\\\\":1,\\\\\\"bounded\\\\\\":false,\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"code\\\\\\":\\\\\\"3p1c@\\\\\\",\\\\\\"validCheckInFrom\\\\\\":null,\\\\\\"validCheckInTo\\\\\\":null,\\\\\\"ticketValidityStart\\\\\\":null,\\\\\\"ticketValidityEnd\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"ordinal\\\\\\":1,\\\\\\"ticketCheckInStrategy\\\\\\":\\\\\\"ONCE_PER_EVENT\\\\\\",\\\\\\"free\\\\\\":false,\\\\\\"price\\\\\\":15.00}\\"}"}]	f67714139d72e03f9cd85cd70ef1c0c4d9053cb426133f2e6a966cbbda47d147	2020-03-08 21:17:22.682-06	2020-03-08 21:17:26.295-06	0	\N	1	11f71518-4ea4-4a8e-813c-85312308602d
16	1	SENT	paul.lopez@@itproject41.com	Su entrada para el evento EPICA Central America Tour	Hola Paul Lopez,\r\n\r\nAdjunta a este email encontrará la entrada para el evento: EPICA Central America Tour\r\n\r\nPuede modificar/revisar los detalles de sus entradas en ticket/event/svconcert-epica/ticket/311f332c-f834-4c5d-940d-f2a5a9bfe6a2/update?lang=es\r\n\r\n#### Info. del evento ####\r\n\r\nComo recordatorio, el evento tendrá lugar en:\r\n\r\n Centro de Ferias y Convenciones (CIFCO), pabello #8\r\n\r\nel:\r\n\r\n martes 31 marzo 2020  de  07:00 a  12:00 (CST)\r\n\r\n\r\nPuede guardar el evento en su calendario de Google, haga click en el siguiente enlace: https://www.google.com/calendar/event?action=TEMPLATE&dates=20200331T070000/20200331T120000&ctz=America/El_Salvador&text=EPICA%20Central%20America%20Tour&location=Centro%20de%20Ferias%20y%20Convenciones%20(CIFCO),%20pabello%20%238&detail\r\n\r\nSaludos,\r\n\r\nSVConcert <info@svconcert.com>\r\n\r\nGenerado con orgullo por Alf.io, el sistema de reserva de entradas open source.	[{"filename":"ticket-311f332c-f834-4c5d-940d-f2a5a9bfe6a2.pdf","contentType":"application/pdf","identifier":"TICKET_PDF","model":"{\\"organizationId\\":\\"1\\",\\"ticket\\":\\"{\\\\\\"id\\\\\\":3,\\\\\\"uuid\\\\\\":\\\\\\"311f332c-f834-4c5d-940d-f2a5a9bfe6a2\\\\\\",\\\\\\"creation\\\\\\":\\\\\\"2020-02-21T17:26:20.814Z\\\\\\",\\\\\\"categoryId\\\\\\":1,\\\\\\"status\\\\\\":\\\\\\"TO_BE_PAID\\\\\\",\\\\\\"eventId\\\\\\":1,\\\\\\"ticketsReservationId\\\\\\":\\\\\\"11f71518-4ea4-4a8e-813c-85312308602d\\\\\\",\\\\\\"fullName\\\\\\":\\\\\\"Paul Lopez\\\\\\",\\\\\\"firstName\\\\\\":\\\\\\"Paul\\\\\\",\\\\\\"lastName\\\\\\":\\\\\\"Lopez\\\\\\",\\\\\\"email\\\\\\":\\\\\\"paul.lopez@@itproject41.com\\\\\\",\\\\\\"lockedAssignment\\\\\\":false,\\\\\\"userLanguage\\\\\\":\\\\\\"es\\\\\\",\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"finalPriceCts\\\\\\":1500,\\\\\\"vatCts\\\\\\":0,\\\\\\"discountCts\\\\\\":0,\\\\\\"extReference\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"formattedFinalPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"assigned\\\\\\":true,\\\\\\"formattedNetPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"checkedIn\\\\\\":false}\\",\\"reservationId\\":\\"11f71518-4ea4-4a8e-813c-85312308602d\\",\\"ticketCategory\\":\\"{\\\\\\"id\\\\\\":1,\\\\\\"utcInception\\\\\\":\\\\\\"2020-02-21T17:00:00Z\\\\\\",\\\\\\"utcExpiration\\\\\\":\\\\\\"2020-03-31T13:00:00Z\\\\\\",\\\\\\"maxTickets\\\\\\":0,\\\\\\"name\\\\\\":\\\\\\"Simphonyc Concert\\\\\\",\\\\\\"accessRestricted\\\\\\":false,\\\\\\"status\\\\\\":\\\\\\"ACTIVE\\\\\\",\\\\\\"eventId\\\\\\":1,\\\\\\"bounded\\\\\\":false,\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"code\\\\\\":\\\\\\"3p1c@\\\\\\",\\\\\\"validCheckInFrom\\\\\\":null,\\\\\\"validCheckInTo\\\\\\":null,\\\\\\"ticketValidityStart\\\\\\":null,\\\\\\"ticketValidityEnd\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"ordinal\\\\\\":1,\\\\\\"ticketCheckInStrategy\\\\\\":\\\\\\"ONCE_PER_EVENT\\\\\\",\\\\\\"free\\\\\\":false,\\\\\\"price\\\\\\":15.00}\\"}"}]	acdfd90cbf2f55103843d8e68c7cda84efbab03e5c6af15041bb8ed6b6fb59e8	2020-03-08 21:19:15.418-06	2020-03-08 21:19:16.268-06	0	\N	1	11f71518-4ea4-4a8e-813c-85312308602d
17	1	SENT	alejandro.castro@itproject41.com	Su entrada para el evento EPICA Central America Tour	Hola Alejandro Castro,\r\n\r\nAdjunta a este email encontrará la entrada para el evento: EPICA Central America Tour\r\n\r\nPuede modificar/revisar los detalles de sus entradas en ticket/event/svconcert-epica/ticket/311f332c-f834-4c5d-940d-f2a5a9bfe6a2/update?lang=es\r\n\r\n#### Info. del evento ####\r\n\r\nComo recordatorio, el evento tendrá lugar en:\r\n\r\n Centro de Ferias y Convenciones (CIFCO), pabello #8\r\n\r\nel:\r\n\r\n martes 31 marzo 2020  de  07:00 a  12:00 (CST)\r\n\r\n\r\nPuede guardar el evento en su calendario de Google, haga click en el siguiente enlace: https://www.google.com/calendar/event?action=TEMPLATE&dates=20200331T070000/20200331T120000&ctz=America/El_Salvador&text=EPICA%20Central%20America%20Tour&location=Centro%20de%20Ferias%20y%20Convenciones%20(CIFCO),%20pabello%20%238&detail\r\n\r\nSaludos,\r\n\r\nSVConcert <info@svconcert.com>\r\n\r\nGenerado con orgullo por Alf.io, el sistema de reserva de entradas open source.	[{"filename":"ticket-311f332c-f834-4c5d-940d-f2a5a9bfe6a2.pdf","contentType":"application/pdf","identifier":"TICKET_PDF","model":"{\\"organizationId\\":\\"1\\",\\"ticket\\":\\"{\\\\\\"id\\\\\\":3,\\\\\\"uuid\\\\\\":\\\\\\"311f332c-f834-4c5d-940d-f2a5a9bfe6a2\\\\\\",\\\\\\"creation\\\\\\":\\\\\\"2020-02-21T17:26:20.814Z\\\\\\",\\\\\\"categoryId\\\\\\":1,\\\\\\"status\\\\\\":\\\\\\"TO_BE_PAID\\\\\\",\\\\\\"eventId\\\\\\":1,\\\\\\"ticketsReservationId\\\\\\":\\\\\\"11f71518-4ea4-4a8e-813c-85312308602d\\\\\\",\\\\\\"fullName\\\\\\":\\\\\\"Alejandro Castro\\\\\\",\\\\\\"firstName\\\\\\":\\\\\\"Alejandro\\\\\\",\\\\\\"lastName\\\\\\":\\\\\\"Castro\\\\\\",\\\\\\"email\\\\\\":\\\\\\"alejandro.castro@itproject41.com\\\\\\",\\\\\\"lockedAssignment\\\\\\":false,\\\\\\"userLanguage\\\\\\":\\\\\\"es\\\\\\",\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"finalPriceCts\\\\\\":1500,\\\\\\"vatCts\\\\\\":0,\\\\\\"discountCts\\\\\\":0,\\\\\\"extReference\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"formattedFinalPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"assigned\\\\\\":true,\\\\\\"formattedNetPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"checkedIn\\\\\\":false}\\",\\"reservationId\\":\\"11f71518-4ea4-4a8e-813c-85312308602d\\",\\"ticketCategory\\":\\"{\\\\\\"id\\\\\\":1,\\\\\\"utcInception\\\\\\":\\\\\\"2020-02-21T17:00:00Z\\\\\\",\\\\\\"utcExpiration\\\\\\":\\\\\\"2020-03-31T13:00:00Z\\\\\\",\\\\\\"maxTickets\\\\\\":0,\\\\\\"name\\\\\\":\\\\\\"Simphonyc Concert\\\\\\",\\\\\\"accessRestricted\\\\\\":false,\\\\\\"status\\\\\\":\\\\\\"ACTIVE\\\\\\",\\\\\\"eventId\\\\\\":1,\\\\\\"bounded\\\\\\":false,\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"code\\\\\\":\\\\\\"3p1c@\\\\\\",\\\\\\"validCheckInFrom\\\\\\":null,\\\\\\"validCheckInTo\\\\\\":null,\\\\\\"ticketValidityStart\\\\\\":null,\\\\\\"ticketValidityEnd\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"ordinal\\\\\\":1,\\\\\\"ticketCheckInStrategy\\\\\\":\\\\\\"ONCE_PER_EVENT\\\\\\",\\\\\\"free\\\\\\":false,\\\\\\"price\\\\\\":15.00}\\"}"}]	f322c8b4f8ef82f288c085c30f322308a211a82dfb074d61ad2804fc88093bb6	2020-03-08 21:20:07.052-06	2020-03-08 21:20:11.257-06	0	\N	1	11f71518-4ea4-4a8e-813c-85312308602d
18	1	SENT	pablo.aguilar@itproject41.com	Su entrada para el evento EPICA Central America Tour	Hola Pablo Aguilar,\r\n\r\nAdjunta a este email encontrará la entrada para el evento: EPICA Central America Tour\r\n\r\nPuede modificar/revisar los detalles de sus entradas en ticket/event/svconcert-epica/ticket/311f332c-f834-4c5d-940d-f2a5a9bfe6a2/update?lang=es\r\n\r\n#### Info. del evento ####\r\n\r\nComo recordatorio, el evento tendrá lugar en:\r\n\r\n Centro de Ferias y Convenciones (CIFCO), pabello #8\r\n\r\nel:\r\n\r\n martes 31 marzo 2020  de  07:00 a  12:00 (CST)\r\n\r\n\r\nPuede guardar el evento en su calendario de Google, haga click en el siguiente enlace: https://www.google.com/calendar/event?action=TEMPLATE&dates=20200331T070000/20200331T120000&ctz=America/El_Salvador&text=EPICA%20Central%20America%20Tour&location=Centro%20de%20Ferias%20y%20Convenciones%20(CIFCO),%20pabello%20%238&detail\r\n\r\nSaludos,\r\n\r\nSVConcert <info@svconcert.com>\r\n\r\nGenerado con orgullo por Alf.io, el sistema de reserva de entradas open source.	[{"filename":"ticket-311f332c-f834-4c5d-940d-f2a5a9bfe6a2.pdf","contentType":"application/pdf","identifier":"TICKET_PDF","model":"{\\"organizationId\\":\\"1\\",\\"ticket\\":\\"{\\\\\\"id\\\\\\":3,\\\\\\"uuid\\\\\\":\\\\\\"311f332c-f834-4c5d-940d-f2a5a9bfe6a2\\\\\\",\\\\\\"creation\\\\\\":\\\\\\"2020-02-21T17:26:20.814Z\\\\\\",\\\\\\"categoryId\\\\\\":1,\\\\\\"status\\\\\\":\\\\\\"TO_BE_PAID\\\\\\",\\\\\\"eventId\\\\\\":1,\\\\\\"ticketsReservationId\\\\\\":\\\\\\"11f71518-4ea4-4a8e-813c-85312308602d\\\\\\",\\\\\\"fullName\\\\\\":\\\\\\"Pablo Aguilar\\\\\\",\\\\\\"firstName\\\\\\":\\\\\\"Pablo\\\\\\",\\\\\\"lastName\\\\\\":\\\\\\"Aguilar\\\\\\",\\\\\\"email\\\\\\":\\\\\\"pablo.aguilar@itproject41.com\\\\\\",\\\\\\"lockedAssignment\\\\\\":false,\\\\\\"userLanguage\\\\\\":\\\\\\"es\\\\\\",\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"finalPriceCts\\\\\\":1500,\\\\\\"vatCts\\\\\\":0,\\\\\\"discountCts\\\\\\":0,\\\\\\"extReference\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"formattedFinalPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"assigned\\\\\\":true,\\\\\\"formattedNetPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"checkedIn\\\\\\":false}\\",\\"reservationId\\":\\"11f71518-4ea4-4a8e-813c-85312308602d\\",\\"ticketCategory\\":\\"{\\\\\\"id\\\\\\":1,\\\\\\"utcInception\\\\\\":\\\\\\"2020-02-21T17:00:00Z\\\\\\",\\\\\\"utcExpiration\\\\\\":\\\\\\"2020-03-31T13:00:00Z\\\\\\",\\\\\\"maxTickets\\\\\\":0,\\\\\\"name\\\\\\":\\\\\\"Simphonyc Concert\\\\\\",\\\\\\"accessRestricted\\\\\\":false,\\\\\\"status\\\\\\":\\\\\\"ACTIVE\\\\\\",\\\\\\"eventId\\\\\\":1,\\\\\\"bounded\\\\\\":false,\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"code\\\\\\":\\\\\\"3p1c@\\\\\\",\\\\\\"validCheckInFrom\\\\\\":null,\\\\\\"validCheckInTo\\\\\\":null,\\\\\\"ticketValidityStart\\\\\\":null,\\\\\\"ticketValidityEnd\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"ordinal\\\\\\":1,\\\\\\"ticketCheckInStrategy\\\\\\":\\\\\\"ONCE_PER_EVENT\\\\\\",\\\\\\"free\\\\\\":false,\\\\\\"price\\\\\\":15.00}\\"}"}]	be77b1043a9dbf96c4ccd34837beb5363212ab467a17614262720a8468b54cc5	2020-03-08 21:25:19.176-06	2020-03-08 21:25:26.264-06	0	\N	1	11f71518-4ea4-4a8e-813c-85312308602d
19	1	SENT	jose.perez@correo.com	Su entrada para el evento EPICA Central America Tour	Hola Jose Perez,\r\n\r\nAdjunta a este email encontrará la entrada para el evento: EPICA Central America Tour\r\n\r\nPuede modificar/revisar los detalles de sus entradas en ticket/event/svconcert-epica/ticket/311f332c-f834-4c5d-940d-f2a5a9bfe6a2/update?lang=es\r\n\r\n#### Info. del evento ####\r\n\r\nComo recordatorio, el evento tendrá lugar en:\r\n\r\n Centro de Ferias y Convenciones (CIFCO), pabello #8\r\n\r\nel:\r\n\r\n martes 31 marzo 2020  de  07:00 a  12:00 (CST)\r\n\r\n\r\nPuede guardar el evento en su calendario de Google, haga click en el siguiente enlace: https://www.google.com/calendar/event?action=TEMPLATE&dates=20200331T070000/20200331T120000&ctz=America/El_Salvador&text=EPICA%20Central%20America%20Tour&location=Centro%20de%20Ferias%20y%20Convenciones%20(CIFCO),%20pabello%20%238&detail\r\n\r\nSaludos,\r\n\r\nSVConcert <info@svconcert.com>\r\n\r\nGenerado con orgullo por Alf.io, el sistema de reserva de entradas open source.	[{"filename":"ticket-311f332c-f834-4c5d-940d-f2a5a9bfe6a2.pdf","contentType":"application/pdf","identifier":"TICKET_PDF","model":"{\\"organizationId\\":\\"1\\",\\"ticket\\":\\"{\\\\\\"id\\\\\\":3,\\\\\\"uuid\\\\\\":\\\\\\"311f332c-f834-4c5d-940d-f2a5a9bfe6a2\\\\\\",\\\\\\"creation\\\\\\":\\\\\\"2020-02-21T17:26:20.814Z\\\\\\",\\\\\\"categoryId\\\\\\":1,\\\\\\"status\\\\\\":\\\\\\"TO_BE_PAID\\\\\\",\\\\\\"eventId\\\\\\":1,\\\\\\"ticketsReservationId\\\\\\":\\\\\\"11f71518-4ea4-4a8e-813c-85312308602d\\\\\\",\\\\\\"fullName\\\\\\":\\\\\\"Jose Perez\\\\\\",\\\\\\"firstName\\\\\\":\\\\\\"Jose\\\\\\",\\\\\\"lastName\\\\\\":\\\\\\"Perez\\\\\\",\\\\\\"email\\\\\\":\\\\\\"jose.perez@correo.com\\\\\\",\\\\\\"lockedAssignment\\\\\\":false,\\\\\\"userLanguage\\\\\\":\\\\\\"es\\\\\\",\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"finalPriceCts\\\\\\":1500,\\\\\\"vatCts\\\\\\":0,\\\\\\"discountCts\\\\\\":0,\\\\\\"extReference\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"formattedFinalPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"assigned\\\\\\":true,\\\\\\"formattedNetPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"checkedIn\\\\\\":false}\\",\\"reservationId\\":\\"11f71518-4ea4-4a8e-813c-85312308602d\\",\\"ticketCategory\\":\\"{\\\\\\"id\\\\\\":1,\\\\\\"utcInception\\\\\\":\\\\\\"2020-02-21T17:00:00Z\\\\\\",\\\\\\"utcExpiration\\\\\\":\\\\\\"2020-03-31T13:00:00Z\\\\\\",\\\\\\"maxTickets\\\\\\":0,\\\\\\"name\\\\\\":\\\\\\"Simphonyc Concert\\\\\\",\\\\\\"accessRestricted\\\\\\":false,\\\\\\"status\\\\\\":\\\\\\"ACTIVE\\\\\\",\\\\\\"eventId\\\\\\":1,\\\\\\"bounded\\\\\\":false,\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"code\\\\\\":\\\\\\"3p1c@\\\\\\",\\\\\\"validCheckInFrom\\\\\\":null,\\\\\\"validCheckInTo\\\\\\":null,\\\\\\"ticketValidityStart\\\\\\":null,\\\\\\"ticketValidityEnd\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"ordinal\\\\\\":1,\\\\\\"ticketCheckInStrategy\\\\\\":\\\\\\"ONCE_PER_EVENT\\\\\\",\\\\\\"free\\\\\\":false,\\\\\\"price\\\\\\":15.00}\\"}"}]	1b80e9d77e5c112e40c3ac157e4257ebee386d531a6925dec29710c1713fad2f	2020-03-08 21:29:32.396-06	2020-03-08 21:29:36.26-06	0	\N	1	11f71518-4ea4-4a8e-813c-85312308602d
20	1	SENT	pablo.aguilar@correo.com	¿Rellenó la información solicitada para el evento EPICA Central America Tour?	Hola Pablo Aguilar,\r\n\r\nPara darle la mejor experiencia posible en EPICA Central America Tour, necesitamos más información. ¡Gracias por rellenar el formulario en ticket/event/svconcert-epica/ticket/d06f56e2-80ee-45a2-ae53-1c39e2eb2819/update?lang=es!\r\n\r\nSaludos,\r\n\r\nSVConcert <info@svconcert.com>	\N	e7b6bf6549993db859a86d9aa85632354c0362bcc9ac8d34714d1f0435e5ca9d	2020-03-21 00:11:14.478-06	2020-03-21 00:11:15.596-06	0	[]	1	fa31ec0a-3f43-4a2c-834c-e0afdeebcce3
23	4	SENT	info@svconcert.com	Reservation complete 00449CA6	Alejandro Castro<jandor.ac@gmail.com> has completed the reservation 00449CA6 for event of monster and man - Little Talks\r\n\r\nCategory: Indie Rock, Quantity: 1, Subtotal: 15.00 USD, Payment Method: ON_SITE\r\n\r\nVAT 0.00%: 0 USD\r\n\r\nTotal: 15.00 USD \r\n\r\n\r\n\r\nReservation id: 00449ca6-fd93-4a4c-857d-09f49315fccf.\r\n\r\n\r\nTicket identifier:\r\n - d3b34cb9-eed0-4628-a48d-5d7db6fca9f2\r\n	\N	f8c663e072a1dbc0738084e31fe7809f8a628db3f549b433c286878dfc219720	2020-03-25 16:11:40.531-06	2020-03-25 16:11:44.377-06	0	[]	1	\N
21	4	SENT	jandor.ac@gmail.com	Su entrada para el evento of monster and man - Little Talks	Hola Alejandro Castro,\r\n\r\nAdjunta a este email encontrará la entrada para el evento: of monster and man - Little Talks\r\n\r\nPuede modificar/revisar los detalles de sus entradas en ticket/event/omam/ticket/d3b34cb9-eed0-4628-a48d-5d7db6fca9f2/update?lang=es\r\n\r\n#### Info. del evento ####\r\n\r\nComo recordatorio, el evento tendrá lugar en:\r\n\r\n CIFCO, Pabello #15, San Salvador\r\n\r\nel:\r\n\r\n jueves 30 abril 2020  de  15:00 a  16:00 (CST)\r\n\r\n\r\nPuede guardar el evento en su calendario de Google, haga click en el siguiente enlace: https://www.google.com/calendar/event?action=TEMPLATE&dates=20200430T150000/20200430T160000&ctz=America/El_Salvador&text=of%20monster%20and%20man%20-%20Little%20Talks&location=CIFCO,%20Pabello%20%2315,%20San%20Salvador&detail\r\n\r\nSaludos,\r\n\r\nSVConcert <info@svconcert.com>\r\n\r\nGenerado con orgullo por Alf.io, el sistema de reserva de entradas open source.	[{"filename":"ticket-d3b34cb9-eed0-4628-a48d-5d7db6fca9f2.pdf","contentType":"application/pdf","identifier":"TICKET_PDF","model":"{\\"organizationId\\":\\"1\\",\\"ticket\\":\\"{\\\\\\"id\\\\\\":350,\\\\\\"uuid\\\\\\":\\\\\\"d3b34cb9-eed0-4628-a48d-5d7db6fca9f2\\\\\\",\\\\\\"creation\\\\\\":\\\\\\"2020-03-25T22:04:57.974Z\\\\\\",\\\\\\"categoryId\\\\\\":4,\\\\\\"status\\\\\\":\\\\\\"TO_BE_PAID\\\\\\",\\\\\\"eventId\\\\\\":4,\\\\\\"ticketsReservationId\\\\\\":\\\\\\"00449ca6-fd93-4a4c-857d-09f49315fccf\\\\\\",\\\\\\"fullName\\\\\\":\\\\\\"Alejandro Castro\\\\\\",\\\\\\"firstName\\\\\\":\\\\\\"Alejandro\\\\\\",\\\\\\"lastName\\\\\\":\\\\\\"Castro\\\\\\",\\\\\\"email\\\\\\":\\\\\\"jandor.ac@gmail.com\\\\\\",\\\\\\"lockedAssignment\\\\\\":false,\\\\\\"userLanguage\\\\\\":\\\\\\"es\\\\\\",\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"finalPriceCts\\\\\\":1500,\\\\\\"vatCts\\\\\\":0,\\\\\\"discountCts\\\\\\":0,\\\\\\"extReference\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"checkedIn\\\\\\":false,\\\\\\"formattedNetPrice\\\\\\":\\\\\\"15.00\\\\\\",\\\\\\"assigned\\\\\\":true,\\\\\\"formattedFinalPrice\\\\\\":\\\\\\"15.00\\\\\\"}\\",\\"reservationId\\":\\"00449ca6-fd93-4a4c-857d-09f49315fccf\\",\\"ticketCategory\\":\\"{\\\\\\"id\\\\\\":4,\\\\\\"utcInception\\\\\\":\\\\\\"2020-03-25T22:00:00Z\\\\\\",\\\\\\"utcExpiration\\\\\\":\\\\\\"2020-04-30T21:00:00Z\\\\\\",\\\\\\"maxTickets\\\\\\":0,\\\\\\"name\\\\\\":\\\\\\"Indie Rock\\\\\\",\\\\\\"accessRestricted\\\\\\":false,\\\\\\"status\\\\\\":\\\\\\"ACTIVE\\\\\\",\\\\\\"eventId\\\\\\":4,\\\\\\"bounded\\\\\\":false,\\\\\\"srcPriceCts\\\\\\":1500,\\\\\\"code\\\\\\":\\\\\\"0m@m\\\\\\",\\\\\\"validCheckInFrom\\\\\\":\\\\\\"2020-04-30T22:00:00Z\\\\\\",\\\\\\"validCheckInTo\\\\\\":\\\\\\"2020-05-01T01:00:00Z\\\\\\",\\\\\\"ticketValidityStart\\\\\\":null,\\\\\\"ticketValidityEnd\\\\\\":null,\\\\\\"currencyCode\\\\\\":\\\\\\"USD\\\\\\",\\\\\\"ordinal\\\\\\":1,\\\\\\"ticketCheckInStrategy\\\\\\":\\\\\\"ONCE_PER_EVENT\\\\\\",\\\\\\"price\\\\\\":15.00,\\\\\\"free\\\\\\":false}\\"}"}]	2248554e7a13b5a3c27bc433b0bd7ba3a9ada27a3644dd1b654bb6ea65f23cc6	2020-03-25 16:11:40.155-06	2020-03-25 16:11:57.911-06	0	\N	1	00449ca6-fd93-4a4c-857d-09f49315fccf
\.


--
-- Data for Name: event; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.event (id, short_name, location, latitude, longitude, start_ts, end_ts, time_zone, regular_price_cts, currency, available_seats, vat_included, vat, allowed_payment_proxies, private_key, org_id, website_url, website_t_c_url, image_url, file_blob_id, display_name, locales, type, external_url, src_price_cts, vat_status, version, status, website_p_p_url) FROM stdin;
2	nightwish	Cifco pabello #34, San Salvador	\N	\N	2020-04-01 15:00:00-06	2020-04-01 16:00:00-06	America/El_Salvador	0	USD	100	f	0.00	ON_SITE,OFFLINE	a30f6677-e718-4385-a60e-62b4d4d9f845	1	https://svconcert.com/nightwish	https://svconcert.com/nightwish/terms	\N	3bb97318e0f371577726af7f66f1caf6234c3b28364aa05616024c2c1ca8613d	Nightwish	256	INTERNAL	\N	1200	NOT_INCLUDED	202.2.0.0.16	PUBLIC	https://svconcert.com/nightwish/privacy
3	whitin-temptation	CIFCO pabellon #11, San Salvador	\N	\N	2020-04-30 15:00:00-06	2020-04-30 16:00:00-06	America/El_Salvador	0	USD	100	f	0.00	ON_SITE,OFFLINE	69455092-891d-4a0c-a652-6238cd09fa9d	1	https://whitin-temptation	https://whitin-temptation/terms	\N	26ee54d9ec94eff10f1ed74b22c7651c34cc3976fcc613039f2b87c0a35fb056	Whitin Temptation Tour	256	INTERNAL	\N	2500	NOT_INCLUDED	202.2.0.0.16	PUBLIC	https://whitin-temptation/privacy
1	svconcert-epica	Centro de Ferias y Convenciones (CIFCO), Pabellon #4	\N	\N	2020-03-31 07:00:00-06	2020-03-31 12:00:00-06	America/El_Salvador	0	USD	100	f	0.00	ON_SITE,OFFLINE	cd2b91eb-c502-4447-b668-6a22905f0c83	1	https://svconcert.com/epica	https://svconcert.com/epica/terms	\N	3fb508b0574b6b61fa17b1d4218a80f72ad670df91b4f4857e99f9efd8b92081	EPICA Central America Tour	256	INTERNAL	\N	1500	NOT_INCLUDED	202.2.0.0.16	PUBLIC	https://svconcert.com/epica/terms/privacy
4	omam	CIFCO, Pabello #15, San Salvador	\N	\N	2020-04-30 15:00:00-06	2020-04-30 16:00:00-06	America/El_Salvador	0	USD	1	f	0.00	ON_SITE,OFFLINE	930ab97f-88bd-4b98-b4ae-8855b4ee2017	1	https://omam	https://omam/terms	\N	758e1d13a621e4ab97c284d404021facc4617d5af0815eb1d7bd948987b8dff5	of monster and man - Little Talks	256	INTERNAL	\N	1500	NOT_INCLUDED	202.2.0.0.16	PUBLIC	https://omam/privacy
\.


--
-- Data for Name: event_description_text; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.event_description_text (event_id_fk, locale, type, description, organization_id_fk) FROM stdin;
2	es	DESCRIPTION	Nightwish es una banda de metal sinfónico procedente de Kitee, Finlandia, formada en 1996 por el compositor y teclista Tuomas Holopainen, el guitarrista Emppu Vuorinen y la cantante Tarja Turunen, todos ellos compañeros de instituto. ​ La música de Nightwish se inspira intensamente en música de cine, la literatura, los sentimientos y en el folclore nórdico. Además Holopainen reveló que antes de formar la banda se inspiró en Stratovarius.​	1
3	es	DESCRIPTION	Within Temptation es una banda neerlandesa de metal sinfónico y alternativo formada en abril del año 1996 por la cantante neerlandesa Sharon den Adel y el guitarrista Robert Westerholt. Su estilo ha evolucionado desde el doom metal, el metal celta, el metal gótico, entre otros, hasta el metal sinfónico/rock sinfónico como ellos mismos han declarado.​ Aunque el grupo sigue siendo a menudo clasificado como gótico, la vocalista Sharon den Adel declaró en una entrevista que no consideraba a la banda como gótica.	1
1	es	DESCRIPTION	Epica es una banda neerlandesa de metal sinfónico1​ fundada en el año 2002 por el compositor, guitarrista y vocalista Mark Jansen después de su salida de After Forever.\n\nSu música mezcla la voz mezzosoprano​ de Simone Simons acompañada con guitarras melódicas y contundentes, usando voces guturales, orquestas, coros, y en ocasiones letras y pasajes en latín, con un concepto filosófico en sus canciones. El estilo de la banda también muestra sonidos cercanos al metal progresivo​ que han mejorado significativamente en los últimos discos, encontrando así por ellos mismos, el propio estilo de su música.	1
4	es	DESCRIPTION	Of Monsters and Men es una banda de indie folk, Indie rock, e Indie pop islandesa formada en 2010. Los miembros del grupo son Nanna Bryndís Hilmarsdóttir (cantante principal y guitarrista), Ragnar "Raggi" Þórhallsson (cantante y guitarrista), Brynjar Leifsson (guitarrista), Arnar Rósenkranz Hilmarsson (baterista) y Kristján Páll Kristjánsson (bajista).	1
\.


--
-- Data for Name: event_migration; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.event_migration (id, event_id, current_version, build_ts, status) FROM stdin;
1	1	2.0-M2-SNAPSHOT	2020-03-17 21:09:38.57	COMPLETE
\.


--
-- Data for Name: extension_configuration_metadata; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.extension_configuration_metadata (ecm_id, ecm_name, ecm_configuration_level, ecm_description, ecm_type, ecm_mandatory, ecm_es_id_fk) FROM stdin;
\.


--
-- Data for Name: extension_configuration_metadata_value; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.extension_configuration_metadata_value (fk_ecm_id, conf_path, conf_value) FROM stdin;
\.


--
-- Data for Name: extension_event; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.extension_event (es_id_fk, event) FROM stdin;
\.


--
-- Data for Name: extension_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.extension_log (id, path, effective_path, name, description, type, event_ts) FROM stdin;
\.


--
-- Data for Name: extension_support; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.extension_support (path, name, hash, enabled, async, script, es_id, display_name) FROM stdin;
\.


--
-- Data for Name: file_blob; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.file_blob (id, name, content_size, content, content_type, creation_time, attributes) FROM stdin;
3fb508b0574b6b61fa17b1d4218a80f72ad670df91b4f4857e99f9efd8b92081	epica.jpg	27841	\\xffd8ffe000104a46494600010200000100010000ffdb004300080606070605080707070909080a0c140d0c0b0b0c1912130f141d1a1f1e1d1a1c1c20242e2720222c231c1c2837292c30313434341f27393d38323c2e333432ffdb0043010909090c0b0c180d0d1832211c213232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232ffc0001108015101f403012200021101031101ffc4001f0000010501010101010100000000000000000102030405060708090a0bffc400b5100002010303020403050504040000017d01020300041105122131410613516107227114328191a1082342b1c11552d1f02433627282090a161718191a25262728292a3435363738393a434445464748494a535455565758595a636465666768696a737475767778797a838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae1e2e3e4e5e6e7e8e9eaf1f2f3f4f5f6f7f8f9faffc4001f0100030101010101010101010000000000000102030405060708090a0bffc400b51100020102040403040705040400010277000102031104052131061241510761711322328108144291a1b1c109233352f0156272d10a162434e125f11718191a262728292a35363738393a434445464748494a535455565758595a636465666768696a737475767778797a82838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae2e3e4e5e6e7e8e9eaf2f3f4f5f6f7f8f9faffda000c03010002110311003f00e2b49b8bef2098747ba79e562a1a42022f61efc53edbc0693eb59d42e126c0df32c7c2afa2e7ad53b4f1bdbdb46ce165c991a4385c85273c0c9e951c7e2fb2b88cc773717688ce59fcb500b123ae7fa571f2cd6cac747344d8b6f87fa36a5fbcb67ba8d33b5b0dc0c7d6a86a1f0e8595c288ef9cdae72cacbb88f7c03cd6be9de3059adcc5a458dd5d4c8022a85f917dd8d694561e2492185ae75a86024e488e10719f73d6a79aa47763d1f43848fc232c6c164b91e50986e9a15ced5ecdeb5b379e1fb8d32cd6e1756b3be4fbf1fda5490e067e5ebd7af5f5a7ea50ebab72cda6482f0dbb664b88936e7d54af463f4ab1a149a45f6d9d2f7c9be605668ae0800b7a60f6ebd2adca5b89248e66c279aeb509165b692381ce5618013b4e060aa9e3b75c56a14b049a08226bd9d0112488e8018c82719381d4d751864d5e38c42a9e75bb26c2c0aee5e9b48f634ba9cd69e1fd31ae1e1ff49980897ce01813ebf41d7a54b9ddec3b24711aada35beac2e6e0cb648e1b691c9e3d71ebcfe750e98b3cc45d4fe63c30a158bcd6c2af6c7e3f4ad15b0baf13cf235bb3436aadb64b9907ef253e807a7b56845a06a7a75c797602d3231f3c80963f81ce0d53924acdea2b752e5969379a8797772b2d888c110a5ba6d201c724fe1e957d6c1b6012dd4970aa7acbc8fc855011f890011b08b6b7596339cfe1da9ff65bc503cdbdc428c37c6463f5ac5fa945a3230ba2827ddc64460601a82d6eeea691019b7ef62703f847f9c516f68b79739b7b80228f2acc472c4fa53e4b79526114728d9270074efc9cd0235edc6d1b431655ef56d6a85a41246cd1ee1e52fe64d682753e940324515305a8d454ea322801e8b91532ad3635ab0ab4c43552a648f8a72254eb1f14ec22148b073526ca9827b54823e29a4057094be5d5911d3bcaaab08abe59a047573caa3cbc75a2c22a7974be4e6ac4856342cc4003a935c86afe39b5b6124366ead22f1bc8e056739c63ab35a74a751da274fe485ebc7d6b3aeb5ad2ac6531cf7b12c9fddcf35e597be25d6efa5668eea6110ea4702b987f356fbcd95d9f79f9b71ef59fb4e6d8ea58371f899ede7c5da12bec6bc01be86b52d2fec2f80fb3dd46e7d03735e19e58dfb641953d1bbd48971736120f2e57007420f22a15566b2c146da33df0c542c41fa5797e8bf106f2c6448afc79f01e8fdc57a96957b6faa592dcdb1055ab684948e2ab4654f71c21005731e30d1a5bfb356894131f39aec4a62868d5d70c322ae74f9958ca32b3b9e39ff086eab240b2a421837a1e6ae47e01bdfb31925601c8e107ad7aa88d63185181e951b8acd5276dcbf68d9e6f6ff0ed446af737259b19d807158f6be1d75d66e63651e5c7d303835ea97049438eb595e5c6b292ca37b7a0a9946da20523948f448ed5542c01989c96c554d56de0811d9901761cd76b246194802b95d674bba9ae559465075a9b58a4ee713888dc869f0066bbcd06ca06b3f3d0121ba67bd733aa69e2da352d16e663d076adff000d5d3be9a171854e0555ee0c87c56596c0a8e86b8270460edaf40d6b75f40d181c29ae2ae142b94e80719a7062e8522c48ce2a1914919cd5a48c3362a49ad02c6aca7ef5697119f0e43649e2a665247152b41b580ed522aae79fca8b8ec49670007777abe14e7350c271c67a559470e702a1809cd2e4e3229fb334d2b8a918c39a09229d8ef4d639a0686963eb4ddc7d694d44c71556014c841a88b9f5a6b3544ed8345857270491d4d155c4a68a2c3e62a1f0ca8be31c11c77104076cd210c80367183c9e9ed5dae9ba5e9da45b2c32f87fce62326440b36efcf9fd2b574fb38ec74f8ad4721170c4f563dc9fad3279974d1bd7fd474209fb84fa7b55caab968428a4737791d90bd17de1fb83a76a007cf6d2a145907a114e7f105dbdedbdaf892cfec566bf36e40591dbb648ed5a9e70d4642ef6e258830405b18233f362af5c5a128a90324f19189219d43f03b67afe79a39d75158d9b296c6e228e3b7785d59770f28f007e159377e1ad36fa592d6e2d11c21dcb22f0e99e40c8f7cd7351e977b6325c5d684ef624902489807407dbb8aab2f8eaf6c52eade7447d4f02312201e5b0fef1f7e68507f6189cadb95b50d42fb4bd523d1aded3ed2f13036fbdbe65041041c1f7ebc5549350d78dd3c77d6a4de00635966e7601fc29db3efc9aed3c2da0e9c6cbed73cd15d5e4c774d309371c9ec31d2b4752d22dee4bdbdd4d2e655fddb020723a1fa8aaf6914ed6051eb739ab7bad4ee2d912016964d00212dc4477a8ee496ebf500d548f5022558ee175259dcfdf69170df4dc0605755637123d8da45a85c28665fdd5c74248ed9f5fe75049670eb7733c172cc608df6ab05c798477cff854f3aea558afa5c3797568ab1ead708a18e51950b2e3b74ab777a3c5e5179efaedf1cf2fc7e40544da198e5f265320451f2346fdbf98aa9358de9df62b7b36d93e5c95dc557d6a6f7ea0d17a08962b5044f14683382131f8f5eb59ee65965335c2998c4711841801700e719ce69ada6436f731ec9565583e69ddf249e800fafb55eb7d1c4ead27da258f7e4f979e809ff0c51a2024b0b980c6f92dbd8f4563c9fa0adb8508553938c74359da7e8f6f6774642fe64a7a12318fc2b640e695c0720ab083b54482acc639a62248d2ad4694c45ab31ad52131c8953ac74a8b9ab31a55a422358b35208ea609522c755626e4022a5f2ead6ca4db40880474c75c55adb8a826e149a990d6e79bfc4df1049a6e9ab676ce5659797c1e71e95c97c39f094fe239e5d46fcb7d8d1b6a83fc67bfe1599e3bd4df51f10ced9caab9441f4e2bdd3c1da5ae93e18b0b40a032c40bff00bc793fad6515ccb5ea7a13fdd452435fc2ba735af902dd150f18518af11f16e9eba6f882e2d547caa78afa4060f5af02f889cf8baf1b1f76403f0e959ce0a2d345e1a7295d331b6ee8573e9c1a7c8a24b60f8f997822a4b7292a7967b8e28422398237dd9414fc7b572dcf49ad0a918560d19e548c8ae83c21e259f41d49217726d9db695278ae71898e42bfdd6fe74b260104f4ce2b54da7739e494e3667d250491dd5ba4f11cab8c8a714c5725f0df59fed2d21adddb3243c735da32d77425cd1b9e2d487249c4a8c2a275ab4cb50b2d364228c91e41aa2f02efce39ad571c55578f39359495ca33247589b0dd2a85e5f448368c134badcea8bb71c835ccdfdc4463f3031dcbdab172e86b18df52bf88ae8b08f8f94f53536917710b10060051d0573f7134b75f339381c014e827fb34040ce4d3b68335ee3500ed2460ede33cd7277077c8c47353cb23b396ddc9a8910b9dbeb551561326d3b4f6b87f333851dab4a7b01b5463016ae6951ac516d3c1a9ee36b29c1e7d29396a3b1cfbda804e47e3556489633bb15b92c795e6b26e7730f9573daa9324a266c31c13566da520679cd46d632001f6f5ab9058b6d1bf8a6da02d236e5c8a36f34b1c7e5aeda1b3d8548c6ba71c55773b4e00ab1b589cd46e9f9d00573b89cf414c7a9d9702a07aa40567c93519e4558651cd445495e29888c1a29c1063ad140ae75e75bb58c82f7b6fb077120624d50d6b5db292c8f97730bef43f20619cf6a825bb165a994316c52f841b46314dd72f2da78560b811b9077314504afa64d28c55d037a1ad65a858d8e96893cab1c857e45279191e9521611fefe093cd91b0ac63248c7a9c66b0eef5ad034ebd8c2c463645f99a38f3bc91fd3fad365d6f5ef10c6d1e8d6262b703679ee36f1edda9f237af40e61bacea4fac5ec5a1e98595ce3ed138e30073d07a5759a27842c2c74bb983ca8e433a732c9f331e3079ff003d6b99d1fc2baee9b6b9b6b8b18679bef3b2b339fc7a56cc3a05d3e62bdd6f521205c948c8553ebb71d453935b45e8093ea8a9a25bdb4962ef2bc7a76a30b154b98be55751c648e84707353278eec154da5da7da2ea370aa2dc6f597dd4f63f5a4d0fc31a710e6f6196f36b9d8d3484a819e0edfd0d6dcfe1cd3e58dbecda6c432b896d9405120f507b30ec693941bd750b3317ec5aceac24863b5b5b6b52e5e3f35cb3213cf6e339e7af7ab7616fe23b17934b0f666554dd1caeaff0038ee7eb9a96df48b89e2f2ac75a984301f9ad2e17120ff0064b020e2aaddbea905bf957b3bd94698315cb219446d9e9bf3903ea28bdf4d00b3e7ebd010b269d34afd091b36e7d7767fa5486d35831349e4c103b9c48fbf73e3d86303f3a9f4cf13472b0b7bfd90dc64297570c8e48e39ed9f7ad8b76fde3c3bd59410c39cf06a755d011856b6fa7daa2c64b99cf204ca7713f97353cf1f9ff2f99e491c74c1fc335b73411cc76b056c1fc8d452c0ca542f23dc52194a12bc2a36eff6aad28a6288d3e5518e7a62ac22d021f18e6acc6b51a25598c629a116235ab51ad4110ab910ab4264b1255a44a8e31cd5b8d38ad112c152a4dbc53d578a76daa248b6d26da9b14d229319130e2a8deb6cb594fa293fa5683f02b2756711e9f70e4e30879fc2b39bd0ba6af247cf5a85b2ff00c2436cce4796f3aee27b0cf39afa334abdb2bbb6416b7114c1546763035f346a1ad3c1a8adca2ab189b2b91907ad77df0e756d4a1f11dadbdedbc8bf6e8fcd52318db8c82401c5674a33e54d9dd8b941cac8f5ebfd5ac34a8f7de4a54632005249fcabc3bc77776ba8f882e2e6d0b186550ca5d769cd7a3f89ee2e6e6799205731c6849dbd4d793f8974fd46de0b5beba8638a39721515cbb2e3fbc4f7f6a534e7f20a128d37aeeccc8253b460fcc2addc379b10643863c8f6615460858b061d0fe86b43c82232ca08239653fcc573b4933bd37629de1de5665e92283f8d2ae2583af6cd497aa16cc32f0a1b701e9ea2a1b470383d8e3f0354b58dd10f4958eafe196a6d63e2936ecd88e753f9d7bb9f986477af9b34397ec3e22b49b38db2804fb1e0ff003afa42d1bccb38dbdaba284afa1e6e3236698c65a8996ad32d42cb5b9c6549178aa93304424f4157e44aa5345b8e0fddf4ace7e45a389d61a4ba9241121603a115cfc967388599971f5af44b88962568e18be623838ac0b9b19fc96fb502a8149cd72d9a3652d2c70326e279c71513722b4db4bb99434b1c6769e959ef1b2315652187506ad3068ae573c549690969011d33d6942fcc2b6347b7f303829c0e4134db1589a0870993e95661b6491f73f41d2a78e032bf96a38cf26b43ec61170bdaa06d98773678e83ad544d38070768cd744f071cf5a8bc8f6a77617324597cbc8a8dadf69f6ad6319ce29924191c0a2e06647681c9a6bda9cf415ad142541a6b41ce7145c0c8fb3f078aaf247c63bd6f343843c552fb39249229a6062c9191d6ab32e3a8add96dfd45675d45b7a0aa4c5633187348173d6ae25bb3f6a79b638cd3b858a0223ef455c3190781452b8ec2a2cf2dc4706bd71025929e66808dac4f62c39155bc497da55829b0d0905c4db7e660032c6073c1ee6a93f80ee61f245d6ad08b773872819b61c7a54674b9bc31ac8b46944d65738649d140670390067a1f6ae84a37ba77316e5d4d9d03c2d199e2bcbf46b996640e44cbc293cd774621145b23c2af00281802b32cf575bad34c96fba62a0280e9b581a95b5696031c57303acaed80001c0c75ae79b949ea6aac9685f8e556dbb07cb9c6455b28258883e9c1f4355620159532c78ce48ad6820000ddc0ac86cc38a47b59a3c052e876327ae4d6ec0a19fccb752ae4e0ab0c8a5fecfb7b9944ccb974c807b1fafad69c36d1c5b5a3002f5c0aa42312eb4a7b974bd03cabe8c63781c119e87d45662ebcb3bcb6779a698e40de59327dc73ec6baf9976393918c64035cfdc969f508a154399012ca57195f6fd28bf7030ee7c39261a6b1580ac9c1b595094c63a06ea3b564c126a7a24e2d2e2d7c8b593214b3f2be8a1bb8cd769034d61766d66dce18e63738c11e9ec6af5dadadc426dae92374938d8c339aa53e8c4d1856325c318e4ce067e6ddcff002eb5a466f30170b84e809f5aa5378662564f22f6e62119cc4a2438fa7bfe39ab0ab70e890dc38899782c08607f3c53d044fe57f163934f44e79a728119085f7b7ad4e23a006a2e2ac22d35540a9d0552112c42ae442abc6b56e3154896588c55b41c5411815690715ac4924514e2285a5e4d512308a69152e2908a560b904838ae23e23ea474cf0bca50e1e63e583f5ebfa5775274af2df8cc1ffe11fb4201dbe7107f2aca6b4b1d143e34799786b403e22ba58997f7664542dee4ff008035ef9a17866cf4663242246217682ec5b68f41e82b92f85ba5c56be17b5bc2a0c9292e73d8e48aec35193530c459c88aac382c33b4fd3b8a88b69bb9bd47ccec89e18a337126dc649c9acdd57c31a66a018dcdb23e7bf4fc6acdadb6a66e124b9f20055c334591bcfd3b549a84ec9031ef8a77f7484bded19e45acf87a6d0e7731812daf623a8ae79b5659a230f94030fe2ee2bb3f11df4b224aa4f1835e591ced16a258e76eeac234f9aecee755c2c99d2889a6b37423861c1f43ff00d7aca1ba2976302ad8e87d6ae4dad21568a1185231e9555aec4c53ce0188e8dd0d28c64b745ce516f465e0ff00bc8a41d4f39fa57d0fe19bc17ba25b4a0e7746a7f4af9f2dedd26872b260264fce3fad7adfc31d5229f4c366265768491807900d3a12b4ec618b85e9dcef5ba542d5618540fc715dccf2881c71559f04e2acb1e7150b20eb50ca2a48bb4e40aa57d62b7b0959091f4ad271511150e298ee64c3a7456f6890ed076f735c878b2c23b665963500b7535e82533591ad69b15d5a9594640e456728d9685c65a9e596ebbe6036939f4aea631f65b348c2e1c8e9de934fd123f3b284f07a115d343a744a43b00cfeb59abb2db48c4b389e28fcc3d4f5cd4eb3c866c11f2d5eba8d4640e07b5322854a8e41340af72164c9a63255d31537caa622824459b2454be41c74abc96fc6715204e3a534857329a023b5345bfb56c341ba916d803934f942e6435b67ad09660b648e2b59a15cf4a6950a3a552885cc3b9b350d9c562de5a979c051c574b78773600aaa2d81c9c7350f7d0a4cc4f21204e465aab32ef3d302b66e6dc66aa34585a43b994f1056228a965ff5868a651761f10dca4623bad2a48663f7587ccafedee7dab8cd7e6862bd822b98e516e598bc4ca46c27fba7f2fcabd02c3fe2673904662b47223661c3b64f3f874aa725a1d5bc5e598446dec23c18d8e0348dcff2ad21249ec65257d0e4747d4755f0ce9f35dc10437b633be2349186e5239ce3af4ad1d33c5979abea2d3cbf670228f2b0f11e3d7af5ae8b53d2747b88e4826d227f318921e04c7e64572773e1f9ac6c34cd621426348b133843b939ea7b362b45284f75a89a6b63b9d2b558efd5268194a2801b62e30de9ef5d12dc4299de58b8c0db8cd79b69be207b7b8864f3a29cccdf36c1b50b81c640e87f435d3eb3ab5c59e8d1dcddd90b605832b46e0966c93df04f7ac25069957d0ea22bf8ada4f28aee900dcc455dfb40750eb8040c918cf15c169bab7d9e288cf15cb35c49e6bbf91bb23d0104815b4359d46f5c2699a4b7d981392483e601e83b1a4d319ad24f25ddc83e49f287c99e9935a72c68a22390189c0cf358d67e6cda828686e21201c2ca9b571edcf35b66d509fddb86910f393f74fd2920654d46d21b881a3607919c8e0823bd7096b2dfdc6aaf1ca4c90c6ff2c98e48cff3aef6559a33299f69049dac0f41595671a07658a30406e5a93045d50ae8b9c118e86a89781af0c0c03b84cb0c76cd4573ae5b4176f6792f30eab1f3f813da96d964954dc4d1ed66ec181c0aa116ade1448d0a0181d33566abc6c5b0147ca3a62a5a69889854d18c9aae9926adc431577132d469c558887350c79e2ad4639ad110cb08b8a9d7a542a335301815a089169f9a8d7ad3c53b92c752114bd28cd3b011b2e45725e3dd09b5ef0c5cdac781328f322ff7876aec7008ac0f12ea29a4e9571772676c485b0075a89e88d2937ccac785e95e39bed03484b6f35316db94c47ae704018fae3f2af4b6f155dde695a75dd85b0617110762c405071c8cfd735e1d3c435ad4ee2e7605f31cb6c1dabd87e1b5a1d43c3b3e95708316cdf27fba7ffaf9ae7935b2dcef6acf9dad0d6b5d7f5f2c164b2b720f017cce6b6604b9b881cdec688e7a2a9c802a5b7d2f4ed163dd24f0c39fe291c2ff003a5bebcb7b7b5320991830e0ab039a7cad2f78ca738ca5ee2b1e71e298a385241d09cd7993daefbc01477c9aeb3c5fae23dd380dd0f0335cad9ea0826df26000738f53531ba5746cad269329491b2de30038e4d49e5b79687d6aedddc472ed8e255cb7de703ad2ec1e4a63d71439ec68a1bd85b0deaeea18e0fbd76fe00b492d7c5b1dcee289b08383c37a0ae2a0fdddd2ff0075b8aec741b96b7b94909e55c1cd73ce6e334d1af2295368f76de190367ad44c09e6a1d3e4f3ed11bae466ad30e2bd14eeae788d59d8aacbce698dd2a57a88d26040e2a161561b9a8585218cc566ea57915b322cbceee95a2cdb4571be201797776b1c51b363a60563565645c15d960ce7cf2c80053d71525bdf24f72624272bd6a0d1b479278cadcc8f90795adeb5d1ed6cd98c4982dd4d6518c9ea536914a683cc1515ad990ccc4903b0adb6b75c834d31a8e9c569c84f3145a31d314d1163b55a64e682bed45857202b818c5204e6a6d9405c1a690c685148cb52702a19268e35cb301f5355710c2b504a99181542f3c4fa4d99225bc8c63ae0e7f952d8ebfa66a4c16deea3763d06704fe748761ef065a9ad1851571c679aad28e290cce9e30726b36e07040ad494f159b375a8652330dbee393455cc5152591417b6fa6695259295f2ede2f3119c956239c8fae41fceb3f4ad16ea7b68eeee6744170e65c07f98eef522b12592fe2d3a559177233f90f36ecab0638ebe83d6bd0a1d46de3b24b0b3b3fb74f100a05b2ee518f563c56d24d2d3a99a773622802697229db98c129b4e7a7435c9cba9476fe1886cd977dc35be05b724ca5c67381f5ad4169e24bbb5956e628b4f84e49f27e69318fc8564f84b4d57b14b974da623b1a69986e3900f53d0608a84acaec7bb3889749d462b0fb1c16927cc41951c2f99191cfcbce715b3a349a86b538ddaa29bdb65d852e9492107195e7afaf7aecf59b3b6bf8e5d4ac97ccb8b18b70745f908fe20cde98ac397c2c8d70a676b8b79e305a392d94154673918c1dcc31e95b7b4e65a93cb666fe95a25ddddf2cb323df28e5256e634c74c1078fe75d3dd9d4f4f45fb2daaa45800ac8015073d7839ae361f19eb5a6dbae957b6496fe5a948678632ad20ed907806b734cf10a5dd8a1ba8a728bc4864dc589faf4eb8ac249ad47736749b8bfbbb3692fa48d823e15e352037e15ae6340985031ec6b91b5d79e5ba863b980c51cfd099385c7427eb5d4dacf1cf1b18e4c94fbc0a918a40417b24696b22b9006d3c7b5737a3ead690adcc524caa501ceeeade841a3c4176e9399518b5be3073d2b8c689e6d64c892010c281ce07f17381fd692dee525a1df47a5adbe8caaf1c323c83733724e4f53cd115b3050257631a8e80607e95ce69baaea33eab6f0452b4aadcb023b74feb5d219cc6976652a88bc824f4e3bd56fa92d589d630a0639a763240accb5bd371712bc647941b6290724e3ae2b4ad572a58f5f4f4a3a8ac58402ac20a8905584a684588ead442aaa55b8b9ad148865b4a90544b522d6aa448f029d9a6d28aa421734b498cd3b154002b27c45a526b1a45c59bf02442323b56b8a648bba9495d5871938bba3c1741b1b1d360d72cef502cd639719f94b678e4f719fe75ca26b9aa59a4a2cef66b759461bca6da48fad759f1565b4ff84b4c76c36cab02adc907ef1ea01fc315c330247b5630a693bbdceba959c9596c51b8335c485e591dd8f5666249a483cf81b74334919f55622ad8404d0b116381d6b739ccdb979cc9ba672e4f73496a104cbe706db57ef2d89b72c54e57f4aaba7debdbca159164c740ddea26b4d0da949736acd2796dc3af95b028e83041ab712ef818114893c33c818d90c0fe1cd6a45f659415488c671d315c15256e87a94e37ea65943c30ecd5d0e9721da8738ddc135991461a4781c61bb66aed892aa63c75358557746b4d1ed7e16b9f374f8d4b64ed1fe7f3add6ae1bc17725ad393fbc89b91eaa6bba6391915e8d0973411e26261c951a2bbd42d5339a81ab566244c6a334f634c26a0654b89307155a28e4926dfd171cd5e91430e95144db5718acdad752895542fdd18a0e4d26ea5154210f02a3273d29ce73c526df97149b0226ebc5380c8a5db4e000152042cb4da94826a0bb93c881dcf403349bb0d183e24f125a6856e0cac5a690e238d46598fb0af3ad6cf896f6217525add436f272ab839c7a9aefbc1fa1aebdaf4fe21d4903a46c63b58db90a07f157a78b681e328d1a953d88a13ec6ca365a9f22c8b26ec4a497ecd8c1cfa1ab3672bdab07c6dcf2187183ea2ba7f883a6c367e25b83085085f3803a1aca455fb3888a75e73e95ba95d10e366759e18f187db0fd8eee40655e15b3f7abab91b72e41e2bc66d614b6d655998a0cf5f7af4fd0b505beb4fbd92a76f3594d72bd016a5a97201acf7058d6b48a0d5578c7a566c68a623e28ab5b0514ac51ca6a31c736a76fa3ca8f25a5a045b9756fbdfdd53e9cf5aeee3d7b49d0e0884490c713a65228fae7b015e63a1d8cfaa59ca5659775e4a4b4a5ce540e9c0eff005aedb43f06a432c770d26d9117e60589271ebdab49d968d90936ae5b97c57f6a49e6798450a90b1c68198b1f43818e4f1f7ab9305e0d63ed1726466b86dd80a582c83b041db181fd6ba2fb1e937378259da3d96acc7c900e6e24e72463a81534eb3180c76b6a2d209243b5c28c838e3e9c81529a4163526fed83e19bb696eed574f7858484295644231e8707db9fc2a0f03c115e416e81a4b3be8208ced0987208ea4e4e54d676a9ac9934f874cb89664b99e78e1769dbe4742724fe42b63c41a0bdec6ba8e9f79245751e163756c231e7e50460f5a7d2cc3adcd4d4a18352de97d08dca0c4c028c83fde07f2ae5e241a2cee3569422c65956f36e5c74c2c8a3a82070d56ed23f145ec7087b28f721fde5c44d9c81d41523ad5c16da80b8986a96bbe3997014a87c803f88f1dbdaa36d18ce0f59d72cb5cbe7b4b5411bac8a0cab95699b3c2a8527838eb5aafa5f8b5ac8358dc2dac6ff0024d6924cc771f4ce4e335cc43a44be525e430b1b386768a4b9cec6421c8500fa0e39f7af508757d1f4384dd6a4592554047984b64e38c7a9fa56d3f76ca3a92b6d4e4f5abfbeb5d3961bdb4951c0f2e2863218371c0e3e9f862b234966b2b68ffb40471acec5832e5bccf5c91e9e95d46ab0ea7e2fba8aeb61d3f4d8f2d00ce2572dc17c03c71ebeb4ff00ecad3b4f8e38f4e6681c83970725fdce7ad437151b3dca49bd4ab2eb56d1cd6e96505ccb1c31333bdbc24e0e30003fad493eb1fda0e99b56481b6ed898b2b4873d49207b719ad4d2acc4b2aa8ba6665073b54035adf62b7b5895096c76dc7803fa54732b03dca7a7ae103318adf93f24607e55ab1c72607455ce7239cd674364b6f24924054abf500fad6ac4fba3038c8e0e29099228a99454408a950d0226426adc4d8aaa82aca5344b2e2366a515047532d6b1910c9053d453454a0715d112588053b14019a705ab40336d67eb7aa5b687a54d7f74e15235381dd8f602adea3a85ae936325e5e4ab14318c924f5f61ef5f3af8efc6d75e28d59605252d23276440f4f73ef4a4fa1515731ee2e25d6f56bbd4ae5bfd648cec7ebd055799548e3f0a4f376c4b027dd1d7dcd127005229958f0714f008e4704537ab74a9906e217bd301ed7426b7113752c013fa560dc45e4ca71d50f5ad0947972e0f1820fd6a1d45419e40063bd00b466b5a3c725bc72db29f340cbc7d7f115a9a7ea21e501d540271c8eb583a4fcb146eac5597b8ebfe78ab8f13c576b38f9a363b8b2f4cd79d5609b68f6694dd933a1d42d72c6e63888dbf7a99090ec92af19e7a77a65beb2cb18520364608351c5788d048631b403b80f4f6ae4e595acce96e3d0efbc277223d5238cfdd9d081f515e9103136c99e4e2bca3c21279d7b60e3aef7c7fdf26bd563388b03b57760f48d8f231dacd311cf5aaeed8a7c8c6aac8dc574ca471240cd4cce6ab4b3e2409ea6a50d8acd4ae55879a81ced6e952e6a0939344810f56c9a7e6a25e94ecd0980a68cd213475a40253d54b7142ae4d4eab8a1206465020e6b9bf176a42cb46b83801990815d348b93cd79dfc40bae0c047de8c85fad4cb7b1505a9ade0ff0017e91a7d9d8e9b70b71148ca009248f08cc7d0d76de20f12daf876c05c4d13cacfc471a75635e73a1783af354d3ec23d42e65454399130391c6d03d315e83ade9969aa85b79d41d8bb539e57dc52d968745b5d4f0af155ddceb5ad4b72f6ab6c666dcb117c91d319fcaabdf8ce956f346b8753861f51c8fcf35dbeb5f0e116fa4bc81d813825777a7a5733ac28b784215e4900fd7279a6a7aa40e3a5ce3f53126f8f190c46ecfd0d75be0ebbd97ed6e481ba30c3deb9cd6d7cb64703a061fcaace9176b15d59de29c0560adf435ac95e062b467a839c5572f53801e30c0f5a86450a2b9ae55888b9cd150b38cd145cab104be2af0cac0ed6493dbcc31809190338e066b92bcf1cde4d25c2231104c363b6493f873dc52f8f754b1d46fd349d3e762b6cc524926c6723fdaea7f1f4acbd0aef48d3ad6f2cb5162b1cb868e655dccac0fa67915d11824aed6a62e57eba15aef5db986ce3686772ce36e3209500f4f5e98e95ebe9add8cfe19b1bd7ba86de5961f30a89371ddfd79ed8af14f106a96ba95e978502205001540a091df155743baf27528b78b991338d9072c4770335a4a8f3c6fb13cf667a85ff0089e7d7b59d2638edc978a76950bc60b90149da17b8ebd6bd2748d434bb94469afd5d8b6e8e2000c71fdd1cfe75e3f657be284fb1cb63e1961f669fed11cad16d771c8c7d306b574bf1af8ae2b7b810786d59fcd632107690739da47e358ca9b6b4fccae63d6350d574bd35d2eae6ed2d6290e5d8cfb32477c7427dab96f1378ce1d52c8e9de194bcd62e9fe569ad61216353d72d81c919ae64cde2ed4b5f1acea9e087beb78e0296f6adfeae33fdee7a9ae735df1278af45ba497ed92e9133b798ba744842c6a470727ad54692ebb93767656fa3789a0d05ec2eaf34ed0ec64767585a30f26d273b4b1e29da67803402525b9d70ea13a8c2a492298c7b000e47e06b86f0f7873c43f12af2e6e2eb566fdde0979c96049ec00e95bd37c1af11d923cb05f5bca57a052549a6e2ff0098d12f23a6b6d2aff4b99dac260966010c9316982f3d3b363f3e29f0d86bd35c16b8fb2fef010c22270bee011c7e04d795ea71f8bbc2579f689e4bb8012064484a1f6af55f09789ecfc43a2c533d8ddb4f6f85924854365bd481d7f2a895376bee2beb636f4cd21ede2dcab217e412ed81f8525cc6f07991f92d70c400a80e0f7e99ab173abdda6e10d93c480603dc7cb93f400d538e19ee9dee2e7503b8e0148080aa076e99cd66e21765192d51a60f34ef02701add5b8c8fa55f8e795ae14fd9d913a673525d4702859165e4f6df9cd10c8190ba3ab13c820f0054b2ae5e1eb52a75a8ede395d77be029e82a75889345993726435662539a8a28f1d6af46981d2ad45b25b248d6ac2ad4495329ada1148863f18a900e2981853c1add12281cd647893c4da7785f4f3757b28de7fd5c20fcd21f403fad72be3af89b6fe1c2d61a76cb8d47a37758bebea7dabc3b51d56ff5cbe6babeb879e77fe263c28f41e945ca51ee6d789bc69a8f896e5ee6ea42b6f193e4c0a7e553ebeff5ae42db2ccd29c977e7e82a6be60b0ac6bc03f28f7f534b1208a2518f99bf414ad62cb56b1ee90719029f70c7ccc702ae5840a5946e033d6a0d4e010ea0ea1b2bc1069a26f765655c48a78e6a6740eff2ae0fd696258ddd41cee00f1ea7b55692f96dedc811b79848e49e001d7fa53033f539312000f278a6df48a26908390303f4aa8f2fda6eb731e01cd36e64dcdfef1a2c069e9f2b258ef1d51cff003ffeb9ae9ad517c9f3570d0ca3e61d71581a5439d2ee1b1f7707f3abda45c25bbec959b630e483d2bcfafadec7af87d22ae5d6b4304e1a3398f1d0d5531c9044783b98938f415d043034926f4c3af5c8ec2a9b4d14f7ef01036463e635cd1a8cea94123adf058f2adadc9ff580ee1f4c8fe95ea31be56bcd7469238e5b0d838726323f0af448f217deb5a12dcf2f18bde448c6a8dc4c23a9a493079aa5295393d6b794bb1c8914e3669eeb793f2af415a1daabdbaed5e95629406c703814c3cd2134d2dcd5362429e94034ccf34e5a5718fa7a8a8c54cb8a2e263d173d2a6518a8d4e294b93d2ad3b087b006bc8be26dd1b6d5ac947f0b091be99ffeb57ad21e6bca7e28da31d4ade5e30c8cbf952bdda2e1b9deea371a94da25a368b2ac576ec85495c8231c83f9d1a358788e1be59f57d46dee23c642a26194fa7d2b98b7b9b8d63c3560d6d3dc2f97103225b3057638f5ab9a169d7b85b8fb5ea76a3392b34a1f70fa1158deda1daa29c6f73a3f15dfb5a69524a98caa924d78f6a776b7da5a4e701c300ff5ff0020d779e3ed5043a43c47b85c93df9af1b8b5032d9dcc40f1b948fc0d5538f36a653924ac6d788608df4a8e7079e1bf318fe95cdd9cbe4db8e78c91f85746f9d4fc332a2f32236303d0f22b8b799b6ecc60a7ca4574d3578b461376773db341bc17ba3dbcdd495c1fa8e2a6b87e48ae2be1feaf8496c647e3ef267f5aec671b8e45724e3cb268b5aea5362c5a8a93cba2914789f98cd9281e4958e59cfad5a8ace291164ba970d9c103b0ae8ad26b776367671c16904a9b8894161900f39eb9aceb8b392c9824e8195c6e52a73c577ba97d8e65128dec31b36eb51f2741c75c77ae83c14fa443e27d356f9a42fe72b16070aa3b823be6a2b27d3a480da5d42c8e641b67dff2a8c7395eff009d60dc2c69a8068e43b51c608e0f5ed493e64d0de8ee7d77730da5ed9c65156381a3f90e3823fa5729ace98ba478a2c75c6b743049fe8d7054f738dac7b75e3f1ad8d2b4578fc39672c57d330f284c048dc13b475f6a86ea7b5d4ade6b0b8541132b4641901c0c75ae45a3d4a37a4bd1636335cc8ff24685ce48c00066be51f146b737893c4779a9cc49f35cec1fdd51c01f957a1f8a7c4d77a47862fbc373de79b3a9448e51d6584f3927f0c57965bc4657441d5980ae8a51b2b892b9ef3f0d65d2741f0fdbc535e4114f37ef242ed8e4f419af4fc89edc346c8e8c38653906bcef4bf09eac6cada28f5748ed362efb716cb8618e99cd7431dac9a5e9171676929de8990fd029f6159dfab379475d0c2f1b699f6eb29ade68f31b291fa57987c30d4a0d1f59bfb0bd9de18586321491b94f19c74aeab5bb2d62c44ad325fdc993052f23ba2cb923fb9d00ae2fc0709baf1ecf6d70c55983e47a9f4a70565244d5d6c7b85beab6f7bf225c452103e6dad9a64f6b107f3e3445751d70391599a859a0b611dedbef8d48f2e74c874f72c3358ed73acdbbc719b98eeacf20330187da4f7fc3bd4191ae1cdcdc7949091bb9dea9b42fe27ad69a5a2a468304b2f4cd6536af14450a997207ccbf7bf2ad282fcca8a1626ddd4823a0a49207735130540152c6a055380b6ccb2ed279c6735651b06b5422da819e956411b6a923d4eac48aa44d8b0a726a406a1422a6420d52421c0d735e3bf138f0e787669a29145d48364433ce4f7fc2ba4c02ded5e07f18aed9fc67e4ee3b20b54503b649268d5bb0d25738379a5b9b9796572f23b16663cd58c08e30abf79ff9552b652f28e78ef5794867794f0882b4b7428ad2279b7caadfeae15c9fad4abfbd9c01eb496e3fd1da693f8ceead1d1ede17b85927388f20b7d28dc4f44684568914476cbf30159f24125cde2c6a72c7b9ad5f10db7d8274303ab4328dc854f6f7acbb525e7ce48214f23d69904d1e9ec93890fef234e58ad729a9484ccea0f1b8d6addea12c51baa3347b861b0dd7eb5cfbb99243ef4d0c5b7427269187efb07b55e696486d02462300f5207354e100dc02fd14e5a81a3a9f0e48922dfd9ca3964dbf438aa8b6f2ef3180770e2b334cbff23542fb88131e5bd0f6aeb6c668dee499976b67923f9d70574e126d753d3c34a338a4fa0dd2bed36aece73803956e9562de175bd91f6ee59863e9e95b26360a1800cb8f4ea29c61db1068d7233e9c8af3e556ef63d154ec8b5a2ca46a16111ebe7038fcebd3a5797188ebccb479228b50b7b89700a375fa8af4fb5659e30e8720d6b435d11e6e315a499426170a76b1c96f4ed512412a5c98b7646064d6f8b507e6ef48f1aa8240193debae345f53879ccc29b38eb4dcd5975e79a876f3c53b5b626e300348c99a9d5571cd23814dc42e553c1a371152326699b0d66c63958e2a7435593ae2ad44b934441928e94bb4d4ab18a240056dcba1235062bcd7e2e218ed2cae7b2c98cfd457a50e0d79d7c5f0cfa04401e3ccce2a56e8a8ee733f0ffe2169fe1cb59ecf510721c98db19f97d2ba9d47e3568e906db7b6799fb71815e0173feb739aeeb4cf86d77ab585b5e413a7933286fa56d3a74e3ef4ba9519c9e888bc4de38bbf14948121d8b9fbabd4d54b3d0ee2d1775cab2799d88af54f0f7c38b1d2d51d944b30c1258679ad1f15e9902d899982a951ed8031583aa92b41686ca177791e5f6064d3e7d8ebf2b743d985676b9a09927fb75901b64e648fd0d699bbb4b791e29364b1e48daafb48fcf3fcaa9dcddc524445bcb3a479c0f30823e99ef571724f9852517a18ba55db69bab2672a55b0457b1c03cfb68e41c86506bc5fc92f7996fbf91823bd7b5d846d16976ca4e5846334abdae999c6fb0d318cf6a2a6da28ac0d0f21b2816e8ed8ce644e801e5ab46182dee639d1a197ed401d814f181d49ac0b590c670a4863c1ed9abb25c34048660fce0ed35d328bbe8649d919f2e037960907a93525c5c19e048e5484638521403f8e3ad3523571bb610c49e49a86e637dbdb23bd6ab7219b7a1f8b7c496245a58ea53323e1764ae4aa0ed8c9e2bd9f4cd3a468639afe4cca5774a8a0b1727d4d782f86af92c3c416b25e43e6db090798b90323ea78af621e2d4bd9a6b5d32782ca45eac32c48c75c91818fa5635e2efa21c1ab1c7fc5ab7b7875fb5f242ab345f3a01d3078ae4b4e4cddc07d181ab7e29bc17de24998486511e10b939dc475e6a9effb3c5e6739519ad126a09151d1dcfa874dd460834a8259a4555d83249e3a5729aedce9575f69787c44d05c49c05138c63d31d2b95f06f8d6daea18acef1d5863a3fb56f6b1a4a5fcf1ddda5fd8ad80ff005b6ef18ce3be0d73eab46742e56ef735af357b78fc3aa5640e523018e3be2bc47c39a82af8aeeee5a3691676743b0e0ae4f5fc2bacf176bfa6e99a50b1d3d94f041c1cd733f0e2de09b5b5fb5edd8ca4fcfd33ef5a534f91c999d569b48f498b55be4b38acad924be2a033cccfb593d33eb5b5a4b4b716a8f3189e4239753c0238c549a6e8969652c9242c36b755cf1564db229716d2f94776e3b40da7d462a373263258dadff7aaaa4f520f734db5bd926948689b628c863dcfa5486cfed1febd8b60f1b0e00fc2adc7108d7686246314d2113ab7ca314f06a1ce05207aab81711b15615ea82bd4c8f8ef4d3158beadcd4e8d54164a9965ed55cd61345d0c719af9d3e2bc9bfc777fecb1afd3e506be868debe77f8a0bbbc7da8633cb27fe80b550dc11cbc23cb84b776e94ebe0cb6d05ac7feb2e1b2dec2823334712fb53ae0a1b99a6ddf347fba41e9eb5a2ee0fb13796b2cb1db230da3827d2b5ef2da2b3d356184fefa362643dd8537c31a72334b7f71fea2dd771cf427b550d46f9aff507954e33c63d68484ddd8b25db496e8b21276f4f6156ef2eadac3718ca1930383eb8ac9850cc85c8f955b0c29758683c8876b6e9707783d463a53118b7b70f7137cc7963938aa64491beec647b55908e89f689232632719cf356e58ed9ed60fb34c19b69de4f041cff00faaa85b95d6559a21d323ad412b144c01cb7f2a7148e36ca9fa8f5a8e5562bbfa8ce334ba95d0b563064176038e95d0e94c6e6d1f77fac8ba1f515cfc1314b6f72715d1e8bb5543eec1c1c8f5e2b9310f4773bf0ab53a386e7c9b0498b7ca5b041ad2b49a37423900d625ecd1c3a70859321f07e9cd451dccd6d69c1048e55bd6bca74f995cf579adb9d2bdb2ca0f978c83923d6bbff000aa83a6c6caf9c6548cf4f4af1dfede65504300fdabb7f871aff00daeeaead5dc03c10a4f7f6adb0d19427a9c98ab4a9e87a793f2d5399be615619b0b55256c9af4dec78c88e400d577e054aed50b1acdb2840d48cf4c2d5196cd45c64bbb8a6e698326a554a9dc0147356a10011512a54f1f1571889b272d8a89db34d76c1c533753930487a935c3fc56b569bc32b22e7e490135dca1ac5f17db2de683710119fdd3b7e4a7fad2435b9f2d5d8c4bed5ee1f0a2f8c9e1f48da4276920027a735e1f70499594f50715dff0083bc610f867c333402c8cd7ed3168cbf08aa40ebdcf3dabaab41ca0921425cb3bb3d57c55e2eb3f0ada879a50f3c83f776e87e77fafa0f7af18d5fc49aa788ae8cb7936133f2c49c2a8fa77fa9aa7797177abdfc97d78ed2cd21c966edec3da9047b454c29460bcc729b91115e7afe35a33ec8fc3d0a338dc2766d83a9c81fe7f1aa0cd8e950492bbc9976cf6ad1ab929d89ad64c481d80daa464d7ac691abc57b671fccb90a062bc7ee65d907929c6e72c4ff002feb5674cd5eead24558d810c7b8ce2b0ab4b995d1a4656d19ed39068ae461d76eada144b8b6799880caf19182a7a7534572f2b3538695e196691a089a38cb128acdb8a8ec33deaacac0950c3e50727dea5959248156cc4a488f74ac7a03deabc2cf7444606235ec31935da975306c79bbf2dc322248318dad9fcf8a7ceaaaa923cb1b6f1caa7f0fb54da8d935bc2241247b0804aa82a573db06b25a56380bf2e3f5aa8abec4b761ee48c923e5cf1cd489abddda0ff47ba70594ab1c0e87a815536339e493418718e2ad457526ecb16ae5f9624927249ad8b4f2e6ba58a6ff0056df2b7e35936aa37851eb56e1951264676daa5c64d44d1ac3ccebeebe195e1b6fb569d375e54138fd6b95d4b4ff001168e8c2f0dc4699c64b9c1afa3bc372c179e1bb72ac92288c0dca411f9d79afc4eb5665763291044b9db9e33dab08d4926948d6504eed1e385e5b893f78ccc7dcd7a6fc35840d4641e52481a2da4374eb5e7d6d0640623af26bd33e1800753b9e3a015ad67a5918415b567717761750ca3c872b049f23479c2838e08ee07d29cb797b6f1a6eb4122a704a30c9f7ad89dc2a824f1b85365952081e570be5a29626b950ce5f59f1a49611c70416e24d427388a00738f73517f6678df57844d2ea5158a95ff550a7f5eb5c6e8dafda5cf8b6ef56ba1c6fdb12aae4e3d0015ec361ac5b5ee96d7112ca1907faa9176b7d3156ef1d0d63156b9e6536ade2bf095d6fbc97edf6a0fce1bae2bd0745d6ad75dd3d2f2cdf7237041eaa7d0d72dac6a326a82447b6800e788a71232fd45719e03d6a4d1fc5cf61236d82e18a152780dd8d114e49df742a914acd1ee2a6a656c5535627bd4ca73537332e2b71c54aa4d57435614f1576132c46dcd7807c429927f1d6a320e56360a7ea140fe95ef0d20822791ba22963f857ccfaede35d6a575331cbcd2b3b1f5c9aba7ab04456059ee9e6c67cb1bbf1ec2a4b587ed57891caa7c90df315fd4d16d032d888d326690ef23a6140f5abfa2788a0d296480dba89181019f919fad6e49b3e25bad3aced22834d9f744c8048b8c7cc3bd7201d402e186e1ce2a2bd9d6e27925919564639dabd2a9190af2a7af6a76158debebeb3b7b991ed1b30caaadb4fae39ac19eebcf98bbe06f3dbd2a077c8e4e315194caac84f39e94d202d5d399001d17180be95497f752633c1ab7316246e1c81d2abc8bb8e6810f1966c53e3b7926731ae402714c8b24803ad69c8e96368cebcbafcb9f7350d9a452ea3e1d302e997332ab48f1b0e83381dcd6969734090095a5403a67bd7a6782bc2d15cfc32666b7125d5ec6efe84f6519fc2bce6e3c23e22d35b6be8d70c0b63e55ddfcab96a2734d33b684d45dd1724b9b573e649296cf0171cd559270d208cb88d5bee8e94e9fc23e298eda2b97b130248db553f8cfe15d169bf0c359beb1fb45c4891cd80c8b21c927fa560a8a8f53a5e2135a9c9cb6653782d96cfca2bd43e16682d696375a94ca3fd21c08b3fdd1dff003acc87e1aeb6eb109e6857cc60b211c944ef8f7af53b3b38b4fb186d615db14481147b0ad29c65d4e6c4558f2da24ccdc55690d48ed8aaeed9ad9b389218c6a27343362a167cd66d8c53cd376e29f1727269ae7934ac00bc55843902aa07e6a50e154b138039268405c040a5de05703a97c438e3bd7b4d2ac65bf78ce1dd7ee83f5a8a6f1e6a16f2219b4968e3c7cfb8375f638c553762945b3d019b269b9ac6d1bc4763adc64dbbed957efc4df7856a6f19acdbd4562c2b62aadf42d346fc6e46464651d707d2a4128ae13c59f13adf459dac74d8d6eae5787763f227e5d4d38a727642d8f24d6bc337da66b2f15cdbba44cccc9263864cf514c564670b801547e75e9115fd9eb3e1fd5ee6332b5d4d69fbc9e424e704e422f4519c71e95c0daf873c432db79f06897f24646772c0dc8f6e39aee4f4d48dde835a650302a2691cf6c7d6abcd25c59cc62ba8258641c15950a9fd69048b277fd698ae3d9803c9a6020b0efcf4a0803a0a63311c8e08ef4c065c48d3cc59828ec028c014eb02a2f2356048ce78eb48f26f9391f39f4a644556f63f9c0f986483d293d813d4f5ed334e8ae34d82461bb28304fa51468dac5b47a4dba30c155038239a2bcc69dcec5638af09d869256f353d5e48e7f217f73644e165f527a703afbd4b1c5e1fb68a0b8fb25c5d398d9e78a36daa849e304723b572c485e1492bd97ad4abad5eda5acb0452911cabb595941c8fc6bbacdb39ee9106a17cb707644651186242bb6703b7355500cf350827764d4e9d2b74acac657bbb936554700e7d4d291919a8f77068525b007414864d6c30f93e87f95367526345f7a9a15e49f634e9e22369c7193fcea2fa976d07e8be21d63c3d7026d36f248bfbd1e728c3dc74ae83c41e375f1469b141776c2da64e5da339591bb7d2b9730e298d18eb8a6e29ee25268bc91858ce3f8702bd07e17c78d42f4e3a28c57031826df8ea4d77df0f2ea0b6d62782570924ab850dfc478e95855d8b5b1ea05031e6b9cf1cdc3d97846fa48ce18a6dcfd78ae94106b8ef8a33187c15381d5e445fd73fd2b086ad127987c3eb15d4752b8492578ca804321c375ec6bd6ac23fb034f696b65348b194733bcd9383d7af24d78ff83f5182ceed097093312a47f7abd6f4bff4f91251744a310197cd2a00efd3a9abaf7e7d763a2925c8745369f60b034f15ac42575e5954035e55aa783679352bed7ede558a2b51e7e08ebb7935e9375a9da59c8d6a270cc83272d938af2cf1678d6eedd2ef49b1317d9ee811239196c742052a5773b2095b9753d774e945d594170bcac88187e22b4500ac6f0961bc29a59073fe8c99fae39adb1c53b58c07f4a911c8a84b669ca686c457f10ddc767e1ad46791b68581c03ee4607ea6be6f813edda97cdf701dcc7d00af58f8b1ad0b7d260d3636f9a63bdf1e83a0fcebca8466cb4d04f12dc76ff66b6a4b4b832d215bcbd9654610c4176aa9190d8f5aa9796f68e36ab188f70c37007d9873f98ad8d3fcab681163580bc8a32d2aeec1aa77924d6f7055a2b4ba23a854c7e4462b544330a48e58ced915645e8181ebf8f4348f6aeb079b186dbd0ee1d2b5e68350b9b6261d28c5067713c9fd4d2d94027b79a296610ec52c15fa13e829dc2c733216271d81ab516d0832324722a291024bb477e9565a3f28a91d855086b291966ea7a0a631006147d4d3d8966c9e6a27f4a92875b8ccc00f5a9e5cdf5e5be996cbb9e49429c776270055589fcbdefdd56bd07e09e82fa878adb539ecda4b6b58d8accc3e55978c7d4e09a5b6a17b687bf697a7c5a6e95696508c24112c63f018ab66307b53fa0a42e2b24b40bb207b78dd94b283b4e467b52e157a523c83d6abb4b52ecb60d49d9f038aad249581e27f18697e16b559afe53bdf84893966fc2b034ef8afe1bd409592596d180cfef9303f314ecdab8ced1df355e493155ac75ad37568cbd8de4370075f2d8123f0a96439e9593288a4726a30189e69ec36d337547528903955c546d25217a8da86c2c3d5b26b9ef1b6ab259e929696cf8b8bc7112e3a807a9ad8ddb4f5af2ef155fcfa9f8be085373470c8a88aa7049ef4435761a5a9e95e1dd22df4fd3228a18870bf337727b9356ef96d64431304c9ea320fe7595a3a5ce96d27ca05bb26767985c8f7e6a79741964cc91240524e598e43fe06a5a3a763caf51b99343f10c9358b98cc326571d31dc7d2bd6343d6a3d674a8aed30188c3afa1af2bf16696f6dadcc449e6214c93e98a97c19af1d2a29d667c43b0b73d011572578dd1935aea767e36f150d22c0da5ac8a6fa71b401d501eff005af3bd36c6d0c57ab75751adc1883ed31172fce4a03d89f5acbbdd4a7d6f5ab8d45d06e241c03c0ce140af54f05f81d60d41750bc5cf971aec43ce5fa963f9e3f0ae8495286a616e79591b7f0ffc326c3c3b6bf6db7d9292d27967fda208cfbf02bbf8ced5031802a041b17819350cd7722a90a8735ccdb93bb3a146da220d774ad2b5bb46b7d4aca1b88cff0079791f43d4578c78a7e14cda7f9979a04ad3db8059ad9cfcebf43debd62eeee45525cedae4b55f16269f9cc8303de88d5945e853a516b53c41e43182ae0ab2f041ed551ae599be5c9fc2aef883508f56d6e79edd02248d920773eb50c512a2e075af416d7670cb7b2211c8e5b19fccd07e52be99f4a99c0c720541147bee6344ea5ba1e94c47476106eb6c9819b278225238a2b7f4dd3ed67b147f31e33d0a9881c1efcd15c0ea2b9d6a1a1c686898feec12dee08aa978d21daadd074ab25da619009fa0aa533333e18e48e2baa0b539e4f42251cd4e839a622e4d582302b56c84452e15739a6a33e06385f5a1d86fc1e71da9e8ace4163c761401a96301684bf618c7bf3572e914446303fda07f0e6aa5b5d79503293dfa7e156fcc57b688f5c31527ebd2b17bdcd96c67bf1d6a26208a9a6e07b0aaac6b4466cdbb28c3d903c7fadda3f21516b52bdb4cb2c0e5248e4cab29e4114eb29c0b4894ff00cf6663fa0aa7ab49e6163eae6b24bde34fb27b7f81bc407c45e1d8ae2623ed311f2e5f723bfe3557e26c224f064ec71fbb756fd71fd6b2be11a84f0e5db0e3371c9ff808acaf8a9e2a8dd23d16dd8b0077cc474f615cfcbfbdb445d2e7931631cab2212083906ba5b19f5c3c58195c373f2027f9572ecc5b8ed5ebdf0f2d59ad22b98cb2b0f95f1debaab3e5498a8abb68cab0f0feb97226bfd45a58e3452d233641c5717a9c8ad7c6523e52781ed5ee1f10f5e8348f0a9b5dc0cf77f2edcf3b7bd783197ed1704b0ef514937ef32ea34b43dcbe16eb42f7416b176fde5bb7ca3fd93fe4d77b9af9f7c0baec7a2ebb0c924a16373e5c809ec7bd7bfc72a4d1878983291c106b39ae564b1d9a72b54678aa7a95fa69fa5dcddc870b1465bf4acba81e4de2a76f11f8ea78b72fd9edfef12780abff00d7ae5f539c5f6a0fe58db1afcaa07602af99cc3a5cd78d9fb45ec85bfe03dbf5ac9b689ddfcc2a71ed5d915644dfa9b36f0c171a61579592e63fba7b30f4359ab3c96f36590391ed5b90aecd39ee2110317531a923e6563ed5ccc1a7cd76fe65c5d795116c3313d3d6a9126f7f689bd8765cdccd1423a80703f2ac5bebe82da1786da75991f924a9cafb66b22ee3114aeb1ce6440c4039eb551ce1083dea9444d9340e66b83237535aaa3cc073d00aceb28188de471dab5d23f290120f3430452652a0e454121c362afbe0963ea2aab420b1cb62a4b36bc31e09d63c61e743a5a4416323cc9667daabe9eff90afa43c1be1e5f0af84ecf4a631b4f1a9333c7d19c9c935e59f0535c8ecb50bcd1242a0dc012c6ddcb0ea3f2af69924a994ba098aee077a81e5f7a63c9556492b19482c4cd2fbd412ceb1c6d239c2a8249f6a81e51eb595afcaeda05f242fb65789914fb9e3fad4731491e5567711f8afc5d79ac6a51c935ba394b78950b80a3a70057a2a689a35ee95e68b2b596161fdd1fd6b9df0cf8721b4d1e28a792412460b3985cae4f5ea2b5731df43f6388e576990aca87193ea0f5a272bb3a611b2381f11e929e15be8f56d0a792dcab8dd1ab715ea5e1ed722d7f4382fe320330c48bfdd61d45792f8c43d959bc7224719670364590bf503b56afc23d48f977b62cdf2e04aa3d3b1fe9f955bbba7cccc66929591ea723d47bbdeabcb7217bd56375ef5cee4348d02e077aad2dc85ef5464bd03a9acfb8bd0d9c1a96cb512edd6a0228a47cfdd526bc962d5ca6b497c7fe7e7bfa5767aaea31c3672798df797007ad79c4b0b1b3671d9b7035b5157dc24edb1ec06f56e6ec136524d88c280ac17df3c9e6bafb1bd73a6e658da30abb5430c1fa1af21f0f78cadd628daff1bd318cfb56a6a1e3cfb54a60d3049296f94003bf6a9e592d0d9ce32572b6bcff006ed56ea3893cc95fe5444e49355b41f87da96b5a835a5e892cad633ba538f99b3d8574561e1883ec497176f74ba9160ec51800bed5d00f115be91a618154b5fc8d8de79c0f5a4a6e3a214a1cdb95e7f86fe13b55482ded657981058999ba8ee79fd2bb6b0b71142aa0615462b1349125c26f1cf724d74f0011c437761494a52776c4e2a2ac893855c9aa5757691c6589029b713191c9538515c2f8cbc489a7db380ff363819a6df44115d594bc5be2c8ed91d51fe6af1ad5f5a9f519586f3b73eb535e4f79af5d36c25998f0beb5952dacf6772d0dc44c922f55615d7468a8eaf739ebd56f45b12dbc4146480d9eb8ab200c7c8c47b1aac920040c2ab76352995c1f9d1587a8add9821d21e3903f0a9b4db333f9b305666423601dce7a553770c30a0e4f6aeeb41d165b1b4dd2a2f99261837715956a8a1134a50e690dd3afa782dda336d37df2400a78a2b6e38405c02383cd15c3ed23d8eae5679b417062120752339201f5aa1b8b3127bd686a463280a1f989c0ace5e3a9af421b5ce296f62cc629ee70b9a621e38229933b01b4f7ab1741a9b4b163c9356060f7c55718005488686089fee0db9ce6a5532471120e548e54d44832454c4e78a9b1486190b45c9e6abb37153ca30bc55439e94d09976de7f29724f1b8903f2ff0a86e25f3001dc9e6ab65cf24d3a2196c9a5cbd477d2c767a378cbfe11df09bd9db47beee6999b71e8a3031f5ae3ef2ea5b8df2cee5e695b7163d69b3b36e0a0e062a254fc4d11824ee272e8562a56be8af03b69f65f0ee1d598811ac45a673fde1c115f3fb47906b66d756d48e86742172c2c03b4fe52f762075fa63a7bd2a91525a8536e2c93c5baf4be22d6a5ba727cb0711af60bd80ac48a33182ddcd598ad59977b6703ad3cae1f18e9c511d159152d5dd95962c2e4e735d3787bc6fac7876402297cfb7e86194923f0f4ac093820530f4cd538a92b315cfa5b46d5adf5dd220d42d8fc92af233ca9ee0d72df126edd345834d85bf7b79285ff808e4ff004ae33e17f8824b0d69b4a90936f7609519fbae07f51fd2b5fe205e6fd7e319ff008f7830becce4e4fe42b8d53b54b177bc4e235297f7cb021f9630147d056d68662b783ed12a878d4f2bdcd7388af7170ccaa5b1ef5d569d6d7cb6e1d74a49d17963e61ce3f0ae921995aa4f25f5d35dc56eb6f839558c607159b785c37ca711ca3795ec0f7fd6b5b53d527964962b68a38501f9900e47e26b1ee274fb1ed272ea3f0ff39a6848c898aef63d14702a9b1323f038ed52484c8fb474ef56e1b5da15c8c0c569b12f53660b7dba6daba203bc1c9f7cd4cca08f98e2ad787a07bf3656864112972031ed919fe94fd5ec92ca59239674608c46e06b365195711aedf948e6aba47b9031ea0e08a8a5beb68d89594923d053ad350b769f61270e369c8a1a761a6892c3529b4bd5e0bd85b64f0b87561eddabe97d135c835ed1adf5081b8917e65feeb7715f2deabfbbbac0c73dc5779f0b7c5674fbb934bb8722dee5bf76c7a2bfff005ffc2a2ac6f1ba05bd8f719660a3ad5096e7af354a7d4540c06cd664f7c7935c3299b4606a497601eb5cf78b6fe58fc337af0be2444dca7e841a64b7ac4f7ae67c59a918f47990b7322edc52836e48b71491a9e1ad7edb54d222845d24778c9ca9c649f4e6b4a5beb8b74f36ee58a2f94aec38de3f2e2bcd13c3d24f0e973d95c08669621f3838c3536e3c39a9fda9a5d57512218f9798bf6ae874e2de8c6aa492d513eb3ace9f25f5c35d627db1911c7eac69bf0de736dabdcb766888fd6b94bf36f2df31b456100e1779e4fb9aebfc076e1f519881c18f8fcc56b38a85268c549ce7767a0497ace49a81ee64c66ada5b0cfdda4b88768030315c46c64c972ee793cd40c58d5d36e1a4c01cd497d69f66b269011bf1d0d219c3ebeee6408c7e53c0aa77502d9e9ca0e0993b1eb4ebeb8956e9a49ca48074c76aada9dc16405e320ed0c0fd6baa09ab2264d1b7e13d02db54b032ce8ae617e54f715e8b67a6e9b0046b6b2861c007e55e73f5af33f09ebb15addc9a788e5324fc023a74e6bd02cee9d98a7607ad655f993b335a7cae3a1ba4a2c6ccc70a3bd719797d6f3ea6f2a36761dab9abbe21d656df4f648df048c1af3bb6bb7757951b9573de9429dd5c1ceccf79f0d5d44d64aaac0be39ad8b89881b7a66bc47c33e357d1ee71728cd09f4ed5abaf7c4f330db610950072cc69a849681cd17a9e93a9ddc16360f2c922a8033d6be78f146bd1ea9aacb8949894f1ef516bde34d4f58436ef391177c1c66b9c8a2321cf6aeaa542def48e6ab5afa44b7a6dfcfa7ea097507556ce0f435d178afc416de25b8b39e0b016b2c50ec95b3feb0fad608882a838e075fa53e3e0953d456d657b98a6ed6232a08c1404542498fa72be86adb01eb503c9147c9f98d3422ee836c97baed9c4412ad20c8f61cd7a86ab345616c6572028ae2fe1f580bbd4ee2f890bf674f901fef1ff00268f17eb17b2dc8b42238c2f24039cd71568fb4aaa3d8e9a4f961cc68691aaa4f645e53f3798dcfaf34570492cb1029b88c1e99a2a9e193642acc4b99034a1509e3ad30460d3dd42cafc0ce695304d75ad1186ef5192288f1c9e7bd355d98e18e71d2a69c0f2f8a8d530b4d306b51d5320a8391f4a9e3391498d13af02954fcd4ccd3d6a461272b55d873561ea07a604278cd3e11cd31ce09fa54b053e82239ce66c74e2954532e7fe3e07d29e87340ba92aae4d5a8192262c012f8c0f4a8a0da33b97391c73d2a50bce4527a94992897f75e5e303a93ef506f553d0fe55229e338a6b3ae3a7e142561b772bc87320ef9141ce29a4e661f2e29e69891a9e14664f17698573febd47e078adaf18dd99bc417cf904094a0f6c003fc6b33c16d8f18581c67e66ff00d04d26b8f236ad76b20c1373237e6d58bfe27c8b5f092e950e58b7a8ae98ea274ad2da3694452b1277679c573103cf05a87b7c166e0f722abdcdb5e18bed3784b211f2efe335647520b897cc26789b2ccc771ef59b79705b21b191d702ac34eb1c476a804fa56548c5dbafd6a9206ec8b1a75b1b99c2920027924d74577676f6d08325c29e3ee8e3d2b9885a54388dcafb8e2a4f259ce59893ef4dad495b1b11f88dec597ec4814a64237a75ff001ac6b9bab8bc7dd34a58fa76a77d9e8309142b032998cd26083e86ad94c53446a5d73d335571729bba4784f59f10794f0c0de591ccce30a07f5af40d1bc0369a4b4524f23dc4e8430cf0a0fd2bbbf0da451785f4d418c7d9d4f1ee335624b746ed5e7d5ab29688de292324d9c84641cd509a16562ac79ae94c044676b608ae2bc6bad8d02cd0261af27cf960ff0008f53582a6e4ec8d54fb85f5e5a69b099ae9d5401c0cf26bcbfc45aec9aadc31036423855ee6ab5c5d5c5eccd35cccf239eec7350188375aeda5414357b98cea39688e8fc1dae5a5bda4d6fa9caa8907ef220dfc43ba8ac7f10788aeb5dbb6214c76aa7e4841e3ea7d4d53f2173c8047a53921039adb9229f312e5271e52ba28715d078675f1a0de6f9623242c30db7a8ac9d807231cd348144a2a4acc49d9dcf6bd2b5dd3b548c9b39d5dc8c943c30fc29d3a4d239e0e2bc5ed2ea6b0bb8ee6ddca4b1b64115ee5a66a116b3a25b5f4402b489f381d9bbd70d6a5c9aad8de13b94a38bcb60c79358be27ba91a0f29582ab0dacd9e95d04c08e17ad719addf44923c72303244e18a776f415943566bea738d6b24b38b711e54f193eb4cd4f4fb8e60704bc3100467d07ff5eb534e9ae25d45d7281ae1f2ab9fbb8e9f4a355b39acccd1ccc0b4ce7f7a0e78c67fcfd2ba149a9244b8ae5b94bc28f6d06be9f6961e6ac4caa7d4ff00faaba2bad68db4b22c4df293915c107686f12e231ca30603d6b46f759b79df72c12a93d578c0ad6a5372926453a9cb1b16b53bf92ec1e49cd6759dacb1867793646dd73eb51c578f713a42aa2256382c4f35b37cb1596893c0a0b02c30cdc156ee31dc51f0da21f16a736fab4a321625fa9aa535ccf39f9dce3d3b54c14725ba9e9408379e05742491cedc9f52aa465db15a5043b1718a5b7b5da6af2c6076a1b04ac40a9b7e53d0f4aab32ba3e53248fd456948995aa9210786e08a4329ff00ade84861d54d44c840fbb5338c3e7f8c7423bd5795e63f7b81544b37fc3d3dc5bd8dcc91c8ca9b806c7d2a0d4959ee8cc5cbff0074e734691bd2c1c0e5656e45497014850400e3dab965a546cda3f0a467003249009273cd14f0929ced46619ea0515b264905d2ecb8619cf4e6923a4bae27fa8a23eb54b621ee3e5e62a667e515248088ce718a8874a681803cd4883bd462a54e940225073522f4a897ad4a0f14860c6a07352b540fd6802197a8fa54b0633d7150cbc95a9a1538ed4dec25b91dcffc7c8e7b53d07351dc8ff4818a913ad3e82ea5b8c100641e7a54ea315123310a09240e9ed53af4a92869f95b3d8f5a8a4c67af352bb62ab3e0e734010963e601521e951ae03d3c9e29891b1e14b84b4f104772e32b14523fe4871556e676b8bb32bb7cc4e4d528a6685d8af5652bf9d48bf303dcd472fbd7293d2c6ec3aa5b58d9e4832ce7eea28c01ee4d645f6a53dfcbba7cf1c2af40bf854d66c1240af1658f1934fd62311db2c9e498cb9c019ff3ed4ec061cf2673e83a5428371a59396db52c718519abd911b92247b454ab480f152ae2a4a4201c743415f6a901141340ec5665f6a8985596350375a623d77e18eb535e68f2d8cdc8b423cb6cf3b4f6aef44a0f5af1ef865398f50bc881c068c1fc8d7a51ba39c66bcfada4d9b25746d0941ef5e17f10f5237de30b901b29001128cfa75fd6bd796e08467e70aa49af9ef51b96bcd4ae6e09c992466cfd4d6b86576d913d1124619c8550493d00a78caf6e6a0b673c10ccac0f506ac281f5fad7590895216786490636a7dec9a601914e3f70d395010322900dbbb696ce411cc9b1ca86c1f42322aa97ef9fc2a4b8e5f924d5572c10e28112039af46f86d7ccf6f7962cf9084488be99e0ff004af384fbb5d37806fd2cfc5b6c92b6d8ee730b1f427a7eb8aceb479a0d1707691eb8b6c5816ef5e23adea64789af64ce57ce65fc0715f425cd9bc3038452cdb4e00ef5e0969e05d7b5cbd9a4f25202d2b6ef39b041cf3c5736154757235aade9ca4361a8c31c4db2450ec70dbbb8fad3af3537bb083cc76d99009fe750f893c2379e139601753c321973811e78c7d6a8dbf2339ae9f671bf32239e5b326d99a4310a93b52135a1040418c8653823a1a259a6b9ff005ae5b9eb4e90f148176c20fad2b5c2e5661cd4f0e0540dd6a588f3544a2ea1e6a6aaea78a7b36002290c7b9c0eb9aa737cc38a91df8eb54ddcabf078a1008c0edc3f6e87d2a3762ca14f24f7a94481860d577063901ed9cd3259acf7907d8ed21b780c72440ef7cfdf3f4a89e733125ce4fad27ca5b2071ed4d906de40e3d2b0d0d86f9ae8480e473d8d150b06cfdd1455d8922b918957e94b1d25d7fac5cd3a2156b623a8f988f2ba543daa6980311eb51053b45340c6e6a456a66ca9550628121ca73528a62a803ad283cd228731e2a07eb53b74a81e802190648cd4f1018ef55dce715661031d69b12229466627da9f129770a08fc4e2a3909f30f34f88f340ba9723f7a9c1e2a04e9c5485b0291435db24e6abb3734f76aaeef8e698982f2e4d49daa15638ce060d297c9c0ebe940ae2b7de15ab0c09e546cac0b919c66b2d42b4a01381d2a6d9d1518fe348a2dccd3af2cc00ed506b1aa4b77e4acc77346bb73ebc63f90a89f721da7b73d6b3e762cf934d2137a0b082cf9356806ec455787005580e2862448bef52002a204d3c31f4a45224a427de9b96f4a692de8281dc18fad42c69cccc3b5464e7b5342b9d8fc3824f88d9074685b3fa57ada5987ed5e57f0bede693c452ceabfba8e121c9f7e9fcabd7cb802b86baf7cd62f43375d65d33c377f72aa5dd216da00cf278af9d87520f1cd7d39e602307915c76a1e12d1b51f104af716cbf380709f28fd29d2ab1a6b5054dd476478ba384273deadc4e64e01e077ae8bc7ba2d8689abc5069f1f971347920b13cfe35ccc0879e6bae32528f3232945c65665e251600361df9e5b3dbe95179e5382bc7ae69e436c1f2e735132903914c0af2cd96fba6984ee4249a7b20ce71cd4136e03e94c92405f6e4a301eb8a7dbdadd5d4e8b08d8dbb8766da07e27a57ad6837f69a9785ad95e086468e30a4b2024115cef889ed974f995a20a8703e418ef5cfed9f372d8dfd8ae5bdcf47f075edc45e1e8ad6ff005282f6ea338de9287201e809ee4547a0169649e56ef2b7f335e19a7db4a3505fb15e90e8c1b721239f6f5af6ed23759697134afb9d97739f526b19d3e495efb974a575b1c07c5bba126ab691039da09fe55c65afdd15ade3ebcfb5f884e0e428c564db7dc15d705ee23097c6cb24d358e297351b1a60231e29666c448074c546c78a590e5147b531109e69d16735186e71562314c44cb9c548a7729538a45618c11513bf96d900d03229b721aacce0fd6acc8eb22d5190367029a258f12203d714e751221c1151c712ff0010c9a252910c2f53da81166d9f30edeebc54a652632a40aa76990ac71dea690f7cd6525a9ac5e8348f6a280491453248a61f32f39a923e2a39893b4e30334e56c0abe84bdc91db2845306f2a318a092c0800d3add1e5042f057ae68019fbc079191520391e95218dd7ef29a8c919c520004d3d690014e140c56e950b30ce2a63d2a075c9e28191498cf156621f28aaae0ee515691405a6c48acff7dbeb52c606dce4e73d31c541fc449f5a9e33d053122ca640c8a716e29aa702919b8a450c91b155a43c53e47abfe1dd29b5ad662b703f76bf3bfd050dd95d8ad7762dda782fc4375a72dedae9b2cb0b0c828413f96735897115c5b4e629a2922954e0a3ae08fc2be8c86fe2d17490a842809c01c60579cc9addbdef8ce2b8d45156dd50ed678f706cf4ae78d76fa1a4a9a5d4a96df0f64b9d0adee239d85fc987656e14023a7ae6a097e1c78990168ed92e4633fbb9067f235e9a5c9d9246c0a1c1047a55eb0bd951c1c938e9f4ac95795cd6349347845df85fc416e5c49a35f2e073fb9623f315cfcd14b0ca6399191c75561822bec6b3bb4b98873f363a57877c6cf0e8b5d56df59814049c7972803f88743f97f2ae9854bbb18ce9d91e5e8703152a9155949a994d6a668b00d3b351834a0d49489334d2690b714c66f7c5000c698690b1fad69689a61d52fd63ce231f339f6a24d257634ae7a47c39b51a7e8125d4836b5cbe467fba381fd6ba69352881fbf5ce897cb85218fe58d142a81d80a84b1cf5af3a4dc9dcdd248e946a91f635567bb1f6f86653838c7d6b2a11920d5e921592d896c029f3024e2a0d20ecee79f7c46bc8a7f15158db708e3556f407aff8573b6e73d292fe73757f7133f25e4279fad16eb870010013dfa57a708f2c5239272e69366819e5758d1dcb2c630a0f61d6a29180196fca9d72be44ef12ca926d38dc8720d5490e78ea69886b3a0e73f8556762f9ec2a73094524f5a808eb4d12cf4cf0a24763e0f4b972079858e3f1ac5d42f1261f3a831170aca7b827b54b6f72b16896f0971b15781eb593a821765676cc6dc10a7ee9ed5c965cd7675395a3625b1b0820d6208d579f3724039e3b0af48bfbb64b6509d3fa579e6851eed6a020e427435de6b40436818f1d2a27ac9174d2e56cf2df16c8b278925d80606071f4aad08c28a83519bced62673fdee2a746e2bb76491c8dde4d92134c6a42d4d2d4008c78a90ffc7bae7d2a1634c78e49212c09da0e298980280f26a7491474345c58a7f63c1769f7c398e51e9c647f23504f0422c639501dc4e09ed42698b62d79ebeb51bdc2fad67a941daad5af96f32a954e4ff153b0ae35a704f14cf303574226d0a0d3af61b98165ba78f6c1e57f0be7a93e958915a4520eac0fd680d483cdc77e698d2839ca827d6af8d3a21d7737e38ab0ba5c1b7784620f1863d0fe145d0599976ec4b371c6334f6940c8c56a8b6b543b48084704d64ca36c8ca7d6a77656a90825c0eb4547b4514ec2b92847b87486242598e00af44f09782acf0971aa279ce4e4464fca3fc6b8fd0f69d5a13c6172727e95e9f65751c412269797c018ae6ad36b4474d1827ab3ad5d17488e03f67d3ed53e5ed10af23f16e8c34dd5e6b9b64458646f9947186af4fb7d5624536dbf32edfceb8fd72de59fc39aadf3a86d9305e7d062b2a726a46b382e53845208fbb8a63c511e59466ab1b894f015547bf341dcdcb499aecb1c62b2420e016cfb534c3291941b87a138a4dc17a5491dc283f31a622378e65193136319e39a88919c1c83e8456a0b95650abc7f4a5110ce76f27b9eb4ae3b1892e015e78cd4ab22ede08ad47d12e7509e3b7d3acde79c8dc5631dab3af74ebcd3a5315eda4b04838c48a4534d315994c91b8e0f19a99180239aab8e719ab696ca472e45532513f98b8eb50cd200b4cf279204878ad1d12c16ef5bb2b73179e1e550c8c4e0ae79a4ecb51abb31c967200079af5af02f874e9f682e5f1f6890027dbdabadbcd034f5b448e1b4b78117b244abfd2abc93c1a769d3dc39da91a139fa57254aae6b9523a214d47de66078aef84ced651382b12ee9be70bd470a0fa9a9fc09a25dda41713de28785805884a32c07d7a62bc8eeb549eeafe7b89246fdf49b9bf3e2bdcb43bd4ff8466c4c7248c9e58c190007f4a5562e9c2ddc51973b34e42abc60605165f7f6e475ac7b8d418640c54965a8a2af98fc81c1ae73686f63a0b8bab9d2d16e238da48c72db39c54cff00d8de38d1a4b1bf1946f538653ea2b274cd793fb6934fbb402da64255dbb9f4af3ff88b797be1af174571a5ddec824841545e839e430ae8a709377429ce2959999e31f865a9f86a492e6d035ee9c3912a0cb20ff687f5ae214e2bd8bc25f166d9996d357876193e52d9ca1ff0aa3f10bc0769f6597c45a06d36e7e7b8813a267f897dbdaba23369f2ccc1d34d7340f3257f6a7799ed50a1f5a92b433438bffb26985bfd9a7531a98cd0d174a7d63515b55711e4162c7b015e8fa5e836da4c0c91659dbef3b7535c578291dfc408cbd11198fd3a7f5af4b2481d6b8f11277b1b535a5caa60f4150346e0f4abe32734a8993cd739656b68a42df749155fc4ba825968f345b6295dd08642f8da3d6ba181428c90302bcbfc5170607bb4896d6249a4c158fef919efcd55287348529591cb88c9e475a9a2038a62fddeb56ac64b74bc8dee62f3e107e78c36ddc3ebdabd139846da3a8fca9140272148f734d7f2fcc62aa5549385ce703eb47998f5c50012f4e4e6aa3f22a7770c3bfe350e320d026685b4ef340909c901702a679084236a823b2f4acc826318182411e953ef67f9ce71584a3a9aa968743e131bb531267217079ae97c5d7e23b3001c0038fad737e1005ef1cf625466a5f1edded758978aced7a96374ed4ae70acc5ee1989e49abd1fdd159f1f2f9abe9f76bb19c711e69846697349523131c735674e459657858310ca4f1db00d56357743557d5e3ddd1416eb8e943d806dc5add421ece46091160c73d6ab2ec44f298978f3f749ad7d527f36f1f2783592ea30703f2a10d88d63130dd1e587a7f10aaed0053f70d58476439fc8d5970085690a9247507914eec564668400f423f0ab30f1f75b07d0d3df69fb8e33ee2a3624f1c363b8a0362d091d58857c5385e4b1e54aa907deaa12c8475cd4cb3db82c678d98b28da55b18a5606066127391baaade2904395c6783839a92366182c703da9d39492160304f5f4a6067120d14bb68a649afa0c2b2ead146c7686c8cfa715e8e6da38e2d35cb80376377bd795dbcd2433078890e3a1ae813c4b24da7c31b63303e40cf27b57354836ee75529a4ac7769730a789e58643f7635653ea0d32fee89f05eb76f0af98cb3ee65feea9039ae2efb519deeedf511201b9446d8ec3b541aa5cdc25d1786e5d22b98c09006e1f1eb53186a8b9cf4665f9608e73f9d46f851c0356ee20f24292c8c08072a738cd5372067ad741ca40cec4f0314cdc03609e6992c8d923a5323ebcf3f8d5d88b9a30dcaa901549c77c55c5bc231f2102a8c2cabd462ac2b06230dcd4bb168edfe1f6a7b7c4a515464c079c76ae975a11ebda5df2491abf9790188e87ae6b88f04db5c49a85fdc5b292d0c3d87407aff2ad9d2efa6b2f0bead7926482ee1598f07e95cd35ef6874d3b72ea798dd42b6f70c8b20900ee28de46083da992c86691e42a01639c0a6e7915d671bdc963393f5af44f0b431e8a6d9d5165d4ee4e40c67ca8fb935e7f6c424aae573b4e715d469ba8bc28e15b1757670d20eb147ed5955bb5635a564cf4cfeda8ef90827e4ceddc4e031f6ae17c75e24885ab6956ad966e2423b0aab37896d9667910916b670f956b1ff0079ff00bc6b8791e49e579a46cb31249359d2a5add9752a696445b4f071d6bdb7c0b35beb1e1f4b68b11cd6ca03a8071f9d78c2c65828c609e95e91e08d44e856cd1cb1c64cfdd5f9ebd4fa556217344ce968cebae34991e5644e48a860d2a549c24c76c67861eb5b116a16ac19e197cc63d6aacd761e456240efef5c27426d3b8fbad223bcb09200d89e03ba36ef55b4982d75fb2bdd1b54811ee0c656195c72b9ec3f4350df6b06d2e2dee95b084ec90554d5ae65d1b52b2d7a0466b45399954f4cf19fd6b6849ad0b9c13d59e41ac69573a2ead716172856485c8e7b8ec6b6b49f1c6b1a4e877ba4c4eb25b5d4663224192808c1c7e15dff00c4bd2a0f13787e1f12d822896051e6e3f8d0f7fc2bc6d7af35db1929c6ece3927076448bd29e334d0540a70c7ad51285cd349a7647a134879e8b40ce93c117696fad142858cb1950476eff00d2bd104c5bf878ae0fe1f46927888c722825a26dbc77e3ff00af5e932da3264043c570d7f8cde1b15064f6a9e3181b98e00eb51f96ea7041156362ac4449f748c1cf7ac0b30afbc55059cf2c322ef8586038e838af35d45926bf7990a9de49c8ef5a7e24be45ba92c9201004621b9249fceb0c2fc83611f535db460a3a984e57d078a55c06a6012740a0fd0d21728df346c0d6e416011de9a78e94c56663c2b7e552796e4711b50031be951b9f94d3fcb90b608c7d4d2cb12ac477124d022185772eece38a90938e4d45123263827de9f91ce79a87b8d6c77fe04d3d5b4f69d9812f213c7518c01581f1014c7ab05cfca4645757e054683460edfc6c587b0e6aaf8f3495beb06ba8d7f7d6ff003103baf7ac232b55d4ea946f46c8f338473d2ae2b10a38e2aac5c60d5bc653f0aeb67221714873e9481b1d695dc2a13486309e2ade8d26dd41c8ff009e6d55460253f4f9563b99091d63205022d4efba6209eb4c311642476155cc999149ebd0d68a14580e7ad228cd6c7734871ebc53a72bbced1c5443af2299204283f2e7f3a42b93d48fc6958f602a12ed9c0a60c95a4c93934ddf9e319a67e34abc3500489335be708194f404f4ab598248c32361f1f30cf06ab91bc71554a15241a3716c4b87048c5150f5ee68a2c1725462d9c52b40c9821b048cd201b4719fa53d5ba6473f5a432d5b4d7088d19c18d86306a49a46ca23321d8b8c28c62abab12300d324959e52dc7a52b2b8f9992b39c5579652338a5f309246daaf2f3d7ad3484d91f25b279a997007039a8871522eecf02a9928b312337de357d62da3681c9aa90b7963b3311d7d2a40ed824b1c5432d1d0685697b0b5c5da6aeba7dab809290d86907700557f126bd68d62ba4e97e67d954ee6676c963deb9eb9ba72891820051c91d4d55ce4e69286b7653a965642f6a4dd82294d30f35a1916627c1ab5bf2a769233c1c1ed59f828f8353c6d499498e9234c702a16e1703a53ddbdea176cd084c9a271b5727a1cd6f5bea076a70073c7d2b12ce3590e09038ef57c6230376178e0fad673499a4763b6d3bc490a08e2550a08c331e95764f1058c50967259f6120e3a7a5704f722052801c36391d314b0cc264962578f0eb8f9cd73fb25b97cc759a2ead1ead6f73632a12f83b18f7ad4f0dea4353d3aebc3778374f0e5549eebdbfc2bce34fb97d3ef12e036446d9641fc43bd4efafa43e2d5d56d03ac2b203b4f1b947506abd96f63455528ab9d6691e2087c2965ace81aea48ff002116c319c823a7d2bcd4632481804d74de3cd76c7c41af2dcd823089620859860b1ae640ae8846cae61525776e889140a902d300a9171eb544a1c052114fc8a6b352034fc35a81d33c43657390144a15b3fdd3c1fd0d7b5c1aa5b5fa486de459021c311dbfce2be7b724f435ada078967d11ae4a8f304c8519589c63dbdeb0ab4b9f55b9719db73db34e9975285a5584801d9304fa1c566ebd3c9a7595c49711bac2bca489ce0f6ae3fc19e2d874cb6bd6b9918b3b1640e7e500f358daef8aae3585b98f2512539da1b8c7a560a93e6b17cda1cedecf2dedc35c4c776e380d8ed4d87e56299e94dc1da012368f4349111e69fad772ec62cd28edcb2eee2a095940c606455913810e3dab3267cb1349202c45312e403d2af45283c1e6b1149505f3d4d5fb79f3804d368499331f9fa7155ee5b11f152bbae4d55b93b928436394ee8d4834aca030ef55e1dca98ed8cd4cddb19fc6a1ee08f53d264fb36916a891118446cfaa91c9ff003eb55adb50171e34bad26e726396028a0fd2b0ac7c5205ac36adc3245e57d78c03fa0fcaa8eb77b736fab5aeb76d8122001bbe08ec6b18c3ded4eb753dd4d1817b6ad657f716add6290afe46911fe5c549a8ea2fab6a5717d244913ccdb8aa7406aa83c5751c77d742c139a8e46e82981a91cf4a2c03d9f834fb385e513ccbf7634c93f8d5626af69f70a96b2db83f34ac063da93d102d5ea572dcf356fcfdd02f3daa8c99048f4a746f8e0f4a2c03a4627f0a556071eb48ca73c77a54423ae280252bb87039a85e120e73561483ce69fb4383d8014019bcaf514e24eea74b80c4546dd463391d734c44e8db4d432125b14aa723af34d273cfb50034514668a604edd7f01483fad1454f419327dea897a51450028ea7e950cdd57e94514d6e26357b54cbfd28a29b12265ff1a7c9fea4fd28a2a0b2949d45368a2a910f70a68ea3eb4514c44f71feb47d0511f7a28a5d0aea23544dd4d1453426491f515b327fc7adbfd28a2b396e691d875e7fc8397f0aa769f7c7d28a2a3a0fa8e1feb65ff71bf91acc3dfeb4515a40890bff00d6a7af7a28ab121d4f5a28a9290fed4d6a28a4037b540dfc54515484cb117fa9a67f11a28a8ea57413f87f1a48bef9fad145522596ff0083f0aa7275a28a681887fe3d87d6a587b5145025b93fad453fdca28a486c6c7feac7fbb4e7fba28a293dc16c09feb3f0ad1b9ff9034df48e8a2a5ee8d23f0b31d7a528e868a2b5325b09de83da8a28188d4fb0ff008fd8beb4514741751f37fac6fad361fbf4514ba0163b1ff7a907dda28a4304ea2ac2f56fc28a2931a33e6fbed49dda8a2ad1220fbd4c1dbe94514804a28a2988ffd9	image/jpeg	2020-03-23 16:17:24.834417	{"width":"500","height":"337"}
3bb97318e0f371577726af7f66f1caf6234c3b28364aa05616024c2c1ca8613d	nightwish.jpg	32225	\\xffd8ffe000104a46494600010200000100010000ffdb004300080606070605080707070909080a0c140d0c0b0b0c1912130f141d1a1f1e1d1a1c1c20242e2720222c231c1c2837292c30313434341f27393d38323c2e333432ffdb0043010909090c0b0c180d0d1832211c213232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232ffc0001108012701f403012200021101031101ffc4001f0000010501010101010100000000000000000102030405060708090a0bffc400b5100002010303020403050504040000017d01020300041105122131410613516107227114328191a1082342b1c11552d1f02433627282090a161718191a25262728292a3435363738393a434445464748494a535455565758595a636465666768696a737475767778797a838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae1e2e3e4e5e6e7e8e9eaf1f2f3f4f5f6f7f8f9faffc4001f0100030101010101010101010000000000000102030405060708090a0bffc400b51100020102040403040705040400010277000102031104052131061241510761711322328108144291a1b1c109233352f0156272d10a162434e125f11718191a262728292a35363738393a434445464748494a535455565758595a636465666768696a737475767778797a82838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae2e3e4e5e6e7e8e9eaf2f3f4f5f6f7f8f9faffda000c03010002110311003f00e5b4db80fe39bb46387f21b3f98ada8224f3d63640c819b208f506b9bb37cfc47b83d8c073f98aea23389cb678c9ebf43fe35cad1d29dcc4f144ca7c4f6050fce635dabd33f31ad8b08a3667564f9e4e4e7b1ae4bc48c5fc5da41ce33e9fef575d66514eef9cb038e3de9b5a204ce67c5d0dd59d85b42f7225cc8db5db8c2fa56959a037c8b93c2d667c446ff897da1ee25e6b46c9d52fd8f0323ab51d102dc778960f23456f27942e38f4e69ba096fecb8f2482a149fd693c672ecf0d4cd1b10db94820fb8a83c35297d2222cc49312e49fa9a2da5c2fad8d8994fd8aea50d9062236fa706a9781164fb05ea962a378fa9c0152dec920b59d40c46633f37bd66fc3ebe965d3aeda7901225c67a761425a037a9d4cc92490ddaa49c3c58008e01f5ae2fc24b30d7b526924fde807791d33bb9c576b9651274c143fcab8cf0b4f27fc24baac6c14202d8e31fc542d988ed34eb7b9f2e2134e6590b162ce338527a0ae7559a3f894b187254313c9f58cd7556f2e248f918d8726b8a7ba65f8a422dabb18673dff00d59a16a173abbc7fb3c2ccacb90725cf619ce3f535ce78aee19f5ad3bca7fddb46ac7dfe6ae8e558a58d96440cb920a9ee0d719e37b86b4d574d8ede3554da140c7604528abb0675d7d339d409072bc01c743547c477170be1e573108e513003073c0ef5524b8bf1741e48c85e31c66a0f125fcf1787fcc9943032020e78c1e284aec6cebac0c8fe1ab07206dfb2a163df3806b0f559251a06a04f1c00a475ea2b4f4cbc497c2f6a80e08b68f3f8a8ac4d6ae847e1ebd6043ed51919f4228ea059f0c5c4abe1a51cb1321fbdd719aea9e791b4dbe2d80be51db83cf439cd70de16bd33e81136cdb972401f5aeb9e5ff893dc6792508fd287b88e5fc077533c1a9f985a5e502ae7d8d74521569176ae1dd143b0ebd2b91f86f72258afbe5da448a3ebc1ae8e19c6779cf069cb4608c3d099aebc432c1e748a220cd85ef83d2b7eefcd6b982570060e1958f6cd725e18bf57f18de468ad950e1893d79aeca7626505ba673fce93560bdce77539f1e2fb483f819e335a52c7334e628b6a206c48ccbd47b7e9587aadec2be3cb28595cb931907b77aea59b2cc548fc4e2863b98fe2ab8fb3bd9a8c233a9c83d7ad6d5e4216789634539452093c6ecf7ae57c7925b2ea5a619f7997f80af4ebdebaf976fdaa1214edd833edcd0d682b95fc63384f0e40c0156f3f1f30efb4f3496be54ba56965d55d846841db9da71d6a878faee18341b769cb63cec0da33ced356b4bb80da3d8aae32204ebe98a3a0227d7c449e1fba60aac5029e5720f358ba12acfa22485368323630bc7e9f4ad5f135ca43e119cb8cfca3381cf5acff000a626f0c46cabcb17c6783d68e834685ddb245a55ce62242c44e4f43906b1fc271c5736579279291847036a2e074ae9f55511f87672fc622624fe15cd7826e20fec7bc9619015f3f938f6a3a05cd88acf2436df9429233d0d73de174867bad4c2c689b082768033cb75aeca1601010db94aee1815c47862e2569758366a24943a8c1f947de6a16c07410d829dcad82ed923e99ac7b2d0ac3524bfb8b8b384ba5d32162a093d3fc6ba9851d2125ce09ec0d65f87b6b585f8c920dfc993f95080ca8b42d3527645b389790385c6455cb5f0ce95717d2c2d651ed5803e71839248a7c26769d9de129b65d98ce723d6b6e0ff9084cc38dd6ca3ff1e345c0ca1e0ed1776459aee19c1c9aa92f83b457bed9f6553b9492589ce722ba68448239189fe2e2b35663fda7186eac08fd68bb11cecbe0fd2d78fb2260be33cd44de15d23ed71402d57e7ce7ae7a576ef08644e3fe5a1feb540c61b56b701467e6fe545d8591cacbe13d2d6611fd957279ea6965f07e991a4445a8cbc817a9aeaeea2537f0918e54f4a92e22d896cc17244cb9a7761647152f84f4b0edfe8c460f3863505df86f4c82d8c8b6a3391d49ee4575734673371fc59acdd40136617b6f5fe628e663b2332e7c27a6c173e5ada861b49fbc6a843a3e913dec968b699991779018e315dceaa856f10c6996c62a2d2b4e8a34bd9fcb53295237639031d28e662b239483c39a7496504bf645cbc60f53d6a483c33a7311e65a2fbf26b72c541d1ac88ea2219a75a89df5174655f202a9523a93df347330b239db6d22c7f7f18b48888e575195cf02920d36c9b519e3fb24581006fbbc7535b369181717ded70f5522b7793c4732862a82d86e03bf268bb1d85b7d06c24d99b383247f70564dcd8e9f6faf8b78ed63dc739e38e9e95d459b5cadfa43e52980818901e9cf4ae5f535c78e9b0570011d7da857034adf4cb3939fb3c38ce0fc83ad676b1616315e5b21b64dd9038181d6ba2b4f2f685183bb9c7a9ac0f110ceb960b9c7ce38cffb5427a8335e5f0ddaa5bed11ab363fba300fd2b3b59d2ec2dbc3f1cad6d182af8dca00aeeaf046913390000326b84f146a0977a3f93044ec04a0838e0fe142dc4745a74364ba6db178a004c60f2828aada76a19d3edc35adc0db185e23c83c7a8a28b0cc9b570bf1067f7858e7f1ae9a23952770e58f6ed8ffeb5717f692be31b9b8801948b762ab8c12719c574f67214d352e265653b77b29192060d12123035eff91b7481effd6bb0b2dcaae5b07919ae275a9a2b9f1368d2c2fba37e548fad76b6806707d7d6896c816ece57c7c77e9f6dbb8fdf9ad5d3d436a9838231dc56578ffe7d36d88ff9ed5b1a50cea4e7d1687f0a05bb20f1c284f0ecaa38e47f3aabe19e74688f6f2d7f99ab9e3be74197eabfcea978647fc49221df62f5fc68fb21f68d5bb21aca719fe0cd61f812c4b59cf2ab92d1cbc2f6391deb62e01fb24ffee906b37e1d19459de1246cdfc0c7b535f0b07b9d660aa36e23250d71be187ff8a93570739566ff00d08d763c1e3bed20d71de172a3c49ad30ceee73f9d25b307b9dc5a3a48b185192aa41fd2b8d99bfe2e92e06729ff00b21aec2ca4d88bb5420da703ae7deb84bc9fc9f890653da32c48ff0070d11ea0cecd9c999415c0ea7f4ffebd717e3a9de1d574f917a639e33dc575365762ea08a549370906efbb8ef5cc78e149d5f49c60e7191f88a21b84b636228e7b89629de7933211f2938118fa551f1c3e34d9e159964c3c7bb098c1f6f6ae8eca2f2ae22558f393c8f4ac5f1ce9621d1cbc24ef92550413c7f9eb44771bd8dcb01b7c33680139fb347fc8566eb702b7876f107cb9c0e9ef5b363191a159ab007fd1a3ce7fdd158be22918787af02f07039fc692dc3a0cf0cdba45a2dbaa918323018f6ae9a4603479392721b8fc2b95f07bb0d02d41c9f99cf35d1472b4fa13965e486e687b81cb7c397468af4a8d9fbd191ef835bf16ff0024127ef723f3ae6fe1e809697cca0e03e727bf15d1c4fbd10282760db83c74a72dc51d8e57c2d011e2fd45db6e183e3079fbd5dddca8455393c1c7f3ae13c2cf2378bef898b6a057c71fed57777ec23881240e4ff2344b70470ba8157f88566c572cbe5f39e9c1aede3daf9665c81d322b86982cbe3bb37f52b83ebc1aee62c249b491cf6a1f40b1ca78e96d9f54d33cd2c0f542a78ea2bae966d93261b0080bfad71de3b741aae9993c0e71f88aea9c47733a039dbc1e3af143d902323e2080de1fb6120de3ed031dbf84d4b04a5743b470d848e1563cfa2d45e3c731e9369cf59cff00e826a6b6596e4695a7c4a035c46818f5c2ed19349ec347197daf6ab7739382622788caee18f4c574fa05d2ea164ea85ed678f968b903ea07a57a65af8534bb58a3115aa65470c4649ae5fc5ba40d3648f51b78f050ed931dd4f143652b1584d7135a4b6571212922141bb9e0fa51a06969a669b770958f66fc92a38618eb8ac78ef5da1746e5a23f9a9e9fe7daafc17e51bccdc4a38d8e0f707fc290346dc33239f9136aaa70315caf83ceebbd5f8eebc7e2d5d1b9f20b3678d9c63e9585e120e1f542caabc2600fab5324e8886505b20a7618e45637871843a75f3b375bf909f6e95a8ee7cbda3b9fe9591a07fc82aff7746bd971f5cd1d0459ba765bd8ff00783cbddf99ad0b772b3c9bb1feaa31fab563b99659add76e0330eb8e4fa55d6905bcee667541b539638ee690cd0132f91c11cb706b36139bf87d371e7fe054c1aa5879841bd833bf380d9fe429b0491cb788d0c89228c9254e71cd3036cb8104647794ff005acfb639d5a272470ef8fcaaeb0678e2c0c90d9c5524c26ad112a402aedb4f638a4226d4b77db62753c2a1c9fa914b7b2edfb37381e72d477c7cd6850c592d82b9e990734cd4464c08c7fe5a27228019295cce39e4f1f95655faeeb7185fba57f98ad49426e97726e3c722b2b5b9fecb61218d3e76dab18c672d9e29a1a346fa46376afd30dfd6a7b39c258dd11b771524027dab0ed67be92d165d49764f1c85991075153ec9255902ee01c72dda801da67cda3d963bc43f954f64717a54b8e9d3d4d47a4a634ab21bb8118a96dc46b7af8503d3db9a00ccb5c1b8bf23fe7e5ea3b73b75cbb2bd7ecc9ffa11a7da2e67bd00819b87a86dd4c7af5dfcc79b643cff00bc6981ad627f7a9924e6b0f55b641af2c800dc18f3ebc56ddaee12211dab2f5652dac2e0f3e67f4a91a2c59220119c6496fcbad60788541f1369ec47461ffa156ddae22758c13f7b3cfbe6b275cc3f882c0ffb6bff00a1552dc4ced358b80914a154162a4006b904825db24ee300008a3dfbd753ac47b8b0049c9feb59179c58385ebbf8a91c4bfa70c69f0607f00ed451a71ff897c1d3ee0a2988e5e08b6f8e2770dd51c91f857576a51e1656c1c90181f422b920641e3876030851867f0aeaed4068a56c7cd91cfe14d82392d66254f13e921115554ff0f4eb5d8d8ec3201bd77039c679c572daac65fc4da6e3a8ff001aea60520c8422e719c8a4f604725e3938d2ed803ff2d735b3a4e0eaadd7941591e354074cb6014b02f938edc56ae94cb16aebb8e015c7f2a6fe1425bb19e3b38d166faad53f0e063a0c611b0c63e322adf8ef8d1a7c3020918fd2ab78630746b72738d838cfd69fd90fb45b8d6e0698c972c1a508771031daa97c3c709a6df648ff005a7f90ad2b924d9ccc4919526b2bc05127f66ddbb0c9f3b8fc8525b307b9d6eee030fbd839ae3bc2cb9f126b0c4f073c7e35d86fc5d05da766dceecfd78ae43c2f91e23d601040e707f1a16cc1ee76567c484738d879ec2b8b9c2ffc2c819c91e591d3fd835d85bbb86ddc80c8738fafff005eb8e7cb7c480471853d7fdca220ce9e358a1754438c2f4c7415cd78b994eb1a493d011d7bf22ba069638ee943b28254819ea79ac6f11e9f35d5fe9d702099e28c0e42903af5a23b8d9d75b3017e9e9ffd7ac8f1bdcac1a023489b984c0803bf5ada8117ed5ba370c02e7e535cd78e6e15f450a06712af5a4b7036a399bfb02c9907cfe447919f6158de227c68176c7a00b9c7e15ab6cc8da45aa39500c11f5f5da2b1bc43b5742bb6603076e7f3142dc3a0ef0949e66856ec060664c574d2288b4520639427f4ae4fc18c1b45882824066ea3debac9c1fec93d0fc8fc8fc687b8ba1c978149363a871801bff65adab421dc9c6324fe3ef581e059247b1d443e4104f5ff0076b7ade411aa95524e3031c9a72dc68c4f0a467fe128bf6772c487007a0dd5d6ea90bdcbac4ac541e7a572fe1281ee3c5976a87821b03fe042bdbf44d1f4eb5026bd31bcbd407e7f4a2d762d8f0f9341bf1e38b4952095a042a0b04246707f2ae9cc120bb8370fbac73f9115edf0d9dacdb6416f105eab85159facf856c752432471ac57007caca300fd6a9c1b44f3a3e74f1ce06ada58c72d9007fc08575ea8b15c4683038c573ff102c5adf5ed35651b5a1621863bee15b777279770ae4e00c126a5ec8a451f18c427d3ec8000937279ff00809abda53a5aeb76b2c8093159f017fe0359fe2c90a68f66e4e31707ff0041a94e8f26b32d8c42478d25b5f99d4f701700fb1e69148eff004fd7e2d4a298c190d0ae590f515c4eade34f363ba8350865783050f94830bf89ea79ed5d3f8534bd23c2eb78b753059e619231c05ab52683a35c5d34f1c71b96e410bd690cf1fb2bc0d3c7c9dae0a0c8c120f23344576c974d0313b5c1c7b11fe4d6a78d6cd6cf5d668c055c2b0fc38ac2d4018ae52753f7983114f728ef747b9fb5d944ce72550ab647706abe84a235bc208f9b0781ee6ab786dc6d9933c11b80abba33a38bbcc6cb80bd463b9a44772473bd95f271c83f5cd626972dc7f66ce96d197ff4f937e3f8467ad6d4cf8842e79ddc0aa7e1ada9a16a1231c017b21cfe34fa12717ad6b77916b73ba4a5521936463b023827f3a89a49b5a8b7cb72e66efb9b39ac7be99ae2e198e4e4927ea7ad3accc887e4245696d0572c3e8f731bee5988f7adad02ea5d3b518bce9372b1daff0043543ccb961d33f8d24618309646541d41660296e33d2a612bdea0323ac68ea46d380702967b8f3759894103ef63f2ac393c51a5f916fbafe1de146e1bba1a9ecf50b5bfd46196da74940dd9dad9c702a2cc7a1d0cac0c111ee8719c550d55c88ed9bb974ab13b11111ee07ea6a9dfe5e1b7000c878fad201643f3123d09acfd446ed2c393ce17f98abb2c8c24e46063fa566ea2e53436618ce40c7fc0853036ae42bb9380d86f987a8a9608916da53fddc902a94b36d9439c8f9855c864125b4a474619a4232f4e60349b7e79f2f8a7db9cdd01dd8fa553d3a459f4885632c70982718ab56accb709d4fd698ca569c5cdde0ffcbc38a8e2c7f6edc67fe7d579ff00811ad8d1b41babbbcb9c26035c3152dc66b42e3c33f61d5dcc8c1ccb1ac78193820934c0cdb5086057de011d8d616aa7fe26e8c1891bbafe15d5dff85a5b5916ec3381b71b0fdd35cb6a0a57508d586086e83e95238846f8dae49dc0d66ea799357b37e7efaffe855a8b6e26951d988dbc8c1c67eb59fab1dba8d9648c975e3fe0554b703b0d51bccb81185383c1c566de285b09020431afdc09edea7bd6bdd0f32e0b67bf4acebe11c762228d15003d07d2a423b90d948e2c60db1961b072081452d891f608304fdc1d28a6239a8e56ff0084ce45de763a12573c74adfb6138d48b293e40888233df35cdaae3c67c8f94c647d38aeb2d1401260f0053608e6f59ddff00096697b188f5f719aeae0c11329cf23b5725adb32f8a34b28a19bb7e75d4db8c09491ce3bd0f6408e6bc5ffbbb0b358cedf9f8c7d2b474c8bcdd5d173c85ce7deb1fc6fc595b7b483f956b6992ac5aa42c41f9b8c8ed47440b726f1aa07d19c360e7ae2a9f87b62e8d0843b808f83f89ab7e37cbe8b208c12c48000aa9e1c8248b47811d406da33f99a3ec8ba96e7c9b17c9fe03deb2fc0cc5b4dbbc1c012ff415ab360db4e01f9464565f82008ac2f10f389bf3e050b660f73ac20f9ca3919ae53c3a58ebbab6e03680c07a9e6bab2cbe727b572be1d71fdb9aa0c67ef7fe8542ea33b0b20f244a76606d2735c6dc5a5dc9e3e9a7b48b7c8176a8ec095c67f5aedaddc456ca49c2aa9aa3e10916f357d46f0aed8c4b80e7a52bd8695d9d2786fc142002e2ee457b871f3311935bdabf853ceb30d6d2932018c1a7d8ea764494370370ee0d6bdadf45282a97085bd3342b03b9e2da88bcf0d6a2ae11b63f1229e87ffaf553c62b1ea3a0dab40c3cb9660323e95e9fe2fd322d474f996e502b05c822bc45a56bbd267b259c85826ddc7518e33447706ae8ed1638adec6c9bef37d9e31b4f4fba2b07c4774b268978c873b4018c77cd6fa9c5ad8007205bc7827fdd158faf664d26f390082a0f1d7a50b713d88bc2283fe11b8a562c1999871f5adab3b59edec6ee57b877594b388cf44e31c551d00e3c396c338ccac2b76465fb0cb807261343dc5d0e4bc17916daa13ebffb2d6bd9c8a0aedea0f3f9d64783187d9b553db271ff007cd6cd8c5ba54031cb7f5a72dc68d8f87fa50b637facdc0c2191a3897fbdcf3fad7796704a435c4832ee724fa579aebfe241a618ec522222814b00a48e7fcff3abfe17f12dd5f584cf012d1c085e467620afa76e78cfe5537ea3b1ebfa2dc9557b790f3d573534f7c6376404e48207d6bc562f1fccf7e65b3be410a11949baf6af48b4d720d66c92ea2752e080e14f19ea0fe46b453d2c6728db5382f8ab67f6a6d3f540394996397f13c1fcc567dfa2b5c1676608ab8dbd8fd6bb3f1669e752d02f201f79489178fee90dfd2b93bf895a72b29010819a865231fc6197d0ed071ff1f2793d3eed6ae9576d68b63234ca225445271d7200fe66b2bc5633a15a2939c4ed9ffbe6aa6b737d9744836e10bc68cdf8014148f45bbd42dd6e2368ed8cf3ede790001ee4d68c57b2dc411b1d39a20bdd5d580fc8d61f8635ab2d4b4b82e5f6191940753d9bbd6f5e78834eb5b72a028723ee8a9db72debb1c0f8f6d84cc24e32415cfe1ffd6ae2aec79d6d03f708335daf892f0ea16f88e1655073b8f15c61f9b4f81f18c865fa914e2366ef86e6d972b1939de9c7d6ba0d36164fb5060325579fc4d71fa5cc63114c0e3cb9339f635ddc6e8c2e1933f710e718f5a4f7265b18fabeb363a522a5dcad1eee4623620fe205733078cb4ab4d12eacc4b234934b2b82919fe2cedeb5dcbdac5711e5d03aefdbb58641fc2b80d660b2b6d16622c20fb43dc4d87f2c64286e315a46dd4cb5e872b1092e1f305bdc4c4f40b1f5ad1b7d175d90e62d3248c1ef3b041fae2bb0d1f4ebd5d3ace7854ab18d0f2dc636fa7e55d4dc69e750d362dd183281f31238cd2753c8d153eecf37b7f08ebd784abdf5bc03baa49923f2ff001ac8d7fc2d73a2cf0a3dc79fe68cee0a78af64d2b409125cf9ac79e131c28f4aa1e2bd0985f412c803222f2292a8ee1ece2f43c721f0f5fdd3aadbc6242def8fe75d57873c2b71a6ead6d3dcce564c9c247d3a7426ba044fb3cf142802a6e38c7715aea80dfda13d7e6ce7e94fda37a12e9a5a934a772c83d1875fa552d498f956ea1c293247fceac5c4612e9db76371c819ebcd53d5186213dfcc4fe752032f64512f07ab015535050da4b83f778fd0d2ddb1d9b82962aca40149784368b2e783dc7e54c65cbe6d921c55eb662f645f1f793a552bf4fdee7b718fceaedbb08ed1c13c05348466e96ca74db7cf68f15a761046a3cf906571f283fceb2f408c4d696fdf8ad7b82c26118188c0c01eb4303a4d3a39ee6dd0c72794d2b6030ea13be2badb7d06c5ac8c4ebbc9fe2279cd72b657b6b650a7da2554d8800cf5f53c7e35b7a7f896c6e91c43264a8c9aa4d75069f42bcba74fa64535b4b234d6ac7310739287d01ae03c51a504b98eea3c119e71f4af479f5bd3b5247b55bb8c4d8f9727bf6ae4b584325a9040dc3f5149d815ce25186d53fed5666b36e8fa8d9cc7ef070a3f315a4d9460a7fbc78acfd4cfefed9bb79a0feb4219d4ea4c225dde6796a0fae09f6acdb868e4b3deb907241073906b5eea48de6e7078cf23359fa84bbad157193cf38f6a4c23b90e9d85d3a01c7dc14557b427ec50e467e4145311cf313ff09c003a79671f9575d68d88652ddcd71ab2799e36c8ea2323f4aecac4a80c84f24e4669cba091cd6b6ac3c55a56173c8e7f1ae9a2fbd2a9efd6b9cd6e478bc53a591f75b00f1c75adfb73fbe918f424e39a1ec868e63c68a63b180124fef075adad2f0ba86580384e3359de306115ac0f8070fdc6715ada5412dc6ae1228d9c900600cd0f641d43c5ee468e5d71c38eb50e86c7fb1a375181b781f9d761a97822ef55d37ecef224449070dcd3acbc0f75a769b1dbb4b1970300e0e0d2e8072370716ee4e32cb9fd2b2bc10e5ec6eb70c9f3bfa0ae875cd2ae74cde2788a7c870dd8d739e082534fba619e66e9f80a6b6627ba3ac5e670707935c9f86881aeeb3df6eecfe75d6b3b1917b7e15c4e8f14d26adacac1288e4df92d8cff00150b660f73ac9ef47d8e48c36e382df4c7ff00aaadf80636bbd05949d8cd2b61877e6b3b54823b7856450449b58100f073ea3f3fce93c11aa4b1c13590401a062eac3ab063ff00d6a97b1713bed2fc3723eaaa25b82e09c95279146a3e1dbd835177b53bd7395e338fd4573adae5da5c096d2ec2303f37cc3afe35bfa7ebf76ad15c4f7627971b5b681823f0a9562da7b9a52a4e344946a002155383bc1af08bdd38e91ac19013e45c960013d71839fd6bdc355bd8a7469ae64458382c5ce157dcd78b788f508754f116db470f6b6c362baf4663c923f4fcaae2433a282e6e6e0da2a5b91025bafcf91c90315435d2eda45e843f302a7a7d2b634f4f2f4b8d4927e43f85617881e78f49bc6520fcc3f9d0b7132df865a46f0f401d4b30958ed5ef5bcead0da3c673f718f3efcd73fe11766f0fdbbbe77798dcd6d33192d5e471c95719f6c50f712d8e6bc1808d3754624f24e3f2ae9b43803dea9dbcf279ae5bc19c695a90191f3918fc05777a047fbf46c7dd8ff5344f7147621d5742b6d4afd967dc0e386535a1a5ae9ba069771a68b3936dc0c3cbb739ab32c1236a6444858ec07db8a8a64d4bcd0e3c96e3a608feb509b3656671efe09b7fb479eb3158f764a11c81ed5dd7865d6091ed624db1f96303e9d3f9d664d24f267cf8c46ffec9e0d6a786806be3ec849345ddc24bdd3a3dc1a49c1e57b8f6271fd6b86d5d4a5c39db9dac4575cf26dbd9133f781fe59ae6f5cb1637a248e47018eeda3f8b8aa31d8e63c47199ac2ce2d9926563803d8572fadea0b717c2d94652245523dc0e7f5aee3598556d6d5c4851bcc6208fa0af37974cbc5bcb895d09f31c907f1eb5512ae69f8319ae7c610c6a0887630600e03707ad7a98d2ada3972ca31ef5c4fc37d37fd2ae2f98671fbb43ea32327f4af40bdc960076a89ee5c3631fc41146ba7c823c0db8af3a64ff0041d80711cfcfb03fe457a16b68dfd973c84f0307f5ae0a4520ce00e301b14a250ba30f323910ff0009aeeb4f264d3a41d5f0abf967fc6b86d13097ef131e18100fafa5767a44e125f2df857e0fb30aa7b92f545a95a548c80a02fafbe6bcf7c4c711ec278249e3eb5e8fa98d96be58eacdfd2bcdf5a884b7003671ce3db9a16e4c763b8f0f6a115cf876d64c0cac615bd88e0ff2ad687c55a59458238da3e305998706b81f08b433c536897123202c648581c6ef515bd6da469b15f0b6956476cf562054bd196acd6a757a76aaa676489d2500f55349e279d5ec03e7e620fe15247a469d61fea1563908e80f245627886476b194e7e50a40a5e40b7312464f2d2746dc0739a9eda7f364827c1c1cd62d9ddc6b1c31364a4aa41f635d269968b159ac5b8336723e94f6096c41791992f12539201c714dd48207b327ee89901fceac5ec4df6b81573b4649fc2ab6abb43da863b419d39fc6a919154fcd29c671c556d4462c2e4f7c55dd5145a42a573b99bad67ea4e3fb3ae7031c1fe74d01ad7d244ca7648ad200095cf23de9f6eec6d9b7005b69047bd54beb78a29c4ca83cc7c2337723a8ab168caab2176e0139a004f0e622d3a272369d84569f97e75f19989d91e368ed9accd354b59c518e83d6afc93ec8b00e371c66a5948916c351699a5827d9bcf5d9b89f6f6ae87c396f0d85cce750ba40f34457f798cd53b3d43ecd6e59ba05ce7d2b98b9b83a96a06e9632cbf74900b71de922ed7d0dc9f46d4ada6912098340c38568c1047b11576da13f6286de51ba4419393ce2ad695acc32e9de5479fdd80841392a6a3d2658ee3549509cc917033fe7de9ab5c896da9c9788ad843748a80004e7a572fac1f26285fae241fcebd0fc7560b14d14f1fdc3cfd2bceb5df9ada3edfbc07f5aaea247446e5da4112dac8cc101dc4706a3d44b98a22c850f3953db8abdbcfd9060ff0000c553bb3e643cf38ff0a4f605b95ecbfe3c60e3f80514962ac6c61c3606da2981ce4fb17c70981ff2c4f4fa1ad992f1ad2fed412444edb720679ac49ce3c70a4f1fbb3fc8d74d16d699030070430f6aa7d095d4ced7b235bb26032368e7d0eee2b6aca18e4f314bfd38ae6bc484b789348f9be538c8cfbd7536440b9c06c6693d90d1cc788b4fbed40db582c60c8f3ed8f6e7a7a9af59d234f8343d3c3a206b87182e7fcf4ac6b685124174e01719087d335d05eb00634242a04dcdec3aff00854b974048bfa65c4b7176a5dbbf19aeb0e9293c23e6f9ba83ef5e776b74448ae09033915dbdb5fb8b54649032e3b1e94e2d7509c5f433bc49a347a8691359dcc40380763fad788787f4c974c17f6928f9a1b93cfa8c0c57bcead74f2d9e72322b81d6ec923b8797681e60073eb437d01239e2cdbf278eb5cbf87501d7356ff689ff00d0aba89be473df83d7e95cbf86411ae6aaccc3ef1207a734d6cc1ee745aa2f9bc31e36f3c76fff005d735697dfd8fafc2ec76c52a8493d81eff9d74d3a4972e790b1e46efe75c8ea5f66b8d698cd208ede2392e47a74fe54915d0f4f883a3411c462080fcdb80356afec2cede48ef20088fd0ed180c4fb573c235bfb28a6b7b891559410477ab76ba7cecaa6e6ee431af207ad666847e31bb58bc31710b3a979d36a8af30f0fc38665954f5ca8ed5d5f8be52da8dbc0c7e50b9da79c7d6b2ed250b3399228c0078c76f7ad169122dadcec6188880045c8dbfd2b0b5e456d1af86c24e54e3f2adbd1efa3bab731aca372ae361383597aec646957c5f2adbd4727de844bdc8fc30162d06df0a54798dc67bd6dcf94b0619e0c6d9fcab37c3d6eb2f87adc39246f62307be6b63ecad75098d0e728c02819cf0687b88e3bc111f99a66a0464fef4f3f80af4dd1ad3c8b3595c63700066b0741d021d02c5a390c6f24cfbcc4641851c773d7f0ad6b9d42280fda2e2f63f2d39541c01fe26893bb04ac74b60882491f196200fe7542481ffb57824a1a3c25aac5aad899623f2339e4d4d7d7296da8c52630a5829cd2b6809ea3b59b289ac93f77d47a77a8f43b74859d9570db39fc7ffd55b37850d91dd8c6322b9b9358b7d2adae6e6670154aa28f538271fad0d6a09b6866a7aa5bd96a21a49393b5b68ff77bd665c6b173a85b19e0b6558e01b5a466ce3a76ae06ff005e92eef659982ee739248c951d80cf4e82b24de5db6678253c127637233eddc7e154a20cebee27f398b6ec9fef35656b5782d74994a1f9986d1542c35a6bb95524824570304819047d7fc73516baaf7302c7b4296755e9d3269db515f434fc19e21bed1825a5cc227b62ace36f0e98e7f1aee60f1af87af88cde881cff000cca571f8f4af3ed2f418eca58ee65999d51836dcf51df3f5a92f2de1b22d6a02908e5036325a93b3d4a8dd1e81a9ea9a2dce9d35bff006ad90122919f3978fd6b88531bb400904cc854e3deb95d5b4f672ac91e38c7ca3bd6d5ac537f602391992d1f927a943dc7d0e2972a4b42949decc7405e0be88e3e653c8f5c575c4f9372854e1650195bfad733a9c45ed05dc447980ac8a47aff00f5ff009d7428c6ff00c391cf07faeb721d7dc7714994752d6b15dd80646dac78643d01f6ae1bc49a7cd6309f3a121339f357902b6ecbc5363630ab6a704ad6ae006318e41e99efd2adbeaf6f796c8f6d99e22db0f98846463b8233d0fa55249ea67771763c96cae8c7a8a8573943b948ea2ba19bc473ff0068c73b1cb0e840ac5f1758c5677627b4f903a06c03d8d43e1cd4ad0ea096daa3ff00a3b7473fc2dfe154e3757129d9d99e8ba2de5dea5a94321663d43127b76addf134091e93290738524d1a5594168886df1b5b90c39c8a83c65791dae873091b1e60d883b926b1b5d9adcf3ab0f31e58bfba7953e87b8aecf4bb868e711c8490c323dab8eb69922d37e71fbc5fb8338383cff5ae92c660b1a48cc0c9b770f6c9c53902d4e8a568dba9f9870091597ad40af0421c33033479fceae4970b2468481b874a2eee209e3b789c6c93ce4cab7a67ad3899b4656a516e886dcb00e383f9554d52255d3ae47190a4d6d5cc2c970c072319fd6b0754dff61b9f9baa9e2981a17e417e7a822b3e0b2b8bed496732b2431e432766ad1ba84b4a540ce7181eb5ada6c0aa012b8ee681105ada98562121c0f4aa7a85cc65cc7d047f32e3bfbd68de5da7da2351804b60d723e21bb16d74a0ab6c006e61e9cf23f4a56b948d3b4d61ae84960c3018f39f4ae9ace02d2c31c72f924f55ce062bcd5edb5159e2beb3759a0383e646735ad0ebf23dca09e39966031bbb52690eeceeeee25d3a692e83282df23281d4fad677876e5aeb539ae9380cf8183d7b7f4ae5b57f104f77722d02153291827a81eb5de78434d482da32dc15a2c2e85ff16c2d71a52391d0706bca35a522250dda45e3f1af6af12856d14c78c329073ed5e35af2ee42d9e3781fad53dc513a003fd0c6debb45559c110b671f87d2a699ae3ec8b1db46acc40cee38c0aa9248c2dc074285bef03d8e297405b8dd3949d3e1c0fe1a2a1d3aeedc58441a55071c8cd14c0e72739f1aa6d3ff002ccff235d4c0017dc7a85ae7b6467c5e083f3888e54fa60f4ae8a20779e38c539741239bf10b13e24d27ebfd6bb3d2e2f3afa301780493c76c5717ae7fc8c9a57aeefeb5e91a540d0dbb4813e693bfa0a25b2147763a43bf52b7b68f8552377d3fc8abf7d70d7171346bddb6fe03ff00af9aa30a07d704887862091e98ff0022aacf72ff00dbb71183851822b3358ad4b074ed466942c5046c49eb267007b62b6b42b1be8e792de790afca76aab6466934dd48c6361e49e94916b92d9ea6aab6af3313f797907d850ac3771b6771ab45772c37314c23e992a197f439157f5fb6f3348b79b1ca128df506ba6fb42bd879d2db7972ba6e1b87205614f326a5e1ebb0a3203961ec7a1aab5999b773cd7537650c635248cf03e95ccf84f74dac6a8c508dcdce7ea6ba9bb42c5c00430079ae7fc2c560d575a9a421638d8924f6009aa5b31336759be8b4a89d495672b93e83eb5c21b84d475284dc10913ccaadd81c91573569bfb4a3b8ba72426eca8cfa1e95cfea1231912db6e36fcc78ef55188a4ec7b86976f12c3e51da15490be98a8b53f12e95a531863904f77da28b9c7fbc7b578bff00685e88fcbfb6dcecc602f9a71f95741a2d835b5b09e71879482a0f6153ecd2d58f9db2c5dced77753dc4d96998e73efdff002e83e95197559155970140e3d7919fd2ad491ac65319e519f27be4f1fa0fd6b3afc3298ae55774328c64763e94ec3be86d08a59cf5da15b0581c6df4a93c410dd9d16e2c998cec707cc0390063ad3239e131288b1e6361e5e7850073fe7deaedccd2dc69e67449e3488f9836a37ef381c123b75a0965bf0f5ab5bf866c1533839ce7dc5676afe22b8b0dd636476b8e24901e738e40f4a3c2dae49fd8f3c378079914b889704162dc631ed5851a097507fb405fdcb31640486723a0c8effe79a2daea34c70b879239a368d99c0cee2c774783c9fd6ba8d37c1761ae68b16a09e2130ab384749d3fd5b7704e70323a135cef88b4b974bb28751b6798da5c7de2c7963d79c707ff00ad5a5e10f145ae8114cb344c567653bd065b1cf1c9c0c1c7383542677be1ad38f8750da25e457368cc42cab80572482081f4eb54fc61abc7677f691120c92ce095ee471cd642f88f479b4abdbd2805e293b403e50ce49040c9c9207a01f9d47e34b58f579b40d4e191522ba42acf9fe2e081f9d435a823bfbed52d6cb4337777288e2c0c13dc9e82bc935fd7135095955cf959caa9ec7b93f5c0abf7fab193418f4cd60e0c6582ca14e00ec48ae426dbb197cc490a70194e4114243450bbbaf2dcaaf2ee70056b5a69b74b0c6ee9e5215caefe0b0e7903a919079accd1ac64d4759f37198adc877ff7735e85a84534c80cb2b318c88f9eca3a7e86ae5a096a655ad9a5bc388e43862587001c7383ea3f034b77622eaf6c611f74bef6fa2fff005f153c8c06588c60702b98f165dcc355b6b7b799a32b08ddb4e39639fe58a495d83763afd7b4eb8974a9041bb82370519247e15cf6a9a74d1882568e45dd0a03cfdd64f97f3c007f1ac7b4d7f59d367cc776cea38225f981aee74af115aeada79fb6aa413c720527f84961c63fef93459a43524dea72d6cd241285725964ea0f27f015aeec6c75540edfe8b1c3e5ceb9ea1b93fcff004ad0856292fb12c0ac91e64dc38e0735ccebc9730df02ec5c5c92cf8f5233fd6a56a53d0e82d2cf649369729dd185dd13ff790f423e9c55af0a5c886f2e34d9b86fe107bff009ff0aada0deadce9f6f237fadb2215b3de33c0ff000ab5aad83dbdf0beb7189623f36dee3b7e9fcaa5f629135d69e16da7831f7589191fa7f9f4ae5e5bc7096b282dbf3e5b8cf071c7f2c7e55e830bc17d6df6a90b00632c197b3639cfd7ad79e6ad10b6b811a306512b10477a7126456d789bcb78891928a6327f515c68562781df15d787df03e4ff00b5fad731246d0de4b1329073c03fa56d07d0c6a23d9be1f6b3fdaba38b791024d6815194771d8fe95cd7c49d684ba98b3573fe8ea3014ff19ffeb51f0d6f48d4b50decb816c0f0318008eb5c75d99b56d7a76c9696e252df4c9fe83f954a8ae629c9d8d080fdb34e13764900381cd74f6e5e1951c9f321230547f10f6fd2b12da048a29e18bfd5abe07be00157b4c9241062570115b383f9f15123489af7b7f245e49208018052782454375ab86023600b487193d8d1a94b1ded9c52c7965461c1f635cd4f744df97c0201248fc4d28a091da787b517d421b8b5918b3404ec663ceded9fd6a3d450b585c2f7119acbf0c5e476baff0094480b708573e84723fad47e26d7ff00b3e67b480079587ef09fe107b7d69daec9bd8ebaee6b6b37135ccaa800e06793f41fe7ad531e2785e3db146515db05c8231e95e7bfdba2ea6335e31690f53d49ad35d613ecae122464032411c83ea08355ca4f31d75eee9111a39622cadc12d8cfad729e2e12c2b117183b30483c1ab9e1ebc1791cb0dc7dce4a923fd5fe3f8f4acdd626175a7c91a0ced72403ce01a496a36f431b48d7ae747ba1e57ef2ddcfcf1374fc3debd52cbecb7962f75e5951192195946548ea2bc5fcb2c0e7231df15ea9e069d24f0c5fe9d6bb2e6e61663b41c6e0c3823da8a91d2e829c9ec73705fc7a978a05cf94554958f9eb8cd7baf862c1e68978031d8d7cec6196c8dc3b921c31c8f420d7aff823c551caa19a426642030cf62060d292d6e3bdd58ef356b167d3e48f19da08e3ae0fff00aebc5b5d88ac32a37de5619fcebdd2feed4da0993952bbb03bd789f89584b7d7e8831c87407aed27fc6948205b91d962047040155af4936b039ea653cffc06a6b9976c4a48ec0722aade366d2d8608f9f3fa521adc834a2a9a7c6360e79e9451a7c8aba7c19ce4ae7a7bd14c46396b74f158cb379cca405c741b4f5ae86151bba6338c561bc30b78919db69900c0e79e95b50444de245e6b6d2473e9f4a182285c69c979e2ed31186e2abbb1e9cf5af4787c84668d73b94aae3ebc66b998bc8b5d7e37fbd3188ec0074a92ff00c490d9df19a4242950ae00f7c8fd40352d9490dd1af1a4f1798949daceca07a71ffeaa7ea52449afce1080ca76919ebdeb9ed375db2b7f120b9420169032e7be4d5bd574ebb9fc4d713db3623ba904b03e7e53db00fd4e28e51a7666f23f9a9b43153ea2ad697234575b5da761d99630715c6ddea373a76a4d1a3ac88188215b23f035b3a7789afe320470efdc718a5668bbdd1da6adab4fa76877b3dc5c2c8a23fdd1db8393c007f1aa9e179c0d2268ddf25d338cf7c7ff005abcd7c57e2a9f50d44e9d32ed8a2652db4f19c575de1b9d844e377cac848a6d3446e8a5a996c1e0ab106b8ed3d9ff00e27aa180dd3a8fc3713fd2bbdd6221f6d4d9ca1524d721796d6d66b35c6443e6b92c49fbe73c01f9d5220e7b58668ec56da0eaecaa40f439ff000ac3918cb74653f415b37d70b13fda0b0df93b17d4631fa1dd58858139ad92d0cdeacd6d0ec96e350864987ee44bb724672d8cff00215d55ca235e490c44f948cc327fbb9ae3b459e47d430ce0470c7248013c676e2bb0b2b779ad19a2df2e3064745242f4ea7a0ffebd4c87163aead99668e5cfca5bcb03e8bc573a269440f0118c1e41f51cd76baa41b2c238cc90212dbd9de41c1f4e33cfb571f702dd2e1c990dc37505976a83eb8ea7f4a4915725b19cb4b2c93b825cb4631c6d50703fad4b7573a9887ecf6f2c4500c30c9c8f4edd4fa552d3104caef9fbdb8a8fc4ff004ad0bcd7459e97b21b1d8c576f9de5632df5a7d45d0cef0ec3aadcea8a96e02cd11f386f3819e807d69bf6899357956edd2770c7cd6dd90db7a807dc0c56a7846fedad740d4ef3517916391fc81e50c3162323f2c1a93475d11e79aff507492c63401207608cefb467a73c13d7da9bdc13d08751d58ea9347a7b5c18eda58f6c4b8da85b2719edc10066b2ee63b7b2bb7b3827f3a3870bbfd5b03763db39a875ebeb4bbbf867b084c36e328a84fddf6fe759315c6257079cb67eb425a0b9b53a08a2432c6afba443f7847f7be9c8eb5d6dc6b104fa4db6956734f05ac6a191e503717c6339ec3dab9ed0efa482f52ee6b425c4644519428ae318c0623938a74faa68b342cd11bc42bcf97b036cfc7d2a5a6526859f50b9be7921bb0ae618f617e9b867835cd5e4896aac91f04f6ab775acc0b89ad9c991936488cbd6b058bdcce3032cc70055463dc99495b436f48bc6d39fcd01dbcc8995954e0b023fc715e836578ba96956b719189a301bd98707fcfd2bcd10e1a14c7cc1483f956f784f52cda4960ef8c1de9ec475c7f3fc2892d0133a3d587d9800c30715c6ea922dcea53cce01c0503f0503fa575daadea5c5a245728cae4615c72370ed5c06a573b350951402ab8fcf0334a2872765a9388c81b8bb71c607f5ab9048b736d7f004225680bae0e412ac18ff00e3a1ab045dbab1218e4f5079cd69691721f5344e8668e4881270373a328fd4d5f291cc99d0e8bae3c9a5486e72d2a81021cf51827f90c7e3504f7ed25e85bae9862b81f43fcb3594f2ae96d6d6b30f9a30f24ab9e8edc63f255fccd51bfd485d6d1182bb4f0c7ae3153cba96a768ea7a3787ad912d5d9583094b46c47a1e9f9706b55ee835adadc48f9dd6e0483d481ffeaac1f09dfdbd8f85a179e66133ca59506496038fe42b2e6d7e4328b568ca7ca7af61fe7159b8bb9a464ac761a54c9753dc5821d9cef8f9ea79047e3cfe75c9eba8c2ea31b7693918ab1a76ac905c24a99c9001f5525463f5a8b59bcf33502e31f749c7bd24b509328449959c01d2322ab6afa57da3498f5280ee74ff00583b819fe9fc8fb5695ac620b790b64c8ebdfdead787235fb09798fee9b31b03ce7d2ad3b6a4357d0c3f0dea1f643a8cceeaa24b1914151ce7238ab3e11d341f32fae01cb29db91fc3dff3e9f9d397c3461d70c2bce9ec0b193ae14ff0fd6bad9ec85ae9922a2431aaa91bf38c8c600029ca4ba1315dce6ad26468653b71bdd88f6a9ed55b283ae1b76df6accb762902fe64d5986723f7a0e79fd2a59a266cc71c4a862524891496c7f09ae42f18457174a3e80d6ecd7a89209636391c915c9df4e1cdc3e49264a71429b3644ff6696da7046e4746fd4564ead742eb5bbb9f24a3cadf9669fa8cb8b28ca9e194567db6143349ca915715a5cce4f5b12b9411b147c9cfa76abda75d79aeb11202b0c31aaf6490cdf21c64fdde326a09227b29d83e4e7a11ebda98ae748924b6372245524818639c023d2abcf33a4866dabe54af970001b49fa5323b9fb5430cad21247df19e33552e6559252149209f981ef53d4a359f4e8a446ddb918aee5cfdd3f8d268f7375e1bd6e09ed91494259d870648987bf07fc699a75cb49088d9bcb1090198b601535b9025beb1a74d1403fd32c18b42c47fac4ee3e9d68f2603efadadef6c2ea5b663228766591b1965619e6b1b41d44e8bad5a3487f75711ae491c0238adcd187922e34e6e6de48bcc841ec093f2fe0722b07c496c1238cc7f7ad801d3fcfad2b741b7d4f69d2f5f13d9845652eb8f933c60d705e29cc3e22572df2cf1118f7073595a0df61a099e5d911197e4f23b8ad4f152b4d05b5ced198e51b581e194923f962b3b599499aba8a66252a33c038acfbb19b5803751cd5bbdb9263f2d2366da99271c0158bacea3e5e9f134182e3a83da96e31f6083ec106580f97b8f7a2b021f135ac36b0c7b59d827cc47ad155662ba2b25c1b8f1844f821821073f435dfe8165f6ab86691b0884166cf4ae2a58638bc708912e06c3c7e06bb3b2b8169038dd81d49f6144ba0a3d4d5d463b0b6905c7cbb9170646fe115c36b17c3506648155109ea7a9fc299aeeb6d7b3f971b8f294f033c9f7ac9b41e7ab38739071c8c73e9428f5655fa15a7d1ae95fed10932329c95f5adcf0debf3d9dcc6b7a0b5bc9cbc72f20b0e411e87dc54715cc908ea030e81ba1a9ace4b6d4a688e0a092408e00fb8d9e6aaf744d8c4d7e5974ef1348b1bfca8e5901fee93d2bd4fc251e9bad787d2fad622b731b6c994b670715c9f8b3c2a9aa5e8bfb295f1b444d95e0327cbfa800fe358566bac785a75bcb5b83195237ae4ed61ee3b8a249490e2da61e2182587c4d7c5e323326578ed8e2bbaf0dcaf05b5a34832920079f7e0d723e23d786b77b6b750c021b96411cb19e406e4e41f4357fc25ad186f8e937809024f909ecdffd7a9926d0eeae75ba8b31bc78f91b6bcbb54676d4af23e599ae0ac609e057a95c8692fe5dd9ce73f9d795ebaed6d35ec8461e49dd53d81ea7f2e3f1a74f7264ec8c6bdbaf3a62887314636a7bfbfe3554ca71814c4600f3d29b5bd8c2e6c68537937124a46460023d724715d34fab24f6915b0b3f254cbbcbabe33f7b23000c7defd2b90d3e4da922fae0fe593fd2ba9bb8d5b4e52b0142a0966dd9ce79e2b39ee690d8b53c733322c2db430ce41e7f3ae6afe6d92dd321e00da39ea49e7f957430dea0f0fb5c29dd2aa6d03d0d71b2effb312dfc5273f97ff5e88a1c9e8745a6445ed5433820460ed5ebd2a7d6351d363b2d896e1ee5907ef0c9bb1c74c76ac8b399a116ef1a9f302f5c9c1eddaa7d6f31d9c64c51e1c64954c609f7c668b6a0de841a45aea3a8da4f1dac424862fdf48a4f53d071ebce29a81640f16d5127a01820d47a5eb177a6dac915a4ad119183923192474c1fc6ade86fa37db56e752bb9a268db760c7b837b77fd6a9a25331cabb472a6d398db71f61d3fad37ca2822700ef7cb6319c0ad2d4ee6d66be9ee2d662e93672b28f9bf31dea08b5bba86d8da32c6d18528bb906e507d0d3e84bdce834b8449a5ee8eefecef91f364a8fc70a47eb59ba8c9f63d416585d04c79257857f7c7bd2e9af0c89f34870390b8f9cfe409fd45741a9e8dfda5a52b2108c83772ccec578e70791f8e2a367a9a6eb4386bc9fed1312618d1b3cece86a77b6582d92546524805b9e4723b54171652d9cfb25423fba71c30ce323f2a93ed05ede48dfe6c1f9493d3ae7fa7e55a19fa8914ea9728c7a0423f123156746f3e1d405c44c633136e0c3b1aafa7241bde5b860420ca478cef6ec3e956af6e0c506d5c2bb7deda3152fb22977674dab6ab6cda7c532b02b20dc00fe139c115c2cf279d3bc9fde39ab692799a34b1903314a181f620823f4146990c524acd326e551920d0928ea26dcb429246f2385452c4f602bd23c35e1c8ad2c21b8b88156e245f37cc91436d19236852307804e7e958da6ea9656e1fecf6830bfc40577ba3dc7daac607955b1d14a360a29ce4718efcd4ca6cb54d1cd6bfa0595e4525c089e3951ce651088c3f20118c9f507f1ae4753f0ecfa7c2275759623dc7047e15eb378acd0889834880e7748431ebdbd07f85703e2a6ba5bc5091b988276fba7eb494db65382b09a04ef0692b2c16e93314284b1e506e392055886ce2bebd4918127bd54f0949e65bcd0b71c9c0f4e33fd2b7ede2f2e52cbc102a65a308ec655bac6da84d0838459c03f402ab19a2bbd5caab6517fc6a584422e64f3a51179970c0b1f4ce3afd6b374bb59567b9c3c60acbb49278a760b9bae30c46095ec48a6e8a243a7dd6c90011c8df230383f956898ccf6c98557dabf3347d0551d2e23f62d4546f07792547e353d066dc1713fd9a3f22621f0ac408c1420e7764939c8e3f3a835e909b7964333b07cb0c1f946467fad695ac1bec210b1888145d9218ce4b6de5739c63a1f5ac6f122cd15995dc3632fcc07ad2ea073ec4a5a2e39f96a0595e382365c74ab138548907a2d51b7757b38cfb1fe756841f6bf95948e3b7b561cce5bccf4de4d69ca36938aca6718917d5ab489132dde6e3616d9e9b6a0b740c064f5eb56ee573a54273d00aa96ca3cb625b6834d6c4bdc73165645887cddb6d3e693cc41e6ed254100743512248ec0a70578047ad1730084aa17dd31fbc076a044b677a96d6f32306dc7eee0d25b4723c82477c027273deabc30f9b2ecdc001d4d589648edd0c70c9b8f4dd8a1827dc9e6668e453bdf19c1da7a8adcd0f54fb06b71cc7688dbe42339ae5a2b8608c84e7229d05c0460f264ecfba28b0f98f46bc7167aedbaaf31306f2ff00dc38207e1823f0ac9d6c933dd21e8412306ad5dcff006bd3f48d457395758dbe878feb50eb6b8ba6e305e2cd41665e80ad736b247e6a8f2c100135567d46e91a2569642a186577120e3daa5f0d4a897934722965ce481dea96a015756f28921771e3d3269db515f43d70c81e257520ab283f515cc78c189d3a32876e580e076ad2d32e0be876a49e447b0fe1c7f4acdf1402fa3a7b38feb58ad19a740d1fc3f66fa4dbbb440b32e49c753456ce82bbb44b527fb94526ddc2c8e56e39f1d8c768cff234ff00116a4f0a2db23751b9c0efe83fad43713c7078dd9e57d8a23232477c566eb68e75d9158fcadb587d3681fd2b44b544dc8d06d895d87cdb319a65b5caa9ba39da70ac39ea45123e463b0aa96c7fd2658d802248c819f51c8fe55484cd48ae1ae018f3905b2bf88a8f49bd367ab488c728d8247afa1aa71b1fb3ca57ef040c08ec41a1f2fa8acbb8132c7bb34582e7a4d85c5d5bcef3891a5b4b8219b1d51bd7e9576ff4a8afad988c3061c30ae47c23e21f2e536774c369e14b577765728a4a7051ab36accbdcf32f245bdfcd6930da14651bd083ff00d7a997fe42115d6312ab2938e3241ad4f16c10c7abb1886d6e0823d0f06b1dbcb8e5da24763d70dd6aaf703d034abf3a8ac931c64315af2ef15485af4a939db2c9fcebb1f09ea04dedcd8b9192a25523bf63fd2b87d7c117f76a792b393f4cd382b4889bd0c6a28a2b6312e582ee902e3a9fe86bb57901d2e2c8ff00590823f2ae474e888d8fea58fe9ffebae9896fec7d2db190d1b2d653dcda1b1434820c73db39015c1da4f406a9eada3dc5ac265254c270c083d0e2a5b4cef207eb597a9cb234c632ec517b76a23b84b62c473496d676d3c58241208352ead7d26a3716f6e1420441951d37119355ec274166eb2ae5623bc7b9ec3f3a962b7961b09afe5056473f21e841f5155b327745ff000d59dfced279505bdc5bc619de398641da33db9a241a0dec044b0cb6575e53b6e47ca19324a8c1c9c6303ad749a068f6977e180d15fc96d73326253866c8048c640e38c0a75bf82f4fb0b8d9a89f3d0fcdbeddc6f03d369ff1a972d4ae5d0f389626880e72b9e0e289666ba9c4926d0c70090319f7fad767e31d2e16b617b1dcdb00bf2c712b85382493f2ed1cff009e6b8888812a965dc01e47ad689dd5ccdab32dd9b797331232b9c75af45f0feb332c29711b1926b71b581ead1f719fd7db06bced71e749bb88f680c3fad69689a84b6379b5b90cc382011e95125d4b8be86bebc56eef6f15ad92d8a38742872a33dfa7d78fa571f30f2d4a15c3e793f89af42993cfb3d46d9a3fde990cb1103ef63ef2ff00502b8cbfd39d21fb4f0549030a071f51da9c58a48af6e9b3613d86fc7b9e951dd16761dc9a48662720f5f5ad2d3f40d535bb845b4b691a3ce3cd230a3f1a3aea1d342ac716f0b629fc4d9918776edf80ad2d3ecbc8b9588ae43a1c83ebd0d750df0d350b7b15b8b39e1b9900f9e253b4e7d013d7f1c56448f258dfff00a6c0f0491a36e471820f150e57d8b8c6db9931422cef1a392322390e073d3d2ba7d275092c844b23ee873b15539a759430dd2a16557079048ed5b2b6ba6590466f2a323a026a252b9a25627d535110694f3c477e07553f76b858b58b992668ae1cb6fc9538e95d8dc98355d3e44b790344d95257b1ae4ce9821bae720229e0fa8e3fad28dba83bf420f0fbf97a8ccb83cb0200fc457556c4b4f707a2a658d71ba6cc61d6e52bd973f91ae97ed2cd0dcc8af8dca067d6aa5b908e735a2037c8dc6f593fefae7fad5c86e2de5d51a59ec5ded27c799b7828c06091eddeb27567399013d0051f80153e94f612882d75269e30e389636e9c91c8ad2da11d4ed2df4fb0b1b396facf5f7f25464c0b0966fa103f9d739a4ea449bd9037ccec71bb8ce7ff00d75d18f08e99a6c49796fad3db9232acc1b6b8f43c62b9f8ec165b4bdb98d94af9e4029e800e6b3d0b573b0b3b88574e5c44cf2284f31769cafcbf7bd31dbd6b03c47765ad1a18f730c12491d2b674f6ba91120698212a30cce58326d1c63b60e7f3ac0f13413436f2a9601738fad25b8ccdbb70d0e5071b6a969e41b15047ad4b212b12463aec1556c037d9c2fae6afa13d45b9050118183d0d613ffad6fad6c5dc8ca9b581e0f158e4e5f3ea6ae044cd79d33a50ff00654552b6c18cfcc062afbc8cda748b80404ea2b255728181000eb42d825b975e5553e622825473f5aa8a0481e5964c1cf4c724d58460a9b7a8ec76f350cec5d8448bd3d3bd3426573f2b1c52529249c9eb53c5180039c31cf0b54411a9688e40c37634658a93d8f5a74f279b20c01c0c002831144249c30ed486761a34e66f0d4f6cf9dd102ca476653915a3ac91716d04e339640727dc560f856e50cb340c3875f98faf6ae82e140d1225e098c18fa7a1c566cd16c7116923c3aab6cf71d339a6deb9935666e84b0a6995edb541246ec8c1bef03cd25f3efd4a57ddfc59c9ab27a1e85a336fd17a9c248c293c4607f622b63f8c547e1768ae3489823824be4a83c8c8a9bc44a23f0f1e49dac31583f88d96c6c787d73a1daf1fc34533c3ea5f42b43fec7f5a2a5ee339f8a188f8ce605013b1b1919ed593af8235c76639fdd8dbf4ff0039ad685ffe2b69b27ac6ff0089c550f13c0d1ea5149b7e478f6e7dc13fe35a2dc97b1899c83eb5060adc230e306a6e9281f853673e59040c9079154496a28764ce070a4f18f422b3607ccaa09fb8857f5ad98db116f247ca339f6eb5cd2ca5598f734e3a8a4ed6278e7115e79838c1eb5dadbebccb0a047cb100fd2b898225da6594fcbfceae41298e0798f1bb85e68924c20ec6cea3a9ff00685c3317cc88bcd67dedeb412c6300a1f6a7d9c28b16e232edf79aa67d3fedb640961b8fddc0e9c62a55914eec7e897ec3c4f6ad6fcb3214c138dc71c0fcf159bae993edb706603cd697e6c1e06063fcfd299a3ef83c4562ae0ab24ca08fc697c44d9d5a619cfcc4e7fe044ff5ab4acccdbba3228a28ab20e86da068eced5c838643dbea6b4c6f7f0ed91ec8ad8fcf34471a9d36d880388467fef91fe34b09dde17b7c1e54b0ac5b37450b75567236fcc3df159da982ef208ce42633b578fc4d5a8dcb46cc18e4a1527dc567a5cf376198e1d7007be47f4cd5456a4c9e86a687621e14796dfcd42db8649033563c577056086db604cfcdb41a9741bbfb0d9fefdd1303805fad60ea8d1cd3993ed0d2c84fae4525ac86f489e87f0e7626902e58166598a107b29c57a0eaba758df5896651e66df95c7506b81f873c68ed0b60113f20fa102bba9e2921ca824ae78ac64fde66915eea3c57589249b5f1a6ccc9e546fb3a60648c6702b77c2be18b0b8b1fb65d2a9689e468dcb11bf6f6ebf8f4ef59de3bb7169adbcc918fde807763b9047f4ad7f0c6ad6274630cb3c71c7145fbc6913015c9c0c631938cf3cf415b7d95632b7bcee70d32a2acad9e642401ec31424acae154fef23fb9c75e9c525cc415adc139de09383db351a6d33c8cc9b82ae71f955a24e874fd4ee6e656372e4393ba37181b5c74edd0f4ed5ae0417b149326c4b84f9645e0ae7d47a8fd39ae3a094097746e720e48c638f5af44f0769c354bd17b712a948b01107f163b9f5c7f5ace5a1a47524d1fe1f457b3c373a8c490a0e5605e370edb8d7746cac2d6d05a4a65b161fea4a00108f60719a79bcb7491a29de340dc0627041ac9d5fc417da04be46a2ad75a48e16e23f9d013d8af503deb3bb9176b191a968be23d3376a5a5ea52de443e6784a0078f41d3f2e6b80f10df5deb1722f648517190c2307927a9c127d3b57adc6d2430aea7a04eb7568ff34b6bbb381dca1edf4ae4fc4d3d85cea1e7c31476f93f3b8e32303ef2f66ce45527606ae62f87a199ac02b332311f2faad318190cd0c8fb9b6e5646e4f183fcb35a76af19f995f622f001fbc7f0a8edac5dc105550be4071d81eb9a9bea5d8a7e1dbf30b49690c5912306248fccf149e2bbf1651a98957cc73839f4ae96cf4db4d3e2de0ab4a4619f1826b82f182837aaeaa421ea474a71b39112ba8e86668f319356dcc3efa9adf9f02003276e49eb5cce92db75587dd8a8fc462ba5700dc2c7ced6602b49ad4ce0f43075873f699c0e40908cfd0e29b6d2c725c5b28c90a08607eb9a3533ba7b8cff007d8feb4ba2c264b80ca090ac338f4aae84fda3d292f0da6813fef267f973b54028a3e98ac0d32e0cda7dcb0f2cee660401803806b6ae638a5d3dc24c246d98d82300af1eb5ce68ee8904e1d5c6188620fb0c563d0d7a9d7fd9c7d8e06b9b961196db1625c047da377439e46debc564789adee4d826f3f26d3b5cf35aea70a1a0460ecfb27530841db0d9cf3ffd6acbf104c8fa749870cea7ee0e4628ea3391339dca5519f0bfc23814403ca8c01d49e053ad9365b16fef516bc2638ddd066ac9452d594a2212dd4f4ac71c3026b575756408aec1981e7159a58cd20ce0741e95a476329ee6944be6d9c9e505391f302f83f956743f7b69ce0d6c410e2cd8ed52a0752c2a84b12a8b40bf2f991ee7faee61fc80a486fa13c722a2b3853c0c671543cf612175ea7b9ab3713c689e547c8c62a916c8c600a6913263e301ce3692e7df8a9260b0908990d8e49a815ca820753de9002c7afe74c57248595250cc322acc8c9246594300bd8d51ef4f1211919201ea050d026696852b477fb4103729e7f5aec5df3a6dca673898b73ee01feb5c9e996708fb35d25d21959d94c047cc060f3ee2b7e297f777f19ea1948fc8544b72e3b1c85f8c5d3fd68485ee1b01d1400092ec053b51ff008f96a743f638e249270eec7f80702aba13d4e87c2715c5aea6a62b88a48dbe59110e723ae6ba5f1180fa049cf3e60fe7585e1b4b71710dd2dc6e1e685f282edc026babf14451af87242aa01de3a7d6b197c46d1d89fc38bff120b4e9f77fad1563c3215bc3f6a707ee9fe74566f719c6ac64f8e642ac0031b722b435e8c4fa53487e6684820fe38feb5460c2f8da6519ff0056c456adc9ce937c4f398d88fc01ad1f4123843feb01ff006aa478c38651d4d41bb353b1761b94e0633c558872ee6b4284e18a9435cf853bb06b7c03e597524e7f9d63de0d974fc633cd5448980ccceb12fdd156666dd2c502f45ea05456f886232b7e1525829926321ce49a6c48dab75c2818a58ee45bab893e50ae48c761d722920015c0cf539a96e2dfcd99978d857938e7fcf3591a941a557f1359380bc489c8fe2e7ad52d77fe422cdc7232307dcd57b9324332649124476e47b549a8b3ccb0ceec18b83d0631dffad6a91937b9428a28aa20edc855d0a0396c98c0e98ec94b6c07f63bc393f231207b1a95d54e8b6c841045bab75f65ff000a8c111daa153c3f7ac19d0b630add82da5c17eaac6b3ec2249ee8abb84cf2095c8cfbd58be0639a4446f925715723d31ed6f161ba88153c2ba36d23d2b4467bb34ade368d3123e9a476766c7e87150c8ba6c0ed3cd750cee3a244bc55717ba644c52e23b87d9d06d56fd723f9550bdd47edf27916b6cb1ab901401c9a94994e48ec7e1d5dbcd7fa864108db587b1e7fa57aacae1ad9581edcf15e7ba05826870dac5c7984ee948eec7fc2bbd8cf9900e6b19bbcb4348ab2d4f2ff0089d07cd6771b4632c87f423fad70704371767c9b68e472792aa09fc6bd6bc73a6437d6d6e93ce2043301e6b745c838cd7056da645a3eb056e6fbc911caa1678f90c84f2c077ad69bf74ce6bde1d67e17bbb9ba10c97309963036a03bb2b827b74e4639f5acd6b49ecaf6e2dee63292ac64153f857a92368f369f6fa9c372b188c91e73afdf4079ce7eb8fad79e788ef2defafbcdb2fbc47cd26e6f981ed8278c53526f706925a1cec6cd1c8197ad6ee95addfe9b22086748519b92413807b63d2b15a164208604fb54d9b9683ccf237459d864dbc03e99f5ab7a90b4356f353d5bc49aac56aac2660c7cb58c6d071dce4fa7bd77961aa6b1a1e9b8d5f4efb55811e5b8f3164febc8fafe75ce783ac574ed7679646f363893cb62a3bb0e48cf5c7eb5d6f88355b1d524d2b4cb5b91279b71be4520fca074539f6ace4d6c8d237dd9cfea2d7da2ddfdab44b67b6b5b95677b491fe56438c7c84e41e7b573379af6a46e5cde418933cee5391c7bd6ef88b52967d72e5837c887cb5079c01dbf3cd66da6cd435082dbc86124b205531b9039e3a7f5a69aea0d3e8ca4bae06951d4f92e386761bb35d2c1ab46f6c258e6dea072d8eff4aced7742b4d3351488cf0cc9b3700e9b776491d57193c1a741e1ed02e2dcb1d71ad9c7f0150549f40413f9d2718b05392277d65a5901760a010107af3cd51d6f64b1b07c91ea066a3d6bc353e888938bef360e8ae99386c6715cfc9725b21e4925c8ea5b029a82dd03a9a59a190379379148ad9dae0e7f1ae9ee6609a94080f05873f435cfc6633a6390989564182076ad49332ded893cef645fcf14e4aec88bb2286a590d286ebb8ff3ad0f0dda24f293b8a92e155812083f8554d6e2407ce89c18de46000eb91d7f9d58f09cdb35a55dd88d87cdcf1fc8d0fe10bfbc7a1cdfda2ba3c904d71bd54652452ad91e841e6b8ed39b16b72a83e769194e7b822bd0bec50b685f6c8cca5a5944602a80a47cd9c71d7e5078af37b2448252c449bfed1b7d475c545ac6899d5c697048089202a5c3b6e2448b9e303b1183599ad26dd2e47236f19e0e7357a2d56574b65667b60c4b2ed420cc84fa9eb8c11c62ab6ae1a7b39c0431ac9f3053c81df8a9ea51cf448c601807685eb4910da99c13572d9775a2a6ec10bc8a815767e7542b19fa8c2b20c91821724d64daedf380619e7d335adac4fe5a796072e3ad66d886f3d0e7033d78fe55a4763297c46edd116d667322e4afdd55e7ff00ad55754b32341d22f41c6e468987d19883fafe956b5d957c90a24cb6de4003157b59b7ff008a0f4f6e3e408df9e7fc692761c91c683d72334e6819103310b9e403d4d3e1844b19c1191dc9a7451f9aedbe40428e327ad59162b3a14c7bd260d4d3a0545203019206696098a29e847604668115e9c10e338e2a65dab92ea1fbf1daa3662572381d3ad0162fe9000d4a061d73daba1b56f367d4c6d23017f0ff0038ae7f484cdd8c6ddc809ce7ad6d692e5b55bd564251d3af6c8a9917139fd457139cf19349656f15c102463f283f28ead5675d40970a00aada64f14176a6752633c1c1c63de9f425ee6969cf736da945208bcb85dc2e31c633fcebbaf11396d02607b32e7f3ae6afd64010c5728f182180073915d06bb96d027527a95fe7594b746d1367c24c4f876df078e7f9d150f8564ff8a72d3cbc9183cfe268acdee3396b7532f8da765c90226fe75af391fd8f75bba18dff0091acbd267c78b6e95f1bbc96c63ea2b659049a7c8ac3e4395233d45530479cc9c12455bb49559554b633c702a0bdb692c6e4dbcc391d0fa8ec6a28f70c81d4722b5e841aca61b797f7520591b83b9bad646af13a5f8de39650723bd6c5a24378a1255c30ee00e7f3aced75238a588424950a5412727ad11dc52d8ce7dd2b2c51f38ad7b4b75823009e4f5aa16856050ed8dc7d6af3dcc6a99dc093e943ec11eec9cb9dfc75156fcfdccafdba1acf866de3a648a9d09c36d238e483fad49650d6210733af73cd673333d9a03d15b02b6af63736922bed202e72bdab1ad2d66bc93c988027a9c9e82b48ec6525a95e8ab17568f69308a465248ce41a876fcd8e3f3aa26d63bc915574fb5cb953f648d94023049f5ac8bbba114013232a381d3069f7fa838b582162802db44a320ee3c5624d1bcb76a2671f37627a0ed9ac94753572b22e43a7dc5eda9be5da40936856fceb66faf58da0fed28364e806c9a26fbd8e808a86d61bdb7b6fb3ac51796a7208907e759faa49640e24f9e6ee11c9028dd86cae650901595d875e957fc36846b304c5772c7966e3a71d6b2c9c827185cf02b73c2b13bdfb49902145f9f27f2fe5572d22c88eb2477e19d6da39643f36d2c7db9aea347ba171648f93c8e47bd72324e5ecc83c6411d3b56e785cff00a394073fc433fad72b3a44f1adb9b8f0d5c93d546463b11cd7902bdcea12c114d334d2711c6ac7279e82bdeb51b54bcb296d980c48a41fa9af104d2d6d350bb82494472dbb13b98e09c1fe1f7ef5ad37a58ce6b52a5c5d4909368c5c7944a6c2781ce48fce9b6cb6c6295aea678db6318d557396e300fa0eb5a2347bb91d71613967c9dee8403f89e2abeafa45e68eb149716aaab28cab860c3e9c715a221e9a99c91c92c8a88bb99b8500f26bbdb4d26e6cf49d36316d2e3cc692e4326093818c127a75c7d2bced5fe7dcc4e7d4568dc6bd7b7507917133cd185c20763f274e47bf14344a923d1fc136d0cd16a6c6128ad39441bb3b40e833f8d63dddbed99af2d115a48246501b9e01eb4df0ceb32d9692fe42310f293c361874e7d3b7a532eaeeda10763be73f303dcf7e959bdcd93d0e5af24b86b89a51260b39629d864fbd4fa16ae961addb5c5da830a13b8819e0823a77eb572e12dae90c8a46ff004e9593716898e01cf6ab4d3dccda7d0d5d7f578b5cd7098642d1f11c6cff002e47a9cf4e49a2dfc3dabbcb1edb1251987cf8ca91eb9ef5cd34655b1d0fbd6be9efa9ada30b2b978c839211c8271fd29b56126cef3c653c76fe1b9a19d977ceea218f7648c60679e9d3a7f8d797088924646476ab7706e67b8dd7733cae0e0b3b9634d8bf7536003cf04d0b407a93dac6b1db5c215f30c8b84e3a1f5ad4d291a6d5ac4074dd012ceac338da09fe949a4846b851b4119efcd7731f8562ba5fb5b428a554b860b8c9c77a972d4a51d0f30d59d1eee431a2a2195c845fe1193c54fa1efb7b89a6ced2b19c1391d78ed5378aade3b5d65762a056891c98c60138e48fa9cd25bca93dac804684280aac48c8ce3a0ce6afa10b73ba86f2e1344b485e76f24100c4c06d52109cf6e79f5ae3adc859249e59a5c3dced48e32383d735d3c328fec18db3b0f9b2e0e4f6c0f43e95c6da917715dda23a24e5f7a17c7cdea013d2a116688f107d9ed0451c4c5d4623799f763d40f4ea4d52d4354d46ee2937df801720c6b952706a2bc492ca1b7f39511e3c32e1c364fd3a8aa9777b1cd3c9222eddc4e142f03a741daa92426cdab1699f4f80aee646182719c107d6a4f22779b9438a5f0b4a8f64f1c92b2ec7e3d391ff00ebad1d4eed2d2d18646ee99acdef62e3b1caeb4dfbd0b8e9dead6856d0dc5accf3c4acb1b000e39aa2d7267c4623ddf36e248ed5bd6b12db59edcb00dce17bd5b7656212bbb98faac8c76a0428bf7541ea45755e27021f085bc38c0023007e15ca4e8d75abc3095219e40a32d9ef5d3f8e64f2f4cb5873cb30e07a01ff00d7a5d520ee70a09daaa3ad4ca85264392076cd420ed5041e69d11daeace370cf4cd68665ad44a39053903be38aa001ec0f157eedcc726d2142b2821476aa21caa9038cd0b607b8aac7783c1fad01327920669a69c0be4119cfb530353488d05d3fce4617827eb5aba3c8e3569e169b6c582e547427fc9aa9a45a9fecc9ae8e7224dbf862974993cbf120246e0fb8107e959b2d6c8afe23da2f1021caedc8ac9876f9abbf3b09c1ad3d7d90ea84745551c0aa8907da8c51403323b0555154b625eacd1b58317f1dba4add41519c8ebd2bb2d6dcb6833e720e0718f7ae7341b0986b4e93283f64e5ce7233d05749ab82da2de139e1475fad6527a9b45686a78378f0cdb02077fe7454fe0b8f7786a0ce06091cd159bdc672f67101e34ba62a762c4df376ea2b4bcd53692a8209079ac8b52478cef727fe5837f3157c5b792659972e24006d1d7dead891cf78bd7fd2a09475f2573588a4e7a126ba2f134467d46d6288172d6e474f4e6b9a60c6038e187ca6ae3b12f735a1dbf2b99bcb047602ab6b81e48a0976205195ca01d7df15574f9f1989f040e466b4a7894dac92a45198460b287e49fa53d98b7473c0337404d4a96b7121016363f856b417ba72c79311561d8e2a29f596394b54280f7ef55764f2aee40b63756f8919c478f56a9dae44b03156f9b041c5321d367b9533dd4be5c7d4b39e698f6eae243681bc945f9a47fe2a5b8d6825c5db09c92c7053047ae453748be5d3efd6674de846d61ed5245622eb32b4802edce01f6aa92aac7105fe3fe54f4d85aa7724d4e613dfcb2ab16463f293e95551ca3861d41cd0ceee143312146064f414b1a6e61e9deab6277675571ae0d62e2d62b5b777ba921585c3018de09e47b73fa56c7883c1a34fb2b7ba88338d989d81c9dd9ce7e951f81608f4f3fda973029f3731c24ff08f5af4af92ea125024c8461816e0d73ca567a1d118dd5d9e332dc016ac92a962a3182383f8d73a7324870bc93d057a078db404d3edbed56eeab167063270573e9ea2b86911ed06dc7ef1d4127d01f4ad21b5ccaa2d6c452855c22f3b4727d4d757e1ab745d31e4603324c17ea00cff005ae51531826bbfd3ad02787ed3200c9dc7f124d2a8f41d35a9bd728f2e87008302695be5dc38c679fd2b43455fb0dd88dd9416caafb9ebfd2a9b4a7fb2ed4c7d4a9da476e6b2f59be7b3b1b6b857c4cb701d7f0ac52be86b73d215093875e0d539fc23a4de4c6e6eac21924ff6854b67a80b9b486545dd1ba06fd2b485d279408ceccf2474028436701e28bbb2d0248edadec2d8cb346d884a9d853b9e0839c8e2bcff00c43ac5b6ad69088a1b981e3c928f297424f5c7ff00aabd1fc73a1ff6c1b6beb1532b5a922444396da79fd3fad79a6a16488fc06008ad60d19c93311553c9190bb892339391ef5662b02fa7bce0c6486c152df301eb8f4a8cc3b64063c1c75ab772c8eaaa190ed1804739ad2e6691a5a7ce96da5459b8911b9f9521dc4727bee14e8a5b7ba32b8cb38c31ce013938e9ff00d7aa765731359792d092e8c4090395e0f6e3f1fcea4fb384c6d50bbba8e493f89a9762d5fa1af696cc8ae4c708c73b994d67ea6f089486b960e7a88e3c0fc0d59b482458f0b148f83f3173c54578b3c07224b64dd9f951727f1350b7299ce5c42ace5a29379f4279ab365a9c9636ad1aaf249e7ea31515d162c4b4209ebb949a8a321c6d6cb1ce40ad77465b32e44f7174e5840396e588e29006573e742ea7da9f0dcdd244bfbc458f3f7462aec730f35645ba90313c82322a5b2d22e69a902488e19c670738af4037692411c4f711470451e6456272ed83d87ff5bfc38a88bca9859d70bd182e319fa0a925d26f9e069d9c10a088de4240607a839e9d783fe442dca657ba7b0d4bc46b0dc21993ca542c78c6063803a63152ae936d66648edd4e3706cb12067b0cfa7f2e6b5bfe15ddd452a5d595f437172ca1f610719ee3209a9cf86f5582d879f672c780598a0dc0b7e1dbeb8a6e4ba0b95f5326294c7a7ddc4931682df708f771c10013c75e6bcf99819189cf26bb8819869778acc9e77259495dc3e6f43cfe55c29ea6ae044fa16a752eb132a1c141938a82442aabc638a924702387ae40e948c64923191c0aa249ecb529ac239044797c035e8fe14d2f47d56ce09ae035dcefcb17e8847518af2fb7b696eae52de252d239daa07ad7ad7c3bd0e6d3a59a3bd700197e42a78ce2b3a96b1a52b9d75cf8534c091b2d9418db81841d2b90f1178762d3e16bab7daa1012c84f047b7a57a44b711f9689bc70319fe75c778b591e1481c6f599d5594f194cf4ac7a9aad8f2dd2556e7c4904a3955cbfe438fd71563c7173e65f5ac1c7eee2c9fa93ff00d6ae8b4cd16d2d6e9de08d639b69c64920027a735caf8b6caf13547b999331b8003af23818ad62d3919495a263cb08b79026f0ea50303b4807233dea0c84707a8f4ab7747ceb2b59b7966506220c99231d38ec39aa4cc58f35aa321cd2191f73f3fd29637453f3a034c504838e878a74b1f9647b8a6211994fdd5c500300083c7b537a7b9a5072c031c2e79c500767a3c0c343785c1cb0f300fad64e9cbff13f5c8c904ff23572df5cb58ad153ed272bc0063ce4552b7d42ca0d585c995d9307eea60e6b3b335ba29ea63ed3aa4891825f3b42819c9ae9345f0f2d9c2f35e1759ca657636367ff005eb3bc3d2dbdc7889e674e76b347c77f53f866bb198ee89d802709533935a22a9c537725874bb2d22dc7d9c36e982bc8598924e2a0d4d449a15e127f847f3a9889ee20b70814e55776e27818ed50ea91490e8976a50fdcceeedd6b3ea59b9e0fc7fc23b0e07f1351517832e3fe29c87afde6a293dc47276fff00238de9c7fcbb9ffd0856823b2142a71806b36d096f16df1ffa607ff4215a3b9440000db81ea455b1230f5dba92df58b392219904242fb122b04c6c11d5c7279adbd6b8d5a173c8482b1a6259b7ab96f6ab5b12cce8d8c7303ef5ab6f2a4530df210cdf285c7073eb5973ae1b701d6b434eb36bb99674218c7cb2938e7b55bee4477b166e340963ba6123e53a83ed4e4b51102b696fbdfbc8c30a3f1ef5af15d7da17c8b952920e9b875aab776fabdadaadec92a9d3da431f9a141da7d08a8bb65d92316e96e558473cde62b741ef4fb8592d74d6427e56c0c559863b45b86926bd4924fe1cf00556d66e239046913ef51fc54d6e27a2b9987236904f22b4751d0af74f8239e60a51f04329cf5aa6aa1e1881381bf19fcabd5eead524d3ed2550af104da323db83cd139f28421cc99e69a3e8d2ea370370db10e4b3700fb66a94a3ca9e587850188eb9c60fad76dafbcc3499a34620ae0900f6ae089e734e2f9b514928e877fe14d422b9b18ede79006b4563b71d7d0feb5d540935bdddbdb453159191a69707ee818e3f3205794e87a90d3af199d0ba489b080715d06a1e2830f8852ead7f7f12c3e5e3903939e3f2159ca0f9b4358d45cba94fc5fadcdaaead2c2d27ee2dd8aa8fef11deb19e517016471f32a8507d80c52cade64b2cb20f28b92c00e7ad31612c9904568ac91936dbb82a0764551c938af41bf5369a2c08a785c27e95c5e8f6bf6ad56d6dfa82f938ef8e7fa5777a8c6b25946b81c483359cd9a40bd1295d22c57a11164fe62b9af11dcc4de4c258ee0189551d01ef5d5c4bf69b281901dbe4f073d79ae57c491430c90ca8815de33b891d79c0a98ee391b5e03f10a468da75d3eed9f343fed0ee2baa9b51998b8b5dc22e92211f3015e3b6e648ae11e266564390cbdabaa3e20d4eede1901449235dacca3eff00fbd4a51ec5465dcea26bd7d1cfdae073342e72541f990fb7a8ae17c5f3092e1255411bca77155ec49cd6cc46598ef9599b24b107a67e95cb789ae0c937dfc846fe429c16a4cde8634b2f9d2346acb1a0382e73cd41916f2bc6c4e5495245104b0a861323beee855b6e0fe469561375724e71bdb3f4cd746861ab2e69570eeed017411ed24065e09cf7c735b36a1a78d2e046a3270bb674fe44e6a8dbe93141898bb12a791ed562e6355be99d5f11b6d0814e7a2823f9d43b32d268bd3de320446766ddf7977839fcab32e48472252557b01d6a28e05882e25f9d5b381d81ff00eb81f9d5a9499b0238b7cd8e647e9f954d922aed9957fe62af3948cfdd5cf5acc19e6b4751564203b1671c127b55141bb23d6b48ec652dcb36a0b18c2e0124e09e87d8d741158b041244db5c0059075fc2b9fb1c19761e9d08aeaed654670b21665006d71d571eb513dcd21b1b3a102622ee59547538ad279acee239ed24b892278827ce240158b671c1c7a5436924620315f10a187cb220c6ff00a8ef58773135d4dab220127ee4151bb1b5548e99f723f0cd4257299d6781619e0d6354f2ae4cd1c3b40f9768dd83dba7a74aeb4f88e44f3249600d19c8c29c73d2b9df08409a2786a18e51b6eee4799803192dd07e58fd6ac4d23c7e68b95d91c386455018b7f2fad4c9eba1a4569a983e2e3a28d26fafa34f2ef76632073b8f6f7af1e0373575fe37d5e3ba963b2824575425e5653c339ff0ac3d06c7fb43538a023e5ce5ff00dd1d6b682b46ecc2a6b2b21936917d1db8b9fb3bf9257706c83c7ae2a8966db8cf1e95e9d23abfc8ca3679641523b74af39d46d5acaf6480f4072a7d41e94425714a3626d16f934ed62deee552c88d9603d2bd6175282f2082e85cac51b0dc88a705beb5e2e08c74a95558e06e381ef44e0a43a7539743dc20f15476d6ad2ceb0b5b4608deae319faff8573d73e204d7e5b8b8810885258a34cf53c939fd6bcdd431511e58a839db9e2badf0a8c433a11ff2de13fa9acf95234e7b9adb8cb3c7309582c4194c63f88f6aa9e213e6e8f30e7223c9fcead5c7eee4957a61e99e48bcd2ee4b648f2ceee3b54adc6d6879a8620151deba2d3fc19a85f22485e18a36e84b64fe95cf4b134321470430ec6ba1b0f13b224305d27c88bb77a75c638ae895edee984396fef1d2d8f80acad879b772f9e131900ed5fc6b96f18594365aeb2dbaa2c2e8aca13a0edfd2ba6b7bb12582ce2e888e53b76e7d4f19ac6f1a5ac711b3911947ca50ae79eb9cd67093e6d4d6a4528e8729bceddbc63e94da28ad8e70a7843b7763229956c736eac39ec6936348dbf0aed7d48ed600088f047d2bb256022700f5435c1f873f77ad4040396c8c7e06bbcf972f9183b0d615373a29ec5ab6606cedc77f2c7f2a975ec7fc23d723d63feb55a003c8833c7ee41e3e9563589564d0240060988e6a4655f06e4f87d30ec0798dc0a29fe0c645d000cf495bfa514dee239cb16ff8aaafc918fdc11ff8f0abe25f9b681f29ea6b3ec177f8aaf909f99a038fcc569a4405abb10720d390a273dac483fb523f980fdc0c67a75ac8b92ed907ff001d008ad5d7414b8999782b6e0ab7fc0c0ae69f7ee0bbc9ddcf06b48ad0893b0922b0620faf4ae8ec4ae9d62233b7cc7f99c1fe5545eda1d376f9ce0ce1436ceb83ef56f49d1aef5fdef14f1a468d872c79fca9c9e828ad41ae4c932c28db831c28ea41aeabc5da649a67822ce1e5b6ca1e700f1920e7fa0fc2b4b40f09e9fa63ace0996e5391249dbe9d855dd72eedee13fb32501e3b8055b9e9f4f7ac79d5d58db91b5a9e2d20463b909cfa1a96e48f26050a4100e734fd56c1f4bd527b4739313601f51d41fcaa3b458ee2f234b99bcb8c9f99cf3815d1e673f5b1b9a268d26a3fd9c8b8c4933162dc0014027f4cd7a1dd4a97737d9ed987d9ad970727ef1e7f4eb58f6e37c2a12371122e630c3681c75da3dbd699e1f9e5f27549665c3798a147a0c1c573b7cda9d0972ab097f009e2963feffcbfa579b4abb24607a835e9aca24656cfde715e7facc221d56e557eeef27e99aba6fa19d45d4a510cc801ab410a9c31907d2aac58f3541f5ab619ba6f38f4ad199c476c5ea0127dcd35c9236e69d9f534c38c11cf3e948a3a3f075a86bc96e18731a6d43e84ff00f5bf9d74ba987166aa571ba4dbb81acaf08c405ab7ccc0bcb81ef802b5b58661628339632103d8d652dcd628d5b51fb9b608db63f236e00cff009ed5caf8a564875131338601176fb03cff003aeaed6148e0b6058e04038f4ae33c4f2eed65fe6dc06d1cff00ba294770919b689824b77adbb4600e3a715936aa0f4f5e2b45095527d4e053608bf7173e55b16e9c572bacdb18f4ab7b9766df3bbb60f4c718feb5b1224973325b03f78806a2f1df971fd9218d768552001d05386e4cf638d206401cf15a160804aac2a837cb27e55ad6281882061bd2b596c671dcda813cc8dd3d462a3bfb77b7ff005c155c2ab2803030063f3c62afe95034d731a9fe26c55bf17bb2c90a886374962239ea08f7fa11f9564b7b1abd8e5d22f2f844189482588fe107d7ea3f4abcd3a4602a80081c102b3f4d5570c2795a3dbc05c6453af13cbb9554b85604e149ff000154d129e857bedb3ca589cfd056511b1d80e4035b5776125b38dcf131619ca3678fa53acf4d173a26a7280aef0b232903d339fd0fe954b425eacc98180b8c671ce41f4aeaf4e6d9f303b580f5fbd5ca85c49095c7ccb83f507ffd55d3e9a8e576b004119fd2a66540dd11fdaa28d11c26086643c73ed54e5827d36e2eae2498bef2176ab29cc43961927b903f2a9a179166cab8f98f21ba8ad66b75bbb76d98e09c83d067d7f4a84ca652baf1e5a359fda658c89d57625b8c8db9ef9fa0ae3756f14df6a6ecb0b3c511ea037ccdf535078951135e9204fbb1aa2f1dfe504fea4d54578e15c28ab514b525ce52d0a8bcf5eb5d9f836cb0b35d91c1f914fd3aff004ae33fe5b7cbd2bd3740b5167a7a5b93c81939f52a1bfad3a8f41416a4728cb73ff3ccd72fe23b669218ee9402100563df9271fcabae9172cad8e0c58acbbab6135b4f03746551ff00a156717665c95d1c0ae7771d6b460b6dc39c6ec551da639cab0e54e0d69c04f5c56d26650406068b049e9e95bbe1a958dccb10382c63607e8e3fc6b2a66063e7a9ab3a0ce63be661d0281ff8f0acdec6874d7ff35e3aff00798d436131586ea318c188e6967626ff0038ed55adf87b850d8ca1a82ce535d5c5d42dc7cd0274fa5655696b4d9ba8874c4283f4acdae88ec734b72cd9dcfd9aea19581648dc36dcf5ab7ae6adfdaf7a2609b115022ad65d14595ee1776b0514e75c05f719a6d31055ab33bb7467a374aab52dbbec994fbd27b0d6e6d787894d760cf03e61fa1aedf8fde93d767ad713a363fb6e10dd393fa57671ed77947aa7158543a2992c72e23807a5b834ed5240749621b8319a817eec287a987b7e355f502dfd8ab19ebe5b0a91963c1ee4e879c67f7adfd28a8fc29f268d8038f35bfa514dee232ec1953c5f76413b844d8f4ea2af457264fb4405482bc83db9aceb518f17de91ff003c58fea2b5418e450bbbcb761f780e78a721233354b469e736e48df25a9653f8835c4b215721b8dbc1aefaf27ddac47239184b462481c0e95c1dd4be75cc8e061598902b4a644c88924e49c9ae83c1d7ef67afc31872239cf96e3d7d3f5ae7aa48267b7b88e68ce1d1830fa8ab6aeac67176773d7d2ed5b5896c66998c2d089139c11c906b2e69a33a7de5b7fcb4b472cad9e495e7f322b9697c56d2ead6d7be4b2f96851fe6c96cd53d435eb996f6ea6b5768a3b918607049e31d7b562a9b3a5d5455d6b511a9ea2d701481b428ddd48031540641e28e4d4f656e6e6ee3880c8279fa77adb64736ecf4ab1bb7362be6b676a2ee2c7a9db515946f369b71e5018665cfbf14eb8d97314aaa4b10bf2864da471d3dfd8d3f481e56912b81d1803cfb57333a4ab68ecd7a2d98e0a49bc67b815ccf892055d41d97a92777d7271fa57716f12c987dab98dcf38e707deb90d782869dc82584a735507a8a4ae8e620199d07bd5c62ab9aad0207b950ad8e7393dab48ac6101450ce3ab10715ac998c762b2095b90831ebda91c00e3e6fc0529690e43487e878a6c4a24bb48cf1b881923d6819dc68a92db5bd8aaaf2eccc47fbdd3f4c55bd4246951222bb4f998c7a54d1955bd85554615b68fc07ff005aab6a7771da46f3bf504ed07bb76ac1ea6cb43683155553c016e39f6e2b83d76557d4a46c12070b9ef8aeda0751a44324ac4ffa202c7b9e2bcf6ee63757af33c65433671e82aa089917ac9485049ed9abe87257206476aa967134f1811104f615a5044725645c38e686342db41732ccff006543e6853839e07d6b9ef165c4d35f431cdf7910647d6bb4d118fd9ee1d7a82727d78ae1bc507ccf1038c63000fd29c37267b18ac0bca73d6b5ac46d5c62b2d46e99beb5b1a7c7b8d692223b9d8e8a16182de663f3c85f60f5c0a6f89d5e5d2eda69142b093ee8f439ff00eb554b399a3d4ec2dd9c145864703d8e39ad3d7b6c9a63ae470bbc11cf4e6b1d99aee7202d91a10dd48e0d36081fcd5628a02f00ff005a9ad9242edb7054f6a912de5f358367cbeb96aab85883508d9e2fdeb331cf05893573c1f6e6e74ed4603d24f94f3fecb551bb388d8f5e3820d6cf80537adea639dd923f034fec90f73903180aa492406e074e7fc8aecac52d7ec90319a58e43c6430618e98c715cb5c6e8a49610bf7640727ea4574763916c8bd40e40a72611469dbcd0c6fe5a22925b8258e3f23fe26b4e2b691c3825994fcd80700564dbb451beeb84dcd9f94fd0d74fa4913ae553ef71923b5436cbb1e47e216cf89af7b6d98afe5c7f4aa375198db3d8d58d66613eb97b2f5124eedc7bb134d9c868551ba8e86b6ec63dc6e9b07da2fa18cf46751f99af4d770b7aca87bffec8b5c0e8712bea70638db963f80aede2f9af49ef9ffd916b2a8f53482d06b361a3e4f31f4fc2aa4ec3c9908ec13f99abf7511578323042608fc2b29db74e222d80db723f135059c9eb96ff0067d4ddba2c8038fc68b66cedc56e78c2c950abaff0398f38eddab9fb36f2f835b2774656b48b73066ce066aee8484ea32ae3eea0ff00d0854113c6401838cd696856fbb51ba60db7083ff435a96f428d8be206a27d7d4554b65537328e4641fe5572fd043aab2b8caa9c360fb541259bda5ea1dcac8ea086539192338a82ce2758e6f467fe79aff2aa157f5718bd1ff5cd7f9550ae85b1cd2dc295464e3d692971f2e45310e6fb8bea38a655f4b2f3b489af173ba29555876c11542921b0a5070734945311d0692ad2ea76654852e0ae4f41c575f6aaf6f3481d839dbcb0ef5c6682e5af2019e51c30aece13b9a563e98ae7a8745323f34f9919071fba3fd69976e5f4c52724b21350477491cf192376232083d3bd4934cb259c4f95191f70f02a4a25f0bb634a618cfef5bf90a29fa2412da593c6d1824c85b96fa7a514db11916cd9f15de9ff00a62c3ff1e157327cb0fd07233f8567d98cf89af89ebe49e9fef0aba4b343b0b9f618a6c5133755971772609c7d900fd4572c401f31ebd856e6baf8bc207f14007eb581d4d6b05a194dea3b6eee7229a460f6a72856cd2911ede09cd5136011315ce401ee6987d3d294300738e3d29b4c415d0785406bb9c14c9f2f83e9c8ae7ebabf0ac263b4b9ba3c64ec1ef8193fcc54cf62a1b9d14b318a2ce0160bc13506897724ba45e174283cec283f4351dd4f188844eea1d91b683d4d1a46574b9e1c60f9a0f27dab0e8741bb6e0a459ddc960a45721a9e253ab2b0ff00572aed247d6ba852c1e3c77705bb5625c5b25c5cea4a41c1249dd8ebcd11dc4ce26df02e97710067935b2a81a3fdd956fa1ac35389d771ce1b9ab4510b9cf15bc918c59627dc83054827bd52569158be381ce69613b2465652fe9de9d1924302300f028d82f73bdd3ef7fb412c6e4603b396603d7906aaf89d3162acc7a499a9f45b416da7da6ee1b696c67a13ff00d6a3c4f16ed393208f9c62b0fb46dd0bf75ba2d06368c938b6009f63815c5bc2679330b12a7b9ae9358ba6161636b1363fd1d4c9dfb0c7f23583125cf99bb72919ce4552d109972d82da346509f3073815ab75342c22bb85c89870d1b743eb4b61086504c7f31e09eb41842c6c31c90ce38e9c1a9ea32c784a63716574320e1b078ef815c8789919bc4928036f03f95753e0f1e5695700f04ca7afd05729af48f2ebb71e60184e060fa55c7e2265aa32205fde1cf6eb5d16956e646c05cb1e063d6b1e084ce04b1fd18576be1cb4449e379195368f332c71d39a7362822a2201e3b9ed864ac300887e0ab9fd735b9a940ad6c6055eb195e3deb3d239478be797032c8581f5e95b53a868626c8cf56c7a566dec5a5638cd3229671e5a265d78383e9d6b616dd5311cf163770732e73f4181593a11897c4135bb64c6243b70d8cf3d6bb583ecde6c8aceb0b0e8c533bbf2a6f4625731f56d3ace1d391e08dd1ca95c1e8a7f1aabe04dff006fbe665c0c052b9f635a3aac71c30caa927988012eee36007d056778325dc6f6539657902838ed45fdd6165739dd4edd9752bfc9c05978ff00beab6f4a60f1ae769253af6acdd6d36ebb7ca72aad27031db3d7daba1b2b085628f6b8cec0726453e9db83dea9ec25a3196c924b738787073c10783cf5eb5d85a32db5a8383bb693c7d33599a5d94015dc3c45b67000c9cffdf58ab5a9acd1e953c8a23370d037caa7383b4fff005fb9fa54d8773c71f62dcbf52c18e4b75cd43704ef041e4f38ab0c6db24c88ca7d50ff004aad2ba33e53763d4d6c8c99bde17b466b89676ec028fa9fff0055758a08d41c1ec7071fee8ac7f0c42f169c1d8f323eee7d056b2c9e65fcce3a6fc7fe3a2b19bbb358ab227ba0cd321391c7f4ac392365d410e7a28fe75bd74a0c91738f9b1fa565ddc7b2fa3c742833f9d4a285f12c0d756d764152400ea31cff009eb5c1c527cfb8fe55e8f2aef72f92541507fcfe35e7935b08f5096dc9d855c804d690dac673dee5d88c72282b149b876561835b9a0ca23bd1bc8532b2a904e73cf4ac3fecc9a3f9dcaaa0e73bbad5db111c1796f2392b1a386dd43b0d1d36b6ff00f133bada030623922a8acee638d32010dd7f0ad0d5cffa63b6d1ce0e6b2c643a72002d508b396d6948bc427bc4a7f4acdad5d7540b984fac2b5955d11d8e696e14a3ad252804827b0a623b0d06c95f41ba89f204ea1b9ed9dc07f2ae424431c8c8c3054e0d7a0d8ffc834e0607d9603ffa1571bae46b1ea92ede8706b383d59a4d688cea28a2b43334f4972b7511071f3019aee6d6269219f00e474c77ae174a8ccb750aa9390d938f4aedc5cb5958b4c8be664f63826b0a9b9d14de866416d31cc87905587d39a8a59311223b6d20569da379b6fb0e555b279e0f26a1d4e3c431c7c3039e156a7a8cbd60e27b7f31d98963c151c63028ac2b03731db98fcc640ac40039a28e515c2ccedf115f919e213fcc55b5058f999e01c628a29bdc2261eb0737ea0951fb919ce6b1645d8c451456d1d8ce6474e65287068a2a8cc71501377ad47451490d857a068b0c49a2d9c7ce18966fd73fca8a2a2a6c69496a6bc3a769f75342b24645c85dc0927001acad4753b1d1afae2d08653b81e1739e3ad145650577666b3d11a7a55e2eae81ed21678c30425885f9b1556e2cdb4f82ef51bd74f2a494c784c939c9a28a1e8ec1d2e79e5dc491cc76b641391c76abf1db5c4d6a2654428dfc59a28ad9ec6296a529239639739033dc52db93e686701829cd1451d056d4efd752b59228dfcec67048da78cfe1597e21d66dee2d9628e42f21939f948ed45159456a6ade85796f252012a0b6c543b871c0c0fe55369eb2b48aef1aec0dce0f1f951453623afb7b0923859c80011c11ef5463c4d1dcbbb80510ed503a0238a28a82887c3124736933e1c8659493c1e8145705773bdd6a173231ce598feb4515a437644fa12e8f21b7bb5c8ca93c8af58b7b3b27b113c6872eab8c8f5a28a55070d8c189e393c55731a91e62467b1f6ad0df862ede807e3cd1454b28e4a148a3f14c929fdca094856dbb864004f1f88aeda6b9bff2bccb59ad4c6a4659632a7f5a28a72144e535dd60ce8d198c129c96edef5a1e1a8059daa2b2a8779ce540e9d38cd1450fe11f539cf12b28f10dc73b892ae41c91c807fad6f68b73164b61e33850815b8e828a2a9ec895b9da69ae246556f35893b701f6e4fe155fc597eb65a5dc5c79461455318cb6f20918e28a2a56e3678add6400d9183daa3886fb7741d41068a2b6e864f73d134bb7fb2d94313004aa0cfd6a58306f67ec038e07fba28a2b066e5b97e7ba8c76049aaf796e3ed08e307f760f4f7345148062f1e6211f7f9fcab89f1241e5ea6d27fcf4507fa7f4a28ab86e44f620b0926311753bcaf186f4ad04945d308da1f9fd37f145154f7147636adafe4d56095e44d9243b636e73bb1de892223033de8a2a1e8cb5b1cbebdfebedbde0535934515bc763096e152428d24c9129e5d828fc68a29928f44b480c724b6bd7cbb78c1fc0b0ae075195a6d42766ece547e1c5145654f766b5362ad3950b2b11fc3cd1456a648d7f0ddbbdc6a9b54e02a331fcaba5bf19d19327037e38e7d68a2b19fc46d0f84b7a36f8f4c814b075756c0619ef4cbc8c070af9e063e5e33c514567d4d0cfb5d461b659220caa039e3cbdde9de8a28ad39519dcffd9	image/jpeg	2020-03-23 16:17:39.378801	{"width":"500","height":"295"}
26ee54d9ec94eff10f1ed74b22c7651c34cc3976fcc613039f2b87c0a35fb056	within-temptation.jpg	20269	\\xffd8ffe000104a46494600010200000100010000ffdb004300080606070605080707070909080a0c140d0c0b0b0c1912130f141d1a1f1e1d1a1c1c20242e2720222c231c1c2837292c30313434341f27393d38323c2e333432ffdb0043010909090c0b0c180d0d1832211c213232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232ffc0001108011601f403012200021101031101ffc4001f0000010501010101010100000000000000000102030405060708090a0bffc400b5100002010303020403050504040000017d01020300041105122131410613516107227114328191a1082342b1c11552d1f02433627282090a161718191a25262728292a3435363738393a434445464748494a535455565758595a636465666768696a737475767778797a838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae1e2e3e4e5e6e7e8e9eaf1f2f3f4f5f6f7f8f9faffc4001f0100030101010101010101010000000000000102030405060708090a0bffc400b51100020102040403040705040400010277000102031104052131061241510761711322328108144291a1b1c109233352f0156272d10a162434e125f11718191a262728292a35363738393a434445464748494a535455565758595a636465666768696a737475767778797a82838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae2e3e4e5e6e7e8e9eaf2f3f4f5f6f7f8f9faffda000c03010002110311003f00e31501cfd6a455f94f5eb4aab5204e31585c816ce3fdc81ee6acac7997e94db45f90fd6ad22f26930220bfe904fa0aaf20c5af3deaea2fcce6abb2ee8625f56148065c0d96c1475db5a30c5b6de25f45154af1732a20ee40ad5d9c81e82936318b1f5e2a658c161c539138a9a34f9c93d854dc4471c5949cfd16adecc1c7d2996e9bad98e3ef4956f665df03b8fe54808a28f86a9d13ee7d69d127cadf5a991395e2901084dc27fa81fa0af3df16c5b6753cf0d5e950265263fed9ae0fc670e06eff006ab4a6fde40712723208c8f5a4b6ea47bd4f8078a8a11895857600fc7344832a7e952639a560290105b8c40475c1a254cc919cff0010a9211fbb61ee69b37011bd185318e2099052baf20e69ff00f2d3f0a1fee67d0d2111c5c063e99a6c2a7cb5707b935228fddc94d8be5b527da80120fbaff5a72ee39c629201f2354b18f90fd686045203f2e71f7853f9f3334937f00f56a7370c7e940091a71d3ad757e0a881be9dfd0015cc20e95da78162c9b972382c07e5ff00eba89bd04f63d06cd083c0e3156ca7069b688454ec98c939ac519b2a320cfbd3b667b54d8193c7e941181efda8b08ae53078069853b7e1564af14dd80605161a2a347c9f5a8990d681506a174ebd7f2a433cbfe2046c350b69bb18fcbfc8e7fad71df316e9c57a1f8f2df759ac807dc9339f63c7f8579faf4ade0f42d6c44a3e7a81ff00d79156b1892abcbff1f35a22808c2b0a8d41da05582b92dc76a857807eb4c08ca3066c0e0af3447f707153b0e0fd2a18798c530b830e950cff00771eb5688c557906e971e833421a18b8111a881e6a6c661fa9c5359312a81548638fdd3f4a8e3ee6a661f29fa5450f20d002839351bf5a9b6f350bfdf2281a003381eb5d5d947b74de9d1857310a6e9917debb28536d8bae3a62b2aaf6131d8c8a6de0ff004653e8454807ca3e9497a3fd0ab0ea4f529e38a637dd352e32a3e951b7dc3543227fbd453c8e68a606ba8a900c0a6a8c8069edc2135888b168bfb907d4d5a55e3351d9ae605ab4138a4043b76a39f6350449ba645ec39ab728db037d2996c9fbd271d28b88859049a920ff006ab55906eaa16a9e66a99c7404d6b3a7cd53218d54e0548461243e82a444e05238fddb8f534844f6b162ce1f76cd4e899793f03522262de3f6229d12fcf21c76a004853e463ef532a8033490afeecf1ef4f75db093ed4804b5506076f5627f5ae2bc69113685bfdaaeeade3db64bf4ae5fc5b6dbf4c738ed551766811e6206315128c5c91ed53638fa531862e57d08aed02503e6a1c72b4e03934920f993eb4808edfeec83d1a9b70bfb935220db3c8bea3348e37466980e1cec6f51430fddbfb5240730467b8e29c7ee482900c1ff001ed21a451fe88bef47fcb939a55ff8f68c7b531842b80ff854883e4c7bd3630407fa0a917a28f7a4c44330fde20f7a7b0f9f02925e675fad3bfe5afd2801ebc577fe0287fd09df1f7a43fc85703d147d2bd4bc136de5e8b07ab02df99359cf614b63aeb65f6cf15395f6ef4d8976ae4002a43d38c5668cee42cb9c9f4a6e38fa54e475e3f3a695f6cd3022db46d14fc518c0e940106da89c12703ad596151ede793f8548ce47c5d69e7e993a8192509fc7b57932f435ee7ac4024b7718e315e257317937b710e31b588c56b4d9512271f3a9aad3022e055a3cc6a6a0b91fbd43ed5aa2d0ec7cc3e950630c47bd592395a818625a621cc3e5ff0080d4108f9455a2381f4aaf074c535b0c738aae067cc6fc2ad3f1c9a842e2dfebcd0811146328bf5a471fe92bf4a9205f901a6b73743e94c698ae3e53f4a8a01f27e353b0e0fd2a2807eebf1a005239aae7990fd6ad918aab10dce4d34345cb08bccbd41ef5d815dab227b0fe55cee8507997a1bd2ba69971338f502b9eabbc8964318caa8a5bf18b27fa53e15ce297505ff437e3b567d40cf419404fa544ff0071aa68416841f6a8a4e8c2ac77184628a711453036621f22fd29d30c4669d0afc8bf4a5b9188cd6222ed80fdc8abbb7e5aaba70cc22b44a7c86a1b029dc27eeb1eb4b0a6d0e7daa69572b415c235171116971e6f646c74502b5644c30aa7a3267cd6f56c56948b9931498c554a871bb23fdaab6ab815040bba46ff007e811a5b7f70b4918f964353b2fee4572bac78a934a2f6f045e7ccde878142571ad4ea211f29145c0c5b9af2d9b5af135c9678525850f3855ab365e32d62c53cad46c5e4881e64c10dfe155c8fb95c923d4a35c5a81ed58dafdb799a732e3b55dd2356b5d5b4c867b7914974c94ce48f5cd49a945bed08c76a9d883c3a54d92c89e848a864fbf13569ead0f91ab4cbea7359770ea888491907a57645dd2196b14c931e620f7aaa925c5ecab1408ccec70110649ae92d7e1cf8aaee2f3574d741d84aea87f227343696ec6a0dec8c6618b953ea08a6e3e5c55ad4b40d6b41914ea36334480f0cc32a7e84715543891432f7ed4d3bec269adc65b72922fa366a4fef8f6a6c236dcbaff78669e07cce3da860423fe41ed4e51fe8f1ff00bb4f86de49ec8a448cec7b019ab2fa6dec16a864b691405e495a5ccb6b8f95b5a159072ff414f41f32fd0d35782ff415247cb7d050490b8ff481f5a39f358fa0a571fe902957991bea05301e72781c9e95ed3e1db616fa743181f7540fd2bc7ec2133ea96d101f7a55cfd33cd7b7e9d17976aa3d8565519323422194e7b54a23f7a6c246d23d0d4a39e7a54a248caf5e29841f6eb53373f4a8f193e9400cd9c64d348e0d4f8a611ed401162a32b93c0a9881d7a0a66067228029dfc7ba23c578bf896dbecfe20938e2419af719d3319cd79478f2d3cb9e0b903186da4d386e38b38e5e6223d0d4170388cf7ab11f0d22fbd4372bfbb1ec6b7ea58efe153ef50cc089454dff002cc1a8ae07cea7de980f1ce3e95551d63c963819ab44844dc7d2996e91caa5644264ce57033f852bd8a4ae4618cf9d884ae3ad58fb1b3c248618e9ee2baaf0f7812ef524133eeb788f21dfbfd0574327c3eb2b753ba79a51d71c019ac27888c5ee6d1a3267962c0f0318db071dc5418ff4afc2bb5d6bc1ac80cb68ce19790ac739ae38c6e97244a30e0608ad69d553d5113a6e1b838e1be86a2847ee854cff0075bfdd351c23f74bf4ad08125388c9f6a8adc70c6a5b81f2629b02fee87b9a77d00e9fc336fd5f15b376b8b83f4a6787edbcbb4071daac5eae27fc2b964ef22595edd696fd7fd11aa4b54c9fc69d7abfe8ec0fa54f5031ecf9b61ea2a16fbcf53d88fdcb0fad44402cdf4abea50d00ed1f4a29d8240c514c57372d8640a7dd2e22a5b31951f4a96f17f70c6b1ea058d2c66115a9b3e4359ba42e6115b0172b8a87b814a45e83d6898611bd854cea037d2a3bc1b6173ed4089f438f1681bfbc49abceb99699a4c5b2c22ff76ac15fdf5218847ca6a1b25dd2b7a6ecd58978434cd3d7963ef4c441e2bd50e95a0bc88fb6572110f7ae2fc3d025c4e65917731e72dcf27bd5ff00883702f5ac2dad9bcc54959652bd15b8e2a35d32e6d6d1248098e355f99c0c9a256e5b5f73a28c6dab3adb3b64dea0a823e957a6d3eda588ab46a41ebc571fa66ad729cdaea11dc153f3c32c655bf03debb282e96e6dd666f9011939ac1c5a3a934ce0f54d267f0bea8bace979110389e11d0a9aef6d6ea3d474b4951c36e5ce403587e20ba8decde2522446046456a784f327856dc139d80a83f435bc64dc75392bc52774799f8b21f2756f3318045714864bcbac2296791b0aa3dfa57a478e2dbe55940fe222b93f05e9ef77af215527c91bba77e82baa9cad0bf6338abb3d73c0be18b3d06ce39a4546be7199253d467b0f415e896fb6400a9047b57016b71608a2deff00ed4b3f4468d18f3f856b6893dca5f490659a2d85e372319f6ae2bb6f9a476a7a591bfaa5ac3756d2dbcf12491b8c3230c822be7af17787cf8675bd91e4da4ff3459edea3f0af605d56feeee5c0961b78c73899b0587e55c3fc4c90dd69a85914bc3283bd4e460820ff004ad68c9a979333ab67138161b6ea26ec462acc1079d79e5f63d6b3e290bda44e7aa36dadad383c92168c7ce7815d1376473538de5a9dee81a2c51448123ede95d04da646f091b07e22b92b07f955a2d6a74bd4ff00964a3e4fa1aeacebb247a309de20f724ecdbd39af3a51d7567a0a6fa2382f13787d20df710205603e600704572b10fbc7f0aeeb5abbd58dbf9b782ce481c6310365933eb5c3a7438f535d946fcb6671d7b3774407fe3e33ef4b10cc87eb487fd7fe34e83ab1f6ae83036bc2f6fe7f8820c8c84cb7f4feb5d1f8b7e22368f39d2b48447b94c0966719553e807ad63786241636fa86a4467c988ed1eadd87e78a9bc37f0c353d6717f7d28b7129dff00bc1b99b3ce7158ca518bbc8a8439d984fe28f135fe5cea738078c26147e95d2e85e3ad6b48509a8b8bf83b824798bf43debb2b5f85563115f3eea699472547ca0d60f88bc057766cd3d85942ca074c1e0547b68bd0d1d176d8ecf45f10e9faf43bad9c2cbde2761bbf2ad62bce06057ce375757ba45f2c924325b5ca306478d88c62bd9fc1fe3087c476a118795768a37063f7fd48ad1ad2e73ca363a80077e08a6b549d7bd319b9c735249111d3151904482a7e06462aa5dddc56d1f9b348a8a3d69302675cafaf15c178e6c8cda64c40e546e1c7a53b5df880b6ae62b188301c1918ff002ac587c6506a70b58ea030ecb812f66fad0bb96a125a9c327faefaae692e97f734e9d3c9badb9ced62b9fa5173fea4d740c897fd4d32e0743f4a747cc2689f9881f6a7d408ae95cda298faee15dcf823c36abe5dfdeaf98e79546e83dcd71b1bed419e8719af51f0cea76ffd98259a458e3518249ae6c4ca4a36474d0b5f53b9b48c15181c629d796d942c40551dcf15c90f883a7437421852565cedf30ae01a7f8bee350bab682281cf9730f98af3815c7cbd19d5cdd8355b8b4442ab3c6cc3b039af2ad7a35fed272536e4e54f635d3dae9d7dfda02da1b4ba5452159e49060e7db6ff005a3c5fa2fd9b4e12381bd0ee1edeb5bd2b4246352f289c0ca36893d969b08fddafd2ae4f6521d31eec2b6ce0138e39f4aac83083e95df169a396516b46413f7f6a96d22dcf127a9150cdc8c7a9c56be8b6fe75fc631c0e6893b211dc69b088ed947b5437cbfe91ff0001ad28230b1800553bd5ccedfee8ae4ea4156d1723f1a7dea8f25be94fb119cd3af57f72df4a7d4673f623861ee6a2032cdf8d4f643e77a8d47ef241ef5436342f1454aa063bd14ee236ac4718ab77b1ff00a239aaf623902b42ed3364d583dc7d46e8c3f722b682f158fa20cc2b5b7b7149ee0559179fc6abea43116dee71fceaf4a9dea95f7cf3c518eec05006d5a26cb645f41401994d4b10c443daa25ff58c69086dc1c2532ccedb7761d704d25db613834fb13f2fb6281a38ad51105fc06220f983cc200ebee6bacd2ee079291b8ea3bd7297490b78a27b7889f2e2e141edea2b7edcac7b79cb27415133d08cb995cd8bf4b2b4b537122c31f39dcd80323b9fa565dec86e74a816dee51c31cec56ebf88a92f6f2291562be7451d402a7bff002ac9b6b1b1875d86e2de4c44cb9c2b9299ee00a515d58ded6473baa4be446cd6bf688e52c164b7979ebdc1af54f0ed93d87866d2de5189045971e84f3fd6b934bbd3ee7c4d676e61f35fccea067a0247e00e0fe15e804fee1be95b27ee9c559eb6380f17daf9da5cac072bc8ae57e1a4a96fa834b21c966c1aefb5db76b9d2ae225fbcc840fad795f86a74b4bc8a3dd86c91267f85b3cd5ad69b4149d9dcfa161b6b576f3428dc6a8c57d0c9aadc43e7a218e3c6d2d8acbb2f115b5b5a219250cd83802b8abeb78353d4ae351dec8c4fcde5b10187a1ae75ae8ced56dd1e9c34fb1d62d14cb1472ede03119ae3be206996f65e12ba118c1e08f6e456af84354b4834dfb12308d8125467a8ae7be256a01b499630c3918c55c1de48ce6924d9e71a3d93df08ed908064627e6e9fe78ae9ac238f4ed56289a33193b4ec2738c8ae6f4ebb165716f3824796413815b93ea767a8ead0c96ceec5570eccbb727dab7ac9b7e46545af99e97616f6c61925550b8524902b063d5b4b96dc43f6a064f3f1b1948cfaf5a8edf51bf8ec156d2232e4fcc030069b73222dbf9874c41303bbe52370ae34bb9d7776d0975cd1ed60496648c46c10820700fe15e7a14c6cc8d8c8383835dadfea336a16900995919c8041e0915c8df3c6d7f3794a15436315d342fd4e3af666793fbe3ed9a7c1c231a61ff58e7daa5847ee87b9ae97b1ce7a078334f5b8d35164505649b73023a815eb96aaa90af4e0578addea72e85a35adbc776d68f24630510166cf3d4f415d67c3abfd4753b6be86f2f05ddbc6a36ccad9e5bb67e9cd70d58c9de5d0eaa12495bb9dd49ae69704de4bdf4266ce3cb46dcdf90e6a47ba86e636114809c60823047d41af24f10d85c68daabb5a59b326d321f9d93cc3e9b97927db35d27835b5ed420965bcb59a18131e509db732fae1ba91f5fd6874fddb94a779599c6f8dbc2d7773ab170cbb09ee715cdc6b7fe1fd634e9a19225f2a5077672a01eb9f6af5cf19d9adee81312a44f10cab2f045782cad73249224cecd1a9efd6b6a12725e865561667d336d7315ddb473452248ae010c8720fd29ec324f6af0ff0478a2f347d56dec4b996cae65d855bb1ce011f9f4af73ea39ab6ac72b5621618079edd6bcefe21ea1ba4b7b2b36f327272c8bc91e95e872b855247415e616112ddeb1717d29064790b6ef419e3f4a894ac694637919361e07bebf4325d4ab1eefe1ea4549a8fc39922b7df6d3aef519036e326bd06d6e6d91555a64427d78cd5b9b634646f43ef9ac7dacaf73bd4227cf728b8b3bd6b4ba5224539e6ad4d830d75be34d2a37b949b0049fc2457224ee86bb29cf992672558f2c8860ff54452cbcdb8fa1a4b7fb841a561982b4ea64308dd0803a915d4786749bbd4f4b2b09cb2e70a4f19ae621c6c4f4cd76fe1cbe9b42bb6b594001c86423a106b1aefddd0de8ee74b61e149a2b177bf904ae06506d0028f4c0a92e758b0b7d3ec8bdec25d7e52036483e95b075bb58b4d92e2e5b081704773ed5e51690413cb7b3da02250f98e3ea4e7b03eb5c715cf7723aaf6d227b446627b08ae61c10e818103b62b83f14ce6e61983f20023159a9ad78aac615df0cc6351ca48a38159faa6bf1df5a312bb643f780e869c69be6d0994ecb5239ee625f00490aa82db82eec741bab943c254935ecef67f66327ee77ee0bef4c61f2d77d38b8ad4e4a92e6b1588cca83d39aeafc2b6dbe66908e95cb273393e8315e87e18b5f2ecc311c919a555d910cde893e5acfbe5c4edfee56b46bf2d66df2ffa437fbb5ce495ec57834fbc5cc2df4a758af06a4ba5fdd9a00e5ad1712b535066693eb53db8db2bfb31a6423f7921c7f15683055e28a9369a2901ad6439535a9729fe84df4acdb31822b6265cd99f75ac5ee32a6843f755bc5781587a00fdd9fad6f30e293064528f947d6b3d86fd4e25f439ad19f1b011ea2a85ba97d50b7a0a00dc1c263daab820127dea77c84aac3fad022b5e376a75b4a2389989e1466a86a37d6b15d8824b88d64233b4b007150df5df9560523396970808ed9ef46c54536ec8e02f7529ed35996f1b256572d9f5e6ba3d2bc411cb32f393c1e4d49adf87fed162155390339c570535bdde9929c165f7f4ad128d55e675eb13d9e6ba3359f99015640a77a311cd733a94b15b2bc902f965882141e01f6ae321f14de5b4650c848618c8ed56f4b179e21b9088c5615237b9ed52a84a3bec2954563b6f04698cd7126ab303ce521cfeadfd2bd059b1037d2b0b4b8e3b6b68a08c611142815aeeff00b83f4a1bb9c92777732e71b902fad78e788adffb3b5fb968f8567dc40f535ec6bf348abf8d79f78874bfb76af7898c9db91554e5caf52a0aeca5e1fd6219350856e06e8d463935ddcbaaa2cb1c76891b5b67e6daa0578c6c9b4cb878e456f95b391d6afc5e25784050588031807ad54e8733bc4d94dc4f4cf12cfa6d84315e5b94177c6ed9c647b8af3fd675392ff3bd8953823359b77abbdfb7969bc9380052ccad143e5bf55aa852e5dc89c9bd4859fe4c54fa799161927404f94f93f4aa848c5747e13b559e1b846190c79ad2abe58dc2946f2b1a5a778aada3994eff002f819cfad74571e28d226b4f91d3cd23935cdc1e12b6bbbc7899b69cf069357f04269ea0acc4e7b66b96d49b357ce8ccd7f581711431c0e4e5c7cc38c62a9c4c5b2cc724f534dd574ffb17d9108c12493444302ba60928e86152f7d46ffcf43576c23335c5b423ab301544f0adee6b7bc33079daf5a8c70a771fc05549e866cf44b8f0a586b52db35d123ece83001c66b4746d6f46d1124d26c6d260d14db65f2a32e471f79b1ce3a554bfb96d3b56b762dfb99a2c71d88a7e91aa599d46e2e2d34996e252fb1ee00e0fb66bcf9ca4e4d1dd4209c134777135bdd2230553c641229f73751dbc3b47159765acaea1944b59e19107491318fc6a2bd5965539352e56469cbaea666a17a938920c655f8ae7f4cf0759a5c4caca11ae159919a2dc08f4e9815ab7524564a5e4238e79aade20f1be9fe1ed0e26b6bbb7b8b8913854f9cb1fc3a0c9a54d49ec26d267954ba71b5f8851e8f18c2457aa01cf41b81fe55f40f98aca36b607b57cb72dedddd6a0f7f24eff0069772ed20639ce6bb2d03c4daa58e264b99a5f2c82f13b960ebf8f22bd1945d91e7556b98f68bd9163b57762000a4924f02b83d06d033cab27cbc702ba7b9bfb7d434613950f04a80b0f635c3e977ebfdb57281c088390bd860572d4d4e8c3c15ae74a34d9fca733dca35b819d9e58e3f1aa77c1a58ed6dd0baa1ce30db77e3a0cd26abe29b38ec5ec6dcbc970c3194e82b99b3d40c3ac599bbb9985bae46c99b38cf7f7acd26f53aec9222f13b5c2c67f73242220721a4dff004e6b8eb690c96b93d6baef18dfee8e4b7420a9e723b8ae2ec8e2274f435d947e138eb2d49e0fe214e1cc447bd361e1d85387dd7fad6c60450ffab03deb66e350496c2da633837301f2ca1eb8ec6b161fba7fdea898ff00a4bfd294a2a5b951763a7b8d59b50fb3da99808f703d79adbb913f87b49326985104871bd704b1fad79c199a2b804fdd038ab52eab318846243b40e327a564e86d6d8da355a3bfb0f155b5bd816bb479ae88eaed919ae1357be49a776857cb566ddb41ce3dab364bb660416634b656d25dce06095cf3550a3187bc27394f464a3fd5a67bb66a77e147d29d791f9378b1631819a8a66c29fa56ab5326b5b0eb188cb3003ab357a9e95008ad5171dab80f0d5af9b7a9c7dd19af4bb650ab8ac2abd6c4b2c463e5accbf5fdfb7d2b5947cb59b7a3fd20fd056484456698a7dcafc8696cc0c53ae80d868039641b649c7fb54cb700b49f5a9a41b6e2718ea33496cbcbfd6afa0c52bc9e28a9ca64d153702fdb0e4d6c91bacc7d2b26dc726b5cf167f8566f71953401f2371fc4456eb7dce9589a08fdd31f57357f52bc3020822e667e9fec8f5a4f41a4dbb222d52f4daa22c70493b1eab1104a803ae09ae457c796b6975316b294b8e002c01cfbfa5775a4598dbb9c1673d58f535e59f10b4e5d3fc5d7014612755947e3d7f506b4a369bb34692a6a28b979f1275298b2db5bc1021e8482cc3fa1fcab266f196b9326d6bd65cf78d429fd2b076d3b6e45752a715d0ccd0d2f576b2d692fee93ed4a4e251272581faf7af598c5b6b305acd6451ed8b061b3b63b11d8d78b888b702b5345d6af7c3f7ab716ce460fcf113f2b8f422b2ad4554d56e690a9cba1eccf6dbd5e36e98e845727a9e881d8b3c7b93e956ad3e22e8f744b4d1cd6ce4742bb87e6295fc41a75d96115f41cf666c7f3ae6509c7a1a732679cdee8c1ee19624239e7dabb2f0d59a58e9712a7590ef27f9565ebdad69d6b6ef0d9cab3dc49d593903f1ac7f0e5d788649560d3a092ed73f74a9207e3daba1a94a1ae8672ec8f60b26e179ad391f16e79ed58ba6d9ebaf6d9934e8e1940e374c08fd39ae2fc4fe23d7edeee5d3272b6853ef08bab0ec41f4aca31e676466e2d6aced4ea5696d7f04134eab2cac1234ea493d381fce9d3e8cc9a84933af320c91e95e33e74a66f38c8c64ce7793ce7eb5ed9e0cf13db789ac160b9755d4a150aea4ffac1fde1fd69d4a4d2d0ba4d27a9cbebde174bb0648d06fefc57173f855849870547b0af77bbd3983121320d73f73a7b1720a753e9531a9289bc92679be9be1f8e1937ecfbbdcd62ea1febe7cf6735eb274d11e2351827af15e63e30d227d2f5a91981f2663be33dbdc56d467cd2d4ca71d0c0670a0d76be0d4c46e4ff007bad70d2fdce2badf0a6b76b68160b9e227e4483f84fbd6b5e2dc340a325195d9db5c59e6e43c448661dbd6997105c5d5cc6d74c723b0e9815a3185b8b659e07122678643914ab672be1cee258e326bced51d9a3d4e03c6002dd5b9038048ac685d5c7ca47d2babf1b436d1598412a1b85901201c9515e7cf39498f96acc3d4577d05781c55b59e86a019da3d4d765e04b7f375792423854db9f4c9ffeb570905d9ca9746007735e95f0e4c522dc3a3a972dcae79c63ff00af4ea688c19d7789211fd9f05ce3779120cfb03c7f855ad0b4c83cb8e55b86419ddb15b033f4abb736f1dddb496f28dd1c8a5587d6bcf5f4dd76d2cb50b8d375213416733c5e5b290e76f5f6ef5c73a6e4ee8e8c3d5e55667ae19e1854e1c573baaeb56d67133c9328f6cd78e0f156b574db45cbb67d056ae97e1cd4f5cb856bc95fcb279c9e4d672a56f899d499b23507f12ea263453f648f25cf66f6af3cf10d9aa5f7c8576ac632ab9f94f4f4f6af6a9ed6c3c2ba0b1daaa02e09af27f17b249ab3ac4e1d0468030ee00adf0efde76d8e7aaf647316282591e3607eef07deb7ad658ece086ec9c80363ae7d0d665ac5e4234bc92403f8d4779204b516b264b93b881dabadeace3a91e6763d6b44be8e7d2dadd4ab2a8f970782ac38af3dbf792d3569e3dc06d7ed5afe0cbb6fb3794410db7e524760dc8fd6a9ebf6a679a4bc8794de4311ed583494accac2b946e8ded39adadb498ef16281e763cb372c3f0aced4fec776332c4aa7ef068c609359f617a88811db6851c71d4d53d53538941584eecf7aca34e5cc77fb54f433efaee5950c6c09083193e9556d862461fde5ad58ec8cda4c9311977f98d65c236ba9fc2bb21a2b1cb395d93467f7869cbd5e9aa36cc69ca7f78e299990c67971ef50b9c5d37d2a54e25907bd43283f692c0718c534522ddada2ea1e65bf4703729aceb8b1b8b6728ca7eb53c1712da5c89d1b695ef5d3699796dae6d8654f2ee0fdd2470d51294a0eeb635859ab3393b7d3e5b83d302baed0f4910c4647e00ea4d6cc1a308b0be51dc3dab9df12eaefe63e95663688db12b0fe23dc7d3b565cf2aaf951a59475663eb77d14fa966dc0db19c6efef554790c883231eb50068c1f9d486ef8e9526e041d8df9d74a492b183d5dced3c22f6bbcfefe3f35ba216193f857716ecbfde1f9d78b42b8e5b9a9d4f96c8f1e6375fe253cfe7594a95ddee4347b6291d3d2b3afbfe3e0fd05798c5acea50c9bd350b9ddef213fa1abb178b75356ccec93ff00bcb83f98a87498ac7a1da8c2d1723e4358be1ff125b6a6ed6ec3c9b8032109c86fa56d4df70d66d34ecc0e66e38ba93fdda2d01cb52de0c5e1f714b663934fa0167028a9953228a422c403e6ad5247d8f1ed59c53cb988c62ae31ff436fa566ca22d1e45b7b0691f80a49a7da46d7570d712f2ee7f21e959724a479368a7ef1dedf4ae82c42aa8e7a544cde9ab2b9b763188d4579dfc59b1ff004cd3ef80fbca6263f4e47f335e870cc315c0fc5abb5161a75b83f3b48cf8fa0c7f5ad287c6873d51e74891b632e01a945bc65b02406a846b9192715721c804aae07f78d7734603a48bc9247e54d721e2dc7ae287676207381dcd33f8719a04400953f28e69a620e4973ce6ac818e956b47d35b55d5e1b34e37bfcc4761de872b2b94741e06f037f6ecbf6cbc565b1438551c194ff857bb695a45a58db2416b0470c6a30151702a8e8d610d95945042a1638d42a815bb01dab5c139ba8eece84b9508f6800e2bc6be2d5aac1add94c1706484827d7078fe75ed9b81af18f8c7708faf585bab02d1404b0f4c9ffeb55d25ef9351fba79c54904f35acc935bc8d1ca872aea7041a8a92bace63d3740f8b13c0896fae5bfda1071e7c5c381ee3a1fd2bb9b2f11e81ac6059df42ee79f2d8ed7ffbe4e0d7cf40f6a303deb29528b34551a3e8a9e1b512799e6a74c7de15e59f11b56b2d42eedaced244996dc3798e872371ed9fc2b860b8390003eb8a70a50a2a2ee3954bab10b5b063d78f4a2d6111f98a47cb9ef53d6e786a08351bb3a65c2467cdcb46ce76ed23dff5addcac8cccdb5bcbbb17dd6b77345ecad8ab6faf6b3226c9355ba65f4321c54faee81268f3fcb2acf6edf764507afa1ac6238cf6a94a2f51dd915d92ea72e72fc124e7350a4422000ec7ad4931e57a819a7c8a3a83f2fbd5888c1e41c646704559b1d4ee348d422bbb290a49190c3d0fb1aaaa46319e8c3151cd857c71c7268d3611f45e8ba80d5747b5bedbb4cd1862a3b1ef4cd1ed4dbc37c194059eee593ebb8e0fea0d73bf0def4dc784a38dce4c523275edd47f3aec430cf35c9256649e5365a4a58f89af2cdd788a52573dc1e457a9e8f02244a428e95c8eb96ab0f8a63b838559e2c13ea47f915d458dd24561e633e1546493dab9eaeaceea6ef14ce67e256a31c52e9d67237eedd8c920f503a5795cb3b5eddbb49cbcadc2ff0021f956978af593ae6bf35ce4f94bf24793d877aa9a1db335f7dac26560193f91c7f4aeaa71e489cb59a4dc88ee6e63b52d0da282ebc195fae7be3d2b5f46f87dad6af6e978b6ccd1ca776f760b91ea32735a1e0af088d7b551737807d994ee66fef7fb35eff6d04114291a2aaaa80001d315152b35a44ba34a36bb3c5f52d2cf84f4cb799e02a14b0daa4904953d4f6e40f5ae6bc39a943732be997a542cc4ec73fde3dabe88bfd360bd8248658d5e375c1523208af9ff00e20f80aeb40bafb6582b3e9e5b7123ac47b7e15309a97bb2dcbf65c9ac4350f04c91bb18e462b9c819ac4b8f0f369f6f2dcdc64ac63233eb5d5fc39beb8d47ed9637971e6f96a248c3b6580e87af6e95c678abc4179a96a7736c9fbbb68e428225e41c1c64d6b053e6e5b8aead72c681a9dbb59496b7722c4c07cacc7008acb95552770a411bb820d67c5f30a98138e3835d1ca93ba31b6a5c6ff58a7d452676cec7b62950e635dfd4539137b019c027ad21106df9d9bd69ac3a67a75ab93858e3f2d00dc4e33504c8095420605098d19ec1ee5f2bc460f5ab96923c32a39620af42bc7153c48a632b8c1e94b6b07da2658f7aa364825ba70334dbd01b3dd2cacedeface1ba4e43c61c1f5c8af16d7ad560f11ea2318dd70e36fa735eb3f0defbed7e1d6b67219ed242991fdd3c8feb5e69e24119d6af2712969deea5578ca602a83c107df9ae4a2b966d1bd495e08e785ba3b1dca31cd432d926e1b188cd5e236b7e14d71f2575a661765529b38cf4a4049a7b039a509eb40740504f34c95c29c01c9e82aca420aee638515140ab2dc4921fbaa76ad1704547792da78e546292290411d8d7a968baaaeb1a3c77071e68f96503b30af2cbaf9ae8a0390bc66b73c1da9fd87577b576c4571c7d1874a9a91bc6e36b43a8beff8fc34eb21c1a6df0ff4aa96cc7c87eb5cfd092e01c515228f968a9117aee3db26687ff8f6c7a9ab97116e1599aa4a6ded368fbc785fa9acca4aeec665aeeb9d4e59d794076a7d0574b6c0ed02b334ab558e0518e6b660e1ab36eecebe5b22ec476264d791fc44d57ed9e225841ca5b47b40f73c9fe95ea97172b0c0c58e38af07d66e8deeb5773939df2920fb76ae9c2c6f2b99cf6b0b6fb64207ad5f640b80a38158f031420838ad681f7af39c91815d9230688d89cd459c7bd58689b9e3a55698f95df269204297d833c7d2babf87101935892761f75703ea4d719183236589af4cf05599b6812503064e4d635df2c2c694d7bc7ac5a3011819ab425db59569292055e3f7726b891d04d2deac28599b0077f4af9dfc57ad7f6ef896eef95b3116d917fb8381fe3f8d7a3fc47d624d3b4065858892e1bcac8ec0839fd322bc691b8aeac3c7ed331aafa1303c514d0734bdaba4c05a5a4a5a002968c514804a9ed6e5ad2ee1b98f1be270e01e8707a1f6aaf8a09a2c07ab6ad0db6a3a25b5e5b006c6e800e3af92f8ff003d6bcca68cc333c64fdd38aeb7c2baa15d212ca525a192628507047420fe7587e258520d5588c10fc82bc038e2b38692b14624e32a49ec290b66253f9d3ffd61231c73501c08bdf3cd6c8446cd834c924f3253f4a8e56e2a21cbe467d6aec3b1e9df0eac3ccb897cf9a468a02b2470eef90b11f78fa9af505932b96200e79f415e2fe02d4e7835a8e2dfb6de5c860fdfe95ea5aacadfd95318db6b3285ebea79ae3aba322d77631b51d4c6abaa471c7b56d602c7cc3d58e3f971583e24f11b3e9aba7da1db1e332499fbdec3daafea1a4aa1899998c8588110c1edcd729acdbcb36a12dbc284b80142afd39ac61cb295cece571859183bb73e076c0aed92dd74df0e9b34ff005ac9f68ba7fee8fe15fa9cd71b676ccf7d67139c096419fa66bb9b2b59f50b740f9cdcc9e74c3d17f817e801cfe55bd4924ae71d4839c944e97c05749fd8c8910dac3ef0f7af40b4ba3c0635e69a6e9d7fe1ad554a8ff4498853b87caa09ebf9ff003aefd7744aafc107d2b8a5bdd1da9591d1473065e6ab5f59437703c52a2bc6c30ca4641154edaf064026b4964574e28dca5a1f3e78cbc3173e0cd45f50d2a6786de60c8ac84e5320e573f9e2b858e30a091924f249eb5f5078874ab7d574e9ad6740c922e0f1d3debe74d5f499b46d526b2987287e53fde5ec6baf0f56eb95ee635616d51946105f70e0d3d117cd5cf738a93cb279a8262413b4f4ae9dcc372ced255b6f556208a7c2e0b213c0c74a646e1a44957a4cb83ecdeb4f854188c87a8c8a42149124c081c0a6c8332835246300b77a1946ea40347cb211eb4c61b66dc3a1eb56a58c6370ea29846fda7f0a2e0755e10f155af863ed925e2c8d0c880a2a0c92c0d73ba8dc35f5f4f7bb0a47712b488a792031ce3f5aaf34225d3f071919c0a9948290eee02a631f4a85149dca6db562174ccf8edd4d35a3dc78e952bb1cb11fc5522c6d8002f3c5512422d32b9c52342108078abf248d144c1a3504564dd5d9727b50aec632f2511a6376307047ad508a6c77e012d45cc9bce7350c4be636de83bd6a959149684f6c9bb7bbf5209a816468e51221c32b6e07d0d5e60b15abb77c62b3cae147ad35a823d105d0bc86dee57a4881aafd90f93f1ae63c3971e6e9a222798588fc0f3fe35d459f0a2b8e6acec4b34507cbd28a553814545c9366570075eb5ce5f4c2e7555841c88865bea6b66e5b63063d14126b9cd3519e479dbef3b1626b37b1bd15795ce82dbe5000abb19c735460a9e594471139ed58df53aa4739e35d63ec5a73246d89243b4579596fd7bd741e2fd41afb5628394886319ef5cfe187f0d7a7421cb03964f50dec3b55cb6ba75c67b552f9cf6a4e7deb626c6c1bb520e7a9aa93307e41aa63712003d694abaf5a5ca2b176d0192758d464b10057b8e8764b158c29b46554578bf8697ccd76dc37201ce2bddf4c50100f6ae2c53f79237a4b4b9a76ebb0f4aba5c6ceb55d57a6289495435ceb6343cb3e2adc64d9dbe4f2ccff0097ff00aebce1371aeafe205eb5d7889a30ea16140bcfaf5ae4f2e47faee3d857a146368239a7ab275f7a7e78aadbc8fe3cd1bc9ef5a588b16775282315006a7a9a044d9a334dcd34b52b00f269a4f15197c5281e68d80fccdc0a02c75fe1689a1b179e54fbec5933dbb66a9f895446d6a0b176da726ba3b687ecba544ae790807e95c9788a7f32fd7afca8315845de772a3b332d08320e48ce7f9556624c64f3cf4fcea718c83d6a09990cafe59250138cd74a24a721cd3e21fbbff78fe94c7525b03bd5a8e2da8063269b6513dbc8c1d724e1791b78c5753a778baeed6d922bf53756dc8049e57a57396708f2279df198d3007a93c7f2cd46d31088aa4f0327ea7ffad8aca5152d189369dd1eb3a6ebfe1dbbcb47770c47ef0590905401d327ad5bf0f6806e21d4f59d88cf70ae2d17ba87ce09f438c7e75e536462930268c3fa6383f9d76da3e8f7d1a2cf67a91b3079d818b03f866b95d1517a335f6fd19cd5cdabdad9c171b7f7f67284914fd783f8d769e0cb95babb201530b0dd136794e065587a573de28fed0815e59555fcdcabccbc073db23d734df00de5bd8eac26bab85862da76ee3c127b53945b86a4def25289ef90456f7763e55c4419318e4555bb58e240a8c197a0f6aab6fa9c6f6e85245756190ca720fd08aced5999a2696da42b328c800f0dec6b8dbe874ad8b9bf69e0d5eb5bcc6149ae36c35f8f508f2bf24aa70c87a83e95a115f1cf5a5aa1a3af72258ce0d79c7c40f0c1d4ed3ed76d1e6ea004803abafa576965781d0734fd465b5b6b47b9ba9522854659d8f02ae2da69c4bb26accf99991c67cc0411d47a551723767b1aeb3c65a86937baa4936970caa08fde3118563ea0571ca482438e2bd48372576703567a1610b2878b1c1f9d0fbd446f2546e30549ce0d4d1b0d861949da7946fee9aa53290e41ea0f354811a50ddb31e55715a5198dfe6c015cec32956c569c13118e694a24b46b008ea49ebd6a37815487407af229914b97c71cd5824a91dbebd2b3d844c11869ceaaaa632db81dbf303d7ad2fdb26bdb1b6b5924cdbdb163120500827ae4e326a58dc0b0940254e3760f7359c5d91b60fbd9e47a525a80d7da26cb0e17803d4d585942ab3b63771c5537e3f78eddf8150c8e4924d55ae04b773ee6241e2b1e67e2accce48aa331c569145221724b1cd3e06daf9a8c9cd0339e2a8ab685a918ca42ff000af27eb50bf27daa75188c28a8dd4002842353c37294bd7889e245c8fa8fff0059aeeacfdebce34e97c8bfb7909c012007e878af46b43f3573565adc96698ce28a54fba28ac091dae4fe5daed53f348760feb556d22db128028d49fed1ab2c38cac4b93f53fe7f5abd145b5338e9594df43b68ab46e391c44bc9ac7d7356105ab303d01a9afee0a0c2d72577249793cbf30c2290b9e9b8d3a50bbbb1d49591c9cd2fda6e2494e14b1c914df28ff78523a847646c12a71906942923e56af4fa68730bb587f154d1a314ce437d6ab946a159e33c645302c6dd8726320fa8e69af923804502e188f9a304fad05c30c1402908d6f0aaf99adc7ec2bdd7495c44338e95e2de098c36a72498e8302bdaf4cc0857e95c1897799d3497ba6b2f02a86a57420b77624000649ab4d28095c378fb57367a24c11be793e41f8d6718dda4393b6a7956ab791deead7572ce0f992123e9daaa911b0ebfa508a00e82a4fc2bd24ada1cac87620e9934b8f6a987d2971cd17248d50d3c2d3f3ed450171290e69d8a43401130a7d9305bf87774dd9a0815259c1e65e46a39e7a51d067762e5ae955507ca001c76ae5f5d8f1788187252bb6f0d58a9d3ccb329cb61803d80ae6bc5a8a2e6374e9922b08b4a561c13b339b65c01b7b557b823cd601703353b1c0aa8e7e66ebd7d2ba10ac4648c835324a578007355a4e82849003cf4a6d0cbf3168edc00480c738ec6ab79bf3371df14f9ee1648d02b1e3ad550d866fad2481234eca70ae370cf35e91e1fb8b558942dc4ab29e02e41cfe045794c5280473ce6b7ecf517b78f92991d70466b39c6e4c91d3f8aa48f7ec694bfcdb8a8f9547e15c81b8481c2f1b5c723d2a0bbba67662247c7a139ace9ae19c60f5f5a51a638a36e1d52fb4e7325a5dbc2739211b20fd456ed97c43d5ad153ed212e508ce76e08ae17ce691429fbc78cd5a958a9407a28ddfe14dd28bdd15768e8effc551c97cba9589314c7fd6c7fdeaecb43f145bea70a13857ee33debc6d8e493eb5ada6996d13cc8d8890f2076acea61e2e36468aa5b73daeffc4767a0d97da27941623f77103f339af3dd5bc517fe24ba5376db201c242a7e551fd4d71d75a85d5dddb4976ecd274e7b7d2ae5acc36f06953a0a0afd49a951cb4e8695ed8858848a3e56e958b2208f3b9722ba78584d690c688d248c480b9e83d6b2efed763151dab48be8628cb8668ce4340ec075c54171e4b0f94b2b7a30abb8561b810922f51fdeacfba396ad16e521b0c7bbe6ce00ab5e6c718c6e15495771c6702ae436b03004f34d8d8e17ca84106aca6ae08c38247ad0963030e1452fd9635e00a97ca4e85db7d4e0997cbf30027800f155125964bc75c6e9339c7af7a48ed94b8da833f4ab5676eb0dcef51d580cfad4e8b61082de466264396a8e48c81cd6a36031c0e9546639fce926067cd19ed54e588f26b4a561b6b3e690608ad22c68a6c3068070683d4d2aa160c40ced1935669d0b511dd8a64a72e7d053627da315308fcc6033851524112a9dbbbfba735e8fa6c8648e373d59431fc4579e4ec91a1407922bb2f0bdd7da6c2304fcd1fc87fa565595d5c19d386e38345460e4515cc415eda74b8d4eea64c156948523b81c67f4ad867012b97d194c51015bccfb901cfd6b9a6bde3d28ab231b557c4723938ec3eb5cb5f5d0b1b50a9cc8fd33fa9addd5e4fde2276dc4d723acce1ae95148211707eb5d94237b1cd5373395874714a401ca9a6139a515d86648ae49e69de620ea29aaa0f7a5f2a8620f30760698ee5bb62a40b8e6a391c1340cec3c0f090cf2fab6057b269e87c8527d2bcabc0c80451a30e49dd5ebd68a56dfeef6af32b3bd4675c748a2a5ddd088302715e39e3fd5bed9a8456aad948fe66fad7a0f8ab535b18dd9ce00524d7895ddcbde5dcb70ff0079d89adf0f0d798c66cb0181e8734f06a089976003b54d9aeb6603c1a375333499a560250696a204d3831a04499a4269b9a426801723356b4d6c6a71e3b735489a9f4b61fdaabe8a32694b61d8f4988cf0d8b3aa19377cc1f39247a1ae77c4a84e9f1cb8e438cfb641ab9a66a3728ef6eafb949c00c40c63a8c9a6eb60cda4cea106d077f183823dc76ae6da48e8841fb391c61e466abca18336401ed537201c74a4918ed90301c90727ad75ad0e62930ddc546ead1b6d752a7d0d48ff0028ad0bc44b88a22092768ea73449d8b8ab9954b8e322a6ba89216455073b7279a8be4e704fd0d35a8c653b7b631b8e29760232a41f6ef4d20838230680177b63ad275a315621b19a5c12bb53d4f145ec2d02d21dd26f607cb4e4f1fa525c4a5f27fbc7f4ab12a948c4092a95cf4527fc2aa4c851b071c741496a2eb71a065945694526383598870e0d5b8dc13430658b88a398162caac06413552de5f2c7cfc7b1eb57125da7800fd6ab5d0f39cb1e1a92ec25d8d4b2bfb99f114012251c6f619357e7d262306669e69072c14b7193e80702b9db3bb16c464166cf4e95d145797373012122c01c7defe78c544934c4f430a5b630be632d81d89aa533664040cf7c569de3b8c968b8efb5b3598db5a45d87a902ad6c54445080fcd9561d455b89863e420d40551e69379c313d29eb6c01c8623dc1a6c19a11391826a7f7eb54e256418dd91ef56d4f159b20b96580496a8d1c7daa58fa2a1603df06a38a4c3000e39a6950b788a189f333bb8e9c9a5602e5c302eecbce4d5394802ac4cea06013f5aa134bba88a02adc3f159ee4e7ad5b98e6aa38e6b58968655ed361f3c5d20383e49c551ad7d11722e31d4aed07eb9a26ec8a7b194aa59b1827e9569449b768c463b9ef504d1496f3346f95614b07dfc94dff00534fa098d91155be56cfd6b7fc212326a12a027614c91ef5952c6a54bb055e2b6fc241479ee3ef640fc2a2a3f744f63b456f968a8d49db45721041a600540e95a4ea31c9c564696c06de6b5a69309c81ed5cb25a9e8a671fe21b8103330ebdbeb5c69c9624e493c935d1f8959649e42cc72980a07735cf646dc679af4a82b40e49bf78652d2e0d2735b8ae3839152ac8a7af15105a1940ee69124cccbb7ad4180cd81de932718a5438914fbd1b0d1e9be1984412da91d3cb19fad7aa5ac9ba018f4af2ef0f49e6ac440ce0015e8b672116f80706bca9fc4762d8e1be27444692d2a9c7cc01fc4d792448249554f43d6bdafc756bf6cf0fddc606595770fa8e6bc6acc00cd21e8a2bb30f2f719cf5159826e42ca23c8cf7a9c73ed44570920c1fbde94aecf92020fc2ba4c443c5252807b8a5c8a02e2814ee314d068cd488334669296801a6a7d1d01d55c1ce369fd6a16f9413e95268970175b89987cac76e294b6291dde9d6b65292f732cb126ee02479dded9adad46d564d22ee2b04586ca203cd909dcef9ed50d9df5c88e5b5b2b72f2cbb5d19133b78e7e9d4734f7ba9ec34c9acaeec809ee9812f212030cf4c83c106b96c6b3aee5e8797cabb59908e41c5579dbe6ce7b74abfa9e23bf7c0c64f4cf4acc9865b7e7b74c5764754624121abf136f8e31e83159c4e6af69ff3b28f4344d685276445a80c5c91ec2aa55dd578bf71e9c554c538ec0988a39ad3b3c3c3e5ccaae3f84b0ce2a822f39c568443200cd290a4c983daa1d86d70fdc7afd0f6a73416614ca8cf9fee6ec11fe34a1c6312286c746aa772eacd8c01f4a9dc9185ca6652e4ff00773d6a8b3162493926a49652edc741d05455762d21475a950f150d491b6d34c1976d95a460a8a4b1e800cd32420b121b23d69a971b5b81dbb5324955b8dbb47d2a56e4d86c285e6e3f9e2b7567db6a22569508eade7961f974ac6b50449950dcff00b39ad7fb4c8b1040e07afee141fcea640ca770c4a13e606f7c5663365f3e86adde4dc919c9aa58e476aa5b0e28989579189ebeb56a2652a0679aa4980f56970451d0245a0718a9958115514f1d69eaf51624b71a879557b1356a4cdb4d8dc32a303d85672ca558107a1a49e6696e1998e3a0a5660589262c3ef55391a8dfd79a89db8aa48111b9e4d56734f91ea22735a2284adbd1b8b776ee5ffa56256ee9c76d9a7be4feb513d86f633750de6fe42fdcf1f4a4b7539ca8a7ea5bbeda73d0a8c5496c3047cd8a13d04f623ba04f2e0d5cf0e4f2c7aa2c51fdc7fbe0fa0ef556f5b77570d814dd267683548194f56da7e8686af105b1e90ac76d1502b92a3145725882b69b27c8a7b0ad994936fbcf000ce6b9bd3a5608bf356c5cca64b0c01f7bd2b9e6b53d05b1e7bacccd25fb82d919cd67f7ab5a89cdfcdfef555af520ad1472b1c0d3d4af73518a9300f6aa240b81d29a6427d29e235c5308029086528ea28a4a0a47a478526fdc2107a8af48b190795927b57927842e32aa33f74d7a869ae0c7c1cd79b555a4ce98bd08f5b4df673a63ef2115e17247f67b729d496e6bddf5360d03faed35e197b232dd4815785620835b61af768cea90242aebb94d3d44a9c6ec8a884f8ced4c13e94e0f23d75ea73bb93eea6e0537ee8e4f34ddfef4c5625a33516ea5dd40ec49f8d2e6a3dd46ea42b1211b81079cd528a430dc238eaad9ab065d8f8ed55a4c1624743458a47a3e97ab5eadd27d8b72e546e2a3a8c63f2ab725a5edcc4cb71333b7dedacf9c9ae4344be7302c6a497071c1238fc2ba8867fb45b2c909759573905ab06ac6526f64727ae47b648fe42ae010c08c1ac690a1500862d5bbafc8d889a4fbca7693ea2b1b7008d83807be6b686c5a7a14eb474601aec2938e41acf6c1638e953d94ed6f749228ce3b554b5455b41fa9b07d4a623a6ec0aac4734e3fbcb8393c93d69a7963f5a16886b4268532455c4539c5450c478c735a112e3a81532643207dc8959f33601cf535a93c65f27a0159b34396e5a88822a514f2987db9a6559a21452a8dcd8c814da705cd00c9767940171907b8a8dc8278cfe3534321824560c3729c80464547805c753f4a44962d563032f2463eb56a568922252657f600d25bb4883e564ff81462abdf4ccdf293cfb002a3762ea54626493dc9c54b32a85014b11d307b5496283cc2ecec9c10ac3d6a167df2b127f2aa4511a9c1a9d5b8c557c1ce2a45f94e49aa132ca9f5a915b9aaa255ce39a955f26a5a11396e6a39994a170df3960a17f0eb4846ea72fc87b7e54804c6382726a37a919d7a118350bf3c839a6040e466994f6eb4cef545056c59b62d93e9fd6b1eaf5bcac21001e0544f60632f17fd318b3673823353c2995ce5463d6aadcc864b804f618ab503a842481e9cd1d04c82e9428e833ed4ba526fd4e01e8d93514edb8f038a4b59dedae5254fbca7f3a6fe11ad8f41524a8e0d15cfff006add7fcf04fc4515c9c8c8b162cd8ed00135d0609b300f403b561e9d1ee751d3d7dab7d0068b83c62b1a9b9dcb63ce35218d4261fed554ad1d62129a9cc54705aabdb4513dc468e7e490e09ce369af422fdd472cb72bd383d695de9d1248d1db397743820f7fa702aa5dd85c5990278f69c91d6a9491242496ef49e5e7bd379c679c5286c77a630652b4da7139a3692323a521a36fc3373e4de6c2701abd7b4a914c00af715e196929b7b949149e08af5cf0f5ef9d6d19273c66b8f111d6e6d4de963a1d4101b56c0e715e23a9c46df56b95238de47eb5edeec92db804f20138cd791f8badcdbeb721da76c9f30c54e19fbd6155574636500e829a58b70a314d14e07dc576d8e623319cf2690a9152e47ad2d032be0fa5380a97f2a2980cc714d391daa5a28021f25e5f98020fa1a81b20e0f5157048ca78355a504c84e3ad0345ed12edad6f860e370c576b6d7a2485cc61439000380771af3946d922b7a1af42f0d2c1f6569a75531970013cf1dcd6356c95c9946ef4303c4aae515df862464018c573f195653bfb74e2bacf163c3244eb17546e98ae52d890edb4e0edc8aaa6fdd2ad623620b1c0c0a921f94163f4a24439cb1e7de9186d254741c56ac058f80ee7e94e863dee076a43f2a2a0efc9a9adfe552ddcf02a581a36f1a922b7acb4b32a6550e4f0326b06d0e65507a66bbdf0f488d70aff002ec8ba64753f8d633b99b29cbe139a3b7cbb22bf539e6b02ff0046783399377d1315eb8b2dbcabf33a806b98f127d9523f91949f6a85264dcf2c9adfcbb85c8c7354248f63953d8d6f5f85719f4ac9ba019d1f3f7873f5ae98b348b2a52d7477be18fb2f836d35d5999de5b868a44dbc2af407f31fad7374464a5b1a356dc7ed0467bd3a2505b90edeca2a35c9200eb57a30635c35c141e80536c91c1e08e3c9b79b3ea5aa93b1964e1793d053ee1c36009246fad4d65006dd2b305001da4fa8a904492c8905a2c40ee0eb9208c157aa2323352dc4c67959c8009e480315139e69a431bd0d25382eeef53ac0319245306caeb9cf15623c81cd21d8a7ad01c9381b4fe38a62265c93d6a576507f0a8508e372b0f5a495048d9888c2a92c4540818835031c74a421800739a61627ad5a1d809cd2514503b054f03610fb1a829e8582b6d23a7349ab8d8e723ce0ddb35a1690db8f3ee6e0e614180bfde3ed59a8aaeca1df68f5ab5247e6aaa893f76bd013fad4b5d0169a94db0cc4a2e013c0ad4d12188ceef2c6cec98c00335559e38a30abc9ab5642ead94b47b017e4e7144b6b12ddd1d1078c8ff5127fdf228aca177a8e38f2b1f4ff00ebd158f2936658d32e495121620b015d2dbe0c5d7835c86923759eeeeb83fad753632868d78c8ac2aad4ed83d059b44b4ba77f3a1de1bb83822b9fd47c29710b33d8a9953aed240653edeb5d9c670acddeb0f5fd6df488c471906e255f97d107ad2a539dec8534ada9810cd10ba8aee45612a8f994fa8ab1aa5d59dde9d6d3346de73b30dc1bee81db1dfad6549746ec9728011091c773d49a6c4be768d2201f3c6e1c7d3bd75f2f567297206b791762a295c761d7f0a8ee74eb675df16549e715929314219721bdab6ec6ebed59562a8f8c7b35369ad50cc5785a3dc31d2a3ded8c67a56aea4a61da095cb0e4035927ad5c75571a14b9273dfdabacd17c49169b66be76e67038515c8d3867b72694a0a5b8eed6c75f73e3cbd75296d1a440ff00111b8d73d75a95cea0e5ee25691fb16ed54b03f89b14a1941e3269c6118ec896dbdc9152561b8648f619a50ac4e03293e8dc54b1491edcb0da47522a713dab9f9b79f73436c92b18e6404984e07706a26948eaa47b55a965b55ff571907d7355e46595b760fe342b8c030081ca9c1381c8cfe549e7007a91f5a660039e9523ec94642fce7a90d4c341a66c9c6e14f0c3a19547e151ef5894811e5cff0011ed50f719e681d8b8582ae43235466e7a8d80fbd3ef6048254d83e5740ddea069176e028fad085618c771ce315b565ae4b6b6e888a1f68c7cdd31e95886a48fa74a4d27b8d9a177aac97319478d39ee07359818a9c838a91bbd454d24b6121e5cb1cb1cd28f73ef4ced4b9e298c73365ce2ac21e8a3b0aa80f35246e41a4d01af13f970678c935a76ba89b6882213f5158692138cf38ab227031f27159b466d1d0a6b92aae01623d2a9dceab2caac1d7afbd545b88cc7f2f0def54a79d8b1039fa54a885865c4d9ce48aa2d26e5da7b1cd6ed9f85b58d4c86b7b27c30c82ff283f9d4d2782b518b4e92f6568d4239429fc40838355ed20ba9a2835d0eb7c012db6b7e1dbed02ec83c17453e87ff00affcebcd355d367d27529ecae10abc6c40cf71d8d6ce9325ee8d72d716b234736c29b8007834dbeb8b8d5887d4663330270e40dc3f1ace09c66dad994ea27149ee615ba90dbfcb6703d2ad0bb451ff001e649ff689a23124671148420ea08ce3dea7b96bf8d4a6f8ca7f7d540c8ad9bbb20cdda6e27c2a04cf503b0ab77ac208fecaa54af0c0af38e3a5361c5b466e1cab3e4704f5aa6cc59f71ee29ee00a7233449d7f0a4cfa70286aa18da5c9f534e0010c49e7b0a7ac25932290118563da9de51c66a45ca8c1a1a4e300502b9242c51082727b53b746e4ef239f4aae8198f3c53fc9c60e7bd2b202470ac46060544f06795a94e17806901c8ce68110792ded4d28738ab00d0a99058d3b8ee5623142b6dcf1d4539cee936a827d00a6b23236d652ade8462818282586064d4933c4cd9890a8f734b181165997208e083d0d42cdba80ea3a350d2286ee715b0a7d3a5655b2e67527b735a60d4484c9b71a2981b8a2a6c2134890a47f29e403c7ad74369701082c028233c74ae3adaf3eceb8dbdfa8ad2b7d41d87d9e73847fb8e2b3a94db674c256476a976ae8029c9af3dd6e69a7d5ee1a7cee0db40f403a56cdbcd3e9d322cac5e37e51ffa533c41602e51750b7196231201fcea6925090e7ef2316da78d6260ec43636a803d6ad47f698e35922b73e5e319cf5aa30431ce0aefdafdb3dea4125c5a031b64c7e99e3f0ae968e768af2852c4a7193c8f4a4491e36050e08e68660ddb14caa18f925799b748c58d30f268a2800a28a7c486595107f11028188aacec1541663c000649ae974bf03eab7e0492aadac27bc9f78fe15d1f87b48b3b3656f2d5a4233bcf26bb98103a8e7e95cb5310f689a429aea78f7883c353e86e92090cb0b71bf18c1f4358c91a11f7f6fe19af6bf1169715ee933c6fd19719f43d8d78c4d00b79de19832b21c1c56946a392d7733a91b3d088c38276c80fd4534a303f7b34f6f2474726a32c9dab6440bd680a47229bbc7a5396400d3023209a021f4352f9ca070293cc76e870280bb2f5d8dd61652824ee0e1f3cfcc1bf9608fd6b3d98018dbf8d6a4df368d6cc586509418ea7927ff66ac97eb4a20858e369a548d0659c851f535eaba6f807484b28bed4924d3edcb9121033f415e7fe16b75b9f11d9ab9f955f79fc2bda2d6ea1fb4184b00f8c807d2b9b11369d91b4237d4f3ff10f84f4fb6b777b489e36552412e4d79ff7af6bf1220f25891f29af19b98fcab9923feeb114f0f36f4629aec2205652bc06ed4c2a54e0820fbd18cd2ef6efcfd6ba081b4a1b69a29c24208215723da8131e970c8d91d6adc53dcce0aa40f2ff00baa4e29b15edd45b76346809ea235cff002a596eaee5500dcb938c9038149924915a5e35c462e20b986dcb01249e4b1dab9e4d75b36b9e18d12196db4bb4fb63ba15f3e4193c8c13cd715387decab33b2f6c9ad8d0f4589546a1a9a95b71cc6847facffeb565512b5e4cd69df6475da1ea3e28d68c4b6a915bdbc6813cf90e0103b81deaecd21b28753d3eeaed6e66f2bcd5900c03cf22b134bbc9e3d343cd7ab6b62a4e117efb8fe8292ef521a9c7bf4db20b0401b7cc460118e727bd723579791d0fe02b858a5b712c473b8fddcd675ded8e3dc838dd965f4f5aa71de4d685e3db957e463a0a77997328c24606fe3e63d6ba9459c3629dc46031657cee192bea7d8f6a8123470776e551c932138fa62ac9115bafef9c3956fbbe9f4aa53dd34dc0185cf4ad51486cf319b90004000c0150e7a7d29738523d690f6fa550c0738f7a57186c1a727df8f3fdefeb4b30c4d263a06a02c46b8dc33d3bd5e132c780082b5428a01ab9a0eb1cabb96ab32153c5448ec8720d4e24120f43409ab024846723a546f2127152a825b914d9368180066801002064d3c711e47ad21395a7e32157f1a0069c81cd588e0699a2810e1a520027b557908dc8a3ea6af584e9fda31b647eed491ee6a64ecae38abbb1da695a2d869712b2c6b2cd8e6561927e9e959de24d3adb5126645d97206370e87eb5241a8f930334b26646e48cf4f6acfbad503316ddd077ae18f3f3731d6f96d63949a29206d920c62a3abd7971e6e72396f5aa91a6e393d2bbd376d4e792b13daa95058f7ab80f155d4e2a40d52ccd92e68a6834521197576c9fcc06d9c021bee9eea68a2ae5b1a44d1b5b9f3ad65b49c16da7823b11dc5409a9dc5accb1b90ea78e28a2b34936d1a3641a8c3133a4b0aed2e7041f5a8858bf9664924014760334514e2f4227b950f5a4a28ad44828a28a002a6b53b6ea23e8e28a293d80f52d3bfd5c6d8e41ae9ade52224228a2bcc91d088f5b91d3489883c95af25f13422df5d991806242b679f4a28adf0ff119d6d8c36c67a530d1457618098a5a28a63418a28a28066b4281fc373be3886603391d5bf0f6f5ac934514902dce87c129bfc4b0fb231adbbed4a7b7f1f2448720954e4fe34515cf515e7f2368ec6ff89b5411ac313c64963dabcaaee513ddcb2018dc7a51451410a632319dd9ec29a4631ef4515d0622b26285038fc68a280253c4607bd48060514502100c915bd35ddf2aa4524c92adbdaa3c7bd7eee515b8c7d68a2b3924c69b4f418b6f6f6c64b9d44c974637c08470878ce4fe62a0bef15dd5d810c48905b018f2d171c514544629cb5364df214fed9e5468eab96460413ce463a1fcaab4b7b2c8ec41da09cf1da8a2b648c5158927a927eb4a7a0a28a650947a5145004878f24fb7fecc6890fef1ff00dfff001a28a404ba8c421d4678c740ff00cf9aad8a28a23b0d8628e68a2988b303ef383d4524fc0a28a16e4f51a8fc018a9d5782de828a28622a162cc5bd688d8236ecb023a114514d96cbaba91650b709bc75cae01fc6acccad2c6be43058f6e70d45158b493358bba3209cb935221c1c51456a6522615225145492c968a28a911fffd9	image/jpeg	2020-03-23 16:17:50.287192	{"width":"500","height":"278"}
758e1d13a621e4ab97c284d404021facc4617d5af0815eb1d7bd948987b8dff5	of-monsters-and-men.jfif	40392	\\xffd8ffe000104a46494600010200000100010000ffdb004300080606070605080707070909080a0c140d0c0b0b0c1912130f141d1a1f1e1d1a1c1c20242e2720222c231c1c2837292c30313434341f27393d38323c2e333432ffdb0043010909090c0b0c180d0d1832211c213232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232ffc0001108013101f403012200021101031101ffc4001f0000010501010101010100000000000000000102030405060708090a0bffc400b5100002010303020403050504040000017d01020300041105122131410613516107227114328191a1082342b1c11552d1f02433627282090a161718191a25262728292a3435363738393a434445464748494a535455565758595a636465666768696a737475767778797a838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae1e2e3e4e5e6e7e8e9eaf1f2f3f4f5f6f7f8f9faffc4001f0100030101010101010101010000000000000102030405060708090a0bffc400b51100020102040403040705040400010277000102031104052131061241510761711322328108144291a1b1c109233352f0156272d10a162434e125f11718191a262728292a35363738393a434445464748494a535455565758595a636465666768696a737475767778797a82838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae2e3e4e5e6e7e8e9eaf2f3f4f5f6f7f8f9faffda000c03010002110311003f00f6274dcfc0e29d8080e3ad73dae78efc3da03bc7737c249c7fcb183e76cfe1c0fc6b80d53e37a82cba7e940ffb53bff41fe35c6a0dec7539a47ae61739a9401d8d7ce57bf15fc5ba8122dee23b65f4821191f89cd73977e35f14b4b89f5ad4013d8cc40fcba55aa5d04ea697b1f56349b298aca4f273cd7c90de27d65cee96feea53fed4ac7fad35bc49ac6dc45777299feec841a3d97991edd1f5e395fbbd29a1463ad7cb9e1cd4fc7da9def93a3ea5a83b28e4b4c4a28f72dc576b77e30f899e118165d5ed6d6f6dba1936838fa94c62a1a8a7cb7572e352eae91eddb71cee1400727278af23d1be3c6973bac5ace9b35a31e0cb11f3147d475fe75e9ba2f88747d7a01369b7f0dc8233856f987d475143835b94a699a3b70b9dd4ddd8c64d3e550173516cf9413d2a596921819b7920f14fcfce01231ee29eb1a8f6cfa5395015e7a8ef4acc6d94354d423d334d9eea66c246a5bf4af17bc7fed6d196edd065e7cc3bba3b13c9fa7bd7a7f8fad6e6ebc332a5b10afb86e2471b4f07f9d797785648e5b594df032ada931267ee0070463f1cd4c96ba92df4203ac6a70e8f25bcf684aa92679063690780381cf3f5aaba26a1a75addc72496c92c9d230c9900fb71c547ae5beb3650ccb1165841c12718c1e9f5156f48f006b1aac16f3dd3a2c6e03024f201f4153eea5a8e319c9e865f8c2fa0bbba6da90b4bd59a362e41f7247f235cee95a74faaea11c1293b9986d71d715ec67e18d9ada819f364fef31c73f4ad1d17c0761a6bf98e01917a107bd3f6bcaac8d161db7767330436de0fbbb19993cb5de048dc9cfbe6bd634dbb4bd81ae2243e4bfdc27bfbd709e308ade4b02f245e6c10caad2803276e79c7e75df69d3c171611fd9d4ac014041b769c7d2b3a7aeaca9c791b8ad8b80903d8fbd3f700b8c67dea3f9f1c0cf7a115997078e7bd6d731b13f2d183d3da90ede08e83a5288db1c7029768c1e39ef5648e555ea690901b68e9de80b91c820534e177714807144653bb9a4e4e39c9ed8a76dce3146c19ce06453011063039ce2be5df8a97ef71f10354dc70a9284fc154281fa57d4db40c13f8d7cabf16ad1ed7e226a885085925122fa10ca0ff00535a535ef19d4d8c0b195ee6f22b7855dd1460aa9c16039c7b64f26bd77c2de11bbd46c567ba93ecb14ca33185e5871fe007d00ac2f05f85e38a2b3545633dc8135c3919d91f61f893fa57b5431aa44aa980a06056156ade5cb13a6861d5b9a47233fc3ed0602d28819e63fc6ce6bce7c6905bda4460b7d3ae10a36f91981c1f4e7bd7b7dd2075c0e4fb571de2db2fb4e972c253e72b84e39cd732a8d54d4ea9d28ba7a6878f6848da8eb90cd78c10b1507e990062beaab18162b082303855007b57cbfa0dc27fc251656cd182c93a2608e7af35f562280a3b715d9bbbb3ce8ab2b1584397391c1a56200db9e0fbd5961f2f1d6a311af52031fa52b1572108a0e01fc69e4704fa74a71551d001f8521504a8e7d68b00c27231d3de955547b9c7e152151d381515dcd6f676b25cdd4c91431aee777380053b00d61b9f767007415c178c7e29e91e18f32ced317fa974f2d1be443fed1fe82b84f1dfc5ab9d55e5d2fc38cf059e4abdd74797fddf415caf873421249f6abf8a510bf0b2153866ef8343b415e45c69ca7b6c53d7754f10f8c2f527d51e59067f7512a90899eca2baff0008fc25bed5196e6fe45b2b71ce1865cfe1dbf1aebed67d0625b7b4d3e0d9308f797c71c7bfad752d6934162b72f2aa44cbbcb16c051ee6b09621b972a477430d4d5352bd9b21b5f05e8fa1e24b2456755c191ce589fad4ff0063b820b88c841cd66daead797e7cad16c1af0e706ee6252dd7e87ab7e157c784aeb5225bc41ab4d7499f96d6dc986003dc0396fc4d2f67cdab25e25d2f722ee52d535bd22d9d208ae8dd5f639b6b4432b67df1c0fc4d70b71f0efc41e24d46e2f6e2d7ecf0cd2975fb4b856c1f6193d2bd7b4fd3ed3448d6da0b6860b7e88f1a05fc1bdfdeb51a411a1676508392c4e05691824ee8e79d79ce3cb23c9b43f86d79a45da4c9ab47617214a811c45cb0f62c707f2aeae1f07dd4ac66bbf12eaf31ce1563904400fc055dd5bc61e18b4431df6ab6871d555f71fd39ae625f8bbe1fb18b65b4d717c57ee8f2b6e7fe044ff4a767733736d6a744fe0bb43cbea5ac10077bf93fc6abaf81ec09f92ff5853db17afc7eb5867e35e8cd18034dbc2ddf2cbfe3483e34694064e99738f675a18948dbb8f034de4b0b4f126ad1391c79b3171f4ae3ef3e0f5c79004535a4cffed02a7f3ad84f8d9e1c6c896daf91c741b54e7f5ad9b7f885a2ea6a8b6baa5adb3b1e4ce08da3f10067f1a7648399ee79b69ba56b7f0f2f659aef46926b69808f7c32023af1d33fcaba4d3fe216832ce22bb9a5b193a66643807dc8e9f8d7a2da43617120bc8a48eea523026dc1cfe1e9f8543aaf85f44d7119751d36de6c8fbfb70c3e8c39a5c89ea6d1c5548ae5456b5d552ee31f66bb82ee02322489c1acdd57c03a5eba924c6248a56fe38f835cd6a9f092e6c25377e14d4e6825073e4c9215fc987f5aceb7f1df897c29722cfc45632119c79a176b1f7f46a5c89170c43fb3a32ade7c1cd656e585af932c5d998e0d15deda78facaf2d9668356b58d0f1b64708c0fb8268a578f99d0bda3fe53e739ee59c903273d4f735a9a5f86b51d4e191aded2598e32a51735e8fa3fc36d3f4d612df9fb5ca790bd231f877aed16ed2c6d52da111dbc6a30028038fa576d4e6b2b1f274f30a4e4d34f43cf7c0fa3436105ea6a9a5f99380517cc206d6ae6255b0b6d62e1b51b747b220fc8dd632480369af4ed4e05b912496d72f15cb8c6f03e563ee3fc2b9f8be1c0bdf2e4b87927995b792aa76b7a715c2a5252f79fdc7beaa53a94572c5a7e6683fc37d0a3b186f23827659a35930cfd3233e94fb3f04787644dcd624e3d646ae960d3f5c1691db4c0ceabf2866c29c7bf3daa4b6d1356b79dd76c2d132e412dc83e95d30aaada9f3d8acbebceb7353765ea52d1749d33446786ca011c6e43152c4e4d55f16dcc034ab9576408c841527e5ad4bbd0f539a3d851770fbaf1be08af24f19f87fc6bb645b9b4964b5c9f9a1f9f8f7c73fa5703c2b957e7e6d0f4e84e71a3c938d9905bfc3f97c45a326b3a5cb6d224ccea6224ab2b0ed9ef5cf5d7877c41e19b91388eead2443959a2271ff7d0ae9be1debf75a44173a6dc190c0a77a263ee377aee5fc4c447b6681f046082bc1f6af4a3a6973ccc4e22ad39de0b438ff0ff00c69d7f4a68e1d6a15d46d8705f1b6503ebd0fe35ecbe1ff1ae83e25b41358de296fe289fe574fa8af1df125b6917f6de6c7a7b4574edb53c918de4fb57257ba2eb5e136b6d4555ed5d8e54a9e7f11dc53942e74e1f1ca56523eaf2770f97923d69c40008ce0e2bcb3c09f15e0d60258eb3b60bde0093f864f7af5056575ca90c8c320e7b5616b68cf49352d5115f5a0bdb296d8b145954aeec671ef5e3967a55c687a95eaea0e04123908aa40e0120923d318af67e438dc401db9ae23c5be1e7ba4b892d22433dc30cc84721718fc6b3a8f435a74d4d9e37e20f15df6a7732595b69d9b48e5ca67393cff515d2bfc4cf10595bd9c42cb4cb64946d561993681d7201e31e95dd7876c60b2d3c5bc91c6d93ce56b13e23782e1d57c3ad3e990225ec0fe66178deb8c11fcbf2a29d583928b454e8548c1b4f53819fe32f8956e1c23da3a03856f248c8fa66b40fc45f19de786e6d5a1b0816d2290472dc853804f6c66b8cd33c0bafea378b0a69b3a82705d970a3f1af7d5f0f68ba5fc3c97c373dd451a7d9cf9aecc33e61192df9d744fd92e873c1566b567805e78e7c43a8068e5d46411c80ab2200a083f4af6cf82adabded86a377a8ea53dcc08c20863918b0040c9233f502bc89fe1ceb313dbcf88a4b19d86cb989b72904f071d6be9af0c7876d7c35a15be9768c58460b3b91cbb1e4934e5285ad133519f37bc6c15db8209e3b51bbaf19a5287f0cd30332b608e3bd646a4a5b0beb414c818383de931b89391f4a0a9207cf8c7a531084f38e6829b8e0d26c393f3669c00e769c5218ac080314fc518cae297a6298846e457947c54f08adedddb6b2aa1819a28a618e83900fea057ab95e7393d7a567eb1a6aea9a7496c4f52194fa10723f9526c343c0f47d5bc49a4976b116ed132290b3bfde0001b41fe95d9f84bc47aff88a79e1b9d323b61129225048563e95a1e12d3a28edaeede78d4ecb972a0f619e2b76dde08f52fb389234651b8267048f5ae7724f4b1dd0838abdcf33d435bf1e4b79710c6b6b6514470a64c00e338e09ab5e1bb2d7351bc96f754bc596d62c850bd18fa8f6aefed65b3bb9a784bc5308db04020edfad56d66e62b4b574880191daa652f76d643f66af76cf32f0df85e57f890b208f762e04a5bb28c93fcb15f433018ae0bc01a54c6fef3549e2021915440e4f5393bb8fcabbf23039ae98deda9c126afa0cc9f4cd3483f4a79cf5fd29b8dc7ad5086f6ef400776739a5c1ebc669934d15adbc93cf22c7146a59d9b80a075340ee56d5352b2d234f9afefa6586de15dceec6be6ef1bf8eb53f1d5f9b5b65922d295f1140bd643d8b7a9f6ab3e3bf18de78f75c161a7ef5d2a17db1274f30ff007cff004ae9fc35a05af861124b8d92dde06182ee099e847bfbd4d4a8a9af33ab0d869567a197e14f87210473eb0983c15b71d7fe05fe15d8eb5a66561b1954c36db3e4da3ee9f502b4bfb459cc7e4a0c8eb291d6a1bcb7bff1514b4b30d1aa480cb7678118ee067a9f6ae293755d93d4f53996192e65a1c3db7862587585b7b4ba92fe72772c31a6d38f5739f956bd521f0c49a808a4f114cb75b31b2ce3cac11e3d47f19f73f956b691a258e8b6be4da4782797918e5e43eac7bd5c9a58a089a699d638906e676380a3d49aeba70715aee7935ab7b4d12b2402058d563450b18e02a8c002b2b5df11e8fe1cb6f3f53bc8e0047caa4e59be8bd4d79af8cfe31ed7974ff0c812367635eb8ca8ff007077fa9af3bb6d1357f11dff009f335c5ddc39cb4ae49fd4d5c9a8abc8e6bdf44777aefc66b8badf0683a70543c79f74324fd17fc6bcfb5cd6fc41ab43baff0053b89b3c2c41b6a8fc057a069bf0e4aaa35e4a030fe14ff1ad6bdf075845a74de5dbee902f047defc2b91e3a9a9591a7b19b576790e87e13d4efade5ba16ed346837377c575369e0db899632906091d0f15258585c29b8d3b4eb93b669b7f2a4491e3b139e83e95e9125b3c30a3646401c8a58bc4f2dacf7268d2bdee7131fc37d51c02b6eb83eac29eff0d7520bb9ad73ec1857ac692c5ad5771cf15a2d800d630ad3947999b3a714ec7cdb2782a57f118d2ee44564e46edf70d818f5a8a3f0f886eee2d219d25684ff00ac88fcac0d741e3ab3d72fbc4f35dc1079d0a4b88b69c3263b1f6ef5e89e01f0fdada78687da6d94dccec5a60e036083d07b574bab682d4a74ecb63c8ad9353d224f3ad6e268981ce6272a7f4ae8ec3e286bd6476de309d00232e801fa923ad7a6df783749bd04a44607f543c7e55c96adf0f6e5033db85b84ff006786fca92abdccb95ad8eabc35e3cd1b5d8a28bed4b15d90018e5f972dedd8d6fea3a6596af6ad6b7f6d1dc42e3eeb8cfe5e95f3aeabe1c9ada53b11e271c93d315b5e1af883aef86e64b7d411af2cf81873f328f635ba9a6883a5d4be09473debc9a6ea82dad8f48a58cb953f50471457a0e95e30d0b53b04ba8efe088375499c2329f420d157688ae789dafc5357d32cec5d4c72c312c6f3b1e6461df3d8576de1ed0aefc416b06a297b0a58499cbae59e4c1c1c7a0ebcd787f8ef4283c2fe269f47b69de758513748fc1662a0938ed5ef9e06d674cf0efc31d15f52bd86dc180b8563f336589e17a9ad6a415ae614e0949bb1d55878734bd3e533456e1e62306494ee3f867a7e15afb540c00315e63a97c67d32dcb25858cd704746918203f8726b94bef8c1e29b8626d2cada14edfbb2c7f326b15ca8e8d4f730b96e29dc2b7a9af9e47c4cf1c336efb4c283da05ff000abd6ff147c616ee1a536730f4921c67f10453ba11eecdcb8c8a1b935e4d63f19ae5485d4b431c1fbf6f2f1f91ff001ae9b4bf897a16a973e5bcc6d4301b44e369cf7f6a5cc877bec6fdef8674ad464f3a7b48d6703899142bfe7dff001ae7effc38f6bbcbc42e2d4f3bd57e65fa8ff0aed2de782e23dd0ccb2263aa9cd4caa076a1abec4b4b668f12f145bfd80d8ea1667090be1980dd807bd711e35d72e35c9de36b90c9090a9f3f518e4fd2bda7e21da69fa568736ac54c4124559020f948638c915e493436b79616c86c6dde52d98ee2203122faf4fd2ae9ce715691e7d7c35384d548232b4cd0bfb734417361fbbbfb5003c40e378eccbefc1aeffe1f7c449ed651a36b1bb721daacfd7ffd7ed55bc3da2bdbddcd7ca1914c6142e782d9e68f1578592f6dc5fc52476f7e8be62bee0030f7f43437cc654f112855b5b43daad6e20be844f6d22c8878c835cd78dbc5365e1ad264924993ed9b4b430e7e663f4f4af2bb2f1dea9a3686f044c915d483f792f50b8fe21db24571d2dc5ceb133dcdecf24b2dcb177918ee21050a17dcf49d7b6b13d57c1de24bcf12432dfde18f6349e5af96b80a457751498501b9af27f85d24371a56ab650878d04c1e2dc72ca08e0e7f0aebecbc4d14370d617ec21b98f8f98e037b835c7561cb51a47a542ade0b999da24916d25caaa01c92715c078874df055c24c0b3f9acc5a492dd99d98fa77c8ae87cdfed05f2c46b2c6dd8f20d55bdd2bc4262db6ba958d9c38c2a0b7248fd7144277dcd6518bdce4eef5fb3d1bc1d6ef60e674b63b916652a720e4023d338adff000dfc70d0f56558b54825d3ee31f3155f3233f88e47e55e3de37beba82e1f4c9efbed5221f9dc74ae674fb5b8b80c6297cb03af3d6bb69535c977d4f2abd56e7a743ec8d375fd2f5a5dfa76a36f74bd7f74e091f51d455e0e06437e75f27e83f68d267fb55adcba5ca9dde621c106bd93c39f152daed62b5d6e3fb3498da6e539463ea47f0d2946db131a89ee7a582acd8539c7a548cdb48c8e0d54b568dc2cb1c81e37c1565390df8d5d201e7ae3d6a51a887afbd379dc38c53c7506958ac6acec40551924fa5315c4da4738c0aab77a9d8d8a17bcbcb781475f3240bfcebe79f18f8cb56f10eaf7222bf9a3b10c7c98a372abb738078ea6b965b579db32c8f2b7abb126ab94c1d63e88d43e28f8574fdc1750fb538fe1b75dd9fc7a571fabfc745b7463a7e865c29fbd3ce07e801af2e6b348906568b5b3b7b9d4ec2daececb79ee2357e7a29619aa8c7533f68d9eaba4f8a09d326d66f6dfeccd347f68f201f5f4fad66eb6afe2bf26e591b4d6db88eea49151b1e806e048ad3f89da61b7b159ada30a91ed0a00e00e98fd056049e3bd2afb448d268505d22ed78dc6738f435cd529b84ae91e851aca71e597434747bb1e12d3e48e3b09a78998196e51d640c7b9383c55af13ea71c7080926e7700819f5ac3f1178e74c4f0e1b3b1443733a61b60f95734cf05f87b50f175fc7757a244d362c1791b8f371fc23daa614dcddda2aad64a3ca8e8f41f1aaf826db4db5d75a436ba8879632064c0b90178ee0f26bd52c752b4d4ed12eac6e63b8b77195911b20d7cf1f17f58d2f59f125869da6c8b2359218e4923fba0f1851f4c7eb5976173a9688b14da7df4b6b3a73fbb6383f51d0d754a1638154b33ea207073c9a46c81fcebcc3c2df16edae163b5f1127d96e3a7da546627f73fddaf4b8a786ea259609525898655d181047b1a8b1b464a5b0f0381c9af14f8c1e367bc9ff00e115d2a525411f6c78cfde3da3ff001aeffe2178b57c27e1996788837b3e62b6527f88ff0017e1d6bc1fc25696d7bad4571aadd8479a42caeed8cb75dcc7eb46cae7450a4aa4bded8eff00c01e0a5b1b55babb406e1973b31f707f8d7477f0e64f2957681e83b557d03c416eaf35a41709711ab6d0ea73cd6a1b5b8ba77f2c857da497233b47ae2bcd9f34e567b9ecc5470edcfecf43cffc63e311e15856ced48935071c0278894f73ef5e91f0f7c61a578ab428fec2120b985409ed73ca1f5f707d6b1bc6ff000becbc57a2437164a906ad1440ac98c09b8e8dfe35e41e18d2b5af0b6b8d75bdecaead73e697e1514750deb9f4ef5e953a74e14fccf0f1589a95aaf33d8fa7b54d4ecf47d3e5bdbf9d61b78865998fe83d4d7cefe38f1fdff8ae47890bda69087e5841e65f42dfe1d29fafebdacfc42d6a2882b0b753886dd3ee8f563eff00cab6759f055a69be08ba56025be72844807dde7ee8aca55e11928df564469ca7b181e18d22d23b9b0975ab2920b4bd3b6de40c08639e377702bda2dac61b3816182158900f942ae05784f83343bcbff13d8c37af3fd8e0932bb89c71ce07a735ef115f477da9be9d6f3249771c7e63a6eff56b9c64fa5716362a72e585dbf23ae8a50576498080927007249aa8f722e5592d2de6ba278cc4bf2e7fde3c56f43a1c2066edbed0e79c30c20fc3fc6b41176461005000e028e00a8a597f598a588e9138b5d1bc4370fc8b1b28883b9b26471f8702adff00c22b25d44a2e759bc607b44a91ff00427f5ae9dc718a8f7a438decaa00ee6bb6387a69ec62e6dadcc387c1b6f1a606a9ab01ed76c3f9535fc20bbbf77adeb09f4ba27f9d6bcfafe8f6f959f55b188fa3dc2aff003355c78abc3bbb68d734e24ffd3d27f8d6bece2ba11cfe672d7de03d4cc8f259f882505beffda6257cfe23151fda7c6de1e8951f48b4d52d906375ab157fc8ff008577706a167798fb3ddc1283ff003ce40dfcaac15da2a5d083e857b59356b9c269ff0012b45b993ecf7eb3e99720e0c7748401f8ff008d75d6f710dc44b3432a4b1b0cab23020fe3516aba0695adc463d46ca19b8fbc47cc3e87ad78a78d1ae3e17ebd6f1e89a95ca5bdca1944521dca3071823bd43a17f8414edb9df7c40d46d557eccd147e6fcb82579009009cfa0cd73baa68ba7cf69149601dc85c4831907e95823c5da678eae601aacbfd99a82c7e5a4c87313f3d083d33ef5ea7e17d2e1b385954f99b40c39e73f4ae6ad39426a3d59b4629c6fd0f25fb0984944e141e84668af64b9f08e977b3b4ef0b2337508d807f0a2af9e5d8cecbb9f3ff008ca6b4f10f8e2ff51d3e37ba8e561e57c8790140ce3ad651b3bd9240938910a8c057c8207a7b57ac7816cf4eb4d2a392651e64f9691d464e73d28d72f74db6d4609668a3910310564507e5c1cd692c4394ac762c0a54b9ef6d2e711a1e82f71208e2b7795b233b1738f735e8f1787ac6d7467fb44799718500719aec3c3977677fa1412d9c2914478c226d07deb3bc40963651e27bd8618dcf4924008ae6c4d4a916a50d57539e9460d5a4799683a45aea3adbd95dced6c6497cb8980c890e09ae9b56f035bda3a113ee507e6f931c53ec9fc3d1de477105e7da6e2224a326e7da4f53c0eb5d8caf06a7641e3dcc59739f2d867f314beb0e5b2652a696ecf37f1368fa5c5a384d3a365bc0bbcb30c0c7a73d7f0ae6b43d0aeb5332ab5b96f2555f7a8ea0e7191f81aef750d19357c40f70d0491370e9d48f420d6b68ba543a49db1317790e6473df03007b0aba98a518e9b992a0f9aef63cfbced4fc3ceaf1c931453feafcc65c7d315e8de1bf88b6ba9c691dc0612000383f793dc8ee3dc551bb116a9349f68789634ced56ef5c1eb1a70875884e9a76cec199153a923d3f3e95e82c3d58d255247894f38a35313f5749dba3f43d1be2be2f3e19ea2f091221f2d814e78de2be7ef0c6af3695a81b79583dab02e51fa0239c8f435ea6353bab9d1a7d3ccbe5b381e7444707041c8f435c2eb9a358ea97138b384db5d21c0cf01ff0ed554ea4671e567a35138b525b1dccbe2d806931c96e8267652632830a07bff8571d2ea17372d39bb999dbccefc01c761599a5da6a5a35ac7f6e8e486299dbcacf3b80e0903d33dfeb4f964dd30620e198e33d6ae3051564734a29cf9ac52bf0d7976158910a8f9b07a9eb815a16f1c9673c72c7bb63e6de3c2f56233fa1c550f337ea8f807ca5236823df9cd6cc7f6dbdb0b65b201a5b6ba7b9540d827013a7e75a2dc6743e19827d12eed6e2484436f347e5ca4f009ed8c9e71f41d6b73c5be1f8f56b7fb44240b851c7fb42b1f51bab1d5f49cb48d6de52ed7e479a083f37f2fd69342f14c474a02fe728b1e511e5182e0743f5c62b9abd277e789d587aab95c2470f36bbaf785ae716b7b2c583831bf23f23515ffc4af13ea11ec92fca29183e5a85a4f186a369aa6a2d34779e62afcaaaabc62b12e62862b4b6dacbba45cb6d39c0cf7f7ade9d38daed6a672ab24ec9e85092579a42f2316763924f24d741a441b22dac3874ce7df39c7e42b26de049ae4346a762f241ef8ae8acd5238a312c9b541059c8e00cf35537d119166c159073bb3d4904802a7de1c4bb5f700bb8b11800fa7bd56b79d24b7dd11222507271d6b9fbfd5e49621690fc912f5c75635318dc563bff000e7c4ebaf09cc21965fb6597f15b939c7fba7b1f6e95d94dfb4268cafb61d1af5d07767553f957cf7e59e09e33dda8daa0e0b8fc2afd9c4b5268fa7745f8dde15d565115c35c69f23719b84caffdf433fad5ef897e29874ff0448d6572923ea23ca85e370415fe2208f6e3f1af94f8cf0d57edb539d225b79267780642a1390b9f4f4a99535d07ceed6674b631ef271ce303f415ab05a279ebe69c673d3af4aa7a291286db8da0f7ad0b97f29954927193fe7f3a96ac6242e87e55791157ef2b1e73f5acb92c5ef75db1b269c2b5c4caa64fee8cf5ad3cbfc8118f03238aa17459756824b762b289014623907ad1b023d6352f1858ddda8d20a3dd4cc3ca0e62277638de47a719af2fd6348b63711bdbc62dd1cb24866200575382063248af47b482d7c4119bd05eda39a00336ee50a3a9219411d89ed55ef3e1bde4d14f78b74252ea4c76d367728f73dcd2bdf735d774705e1cf0eda5e6a51a3092772db553ca2173f8fd6bd1fc71e261a7684fa0e90f24326d1134b075de47dd07b018e48fa567f8760d574ab896c12d91276082d91a512f945b39c1f4c0ce2b17c6f689a35dc1662632dd471169cff0877feb8fe7549d993a9c1e89a7c926a6d805ca9e4f5cfad74b79310ee4e3038c62a3d1e18aca0339fbc57a0aaf2959df8c8c64919a2f722e5566cbe73cd6de81e2ad5fc3d7024b0b96119fbf0b731b8f71fd4562931895c8ddb4122acd9422624a06049c827a7145ae24da66bf88b5997c6de28b3b9d4a68ed6ce2409e5e4e107563f5269bace9771f686b9d32346b52bb238c0c82b9ce411ef583a84f14729533281eb9e4d75be070f7ba45c22399fc87ca853920115cd5f9a2b9d1eee555e151fb0a9a27d4d2f00e9d7506a8f73756ea9e62011c28392c0707f9d7b059591b3d2666941f3dd0b49ce7071d3f0ac2f07e8732cbf6fbb8f604cac484f3f53f85760e8248d909c2b820d674937efc89c6ce3cfc907a22259e2b7d3d2594858d230493f4af07f16ebb71e35f121b3d3e3ff4557da020e6423b9f615aff00137c74f65a443a240c44be585b82383c718fc715ccf84afb58f0fe9ebadc9a4c52d84bc16190ea9eaa7a568e152716e08e094e10b293b1e91e1cf0dc3a0d980aa1ee987ceffd07b5686a9a70bfb0781b0adf7958f406ac595ec17b6105edbb6e86540e87d41af3bf1df8d750b6d4e3d23430c9709896699c70075c73dabc4a346ad7ab7ea8f49d485385d6c6778e6eb53f06da5b180a453dc72920e481cf23d2ae7c0033dceadaf5dcaecf218a30cee72492c4f5fc2b17c5be2a93c512595cea7609234118409103866ee7f1f4ed59c9abc9616e5ecd64d383804ec0ca5b1ea781fa57bb0b42164b53cd9c9ce5cccfa6c5e5bc939b71345e701931ef1bbf2ae77c43f10bc37e180c2f6fd1a75cff00a3c277b93e981d3f1af9dd7c5d7b2c8aafa85c6d4276c84e5d41ebcd62cb6d69a85f08e3b858d9df992524019ee6ae2aef514a76573bef10fc73d735299e0d0e08ec61638572bbe53f9f02bcff0053d6bc43a8485b51d42f662dff003d2438ff000ad9d7bc30de0c9b4bba3709722705fcc41f20c7607bd6ccdacb78c6e74cb3b6b65096dcbe100555ce726b5ba8dac8c54d4e0e773cfe382eb76f56656f5cf356e2d2f55beb84821596695c6428e49af6df09d95a4575328b78cfcfc7c80d69f8ab5387445b099ed923b79a7f2a49c281b78e07afff00aa94e4d45c91cf85a92ad5941ab27d4f079fc3fe23d153ed325bdd5b283feb14918fc456be91f13bc63a148aaba9cb710a9e62b91e603f9f23f3af67fed0b3d7041608219236e1f69072bdeb0bc47f0bac351b979b4cb9303b7fcb394654fe23a5452aaea5f43d0cc69c308e367abe84de18f8eda6df3a5b6bb68d632b71e7c7f347f88ea3f5ae6fe3c5d5a6a12e837d65711cf0491481648db20e08ff001ae6759f867ade9c8f34b005857a4aa72a7df35cc4f657fa7284ba89bc9c9c2374fa8ab8f2dee8c14e4e1768f4bf85be09d37c6be0dd522b8062bd82e479372bd5415e8477153f877c67a8f8035d9fc3dacb2dd5a4326d728db8c7eea7b8f6ae63c29e3c9bc21e17d5ad7494637779221591bfe588c104fb9f4ac5f0a787759f19f88fc8b4dcf23b179ee24e550776634a74f9ef72e152da1f5ae9d7b06a5631ddd9cab35bc832aebc83457cf939f137822ea6d196feea05472ebe493b1c1fe21f5c515c9c86fcccf4783c02fa7e910c565a9c91de0cf98ec9b91cfd3b57916a8b70da9dc7db6ec908c5770182dfee8af403f14dacfc251651a4d5d94469bc704e397ff3debccf53b4d726d266d78db4b2c0d2ed92e71c2b1fe9db3d2a29539395ce89d57c9cb7d0eebc2abe2ef10e9e9a568b2b69fa4c47e6ba6e09cf500f53f415dd693f0e744d2a557d4964d4ef18e45c5d316527d36f41f8e6bcc7e147c4b6d1674d0f58933a7cadfba99bfe58b13dff00d935e9be20f8afe19d19cc4276d4251c14b70197fefa3c574fb2e4d11cca5cfb1d63c30430a5adac71c45f8c46a142af73c559002e02f0ab8000af191f1d122998daf86c0427f8ae7e6ffd06b46cbe3769f2906fb49bab6cf568dc483fa1a4d335509763d4a6b4b6b9e2786371ee39ac7b8d165b6732d8397007fa990ff2350e87e38d035c4dd6b7aa4ff75c608fa8ab7e23d52eb4ad0e5d52c615b95830f2228c968fbedf7ace74a13d1a0e694353cb3c45a8dd6893ca63b7594484e23660b2213db07a8f715c27f695fdeeb705e487cb96270d18ebb4839af76d5ec740f1df84c6a8ec9144232e970df29888ea18f6c1af1c5d305bda5c5ec5225d410c665495481b87bd68e7350506fc8f3a382a30a8eac23a9e997da2ff00c245a5c1a8da62defd903ee5e031f435c76a5652cb632a79021d46238248c329f6f5159f7865b7f0b2eacde27b8378a02c504339010fa000d5ef0b78aa5f17c69677ccbfdb3182239471e7a819da7dfaf3587d52a61e376ee8ec8e22155dad61ff00122f609eeb4785194fd974f89640bd988c9fe62b8cbeda8d06d6dc37139157bc469e5eb12977daac7209180bc631edd2b064cc52795e60646f990e7a1aed8bbc6e734d5a4d08b2793720b29dbbb191ce3d2bb5f03dc4235357798285662020dd9f979efc74cfe15c5c81cde45b0651d7e6f45c739357ad2796053e55cb59b33865b845ce1b040cfb609aa890ced7c6fe0a8b528e4d4f48b88d670a0c9096ff0059c0e7d8d79a4be1ed7b0636d3eeb00f40848ce33c7e02b5ef67f16e5c7db259a37e9244e30e077047e1f8d566f1a6bf15c9fb6113bac7e56dba8f7ed1d78ddd2b502e697e1bb982dc4b79a1cca88374d25c3aa0c75180c475231df835caecfb56a12ac11ec4663851c8519e95bb26b5a86ab7b1cdaace4ab326d881c20e9ce074e3daa95baf9523c8a997918be07a678a993b2045886da3b489a590855638ff00740e94b76e2f3cb0331c2ff31078254773e83daa531925e5bd706de201913a027bfd79ae6ef6f1eeae1e4ce14f0147a524aec66cdceaf0dbc1e443890e3e62385cff005ae7cb9cf1c5329caa58e00c9aa4ac02124f53498adad3741b8d46fa0b0b789a5ba98f083b57b3787be09e9d6f1a4bac4a6e25ea618ce147b13d4d44aac626b0a3299e018a3a57d4b7bf0ebc30f0089748b6403a945209fc6bcc3c59f0b12d2092e74866dcbc9858e723d8d651c4c1be57a1acb09351e65a9e636b7f7366fba09990fb1e0fe15bd6fe29699945e46338c174eff85734e8d1b95604303820f6a6d743499cad1e9f657315dc43ecb2ab93cee1dbdab3a489bfb4e30e76ed6fe55c8695a94da6de2cd19e338753d1857691edbdbe3247cab8c8c7b8e2b371b12d1eb7f0fbc390d869765a86f94cd2c6c5a32c4a65883b80ec6bb9785245c328e075aa5a3c22df4db4880c054007e55a6d1293bf2d91d39aa4bdd365a58e51340dbad4bab4d2830c6498e31d73eff97eb5e3daa4afac788e59ddb7896666cf5e33c7e8057b4f8a2e5b4df0ddfba3618a858cff00b4dc7ff5ebc620b76b78e4b92bb58f0bfec8a99596844f70d4841b808c6c3b70510f19f5f6ace667580e4286fba31d2adce76a0627739ea73c9a7c516f5fb4c89fb94cf07b9f5a9333263b279258a3e06ee7078007bd49a8ea305959ac16037dc5c0c2b76083bfe269b797322424449892ec8890770a7ae2886d6cf4993ccd5274f3081fbb1c9f6503d055241639e974d7dc24b82cce4e4e4d7a77c1db5dbfda2f164f98eb1ed3d8d7157b2add33c91ee8e3c759001fd735ea3f02104f06a678648670d9c752462b2aa9ca363a30b53d9d4e667b15bc2b05b244bfc23afad63f8a75c83c39e1fbbd4a66e51711aff798f415bac73f8d78e7c46b89bc4de30b1f0c5ab130dbfef2e187404fafd07f3aca4d4579236bb6fccf28d72caf750d31f5cbd90f99757244608fbe704939ec0702b3e3d6b5c1690e9be75c342a70b16e38fc2bdcfc4be148f50f0fdbd95a44a3ec9fead3d463047d6b8b1e119ed954c36eed3310a06d2369ce2a30d8e8ca26188a325249abdcf48f0469520d0b4ed3cbe638a10f33fa64e768fcebce3e23f8922d43c57716ff002c767663ca3e5801a4da7904fd78af70d234e8fc37e1c489df734316f99cf566c735e1f2f866c35abcbb9c65e691d9f93c6e249fd294211a6f5dd9d0db92b239ed3f55b8d4dcdadb98e1463b8b860a540ed9354ee62b8bb9248e6f35d51b69790e715ad16896fa54f0c9b44ac33e6a28e38e833f955c58afb33c70dac0ed39c96ebb4f53d7ad6ae4ba19d9f5388d5d4193644d12451a852637ceec773d2b25bca8f052462c39ce38aec2e3c25717330b7863926b973f36d5e07b568c7f09f534d3e5b8b80124552c10f538ad956825ab054672d919fa1dadff008bf4c8f49b78a499ed5cc91c7bb202b601c67dc57a2785fe12eafa35e7daa6b8b48cbc654a024e335cf7c2694e91e365b43f767429cfa8afa2930707150e777a33378783d1a390d3fc217560de643736fbfde338cfe7557c53e06bcf15c56eb7d73014833b110b20c9efd0fa5777d4e3819a5fba30707d2a3916e6cb456478cc1e1c9fe19a5ceab3c524f672288dc44e18273c1ec7dba77adfd17c59a36b170891dc88e4383e5cbf296fa1ef5e8377636f7d6cf6f750473c120c34722e41fc2bcb750f83d62da8b49a65c5cdbc4cc596204155c8e304f400e78ad6137047257c2c6a4b9fa983f14bc4f32f89974a9008e2b79127404fcb22e38feb595abdb6a5f1024592c6cd163b7847cc1762920741ee6b27c75a69d0f5f1a6eaf711decbe582970ad9755e801ff035e9fe01323784639a392263e6603839ca818e7d0fb7b5118f34932ead7e4a2e3d4f9fef74fb8d36e9d248da3950e1d18608af7ef829ac68971e1f6d3eca04b6d4623bae1739697fda07d3b63b556f1df84acf5ad35af4b456f7718f9656e37ffb2477af1cd32eeffc2dadc1a85a168a781f3cf191dc1f6357276d2e7250ab2b27247d7ed6b6f31dd2451bb74cb20268acdf0c6bd6fe25d02db54b6185947ce84f28c3a8a2a2c8f4149773e678eda2bbd7218d82c4b34cb1246873b4138c0fa66be8d7b3d2b48f0dbda5ca451e99042564571f2ecc739af9c74502e7e22f87ecd7955ba8cb0f7ce6baef8c7e309350d54f86ac243f67b761f68d9fc727f77e83f9d4c636499b27cf2e5380d6a6d19afee22d0ad1e3b533168e491b2e54f6f615d6f877e1ea6a1a61bdb99e28c2292cacdf37b1c573fe1ef0b5deab7c96f1210ec3396e807ad7abc7e1b6d074d78507df0374c4f24ff4ac2b57e447a787c35da8a76383d07c276bafddcf6f6d742178885d92a75f714babf81cdbeab6fa5aea10c6a537cb2c9f281924003f235bd068d7bfdb2b7c0a411ef12b98d89271d147f33f5adef12785a5f105a457f67b05ec4a7f76ed8debd71f5a8a55973a162e8d458795b4678f6bd66be14d6e38f4fd4fed2554169236c8aef7c29f10ae84422926dae463e71946e3b8ae1b50d075196ec3ea1025b6e277ca4e73f80aeb341f0ed9eb5a65cc0b98a7b365f2651d4a9e81bd791fad74622516ae795974255138c9937886f65be4fec08659acb4d794b75cacacd9605bdb3c7155bc3de0bd75bc3babcb2c63fb3e3421141c997fbc57db1cfd6adf9125a86d3b50811a588109e6ae430f507fad7b9d87952e9b6ed004f20c4362afddc11d2a613e68d8aa945d39dd9f24dce9379e425b43790bdbbb16237e083db70fc6ba0f075849a26bf697d812085f0707ae7826b57c41e1e1a77c45b9d3e2b70eb2b1f293a7cac372e3e9c8ad2b5b1b5b1d42dacf51ba8acc67cc7129c10a39c52ad55f27299463c92b917c4db182db5e8268e429f688f7c8abd8e7fad713f64b79236ddb703a15047ff5abd2be28409a869fa56a360d0cd036f8ccab8653d08fe46bce7c91f76689588fbac992bf976a306dba28588fe2329cb70668a2b38d5559012cf9e0ff00b5f5a9671bb4d5cb12d29057d401d0d36f2d58fccacc245fba01e051138b84ca8064ced65e8233fe15d4f63124d3754b9815d57046713444e3775f981ec79eb5d3e853e8579084bf058231024b800b2a1eb91d180f51cfb77ae3aeade5b706e02ac800c3a1e845451deda9b77996355c63e524f5ad23ef193bad8b7e378a383c59716f104110705421e318e3e9c76a82ce3323163d581c564c2c6e2e5e523249c0adc8418678870142f19f5f4a89ef6345b199ad5dfca96ab26fc7ccc47bf6fc2b16ae6a92a4d7d2327ddce2a9d68b618a3935bb6166b6f6af792264463bf763d0552d26c4dd5c062b98d3e66ae9ef6dbcdb0b6b741b564dd2b11e807ff58d673959d8b8aea6f7c1e33a6b7a8eaa2c64bc748760daea0a927dcfa0af6ed33c41f6f631c9a7dcda30e827039fa609af07f01f84355d5a69e4b3bb9adade3c07f2e4d85bad7b868fa29d1acd6379de46ce41639dbec2b9ab37cd78b3b70eb4b343f5df10a69ca112ce7ba989c08e21cfd4e7a572ba8ea9aa4d6ecf2d8da46a54930adc6e940f5e0633ed9ae8b56d2e3d46e65b699e445930c190e0f15c4c9f0b268f5b4be4d4e510236e2acecccc3d09358a4a49b91b4b9d6915a1e25af2a8d5ee4af1fbc35975d97c40d362b1f12df2a0383b1c63a0c8e6b8daf469bbc51e5cd5a4d00eb5dff8595a5b9b1424e4941d3dc5707121924540392715e87e14da35eb15040c4f18fd689b20fa3621e5c71281c0c0ab4ed889b19e9554901579e3205134928695593116d055f3d4fa534ec8d4e07e23ea456d6c2c339f3dcccdf45e00fccfe95c1cec4c602838fbcdf856cf8feff7f8a913398ede044c0f53f31fe62b9f8eecbb48b927702572783c566f5665277645a8337d9cca63c90bc60f2c7a0159d3b6b0da72c4561b4b723f89b73b7be2acde2bcd3c30a485405dcc33ebd3fafe75937ef1db46c4925b1d59b3cd17b0918d7f24cf70a8d70cfb4e433f6abfa458c325c096e2369ce7f8d8a83fa126b26d364f785dce7b806bb0d3494c1f29493d066a9bb680d94f57921418c202bc08d0602fd6bd33f6792cd06bde9e64671f83579b6ab148b0c84c58c8f4e2bd0bf67a7916eb5f8ce02ed89b1df396a9e8caa6f53db6f274b4b69ae64384850bb1f40066bca7c156b25dbdf7882e47efefe5664cf64cff009fcabb1f88b73243e109e180e25bb75b751dcee3cfe95936f716da4dac1a7c4a646850290bdb8af2f16aa544a9d3576cebf6d4a82f6959d9235b18a769f6e6f75a4465cc76ebe69ff78f0bfd4d5482fa2b925572b22f3b5bad6cf85e3ff419af0e4b5ccccd93fdd1f28fe5fad71e0f0cd556aa2b34744b134ead253a4ee98ef174c6dfc27a94819548b76c16381d2bc2fc1ed757d05e346cea0700a8f98e7ae3f2ebef5edbe3c805d7823568db3fea09e3d8e6bcd3c141b48f084132c5bcb3333363a727ad7ad51d95cce8c79a460788f4db8f0ee9704b23932dc4adf2752411c01fe353784b45bfd4665660445fc44e7e63e99ef5d4eade2cd3bc88dee134ebc78d81f27cd01c1f55cf535bfe1cf1058ea569bec6dcc617aae3a1acf99f2d99d1eca2e5a1a9a6e8f6fa64036a832e396c54b3a86073c835cf5e78eecadaf16dbc97690921b73040a7f1ab435f8e72a1046e18e331c81b1f9529356368db63cdae12d341f1cc379149b4473a9653fc393cfe15da6b9f19b45d2a77b6d3eda6d426538254ed407d33587e2ad09aefc67a25c5baaab4a58481c70768c835cd6aba5da68faf496d3e937d7fb9f335c4436aa96e70bc1ce33d6b583d0e1ab1e46cd1bff8cfe2ab8f9acac6c6d53b6e52edf99354edfe3378d21933347633afa1871fa835d6c1e08d29046844af14801f98e08cd56f881e1dd03c39a2d9adb26c9aea5086527944c7271ebd29c6adf433e468b5a37c71b79dd62d6b4c92d18f59613b947be3ad6df8cbe24db68de18fb7e8a45f5c5c0222741948bddfd3e95e7a3c35a5dd5fdbc3a3cb717b04a9fbc0ebcab01c9155e5b393c3f77beca4fddf2b24320ca91dc11de8752ccd1d27b3381d2f4fd6bc6de26f2a1f32eaf6e5f7492b7403b963d857a6436baa7c25d7a186e5cdde9572017c0c2b8ef81d987eb5e8df0cac7c3f069335d6916a21b999f3739e486f407fbbe9517c614897c0ef71220610dc46c0f7504e0ff3addcf9b639274acb5dce0350f18457be2b8a467cda5bc84c40820329e8d8ee31fcaabf88b48baf11da5deb70da46a89963b136f98b9ea07b0e6a0f0b490dcb3d8b22ca40df09f2f7363b815ea5a4d91b3d1a2b5940e14ee5edcf6aca95273aaf4386be21f2f234786e8de20d5b48b26b6b3d426b78f796288d804e00cfe828ad7f127864e9badcf0c28c616fde47c7453dbf9d15a3a7a92aa68733e0ebd487e21dadfc8c365bbc93649feea311fcab47c27716b77e259aef5389a769a5dc40192493935cce856ad25f5d96424436f2b371d38c7f335d5f8034afed2f13ed62cb143199491c124703f53456765647bd818a72bbee763e30d4934fba81f465fb398d95b0072d553c4bf1354e891c76f116be7ce4b8f9500e33ef9abdaae931cb7eeb708cfb06d00f18f7af2cf125b487c453db41160f982389471e8062b968a5525cb2dcf5f1b0787a5ed20eeba16fc4d6be2ed3ad6c6ff5792e920bd8c4b09de7681e981c03df15dffc3ef88569a9e99fd8dae5cc76979047fe8f7acdb43a81d1bdc7eb5c9eb3a07c4a9b4678f584d424d3a101d9669832281df935cc5878435bd462135bd8cbe51e8cdf283f9d76ca14f96cec7cf46a5694ae9b675d75ac45aa6a91422e37db348556424f19e3af715dc6a9258f86bc38b0e9b2aa5edc0123c9247c6d18071ea79e9ef5e377fa5ea7e1c9214bd85a166f99324107f2af644d39bc43a0e9827996364803f9a8809e40c8c1c8ae69b8d3b37b1d3469cace30d19cb4de24935bd312e278cfdaac994b498c6e8d8e391f5c57aa7c35d57edda04b017cfd9a4da3d94f23fad79d7fc23d6fa668fa9a2bbcb2cb13fcec7f847207e82b9ef0778bf5cd0efcc3a741e741395f3c18cbe319c1cf6eb514b964db8ec74e328ce8c63ed37b1de7c608bec1abe8babc0712e0c65bdd4865fe66b99b978a2867bbd5f4e6b896ee31247a83cb8452d8e00e9c6718a3c7be25d4f5eb1b482f2d7ca10cfb836dc649078aec9f41b2d5f40d345c2b88dad941d8d8192064d456af1a328c9f53cf950f6cacba1e5ba45e9874dd46c44ced68ee248509ddb1c371fa673f8539777276e3dd4e33f856b6b1a1456693c1a746628a1639da72401df34cf047875b5af1245334a92c368c24955df83e831f5ae98548b8f3a39e29ce5ca644b14896ed2886408bfc5b4e334d83c25aec9a4dc6b7676d8b58c1dfb9b06451d4807ad7d16aa91c4524b65119e30ab91fa556bfb1b4d4f4f9ecd9d922950a3088ed201a9f6e7747051eacf9c629d5ed4664ddbba64e739ac7b8d3649a53f67fb9d48cf19aea3c63e1b5f076b2b6f04cd2dacabbe32df793d8d6742e76804f3df0735d0a5d6270ca0e2eccad6f60b03a28e768cb7d6a3d5aed521554e5d86063b7ad4f717a912b338007a83f7bd8562a4e033dd3804938553da9a5d492192d248edc4cfc0270077a65b40d713a46bd58815a7a7cf1ddea19bb03c9da40c9e14e2aec5a6476923307ca7556c7506a9cacb51a5764f69a7aa69f33ab10a5b017a71ea7f5a9efaf30628e35c622d807a1207f41fad473cd24380ea46f036c7d78edc5466260dbe73fbc73855fee83eb58a777766ad59687b67c212bff0008cfda0803ce95b8f40381fcabb5d63ed725b1fb024665070be61e01f535c07c31940d01963236f9cf803b64d755ab6a7ab41005d2ec3ed127ab48aa07e679ae4752edc4f4e14fdd5244512eb635b8e4ba1035b818f909c8f53f4f6ad5d46e843031e800ae5a2d5fc4e6fe35bad24084f04f98808f7e18d5ad6ef36e9ec58edc2926b39369f29ad925cccf13f1ecdf68d5ee6e01c867c7d0018feb5c19eb5d06b17cd7c6edd413124b80739ce73fe15cff00f157ab4d5a291e24e5cd26cd4d22d4b39b961f221c027d6baef0a40d378a74b8e3ce5aed09ff0080826b034f915b4ad806d28c7bfeb5d2f80485f1ce90091f7dcf3fee9a97ac8cfa9f44153e5807d453ae8136ecbd8e052ee0f18f7e94db960b6f231230bc9c568f6353c17c4b3adc78875627ee89cc6bff0001c2ff004ac6de06255394030734b77399aea7724fef2476c9f724d645fdc3db5a98f77fac3b71dc7ad628c5abb2617630f2ae46e24f1d876fd2b0752ba32becc938eb52df4ed1aa8420023a56648e6462c7ad5c63d468d0d2e30433f7ce0575361045b94b9f9bd8e2b92b39c0558f6b100e4edef5d2d84114ec0ac2c0f7fde1e2935adc4cd0d4cc90d939dcb246c3f8b823fc68f8709acdcf8eed23d06ebc8627cc9f2c42b460f21877aafabdadcdbd9e616668c1f991b9ff00f5d3fe16ea6da5fc48d3248c12b72c6075f40dc7f3c525d584373debc6a7ed1abe81644f1e73dc30ff007178fd4d70316bc9a6f892feceea548eea0b862379e181391fa1aefb5efdef8c6dd8f220b338f62cff00fd8d7cf5e27b2be83c4f7e5cbf9ad33125b9c8cf07f2c57261eba8e224baa43cc302b1541465dcf50fedc136a4e60915da185e69993908369033ee4915eb5a1c3f65d0eca1cf2b02e7eb8e6be6df0a4170b06a32807cb3008e460300966503fad7d3917eee355ec140fd2b494fda55948303848e1682a68c7f174c22f0cde96e03215ae43c19e53685040e01050020feb5bfe3b9165d216d3780662462b88f015d89ac4a6ecb231523e86b39ea7a342694f94ec13c3361e6aba5b40029dcbfba53b4fa8e38ab5636f05acb2adb448800c1daa055d8e5c4581dc573320f125aea3722cfec73db4c7e466c831fd7d6b376d0edd752fdcf876c3558cb4b6b04a0fde0f186cd4961e1dd3b4a26486d61472304a20156b4a8ee6d6cd05dcb1c931fbe63042e7db3535dca3693d80e6aacb97526edc8e6f522afe24d3182e4acac07b650d6bb6853bb1125e8f209c9dcb96c7a66b968ef4dcf8f6c2de2467f2833b63b67b9a77c44f1b2e87a6cc203ba5dde544bd99fb93ec3fc2882bfa98d595fd0e827da66c428f2ac7ce5149e07f3ae67c56961e25bf8acd352870918cc2df7c1f5da704560697f1ca28bc1f2477d6bbb5a800580a0c239fef1c74c771debcb22935bf17f89bcc8bcdbad4eea4cfcbeb9fd00adbeacecf5b1cd1c4a525a5cfa43e1fe810692b76d1c6198615657e4fb8a778bb4ed1ef81f2d963d462f9b7a2f04fa362b9bf873e2bd434afed4f0c6beac2fb4d89a542c72cc072573dfa823da9d65afac925c7d9a68e51393904f2093df358547ecd28bd4eec3518e22a4a4ddadd2e64e9ba84be13f132ca095b6918174078653d47e1dabb5f8b5b2e7e17ea3227cc8563753ff0002159de24f0d490786a3bc7406e1010e3ae32722ae4613c45f08a5b49d892233031ee0a9e0ff002aba126be239b1518ddf23ba3c4bc0fa9496da958dca93fb9902bfba9e0fe95ebde27f148d2f51b2d2ecda26bcba206f90fcb10f53eb5e39e0fb27875abcd3e5c79919231ea41ab9e31926b8f1bdd4c81a350c806f3f74051c8f6ef5d4e6d3691e4aa7075139ec77da878ce6d22f64b2ba834ed4e48cff00c7c2b1518f4c0cf4e68aa3e18f03e9daf68e350be9af1a59257f991c20619eb8c7d68a4bdab26557029b4d1cb78034efed0bdf1445201e6b5849b73ea79fe95d1fc38b4485af6e0c98242c64639c75ad2f0df87248be233c88db6cae6db7caa07deda47cbf9e2a4bad1ee7c3dae5cc30308d25937c6c572a54f63f4ace15e319a9cb547757c3d6a9879d2a2ed2e86d6b925b5ac323c52655533b9863b57153e8963e33b05d5f49263bb8d82c88dd9863ff00ad8ac9f156afabdc5ccfa74bb1115b6b7979f9c7d4d6dfc39d32fb4a927b9954fd9648f731cf1907a9a78daf4e769c74688c8f058aa0a74f12f47d3b1bbacea5a86b7a99b2bd8da1d3ad634d917fcfc4981966f61e95bba4e932ea10972c60b64054363ef1f6f6ae2aefe2118ae1ad2f34bb6bb6562125490aee1f91ae77c4ff001235ad5ed7fb3a32b616ac36b456c09761e84fa7d2b9e319557ef6c7a12942945f2193e3cd486bde278ec2c1bed1f662615914fcae73c91ed5b57dacf89bc076ba7c77935b4caea40b72030d9f51cd72fa37872e75d9443a2664bd43b8a13b5971dc93543598357b8d40aea10ba4b1e223b87a7ad774553e5e568e0e6aae7cf167ac8d72cf5af0bde5fc5b2368e0632283ea0e3f5ad5f82167e5d86ab7a7256491221e9f2824ff00e855e4562f7169a649a6424c925eba294039383c01f53fcabe94f06787c7867c2b6761c1982ef9987f148dc9fcba7e15cb18283763d2c4d79548c54b74715f1b6e963d2f49b7c8cbced2607a2ae3fad5bd035487fe116b0de8ed1c702ab923038fad71df16b523aa78de0d3a23b96d2208476dcdf31fd302b71ef9e1f07c367146a4b463202e081f5ae6c5d3552d16461f4e691d3245a4eb5a65e476b2a93344d13bafde5dc08ff1a4d2b46d2fc3b6f069f6f18b7889f926c80f231e4e4f73599e01861b6d1824b2aacf3caccc18e0f5c0fe55bf77279225b6bab2fb6593f380bb8afe1dff000a22b9172f4378c535cd6d4d04175129db324abdb236b7f8524b74b1445a71e58c724d61d8369d07cd6d7373121e904921603e81b91f9d6478ebc5a345d21a28268cdd4e36c608dd8f538a77bcac8d12b2e6679e78fb524f10eae12cb0de539553fdfdbc7f8d72170d3dafee2789e1917864605589f4ff00ebd5fd3a392ff568a38dc195df96faf7af6992d34ed534b44d46c12708bb11a641bce38cfa8cd74aaaa9e8d1e73a2ebc9c933e7b92dde79374d26d0074ec3daa2468103a60bc7bb03debd3bc4df0d216b596f34a9e41e5aef36edcf1df07fc6bcf6df4f4230b29c86e415ae884e3257b9cb5294a9bb48d3b1fb3d9da903cbc3720f05b3ee7d2ae4f3ca6d6390857676d9163dbaf14d820b32ac30577724fbd599e64fb1c48405dab845ea7fc9aca6eec708990d235acfb8fcee0e493c927d3350196491de59d8ef3f36d3da8bbf35a62fb71b3a0feefbd4b15af98e8cccb90037cc7fcf6aa5b5d96f73b4f00f8b2d748be934ebc71147290637278ce31cd7b3c7025e439137c8c38239af9b74fd2db5cd4ded622824666099e0647415d0e99ab788f44cd945752a79671e54a3701f9d72d5845caeb73b68d6718d9ec7b2cda55bda02eb33138fbcc6bc93e21f8c625864d3ec642f237cace0f03d6a2d53c49e20d461f2ae2ef621182b10db915e75aab79b7cd1c7c84e3eb55428de77646231178f2a2d694a92e99771b11bcba151ebc303fcc563cd1f972b2f3c1ef5a561235ac667e400719a2e624b9b908adc91d4d7727ef1e7b5a0cd26e76bc913630ebc67dab7f468350b9f1269f1e9ce16e55f2ac7a0fafe75cc3db4b69287c700e4115dd7c37b83ff09ed89756688a904819c7719f4a524af7252bbd0f66d235eb968560beb7686753861d7047a7b53f59b9b2d3744d5f518772492c44c84b1393b4e0e3b726af6a1a7a3b79b1101bd6b96f1c4860f046a85d7ac6abc7bbad66a4d9ab8b8ee799d85b5bb5ab4d3ca136ae727a7bd7273dcc77733c8581e76c63a607ff005ead4f7667856c6297687f9a427d3b0accbab26858b29f93daad24b466454ba7dd70dce71c5418a9e0825bbb848a252f239c000724d77d6be0cd2f4e8a37d4da7ba9d973e542c00cfa63a9fd2a9c9456a38c5b388b211c4ad24849cf0a83a935d158dedf14555819221c7c8a07f4aadaac96167747ecd69e511c6d63923f5a4d3efa27dcde634520fba4720d26eeae4b46dcd72f2db95370411ced71d0fbd6c7c1fb1b493e2621bbd914d040ef0a13f7df8e9f8126b89d52fde601cf12a771dc53fc3b7d76de2cd19ecb71ba4b98c211d4e58714a2ae822accfa6f578c7fc2492b639fb3a0fd5ab2753f0fe99abb2bde5aabbaf01c7071f515b7abab2ebe4b6ddad6ebb71d7827355dbad7cc62dca35e4d1ecd14a54d26731af585a693e18922b2816289648c903bfce393eb5ea6a3280fa8af37f16c65fc2fa811d562dfff007c907fa577905f429a65bdc4b20557855f39f502bd0cbe57a6dbee618856924733e3c2a1b4b4dea49998eccf38da7f4af1fd2355ff00847bc617b664ed89a6c8407a035e95e32d77c3b9fb6cc631776ea76393f3b7b002be78d4f55bed6fc42f76c4f9eedf22aff081d00fc2bd08414d3479f7942bf32ec7d38977f6bb053149b588e71ce2b3ca5f237c97ba861b8c2428547e95e51e16f88371a7dc4505f10235f91c3751ef5ec165e33d267b78dd6e630afd32719ae674e507a9ed53af1922cd92dcc2e1a5b869233c1de801fd2a86b9adc50064dd850092d58be26f8836366c60b62247ee41e05795eb1e23d47c4da99b3b1563e61c613b0a74e9b968675b1096db9d8f833c5766bf12268ee1c8fb5030c4fd8367807eb8a3c457ba75fe87aae997b6ad3df35c136cc38689fb9cfa75af2b8227b7d54489237991c8486ee083c1adeb6f13fda356b81aa15579c9ccd18e031ef8ae9f65669c7a1e77b7ba71672c2c73a88b432756dbbb15f447c22d0b4ef0e412453089f51b9395b8f54e3e51e9cfe75cce93e01d227d15a592549ef25f985cc6dbbcb3d80c71f5addd0eca7b787ecf71205784e0be7b7a8a2a57e814e9b916f59d067b8f8ccb756f0aac12da08e79193209208e3df0054d6ff000ee71ac89dc4690efdcecadd40f415d4682629a596ecbb4862006e6e4fa67eb5cfc9e23bcb9f195d5adc3cd1d84498816318f30f7626b9275572f3c8eb506a5cb13b6d56d92eb45ba808f95a2207b7a5731a5d80b7f05ea316301dd9881eb819fe55ad1ea32b689287f9897f2d49ee0d4f2c020f0b4f1e002d1b31fc79a74ea2a8d496d609c3960e2fb9e21a769261f1d5f5c2a1f2f623640e0311d3f4aefee7c31a56ac6096eed0493aa05dca482d81df1d6b2348b433eb37d291feb2658d7e8a00fe79ae8f4ff11ade6a9736b6a12016b3180b11cf1eb5d51ab18de4d1e64f033c4cd420ed6d4d1b4b078ad523b5b5db0a0daaaa300014548de239a0631b2c6e57f894601a2afeb91345c3737aea73d652cda25fabca818c47cb9b1fddee47f3aeab57b1b5d4b4f12b3a8c0dc927ad727a18bad5f4e9ae67462e279044597064841f94d3dd2e6309199dcdb4630919e8a49e6bcd84d2bd391eb3be938ee674fe1fb2d4e7f31fe5b84386c77c54f33dc586932dac6ac636c02a00e477c568597877ed3a845aa34f2c28a855910f137a13f4e6b52f7c3d3dc46a2390301c8ec6b3a946697ba7752c5539fbb555ae78f3e82afa9da40217687cc2f311f2844ec33d8d696930daf8535bd5af27b25991232607986772e32307f215dacde1fd410842a0ae7d6b6753d2acf55d024d3ee2d37308764641c1fa67deb6a15a6dfbcad638735c2425412a0eeeff0081f36699afded9f8b9354d3479733cdb8a2e48c13c8fa5771e3ffecdb895352b4923d93f3c7de27d857451fc32bd8a6ff4086dec15861a590ef6fcb9fe755b59f84534761ff12c9daeb5147592396460aa3d576f4c77cd75b6a724de879b8775231775a1e4d2de6a7a2ebb6f30865b6bab775923591083ea3835ef0bf1477787ade6165e4ea6cbfbd490fc887d7df3e9581ac4d06a163a635fe9f1ff006c5a8689e45e49393f2afa81ebdab22e12db4e0cd2402eef48f9631f72327d7d4fb5129a6ac8d173377667dc06bcbf96fe79024d70c5cb2a00ec4fa679aba2c2fe341289266dd8da66b9007ebc55dd2f4e95647bbbb5596ec0c9121c456e3b027a13ed54354b869a4764d72159573f228e3f5159ddc9d8ad95d8cb8b2d6920919ade718f995830746c7604138356f46f1f4288b0ea4f21923c08a5c9cafb63ea7ad7209aadf5a5f9c4a416e43c678cf63e841355f50417ee97b69108ae94e654ed91cee15a3a6ba8a355a7747a0f8f3c71a75c59456b1d8ccf36d0fe7e0c650e3b1eb5e433eab35dc9bee6696493a6e66cf15d36a7a86a3e21b6885f4e649605d8a8c31fcbad7193a7972b2e08c1c608ad68c12560ad5a4de8761e15d56cb4abc378c1a47543b36f50c7d6bbdd37c622f4aa79aa0b67871c8af0f49191b72b107dab46db536461b8956fef0a9a942faa153c435a1f44daddc6c99322b64739af16d5ed1ac3c47796d0c4769918a71fc2795fd0d49a7789ae619155e52531d7351eadaa26a5a935c08c890aaa939e2b2a50716cd2bd45512b8e489e589c2a7217e620e02fe3559612ae8e18809f79fa8069e8679488e3dcc1797c2e4002a37bb68e20b2aec404923d4d524cc96823077dd82143f0401ce339cfd2a1f2a42cfc9299ebed8ff0a90319502c670f27240ec0f4154ef6492146419c9ca818fd6b45ae827a6a6c780b5111f8d2d958feee593033d8f6af6bd6fc3906a244f18093a8e1b1f787a1af9bf4899edf548254386470c0fa62be94d0359875dd3567b670cca36c8b9e55ab9b1916a4a48ebc1c94a0e2cf3bf1058b69b03a326d99810b9e0579cc31856757e59773330ef5ed3afc7fda8124b9466519fb3c017927a6e3fd0579cf887417d2aea389e311bcd18936e73819c5551a96566655e959dfa1ce48e60d311197f7729393e841ff00ebd53b5bb8e0bd824923f3228d8164ced2c3b8cf6a75e8996040c498f276d50aee8ad0e393d4f76d0750f0cf8dec5747834eb2b20463ca750241eeadd49f7af46d1bc31a57866c16dac6dd2351f7dfab31f526be468a57864578dd91d4e4329c106bb2d37e246bf1de69cb7da9cf2da5b3e194e18b21eb9fef71d33594e8b7b337a55d45ea8fa6dd141f2e400c6c300fa1ae33c7da7cf71e16d65c5cc8552cc816e14156218307f5cf07f2abd178c34cbdb5896c2e3ed6658c491aafde0338f9bd2b363d5edaff00519acb53be897ceb76431f455cf1c9ee6b96353965648eda94e352376cf9a4bb972d939cf5a9d66b9b802105981ed5ddcdf0f3525b8952dc5acd06f3e5caaf8dcb9e0f3ed4db9f0a4da45b219429b824b945c1cf2028fccf35dede9768f2134e5ca33c2fa541a7a3dcddcc9105e1e5c64f4cec5f7f5ad5bdd62e2ea516ff00616b4b5206d014b161ef800fe1c567df6a91e97143146ab25c84ce48c9572793f5a821d582c8a6f3cc9a41cec925e33ef5cd26dbb9badac7436de1dd06e515e5b62653c9705947e5926b2758d160b624456af1a74f91990e3d7906ba8d3753bf36df6cfb246d6ebc6d8864fd7b569bcba66a7687fd3cda3b75591c6dfc411c7e359734d3b9af2c5a3c865d32db76f86e99c7f1c320f9bdf0457a37c11f0be9d75ac5ceaf3ccb24f66716d0e7a67f8c8f6ae47c43a7882e0a0015c7dd9000a187b60e2a9786b55bad07c436d7f14cf1323670bc6ff507d41ae9526d185b53e98f11ed82f6cee65609118de37763800f047f26af3ff1078dd2d2545b078dd54fcce41391ed5cef8dbc7777e25bddb6cef169f101b21e849c725ab8d79fce74393cf35cbf558b9b9c8258a925cb13afd4fe206a3a8d9496c91c3146e0abfcbb8b03d473c5624da9ead7db239f51b8f291422a798428503a63f0aa56f012e59c63b83e953ec0f1b12e39e01c1e6b48538415a2ac632a9393bb650ba895f2b23b1672141ebd4ff2abde16f094ba9f8859239232618bcc196e1fa703deb2b51b29a4c491cdb580c6df5153585d6b7a43db6b16b705821cf03923b83eb5af2fbba31c2ca49b3d1751f87305f2869ed5925031e645d4fd6b11fe1b5dc7f2dbea922a83f75a3e9fad7ae785bc4765e23d1e3bdb660188c489dd1bd2b695d33d17f2ac3de5a367a918424ae91e1f6bf0a2faf6e7335e48c87f8b6915d1dfe8fa17c38f0bdcb2b29d46e22648d8f2ecd8c71e8066bd0f5cf1058e81a4cb7f76d8441f2a0eaedd80af9d7c43ab5ef89f5396f6f1c9690ed441f7635eca2a941bdd9856ab1a6ac96a62a7916b7e20977b4847240e01233570dbdbdec5b5ed2252a3e528306a2fb3b4f2b3c6728b81b8757c000f3e95a36c04795c718e99ad9bb6c79f622b2b5b9d2d4cfa76a5716cfd823f1f88ef5b09e2fd4d9d3edb219481860802e7dfa5515119721b2c0123a55279304818c29c67d6a5a53dc6a728eccfa07c11af68da86886d2cae945f3a96785fe57cfb0eff008521d3627972d1e641c6dee2bc12dae2586649a290c73464323a9c107eb5eabe1ff8a511d2254d707fa642a4adc2273281db8fe2ae2c560fda24d3d8eec2e3395b4fa9dd4ef0ac96f6ecc91c698047a9ef5a9a961b46ba0b8dbe4b639f6af29d135bd4bc496b3ea37302c16e662b6e33f314f7ae8a6d52e8694d14b362145efe83d6aa9c5d3ba7b9752ac5dadb22be891ed90ae3e689773363f88f35e4306bbaa68de20d4dc02659ae5cc91bf4ce4f35ed5a7b4769669e6b85773b98b1c727b545368ba45edf7dae6b185e707ef95e4ff8d76c294654f96e78b3c756a35dd482b2e84fe1eb3b5bcd0ad2eb5147fb54c9bdc02405cf6a2ab5e5d39b82b1315451b401d28ac9d3a4b43d0599631abdce8e4bc8ad25f365c05230aa073ffeaae7f51d6ece397cc92365898e1c15e47b8ab5ab5f18b555b8d8a54a8c06e9f4ae13c65aba35a4c11c24efc8d9fc20739af2e369fba7d1c30f1f66ea4bb7f48f41d3f52496d11ad674b9b51f74a9c95ae8edaea199408e504e391debc8fc27a4cfe21f0f7f6869970d63ab44c56745385909e4363b67d3a565cbf10755d0b5296c7515b4b99613b5b0fb5b3fef0e2ba69f3c64d2d4f2e5529c97bda1ee6ddc95aae42090e01f5af2bb7f8c91a32acda55e03eceae3f3ab23e2e24cd983479ddfa0dee147f5ade4df544ab773d4a41b828c7b8c571be3cd724d2f46f2eddcc73cedb094e5957be3f41f8d70bab7c53d7ee8886da182c831c0318f31fe809e05323b7d46ee4825d4a592491db73977c96f6fa529129e9634745b09e4b41712ab24857003758d7d3fa9a82eaeb45d0b7dc5d4a1e48d4f971af24b7f324faf6a9f53d4e3b4b23196db1c7f3c80719f63f53fcab8af0cf86eefc71e2396e2e5b65a44dba5fc4f0a2a60aeeef62ed6f763b916a7e23d53516511a7976ec32b1444a803f03d69969e14d635968c90719e1dcf23f1af74b0f0668f6712ac36b1a003070a39ad68ec2d6de3db144a2b4574b412a49bd59e3517c34bb4443e68ce7241e805457be069ed2da478a5dcdd42d7b2ccbb14d605c202ce5ba0ac5ca57d4e9f610e5d0f158ed649acae2e4a1df6e0ac871dbb1fceb95d68a4b324e830cea378f7f5af5dbc4b6b4b6d66250bb2e2360a3a10db491fa8af1ab92af08fefa9c1aeba0db679b5a296852a72a96ce3afa7ad36941c1cd751ce5a58ded8892460a71955ce4fe556ad652415ee5b73366a8226f39639e2ae470b49111164f38c0ef532b5871bdcd517b3db4a121185382d83c62a1d5a726553215625780a30abed4fca0b558c2e1d3966ce4f4e0567e05c4eb1cd379617a6e5cd6514af7369bd0b562ef1396605594825bd2aa5c5fb25db3c782791c8ad6bab888d9c92e00964955d411d7afe9d2b18c6654054027716661db3dbf4aa8d9bb912dac161b126124c70a4e33e84d773e10f10d9f87f568e4b969613bb6c8d1fccb221f519e7d41ae3ae6d56dad91c0dcb200c3eb51c4c55431e5a2e5770c8c7a53705308ce50d8f5dd7fe2b6916ec4e8b64669ba79b70bb47e0073fcab83bfd6b51f10cada85dc8ced8da582e15075dbc74e95ca4b2333efc6327a0ae9ed679934cb2b7288967207218b609248049c7d0e33d3359ba3082d0b75a737ef330ae65f3ed5a20bcac8581acdc56eead0c1a5ea3756b14ab3c273b251d1b8edf8f158581bab78ec62f70c62948f4a43c9a79c631ed4c469683aedd6897be75b9c92a54a96c022bb2f05f8ae08efae2d35085164bb90bf9cd9393d94e7b7a579b8e2a413b0db83d3a1a4924ee29de51e53e8092e750965096b6712443fe5a4c703f0039ac4be59e6bfb87b89549588a828bb71df81f55fd6b0bc1bad6a7aaa5b69614dc48efb15bcd21947a9eb900549ae4b716114f0190c92433b40d2f42c41e4fd39aceac9bb2230d4dc5b6ce32eae259afa429cc8df286fee81e95bbe1fd056fa6512dc144c1676f415937b64d15ec215b699630f8ee01ff0039fc6bd3bc05a644e3e65ddbb0003e9d49ac6b4f96276d2a7cf2b0699e18f12c6049a5c4a6107314b718048ff77356f55f05eb5ad00d796ab6b727867819b6b7e1c8af59b75508028c0c7156b6e533bbf0aca376af73a5c63176b1e18ff0ab525b509f6fc1cffab6fba07a8ae6b5af87dab691cee59e23d76ff09afa1ee5727e95ce6bb1acd653a364e636fe5593ad38cad73678784a1748f068a19a244591bf798c127bd32d51a5b90bb70aa496c76ff27f9d42d75e76e68d8b345ce3d4606e157adcac56bf681d643bb8ec3a576adae78738da45e236b84c0007519a6b2e652a4e141f9b3514526e0493d304e7a1a9269028901209ea31480604491a42a3000e33eb4ba290fa418a43911c841f6cf3fd7f4a450be404246ef63ed5168ae12f0c0c7e49971cfa8ffeb669a0b9369bafdd781fc46cd6ff0035bcf82c1bee91df8af7fd2afedf53d321d42070619937027b7a83f4af9fb5bb41756e8187cf09c67fd9e9fe15b3a5f8925d2fe1e5c6931cc4cf7174d1a8cf291ed058fe3c0fc4d4ca0a7a9d587c4ba774f61de38f119d7b5968a294b595b9290853c31eed5cbbc72330813e5671f33771f4ad3b6b34b2b692eae87cebc471fa9f5355ed048646988cb1cfe356b4d8e69cdce5ccc7dac46284a28f9507208a445024705b9e7000cd4e8afe6c8147cac32091ef9e948f03094ee621b383c8ef52046811dd95438dc7d3bd51997f7a53afcc6b4dd11673f330e9820d67ea80d9dc48e10b6704027ad344c8881e339c126a6050c6303277639aa56f319cb6e0148e98a9f203c6075de7f21577333d37c21adc573a5b594cf1c32da29e4f01a3f5fc3bd5dd2ee4f89b5631db1ce9566c0c92e3fd738e8a3d87535e5f140b717415d9b631c10bdc1af68f0ec9a6ae982cf49411adbfcad1b8c1e47de3eb9f5a29d24e4618bad350d0e2edf526b8f105c58eb504de6594e5a18d1770941e338ef8ebf8d77336a2268e268e311b9006c0b8da31d3eb524b13c059c4424cf46ee2a5f0eda4171ab996e1c175e553b13ffd6a534a0da4f50c3ca7898a8b8d9236349f0cdb3d82497c84cee77100fdd1d8515d0ab6d18a2b2f74f5d5348e1adeda6d5f4c95ae608d8ac8c99c64363a363b7d2bcdaffc2f7b6d72d1ddd9978cb7cb3c20b29fa8ea2bd77c3efe5da185987985cb14079ad1b8b68e4e4ae09ea4571aa56d60773a8e5eecce174dd1efb48f04ea2d6fba2b8b9db8dbf7827723df04d79df8bce87068a9650c2cb76f92d29239c735eeed693409f23068cf635e61e2df02b6abaac77315bcf3db004b2a38241cf4c7a56f84aae93719adce0c5e1a53929c1e88e43e1ce97a7ea77b6fa76aa5e33323bc3221c367a056fc8e2bd1aebc0561a7df448b34af130cb6e0077f6ac8d03c302cb57b49974fba89a3901deebc281eb5ea375141771e19b047dd3e95963b9acd537a9d385b5939ad0f2bd6b45d36db5bb77b5016de26fde232f5ef9acf7d6836a9733f511a6107a13fe1fd6ba3f174315be8b792ee569d410b8183c9af288ee649c4ce09e77673dba0fe42b2c349d485e4b52eb5a12b266f6b92ff0068df6d89cb2483ce2076c0e9f5ce6bd07e1ae9ab61e1f91b82d24cd961df1c7f3cd796787e7132bcc4e0ac4c4027b024ff00415ec1e0a68ed7c21a72c8e033421ce4f73c9fe75d0f4562a82bb6d9d7a1e3d2a327e6ae7aff00c67a169d3ac175a8c71c87b75ab169e21b0d40c82cee639822ee62a7a5394adb9bc52b9a5718da7902b1ae1330c8cbfc42b9fd67e22e8f631ec632cb2e70638d79acd3f11edae6d23f2eca78cb9da8248d8027ea07350e127aa457b582d1b38ff12de4b1eb054b7cadf2927d41af3cbe4db212bd09fcebd1fc67a7c92da3dc344c8ec3cc518ef9e6b82d4a38ede08e3deaf331dedb4fdd18e87debae87747975b7664d14515d27313c4e08085475c93ed5a76931b2923980236365430fbd8e6b1958a9c83835335c4b2801999b1c0c9a96ae34eccb97372d35e4a53e4f31892a3a73daa3823265cc8dd3a83de96122042cc159c8e98c914ddea23077658ff0fa516e8569bb0b892493258ed54e1454e0196db3b82ae4123b8f5350ba79d771c47e5dd81f37415206586d67470ed333000ff0eda05737b56bdb7d4b4ab5d3add111ac23da8c83265079258fd7a7d6b9c65119dcb9c630c0f635ea5f09bc3961a9da6b6f7d04225862564967e115483cfe95c5789744b8d2d4b7d9c88e493218039c11c03fcc7d692d1d84cc184bb5cef8073bb088467af4af48d57c3b2c3a1dbca636db0423cc468f6f181920f43ce6b3fc13e1869efad2f6e2da648570fe5b0c990e7ef63d0022bb0f88a65d2a4b882374fb34cb149e5ab10083c91d4903e5a53d5d811e49a90791514c85820daa188e075e2b2d40c8cf157e567b976214e01042a8ec7b55390008bf290771c9aa8ab206458f9bef67de9ac314e51cd35bad508234dee1738cd58b4b5fb449b4b05e7f3fa5565255811d457a1fc2ef0fa6b9e2169ee1775b5ae252a4705bb54ce4a316d950839c9451e8ff000b3c1c341d3fedf7283edb7233c8e513b0ff001ac3f14e837116b5a9331436ef3ef4527e66ce1988f619af5fb740aa0a8c0c550d4ada3bd3343f2198283c8ced07807f435c0e72f88f4fd8c6ca07cddaa5d893587b8c12a9fba18ec1781fa0af5bf006a1a3cb6a0dbea10bdc630d113b5d7f035ccaf85e2b1f1d4505ca87b799c9208e0915d8ea3f0ff43ba717315a6c2393e49da7f0f5a552709593269539c1b71e874f71e2dd1b4df96eaf218c8ebba4031fad548fe267859e4d8babdbab7fb4d81f9d7cfbafe8f72d7f75e4a26c89cc7819cfcbd6b99304aa7250fe35d10a4adb994eb3bfc27d55378dbc3a21699f58b2d8464113027f2ae5354f897e1840f125df9db971b9549fe9585e24f06595bfc23d3ef23d3e283518c46f2c801dc431c73f98af1ffb348aec1909da70454470f096ad952c4545a25b9b7a7c96cd25cf96d8f998824e370e71fd38abc25c5888875dd8fc335cfe9e87e76e70a41c76ad6328f941380324e2b79596c7992d646a79982a800e40e3754a0a302c5413d3eb59d0cc76191ba9ce0fb55b67f2e00ecbcb67078cd6604f12798c0b9ea7a13c01ef5993b79643c248911b7a9f706b45250908f9b1f2e303be79ef59f74c0de103183d855459323566b98afec3ed70b72ca5644f461fe7f415562d2ef2dc5ade4906db6b80d22c87f8f69c631f5c566e9d38b3d4d7746d2c329daf18ee7b55db8927b19a4b28cb3104eddcc5962527b67bd569d037d474b349336d624807bf7353c6a13f8f685383c556b2521d7bec5c9279e7fc6ae485627393c13918c726a5e80b5d494ba99090c73823d72314c6919a41b891d09238cf6a25426e5b606e4e4906878b84f9b8538229144d3066727cb39279eb593af3379b6e0e172982cc7b8ad471b366e2153b63e9595e23b73241164ae471b87f9e94d2bb1357456b3b75490b79aaf91d0559da15d1473b41c9f7ac2b26b98663956feef26b5ad4b6c24b862c7bff004abb5886ac5c8a431dc230f5af50f06c9035d493b31133c02303b100e6bcb14339ce31e95d1787bc42ba55e43248a196360769efeb45dad512e2a5eebd8dff001978ecdb4c746d14f9fa9487612a322327f99ad6f0fc77da469f662eae9a7ba8c65dcf63d71ef59165a169235cd435e827cdc5c4ed25a81c08d4f4fc6ba8b78248ad7ed57c150819209e0fbd71d6ad147a787c35ad63acff0084a20555cc6c49504fd68ae66cf4ed575580dd59470080b10a662416f703d28ae5f6f50eee5a2b466145e28934fba8edb5c8bc876e62bd832619476607b576d63af196019912ea23d244233ffd7ac24b3b7bbd2d2dae20496df9c230e833c63d2b96bdf086a1a7cc6ebc3b7d247ce4c0cd8fc3d0fe35dd5284a1aa3e7b039c42bda0d6af63d7c5f5bcf6ff00b99149c7dd3c11556cc31279efcd79147e36f106972343aa68be7f9632c554a363d7b8ad7d37e2ef87dc859a5b9b360795963dc07e2335938ce56763d8855a7ac6fa9dd78aafcd969652338790ed5c7515e47e22d6a6d3c5b48b2c8e8f3a2b9690f4ef5d95ff008b7c2daf2a1935fb64551d338fe75ceeb9a7f82f567b42de2cb686184ee922dbb84873ec78e38ab8c7def79195495f44cad3795324d6f6f32c9952a0a90724d7136c8f6fa8cb6b3298d89605587f78706bd0a1d7bc05a3c9149fda935f4b0b7c86080e48ec09eff8d72fe2ef17683e24d42396c34eb8b59a338f39c81b875c1514d45eb65a0e36b6accbd3ade685258b05719e7dbbff005af65d37c3f1ea9e1ab18e4b99e30b085db136dce38e48e6b9ebad16ca7d2acf54b72c90dfc5b194b708e4751efb85755e08ba79bc3d6c24043a82ac0fae6b16ef2d4eda504ae8ceff008579a3077f2f4d42ce36b4923b31faf5adad1bc39a768d1986dad954152189e49fc4d6dcf70b1267bf4a82263238c9009ed54f7d59b46292bdac72b67e19d2e3d6679c5bc627690b2bb2e483ed9ae8469116f59673e6943950c3807d6b1b51bdb7d3afde637481a32329bbb9ade92f81b6e7838cd4a7dc765f64f39f8992a9d3fe5ea49000fa57875f432dbcbe54a06e00135e9df1035332492053958b91e84d7995ede3deced3c8a0337a7415d7878b48f2b1324e4ca7455a8a24921279c8350bc4549f4ae8babd8c1c1dae463ad4c2e0a81b51430fe2039a868a641279accf9249c9e79eb52c2ab2ddaf9bf2465be623b0aae2a6138da434618e3839a00b96a53ed40bc6ae587f19fd68bf322cab092ae911daa4743dea04983c3e5b1085724301927daa379da47009f973c0a9b6a3b9d5e8776b6f748245692168f0f19cfca7b8fa1af5a4b285f43b37484b899b6c5199024bb8e000cde9c7a719af0ab2bc963d4012dc152a79ea315dcf8531a8f8827b249a4b69e40cd234b2700018c27e273f854f2ea367a2ea96d73a24ced125bc04ca648e38dbcc917e5e8ce7aae474c5799f8c7c513ebb6305adddac5f6eda649e5071b704e001d8e2a43a85d451eb76d2ea2fa85c4711596edcb3658b040ab9ebc77ae0de79790012a0f3cd34bab2495c23c465b79648e55e0a83d571d8d5617005ac5198d772bb36fee738e3f4a792a25214fcaaa4819ef8aa8adf39273dea900c39dd83d69add6a668f6aee2724d424e69802824e057d1df0eb401a0e831e53f7b3a2c9313ea4671f8022be7ad3e74b6be86692212a2386284fde03b57d34756d9e0a93522ab14b2c1bd501fba586140fcc0ae4c55da51477e052bb93e86fff006822ed1fc3dab97f156b8de1dd6b4fd6c03259cb8b4bb8c75c124ab0f7073f9d6998b36f656ec707e40c477c0c9fe55cd78f6e6c8a14ba6062b545902e7abb138fe46b8e94a5cc77575150ba342ff56d07c437f11d365325c4437b308caed3c7a8eb5d0d84a5a300f515e67e0b766f07de6adb7f782e5dc81d768edf957a0e917b05cd8c7711c8bb18021b353555a44d095d6bd4e5fc43e0496ef559efecd4bacedbe488b100b7a823a7e559961f0fc5b5e457fac5998ec613bbcb8c9919d8740401d2bd465bc8ed2dcbbe3fd919eb58579aecf72ad028da78f9514b363f0aaf68d7a94e09b23d5fc47a1dcf87ee2d2f1e444b8431889e260e73c70315e6971f0feeac9d63699444e3e4768f39fc41fe95daddea36b0e9824b95b8051f019ad9fe5fd38ef54efb5817da65ac093659a400313e9fe45542a4cceaa5a9ca37c37d65ada496d5eda6d83fd5a12a4fe6315c4cde65bcd2c33c6d1c8a70c8c3041fa57d13a0c17037cb2be547ca001f7aa1f11f80b4bf14465e787caba03e5b88f86fc7d47d6baa33d7de38258556e68ee78244d940303b75abb7380a1470d9fe1ef563c47e19bef0bea496b769f231cc528fbb20ff001f6aa123b492c60b0249c93f4aaf43925169d9979cb8892301be63dc7154eed713a48181078fcab44c40bc6776dc28fc2a96a10b0b66705488ce781ebc7e5c5088906991302f745485cec43f8e0d5cd6484badc57e6318c9f5e2ac5ba01690443a98c11fceb3b5a98b4c23043144c0f7a056d2c3ad006888032c46483d2ad4d110a09e3207f1707debb2bef02477de1ed3752f0ec6accd6cbe7445b976c72413dfdab8abbb79ece4f22ee17824539d8eb834af73595294371d37fad1b464608196cf3c5239c63a020e79343ed38da5b8ed8cfe3934d94868c145c007af1cfd69b245965dc911db9600723fcfb554beba0c635c1c640352493b10bb5ce7154ae81debb871b720e6842b5ca8ad18b898ca70a58a8fa55f8003f3a8c0e807b56669b791798d0cdb436e38245694b3c308cbca147a035a58cda772cc67048f4a73c6af19e2a94379bb2de5b08ffbcc71520d42d8b615cb76f94138a04773e05f10693a758dd47aab8596dbe7849e4b29ec07ae7f9d765a1d95f78c2e96fef54db6908731407ef4bf5f6af0c6d41b4ed46def6288398dc380e99538ec41af45f097889648e49eeafef1b54925ca2873e5aae3a63a015c7569455e6cf530739554a95ec7b8c08a9104450aabc003b0a2aae9f79e769f04d210af220620fb8a2b91389b3834ec7308d1e7691b47418ed43b8881230c2b85f877aa6ada9789751f0eeab7292bd9a315971939560319ee2bd027d2aea35398f7aff790f3f957aeead9da5a9f30f2b9a5cd474653956ddad19de5dd2f4d98af2fd6f46d0afbc556da75daada2dd1245c45804360e011e84e2ba9f14cfade9310fb169b2dca32fdf087e53ef5e513d9f88aff52fb4cfa75e993391889b8ae8a95e138251e87260f038aa55e552a69a7dfe676971f084a93f66d491c76f32320fe99aa3ff000a9751321ccf6cc3d771ff000af5bb32cda7c067c89bca5dc3a61b0334eb8d434cd2ed9ee350ba58428ce0f53f41deb24a2d1509e31cf95b3ccf43f8770c3a8c9697f730a79782ee01206464573dad6916b61af3a12d2408fc3c5c122b6bc45e3c6d42ee41a5426da12bb5a47e5dc0fe55c6deddc9e433bb966c7249ac23426e4db7a1f411aca34d47a9a9a9eb325a5c456765a8bcf690c9e62c4a484041e0e2bda3c3f791cf6105cc18093287c0e809eb5e0be1ad2e6d567104510926705947703fa57a8f86a5bad12fa4d16f12411c7cc5395daac7f880e7a7bd655a9f63b30d5b5d4f4192745db2ccdf20ae775a586eb518e58ae6e6095d484f2e62013f4e99ad29da1b91124e3f744e187ad4a74dd3e200c71a6ceb839ae547a5a48f33d4744b4d33564b9d4ee25b9663bd9f3fa715d7d96bff006eb62df669e380a911bc88541c7a66b4aea2b264dab0a8f754e7f3ae63c53afad969b05b42171e6107d47157f1686334a9dda679ff008d668c44554e59f03f1ae00e7a56c788b52fb7df7ca7289c03eb58fd4d7a54e3689e44dde572ee9e416646e846453e78b048a7c5110a85060639ab2f009140ef4dc1dee870c4a4b965b188e30d4dab573692a316db95f5155b1546774f615704f2714869294e680155cae7048cf5a7c0f1acaa6442eb9e4038a8a941c11f5a00ddd3e583fb56dc35a12acd8da5aa3d40c90de45217dc658d641ce31914fd3f3fda962c3ef16ec79e945e812689a75d824b0df6ec7d0a9c8fd1a92026491edbc3dbfee7da6e02e47f75064feadfa564cee4c5bf272e49ad4d724d96d6962bcfd9a142fc1e19b2c47ea05645c13e544a7d298091e560772339c2ff009fca91132403db93c53b00411839c93b8ff4a015ea4fe5400d94e01008c7b54257e5cfbd3db2cf8abf0d879e416c845f4e3349b48119f0a9695463a915ecba978d34c3a5dada5a452cbe4801832e10b28e0f3d81af3db682de343b220adebd4d049f299321704d7355e5a8d1b52ab3a77e5ea7a2dcf8f352bc92d64b3b3585e3c860e7706cf1c7735c0eb7a8de6a5a9bcf7721323b12cbd811c631dbbd77d656286d51c87c3461f0aa38c8cfe24e7a8e95c378920367ae5ca84d98937edc86c03cf519cf5a518463b234ab52735ef33d27e1708e6f0dddd9300479ac181f4600d73f6bae5cf82b5d9b4bbb0ef66642533d949e31595e09f127f606aead313f649b0b2ffb3e87f0af40f1df86e2d774b17d68c864dbbd25cff23583569be6d99bc257a6b97743c5cd8eaf7501b2754de40765201c7e15dac16eb040238b6a281ebc9af9e34bd4af3c3b7c89748ca532573d1abd1f4af17a5dc80b4a11b6e5893c01fe358d4a328bbad8e8a55e2d6bb9da5dc6c011348595f901b1815c7d85ad86a9e3992d5b6bdac311709818f3060545acf8be2b8d3c08ae0e5782c3b115e79a3789dad7c69653994ac4660b3374dca4f7aba34a4db6457ad1ba47d276f6f1468114018e8055a047200e57ad65dd6a30db7d867c811c920898ff00bc38fd401f8d5a96fe282fa0078f3c155f72067f967f2ae88d9226576cc3f1a786d7c4fe1c9a3541f6888f99093d438edf8d7cf08a45d049410ca4a907a839e457d422eb75cbdb8eb22971f81e7f98af0cf1d786ae748f104fa808b365752964753f758f553e869c66af639f1349db98c28db75c6ec91cf5078ed52aa24f28866cbc4725ced2338f7fcaa9db48509627a8e6ad5acf248af2b81800018e381fa77c5688e0dcb77d222a191548c26d503f202bae83e12de6a11437afa9c5148e81bca3096007619cd7296d6635ad4ad74d69bca7b8955778fe0eff009d7bc69b63716967141fda4d318d42879100271f4acdb3af0f4a334dc91c2e81e1ff0019f8467fb3c525a5fe9858911f9855973e991fa575fa9e8b65e21b541a8e9c0b81c127e65fa115bd1c6c63f9c866ee40e291c6d200e949abeaceb4e2972ee78478afc1977e1d98dc5b869f4e66e4ff00147ec7dbdeb989270dbbe520839c7a57d2f340970855d032b0c1523208af3ed7be1869ad29bcb4925b652e0c912e0a804e095cf4eb9a699cf570e9eb03c6e59b320080f14d9cbb9038c636f2457ae1f831a78393a95e1fc147f4a960f83da0a95334977363b19719fc853728a22384a8f53c1fece04c42caa24ea79ce0fb5598609a3e4db876fefb357d27a6781740d331f66d2edd587f132ee6fccd69dd785b47d42030dd69d03a91d760047d08e6a955bf41cb08d6ecf975c4f2603aafd09c8fca9cb148bf3090903a10bd3f0cd7a278ebc007c36a2f2d59a5d39db196fbd113d013dc7bd70e50a6e2bf7873f5ab52b9c538ca0eccad3b37940493960c38de31fad7a4f803498ded6d2fe7b691d02b6f918e10804e3eb5e7772227b5dc140dcbd057acfc1379affc21ab5acd97861936c44f55cae481fcff001acab439a0eccdf073509dceae4d62dcbf3e637d0714573c59f73286e14ede9e94579fec607a3f5899c97c3db968be34ea68c0ab4c6e0608f7cff4af7a66dd0e735e06ac74bfda1531c092e707dc3a7ff5ebdf368f289ec7b577d5dd3f239a8af758f257ec659b0540c927a0ae135af1e785adeca5fb3bfdaae412aab0c7d0fd4e062aff00c40d5574ff0003dda09bcb9ae310200704e7ae3f0cd7cf6cd90eb9c0041e2b4a749495d98d7aae2f951d55e78e7579c1486e8dba76087e6fceb97bcbb9677669657918f259d8927f1aae1b0d9cf5a8a462cd815d718c62b4385c9b77638c985c819355b5193302203cb9a9f181cd502ff69bf451caa9a18e0b5b9de7c359160f185bc5fdf81d3f4cff004af51d7ed2292286f241f2dbb12f8fee1e1bf0eff857896877c74dd7ec6f41ff00533ab1ff0077383fa135f414aa92c4c0e1a375c107a106a271bab1d14256d4f3ff00116af7de1b9e08557cfb3930d1b9eabed9ae9749f1769b73670991d0bb8c63d0fa566f8834a4b8d286973976b738f26e0726323a035e77aaf86755d2ae952d6713467eebf4c0f5af3fd9a7a3d19e82ad283bc7547afea9e21b0b5b632b6cc6380315e31e2ed71352bd778b383c018ed556fe2d558147955c0ea03f4acd82d07db0167f30c60b39edf4ad29d248ceb621cd58c9bd016e0a8ec067eb504637381ef4e998bccec7a939a581732ad761c66a47c28c0ab21b239aac9c5584c6339ab472df5245c1fbc2aa5d5b5bf966565dbe8055ace460551d4e4fba8bd3a536542f7d0cb38cfb521ebc54af03ac61cf7a86b33a0297a1e29334ee38e6803674d19bdb76ebb72c78f4535a1e1fb36d534c7b23c24774933313f757a31fcb158d60e5ae515080c411963c7435d8f85fc37ba0ba56d4618c4d188652a0b18b710781c648c7eb49bb01c85f5cfdaeeef6e338df212a07d78fd2aa5c1059171d1476ae87c4d6ba25a5a4234613b47b8c6f34dd6565eac07615cece08953273f2aff2a69dc07c8bb58aff00778a68e47afb5372492c4f53562342a3730fa66936059b1b22642ce3098e7deb5f6858fe40303a0a86d50a40abedcfd6a463c9ae6949b6558852428cc4e391839a64d1fef723bd48ea19463871d3de9243bd15b18cf1f8d24347aa787cf9ba06992032e444061189c638e39e4f1d2b86f1d43f66d7d0329513429b7924771dfe83f1aebbc192f9be1689491fbb771c1e1793c9e0e1b9e2b17e255be6eec650854b44cb9c63041cf231d79a6b73692f72e72fa569335edb3dd4b911c6db001d5dbd3e95dc687ae4fa0598b0bf679b4d9490a31f3c3ee0771ed5cc687ac69b696261be59e17593725cc3d573ea3b8cd6d9d5b4cbc96392e2f2def228d4e36c8236cfa953e9ec6b3a9ccdeda174b962ae9ea4fe205d22e2cfccf3a192261b95b2338af35bcbb86de52b652c8477cf4aef56cbc2faeaf97673912827f76c31dfd6b32f3e1b491c9fb8bb0549e0b2f14e954847491352327ac4e29f509de311ef214761496d117258e73d8d5ed4bc3d71a55f7d9ee1a36e32191b208a9522552ab8e3e95d129c52d0c2cdbd4f5df08f8962f14784e4d0ae18a5fc0a02bf7254e55ff302ba6bfd68def8592f628bfd2ace5494ae7a32361c7e5b85786e889226b56a209a580bc811a48890db49e7a735ebd1edd22caf3ece913432a3348b3120a305e5b1c7503f3ae692d7dd3b29d44d7bc750751596ff004f947c858b2a8ee72b9c7e95e53f14753bf8fc473587da58da05497cac746208cff9f5acdd4fc7fa9dfd9d9436f0c76525ab07f3a3396760303af4fa573b797b7baa5ec97779334f3be3748dc13fe144636d58ab568ca3cb11d69be48bca38dcc4f6e40ad9b554263503e566fd179fe758b0c8d113b4e370c1ad9814a5997cf48c7ea735a5f98e06ac473bc4b756ee656411cc8ef2a1c100373835f4469c2d8da4660bb92452a0862c1b22be6d78bed0043c12fc7d3d6bd43c11e36f0c3e92b657df67b49a00232921081801f787d69493d1a3af0b24af167a907c2e14e6a27724e08c1ae7e1f18786e597cbb6d52d38fe1128ff001ad18b54b2946639e37f70c0d439696676460b7469a631d6ab6aa33a748bd4b3228fc580aa175e23d32cbfe3e2fade2ff7e402b35bc57a66a57b05adaea104811bcd90ab83d3ee8fcf9fc2929e8128799d71da060d40cca3d2a82ddb30c89148f6a6999f3c0acdd64cda345aea6887029de6e2b33ed0c9c9a635e8cf7a5ed520f625ad4ed60d574cb9b1b950d14f194607debe66b98dacef67b3986d92ddda3607d46457d1326a21057cefe2fbb82ebc61aa4f03008d3f4f70393f9e6b5a33e76ce0c7524a2999e4868c2e4f1d49af7bf8710c5e1cf02c16fe5137172c6693b7dee9fa62bc26c23fb4cea8d14853702db476f6af5ab68fc59e2854834db71a669eaa17cd63ce3ebfe14aaca56e589380a50779d4d8dd9750d16c65686e6eada3973b8abb8079fc68a92cbe186810db85d4267bab9272f2b123268ae7f63dd9ec2ad4fa23cdbc7a7fb33e3659dd13b479b6f267db807f957d03110f113d85781fc78b66b6f19d85da8c17b65e7dd58d7b868d73f6bd2ace63ff002d6dd1ff003506bb67b2678f07ab47937c60b89bfb66c2024f92b01651db25b9fe42bcc1cfef0f38f97f957a87c659776b5a7c5fdcb727f36ffeb5796487f7a9f522baa97c08e1aff1b1b904e69300649e69b9207be68cf1d79ad4e721bb9f6467d6b3ed249127dc9827d0d497a72d8c9a974f8f64a77005b1c7b543dcda3a46e5e126621bd4a93db35ecbe1af165a4be0c82f351b9589adc0825673d5874fcc62bc77cbe339c9aacd6d3de5cc368932c6b248146f6c2ee3c0268921539599ea97ff00137428ee8449e7cb193869553e5fc89c9a7ea62db5bd01eff49bfb598c2371899f60c7a72720fe95e7bab78075dd2ad7ed17de579408194937633db15911693308cba31eb8e578cd64e3166ea6d1d85d685ab9b08ae6fbecf636b20ca8f306587ae7fc2b2a74b4b3b299637046dfbfebf87a573d7b35e6560bc498bc4a154484f03b607a554850cb2856ce3bd3514b626576eed909eb562cd499bdb15118f2f85e6af5ba04591bd062ad0a4ec894b01c03cd4f164ad5287323e6b480d895513096804e0b31e8a2b3894698cb2b640e83d6a5bb9ca42aaa4ee7e69b6d6678924e4f5c50f52a2acaec598892dd9b61507a5649eb5b3381b5874ac723e623ad265d3d838c5253b63633b4e3e94da4685bd3a5f26f617232a1c647a8ef5eb3f0eb4cb393569b4779dc48d13b3ec6e7852c5b9e3fba2bc741c366bd87e16424f8d97509a68d56e6d1a18c1e0bb98f2703db1fca934079b6bd3a3ce9042c4c107eed33d4fa9fcc9aa13a625c7390abd4fb0a76a2ad1dd3c67aa311fa9a4811e79318667380140c927da8012084bb01cb1278503249a9e486e52fd21b98a488861947520815edff000e7e1a2e9de5eb3adc61ef0e1a18187117a13fed7f2adef8936d632f85aee796d6292e2251e5c8506e5e4743584ab24ec8e98e1a4e3cccf0e4236723ea691871c74aed9be157883c857864b3977286dbe6153c8e9c8c5625ef827c4da60669f489da35ead162403fef926a7d9c96e8c2e8e7c8c8e9c74229a33e5138ce1bbd3e70d1b157428c3ef2b0c1aaef28549304e383424c0f46f0012fa5cc83cb212639dc338c81d78e9c1f4a8be242674fd35b8cc6ccbc751900f3c739aa3f0f2e815bf4d8a40d92648fbb8cf3d39c7a55af882c06956bfbb55db31e076cafd3bfe94acf98dffe5d9e7ea03a1181f29c7e155de08bccc6c1c8f4a9629333327f7d45048f94e39ad35460561060e51995b3d41abd6faceb5660a43792c917f71cee07f034436f3bd935ca40c6043879029daa49e326a388fcd83ce69b7dd0276d8517d77a8dc196e9f2ebc0e31c67352e1b70e6aba1db3364fa55b3d57a7ad6532a25ed144835eb1f28e5fce5c7e7f4af44f185f9b5f0ccc8bc35c11127181b4f5ec324608cfbd79fe848adae5b17059431240f615ade3cd4c48f656f1b330cb315618e7039c6060107a629455ca52b239841b9c92381522a9e0f1cfeb491950aaa0648e4e2a427e5f6fca95892263c1e6b6e599574a420e3705cfe5584f228e322afac81ede2127faa8a3f324f71d87e344772244c9982d09e3ceb8181ea89ff00d7aa6b6d13ca199172410723a8c532dae1e69669a4ce5b07e839c0ab16ede6dd46a49c73d39ed5b18b7a8f367149167cb4207a8c719a58ad93636c778f039d8e463f2a744f220dac854719f9c7ad4911562e3191c1fbb9ed4b5344da297d86de453bff0078d9e4b124feb55e6d3618e16920251d5bef21208ad28b698a439e43720e2925da6ca4c95c83fd28e66177719a778a7c4fa480b05efda22032126e7f5ebfad74965f18aeed9c47a869c4e3a98dbfa1ae42d4f2a1c9e473ed505cc0aecc1632cfd900a97184be246d1c4548eccf5bb2f8b7a0dd0025792063da44ff000ad37f1ef87bcaf33edd1118e8b927f4af9fae2c046a4e46ef41d05779e0d874c8e1b737898ce371ebdeb2a9429c55d1d74b15525746eeb7f10a6d4524b5f0ed8cd239183398c923dc28feb5cd681f0f755d6ef849a82b59da37cd24b30c337d17ae6bd634b96c609e5fb028f219b1cae0915a2796383c7d2b058851bc608ee8e07dada55595340f0af86f41445489ae244180f28cfe95d15e6ad1c50f976d804fa0e0566e1b675c0f53c553fb5590de0dd44eca7e658db711f9544aa49a378e1e8537764be73b124c8d93ef4543fda50af091b38f5005159fbfd8dbeb14bba38cf8ef0991b47ba38dc37a7e441feb5ea9e1dca787349380336b1138e9f74570bf1bac964f0cdadc05e6398fea3ffad5d47c3dbd3a8fc3dd1a563f32c02363eea4aff4af47eca3c05f1b3ce3e301cf8aa153dad97f99af309c9e7d457a1fc54bcfb578ce788ae0dbc4918f7e339fd6bcfa70a7e5240f4aec82f70f3aafc6c85d81dacbd08cd2f615029da853a953c5383f0455dcc9a2b5d2679e6a4b6263da71d08a1c73cd21e1327a5497f66c6a9031c5539d783fceacdb3892d918f51f29a8a719156f544b4d33d73c097d2f893478aeaf9c3bd9a9b70879c9c0f98fbe38adcb6b385659e25b7857e60e4ec1dc63fa5713f07ee7fe429664f759003f423fa577ef19179b94fde5c1e7d2b9da4752d8e6fc69e12835fb3f3ed8ac57d021db263871fdd35e352d9dd58ab7dae268e4700a8618c8f5afa20ef9ff007709c804ee6af07f114fe7ead71872e88e6343fec8381551226ec62469f3e739cf7a9e43b2c09c72c68036c45875fba38a4bff009228611d8735645eec9ac1018c13d69f35c87dc899c74cd301f26c59870718aab6ff003285c8cb1269b76d0495db64f6f17da6e1a5232a385ad06c2ae4fe155d24485161846e7f6a73230c348db9bd074142265a90cec0a31acaff0096a7eb5a731f971dcf6ace963f2dc67a9e69334a67b8784fc11a6dff008599af225f2aed55d429cb0c0e086edc93f5e2bcbbc5de169fc35aab5bb65e06c98a5c6372fbfb8af68d13c436fa6783f4a94c66491edd44302100b617924f4007727a579ff88b55f1178e229638ec606b389b3b923e10fb39e6b9294aa3936f63d0ad1a51824b73ccfbd76ffdad0e8a3c21a9d82626b7569253fde6121047e5fceb9cb9d0350b7492430968e338665e40a48efd9b4e4d3a689088e5df1c87864ce370fa1c574efb1c62eb92c371e21be96d8ee81ae1da3e3f849e2bd9be1678092ce18f5dd4a1cdcc833046c3fd5a9fe2fad701f0dfc287c47e23fb44eb9b2b66123e7f8cf615f48a6db784280060715cd88ab6f751d985a3cdef30b89c44b5e75e30d516e750d2f4d6902a5cde46b20eecbb8715d1ebdaac5676b24d2b854404924d7852f881b5bf1f58ddbc823822b94d9b8f006e1cd73d08ba93bbd91d5899aa70e55bb3e90b4d4ad2e6f64b485dc4d0fde468d94633d412307f0ad162a8a5ddd540ee6b363986ecc78e475a98c56f2e0cca2561ff003d3903f0e95ecad8f1cc9d62dfc29ab2b45a947637071c938dc3fe0439fd6bcafc67f0c20b6b47d4bc3d72cf120dcd6f23e491fecb77fa1af6a91a3e802000745c0ae3bc65a7d9ae94fa83078cda66e0ac4c4072a3b81c54ca3a0ae791f826ee7b6b6b978a289dd9d3264270a064f6ed5a1e31d4279b478e0b882dd5bceca98c1dca71c9c93d08ac1f0f3fee9dfe41ba4dc776063a7f8d49e2071f67b489828c966e3b703a9c572b5ef1af4304b1051f18da3934e9701c302793e94b732acf75b44491294002479c703af3dea295d8fafcb8e286845b8a5916d4c224611336e64cf048e86a2438907639a6c6c0923a0cff003a42c16651efe94986838e7321f6eb5655b31af73500f983e0e49e39a72bb2a018e47722a1ab8cdad10ffc4c83f23cb1bb763eef239c77eb599e20bc33ea31056dc55719c7af39e82b77c37a71bb8a58c4731be9597c8882850e3ae771e83afe55da587c19b5965173a96a331761931420003fe04473f956d4e937b0a52491e55036178cf5e7deac313b464e6bdc57e137871610a23b9dd8fbc26e7f963f4ae735cf843242a66d1af8c98e4c1720027e8c38fcc0a99e1e60a68f2b93001c0c54d7b3f936515b8eb280effee8e8292fed2e34ed43ec7a8dbbdbc88d87590638acf9273757524ac3193851e83b0ace316b726a3d0d1d26610dda48d12c81195cc6ff0075803d0fb1adad53558f55f127f687d923b4471b7c88061400b8fe95cddb12b27afca781576ce467bf857018e4803f034fa18a65f0d172c54e339c1f4a1241b5d76e32a3079a5595fca606d80c8ebbb3491465f716c0c71c90052351b0a8c391c2e0f43520476b662482c5700134d899515c0c12493f4a616da9bba63079ef4015ade151f34a7eef1b4719a925b8487788630b91d4d30ba3af0dc96e7039a4b87558b3cfdd140151622d1bcafd0fad7a0f8774c82f3c3b6d26f58e70a4061ce7938c8c57093bfeebcb5c64d7a2f84d4ae83001d58b63d00cd63555e274e1abba527657b9afa6ddae98fe53235cbb701feea8ab9ab6b77d0e917735b048e48e17752ab920819ef54963f99cb8c1cfe95bb2e95e478175bd4670326ca5110f41b4f358c29ae6d8ea78aaad5afa1f3fc9aef8975f98c4d797b74c7f815891f90aef3e1dd85f69915fc77f03c52395650fd48e6b2fe162fef750723b2807f3af459411705d54060b82c6baab4adee2472c5ca7ab6598612c84800e4fad145b48c6107e5fce8ae5362f7c5bb612f829fb94957f911557e0bdd0b9f009b7072d6f752263d01c37f5adff0088f07da3c137f8c12851b3ff000215c97c097ff8926ad0774bb071f55ffeb57525a3239bde472df15d00f193308c2308132c3f8b835e772a6fe319aeff00e27cf732f8e2f5648f0b10448c7fb3b41cfe64d7053b056c487fe02bc935d905ee9c151de6ec66b8f2a4df8217b8a988f9320d32e54bf555853dcf34d865054c60938e99ef409abab8a482393cd294fdde73d698c30d52e46cc77a109f9162cdb6864ec791f5a7cdd3a540ac23dac3b1cd4f2107207719aa44b3a6f8577061f16cd16ec79909e3d70457b04e9fbec1e84e31eb5e09e0db8fb278ced1f380c4a7d7208af7f976ba2baf4e083593475436333c43a843a4f85af6786030ca88407cfde63c0fe63f2af9fe5f9e4f524d7acfc50be16fa45a58af5b894bb0ff006547f891f957956304b7a56915a18547ef0b0461a4e7ee20e9ea6a85db99af71e86b45d85bda9271b8f35976c0cb725bf1a1f608f72d5eb62d157dea84658b00a39abba89c222d2e9d6e8cb97dc18f423bd0f71c5da372d411885385c13d58f7a94c4cc3716e3da98c4ab604723e3a0380297370e9f3aaaafa03cd1633777ab2bcb9524a05047f131c9aca918b484924fb9ad59e38e3521b238e4565ca096dc170a7a54b35a6757e1d95f54b9b4b5b8bb3cafd9922ce02c7904fe7935ead777b6c6c52dd22169a2dac44218c7cb772ab0529c73d7381f8f35e19a4ca63b82a1d50b0c06638c7e3dabd8fc2b676d2dba6adaacc60d1ac5de4b6877e37cec3271eb8e00f7aca4b5378b5d4ddb7f09c17b68b7dadc6b0c522b7976a242042839c673927a77c026b93d7bc2b69afed7f0f687e55bc47e5bc0c55a5e48390d8ce7ae7dbdebb4d3a793c4da9c9a9dd472476d0334504323eec838e6bad5b55f2c6e002e30140e2b9e55b95da3b9d94f0fceb9a5a2380d2b44bef0a3b5e693691cb6c902f99965dd29cf20281907f1ed5d7cda9092d1676478f72ee28e3057d8d3fca16ced1ee650cd9471d8fa579d78def3c43696c74e99da796f243b2ed46014c0c8c0e86b9f5a9a753aadec3dee8715f107c5efaa5e49a7da484dbc67e7607ef1ae1ed894981f5cd7709e0bb516d9769e6b8ea76aed1fad73f7ba14d6136e247964e3820915e8414631e54795526e72e664d69aaeab126c5d42ed401c22ccc3faf4a99f50d5248dd65beb8391c912b74fcea9ada4498219c363b350d091d65900a9e7becc8b16c5edf800adfdc0dbff4d4914db9d7b5336f2c2753bb647428f1b484ab03c118354c820713383fa5432b920abe1d71d4706a9377dc0d6d210ad9478fbcc38ce78cff004e9506b4ead7b146157684c63d335369ce8604298c018008eff4efd2a13a5ea1adeaf25b69f6b2dc4a30b851f771ea7b53ea56eac8cc2a63923c8cf6c9a924c92cddb1815dab7c2af148b7137910161f3797e70ddf4f4ae7efbc3fab69795bdd3e78b1d494257f31c54f320e492dd1990216255549638c01c9a8e7dc92a1390475cd58b395adae565451b90ee0a7a1fafb53356b992f2ee4ba98af99236e6daa1464fb0e94d6e4b05ddb9f18c03ce68690142474ef4c4218b003938f7a64d2792b9ee695b503bff8633e2f6fb56bc94186ca2089bb03e66e3f90fd6bd7f4e82d6fb12dcdf0b99cfcc56398854f40a14feb5f39da6a71c3e156d36372b25c5c79d3b138c0518503f327f0a6db4ef032b5bdc3a38e8c9210456ded7934b11cb7d4fa9628ae2ccfc92c93427f8243965fa1eff8d4ece244639ed5e1de1cf8a1ac692eb0ea04ea3699f9b79fdea8f66eff008d7ae691e20d37c45666eb4cb91260e1e3618743e8456f4ea465b09a6733f13340b6d4fc2d7374f1afdaad23f361971c8c751f4c57cf10b1de338afac3535b6b8d3278ae9435b9056507fb98e6be52bbf27edf31b50df671237941baedcf19fc2b3af116e8d0b7281ce79054e29606c5fc2dbb6e5ff2aaf6adba653b477e33ed5b5e26d534ebfd52deef4fd312c228e3456894e4330ea6b912212275601719c1ef939cd2270d20c9fbc41ee0f5e6a07795243b421cf7cf2052a4e49761800f5ef8eb433524504ee45ebbb8069e63611380149c0eb51a30d9b831ea780053c6115b7127233f85202bb42c2db7e173939e7a5559d808c2e41c7a1a9d81923c2e7613dfbd529f7197cb4e71d4e7a501b0a373499046ee7f01d2bd62ca33158dab00b8312fca3e95e596d108f96c9241c935f407c3fd22cef3c3963a9dcb07f936953d14a9c73f954545a685d1f7a441a7f84ef351b0967327925bfd5a91f7ab0bc4dacdfe91e0cd674d9d81cc462da4e76e78e2bb5d7fc5d0db836ba710cdd0c8a385fa5796f8d198f83efa6772cce579639272d59452e7491d1276462fc31db1da5dcc4e3f7a148f6c75fd6bd09d8493068df7294ce0579ffc3f8f1e1cb96001669cfe400aeefc2fa74da8eb3e4a1c2eccc87d06456d56376cca9cacec6f5868f3de5b09638c6dce28aefed2cd2dad9628c0dabc5159fb1f33a39d181e2d88dc784f54871d6063f9735e75f04a5305c7886d31c8963900f63915eada9c227d32f23e7e785d71f81af20f83b2a0f1aeb50eff00bd6aad8f70dffd7ab8f521fc49943e2fce27f16c51c780e96ca1cfe24d79dba98b2f24991e98e4d779f14e278bc7576d824c891b2e7fddc7f4ae124408dbe41be4eca2bb60bdd470d577932a3a79aa647408b8ead59c7e5cc899207563dfe95a92c4d2e5ee0e1072116b32773719200489380050c2021901ef522377ce6aa230381e86a619c7148a7144ed200bd474a92294bdb8e4657835424241e7d29f69294948e36b7069a6270d0b96131b6d7aca6071b66439fc457d216c77d9c609ce063a57cc933149d4f42a78afa3f46b87b8d12da6073ba30c3f2a97b971d8f33f893786e7c471420922084281ee4e6b8d91c2144f5addf15ca67f176a2cc7849367d300572924fe6de120fcbd055df431b73498b7d7064f973c53ac130a58f7aa9364bf1cd5f84795064f6142d5952d2362b5fcbbe703d2b46d555ed51718602b233e65c8279cb574488a14151c76a71d58a7a2488d4b2f0fc8fef0feb4d7693a2703d6a72a3f1f5150c922c4a49193e82a999955edcb1259b8eec4d54216e2e422fdc5e053e732caa59ced4feed4fe1cb537baf5b4006417c9fa0e4fe82b26cde9a3d3747f06e9efa7416d716a8cdb43972bc927fc2b466f0bcd6b62f6e2e679208b7cf05b823689b1f293ed5d2409b6347452791db81fe7e95b1e52b40ae403d3ae057139b676c208a5e198a28ad21d38ec8e48a25e15b2338ce73ef5d14971e5afd9a4f9a4fe1e719f4ac29edcdb869edd545c142109e3271c673ef5c2f81eeb50d75e5b7baba1fdb16f21c994632b9e777a91dab18d3bdd9d72acd25147a7230bb4686e10ae495ebd71e95cef8a678ada3860bf796228ff00b99d537249eaa7d0e2a78f539f4626cb5fb199109245dc40c91364e7391cafe22ad9d4ad65b574761776cca5a29211e61c8ec40efef4a29c648da4f9e0d1cf1d336db978c92c46324579ff008b34c9161339dcac39c6719e6bd69504d688e159599738230471d3fc8ae4bc5960350d15d107ef4292a480391db826ba62ee7973824795a1c2e07eb4e3bf3903f4af57f859e16b09347fed6d42d63b8b899c88c4abb846a38e01ef9cd7a63a697669f35bdba0ec0462a79d265ac3c9a4cf95a40dd0822a95c460838cee3e95f4eddea96332b25be990c8338dd2460027e98ae65fc31a4dc6a47506d3ada3901e422ed5cfaede99f7a71ab1b8a587695ee79df863c1f7f73691c9783ecd1372a9fc6e0f4e3b66bdcbc3de1eb2d1ac561b4b748f3cbb01cb1f527bd727707ecf287de4e24f9971c9039e9e95dd45a8c5347118dbef8c8078349d4bbbb37852e545d28a4638c54125b4320daca08f715286240a71038cf434ec994b4dce6b51f05687a912d3e9b0173fc48a15bf315e67e32f85525bdb3dd6842570a32d6cfc9ff809eff4af6d70c1be46e3d29eacb345b580f7a98cecf41ca9464b547c8237c6c55f2acbc104743f4aab7272c173c9ed5f43f8afe1ae9badcef2c5fe897cdc89507cae7fda1ebef5e2daef8235dd02f992f2ca57815b89e352c847d7b7e35d109a6ce2a94650f433a283ca000cb14f6e9561886e0c793efd6a38a40ab9231fcea71213d50903d0d449ea6646ce17a923d9aa7d3b5cbed03518efec6574917a8fe161e8c3b8a8f9e73923d08aaf222004ab1503b751f95383b3158f5fd43e2358eb1e08d46ea0125bdcac215e3618f9db8f94f7af0c563d3d79abcecf35bb26ec20c1f95be5623d47e7544312785cd7439396e4d8d1b6254c6de8c09ab7756e5d1c74354222c607383d7b56b30deabc9cedc9c7d2b1663b09624dc4512ff18c29fc38ab92c6b6f2043e9b8f3eb5996929b2d530c70923753d8d694f20173bc8dbc60e79fa5291a2d48a33bf8e4f5193db35348e5e360323f1eb5109910bcb248aa80f191f7aaa1b9fb4e55182a1efdcd2b0dbb13bb862a91727b9ec29a20d887279f5a9a0895230001d695b76480b4ec64ddc8508276935df786b59bd87407b012b0b7593700bdf3dbf4ae22355032f8cd777a65badae976c807ca577b1f7359547a1a51bf3685e4dbe702498c019603a13f5ac3f1cbeef094be59c2b488369ebd6b6bce11bb2c5163775cf7358fe3ab2bb8fc1cf76f09584ce8b91d33cd654fe34753bb457f03413c5e1137011bca323e5b1c67815e89f0e5cb6bd74a41f9a0cf27dc5666830adbfc0cb175182d216381c9cb9ab7f0f180f12310fc9b66041fa8adf76ccd2b347ac29183cf7a2991b80a47bd145d1b58ab31c875ec4106bc43e19b9b2f19eaf903cc50a3711cedf3369ff00d087e55edd9279f5e462bc4b478dec7e266b11e301e3ba51ef821c7f2a95b04b745bf8ce8a9abe9d22a8123dbb02dea0371fd6bcbc82066bd87e2fd979fa5e9baa2ffcb27311ff00758641fcc1fcebc7657dabc0c9ec2bb28bbc4e3aead3652ba191b49fad675c0c2ec5185fe75a8d19237b9cb67a7a550ba5e77118515724650f88cde86a78a4cf0d4c09c176e076f7a6ab0c608fc7d2a0e81eebbe40a3b9ab30db6dc9619a8edf61932edd3d6aeaca87e55e47ad34653935a2285caf21bf0af66b2f14dbe8be00d2ee994cb24c162551c608e093f957914911743c71561f55f3742b4d324dd9b79d9d48f42071f9e6934545e84bab5f99e5b9b827e7b895987e26b09490722a7bb97cd9703eeaf02a0e9c6314d8e2ac890480364f3524d75bd022f031554d14876459b200ddc79e99ae8801818238ac1d2d775d8e33806b6c024f1c55c0c6a6e2bb11c0eb555f7752335640233922a29781d78a6ccca138f908cf3dab5bc07b0789977f27cb60b9f5ac8bc3fbae3f3ab7e116dbe21b623aee007e62b296cce8a5b1ef9a7c6b2da019e7380463919faff5ae96250b02e3b8e3071ffd635cf698c7ec793c92dea39e47af1fe7f0add1265540c9e9d06eff003f5ae17b9e8436197710784ede79cfca37f7f7ebf5ae4b51bc83c3da81bf9ec96681bfd61421668fb65718ddf4fad7597332950ac41cff007b27f97f2af33f89baa35b698d1212ad232a01f2e3ae4e3bf6fa5108f34ac39cf955d1d77fc25b7576522d1a396799988305ec65085033bb38e476c559d3f4a4d42696eafb49b4b39f20edb72dc91905b8e9543c2bacc7ab68b6578082cc9b241c9c30ea0fe5fcaba2867026641c77c00c3f139e7f2f7aa7a3b0d4dcd5d8e3044b6e1146074ebdc7b9ea6b98d5e3314619dba1eadffd735d2c9382cea083c7f7b3c7b93d07b77ae77577ff00476217187195c6dce4fd47e79feb445ab99cd6837c017f8f0a009825269140f4f98ff8d6bbb179d9e625b1d4e7f4ae27e1bce638f57d3d8f305d1201f43ffeaaeb99879d927712dc2fb7635cf38daa33a54ef4a362c46be7a9dcdc038e3f2c0a733246fb597008c0c76e29c6ea308cac8172377031554bbcecb6f0f320e00ea0fbe7b524ec53a5276295cda192358644f30e76a347d08f7f7ab1a7f8726b1952f12ea669d39116f2531e98feb5b9a4e930dbb3c98c9279f4fc2b5481d100cd52bee6ce31d88e2bb596152a707a107a83ef538914a8dc79aa735a97ccb190b29fc9beb45acab3078674d8e38653fcc7b53e795ec4f2452ba2c4d279685d81d83a903a0aabaa0b94d3259ac006b9452f1293c31f4fc69f6af244cf693fcdb7fd5c87f8d7fc47434893476adf6677014e4c793dbb8fc28765b82bbd88ac75383c41a243771655d87cca7828e3a83ee0d63b6bd791de4961245179aa78df26372faf4e6a9c371fd89e32758f8b1bfc3baf6593a6e1f5ad2d7f43b6d46f61b872ea546d050fe22a64efa846e9591e73ad780356d4b57babf592ca2f3db76c50c00fd2b0eebe1ef886d94ba430dca8e4f94fcfe4715ecd0c5e546b1e490a300b1c9a463b0e474a157923296160f53e759e19ed66686e22921994e0a48b8355651d4918fe55f41eada469baedb496f7d007057e571c3a1f635e19e24d35b40d567b19240ea06e8dcff12f6ae8a5514dd91c75a83a7af4397272e727009e956542606dc1aab9ceee3a9a7c4417c1e0115d8d1c8cd2836f978e0135aa30618ce324c60fd38ac6b705930181239e456b23136b065b682a54fe1c565b1932b5e4624460460f661eb51adeb4b1aa18c198707f0ef5a3228625481c54290856255704f5a486a564564b5699809493c71e82a4364573b7156f046188a33939007e068b5c5b90c5b97e56241ab1cb704e3f1a8cbc7bb0c467da90cbc8006314bc845cb4851ee635dbb8b300335d9d85d5cea538b0480a5da75882f6f51ed5c76928d3ea36c9b78320c83e99af70f0bff66cda4ac3743c999598473f4753ecd5955ec7561e371743f04ec2b71a93ef3d4463a0fad647c71db6de00b78230155aed0051e815abb4b1bdbeb290dbea0f1dc444e21ba8c60b8f461d33f4af3ff8f174a7c31a5a290435d13f921ff1a54d2b9d5256897ac22f2fe05e9dbba08d5f8f77ff00ebd43e007f37c466662bbcc457f41fe15b36b6bbbe0dd8c03bd9c6707b74358fe0a81ad35e810c8aeac1b057e954dee656f791eaaaa08276feb453e3da179e68a0d2e52b7ff8f483feb98fe55e3cdff2572f7fdebcff00d10d45142d899ee8e87e26ff00c93d87febac1ff00a0d786cbfeb47d68a2bab0ff0001cd89f8c8bfc4566ea3f797eb4515b4b639e3f1156eff00d6affba2ab77a28acce81dd8559828a284448d13fea7f03592dfebff00e0428a29b269891ffaea927ff587fdd1fc851452352b77a28a2802fe95ff001f7f856e2fdf6fa51455c0c2a6e37b1fa5413f45fa51455333285e7fa81f53573c1fff002335a7d4d1456323a29ec7d00bff001ee9ff005ef0ff00e8cad4d5ff00d5ff00c01bf9d1457033d080fb8ff8f5bbff00ae3ffb48578afc53ff005c3febe9bff45ad1456947e326b7c26efc2bff0091726ffafa5fe695e8107dcb7ffae47ff416a28a27f1052d8d1bdff8fc8ffebbff00ed26ae36effe41b75ff5de1ffd0928a2a16e54f6323c1bff00239f8a3febaa7f5aea65ff008f98fe89451515bf88cda8ff000d16ae7adc7fd73149e1cff9085cfd1bff0065a28ac8ede87516bff1e47fdda78e9ff0114515a2f84cfab15bee8aa53ffc7fc3ff005cdbf9ad1454c8684baff90859fd1bf9564eb7ff00217b1ff81ffe8145152ca5b195e20ff8fcb3ff00ae67f9d75777fea13eab45149fc2453f89958f53f5aaedf73fcfad145665b203f71fe86bc47e2aff00c8cf17fd7b8ffd09a8a2ba70bfc438f19f023865e86a51fc1f5a28af4dec796cedfc07f73c47ff006089ab36dffe3cedff00df3fccd1456323191237fc7cc9f534bfc43fdda28a108493fd55403fd53fe34514981523fbff008d598bee37fbadfca8a2a56e23a1f0cffc846c7feba8fe46bd7b4cff00913753ff007bff0089a28ac6b6e77e18bb17fc7aea7ff5ceb82f8ebff208d27febbbff00e8028a2951f8ce9a9f01e816ff00f24aec3fec1b0ffe822b9ff07ffc86f4dffb69ff00a09a28aa662f747a87ad14515451ffd9	image/jpeg	2020-03-25 16:04:46.974133	{"width":"500","height":"305"}
\.


--
-- Data for Name: flyway_schema_history; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.flyway_schema_history (installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) FROM stdin;
1	1	INITIAL VERSION	SQL	V1__INITIAL_VERSION.sql	897589132	postgres	2020-02-21 11:10:14.464246	969	t
2	2	ADD CONFIRMATION TS	SQL	V2__ADD_CONFIRMATION_TS.sql	-1751113969	postgres	2020-02-21 11:10:15.451612	1	t
3	3	ADD WEBSITE URL	SQL	V3__ADD_WEBSITE_URL.sql	-600617139	postgres	2020-02-21 11:10:15.460484	234	t
4	4	ADD PERSON INFO IN TICKET AND EVENT IMG URL	SQL	V4__ADD_PERSON_INFO_IN_TICKET_AND_EVENT_IMG_URL.sql	-693515435	postgres	2020-02-21 11:10:15.742392	153	t
5	5	INDEX	SQL	V5__INDEX.sql	1452463591	postgres	2020-02-21 11:10:15.905603	405	t
6	6	ADD NOTES AND USER LANGUAGE	SQL	V6__ADD_NOTES_AND_USER_LANGUAGE.sql	-1341133418	postgres	2020-02-21 11:10:16.317014	1	t
7	7	ADD SPECIAL PRICE SESSION ID	SQL	V7__ADD_SPECIAL_PRICE_SESSION_ID.sql	1020935542	postgres	2020-02-21 11:10:16.323472	43	t
8	8.1.1.0	RESERVATION PAYMENT METHOD	SQL	V8_1.1.0__RESERVATION_PAYMENT_METHOD.sql	58521672	postgres	2020-02-21 11:10:16.372177	1	t
9	8.1.1.1	REMINDER SENT	SQL	V8_1.1.1__REMINDER_SENT.sql	-1404348920	postgres	2020-02-21 11:10:16.379499	295	t
10	9.1.2.0	ADD PROMO CODE	SQL	V9_1.2.0__ADD_PROMO_CODE.sql	429192879	postgres	2020-02-21 11:10:16.703067	223	t
11	10.1.3.0	ADD EMAIL MESSAGE	SQL	V10_1.3.0__ADD_EMAIL_MESSAGE.sql	1032714162	postgres	2020-02-21 11:10:16.933001	71	t
12	11.1.4.0	ALTER TICKETS RESERVATION	SQL	V11_1.4.0__ALTER_TICKETS_RESERVATION.sql	316451232	postgres	2020-02-21 11:10:17.014052	17	t
13	12.1.5.0	ADD FILE BLOB TABLE	SQL	V12_1.5.0__ADD_FILE_BLOB_TABLE.sql	-248931893	postgres	2020-02-21 11:10:17.037235	49	t
14	12.1.5.1	ALTER TICKET CATEGORY	SQL	V12_1.5.1__ALTER_TICKET_CATEGORY.sql	-1747306891	postgres	2020-02-21 11:10:17.09177	97	t
15	12.1.5.2	ALTER TICKET	SQL	V12_1.5.2__ALTER_TICKET.sql	-1125691468	postgres	2020-02-21 11:10:17.19973	1	t
16	12.1.5.3	ALTER TICKETS RESERVATION	SQL	V12_1.5.3__ALTER_TICKETS_RESERVATION.sql	1135807079	postgres	2020-02-21 11:10:17.207755	104	t
17	12.1.5.4	CREATE WAITING QUEUE	SQL	V12_1.5.4__CREATE_WAITING_QUEUE.sql	182341144	postgres	2020-02-21 11:10:17.320263	124	t
18	12.1.5.5	CREATE CONFIGURATION	SQL	V12_1.5.5__CREATE_CONFIGURATION.sql	-1446943384	postgres	2020-02-21 11:10:17.451451	370	t
19	12.1.5.6	EVENT MIGRATION	SQL	V12_1.5.6__EVENT_MIGRATION.sql	1006091918	postgres	2020-02-21 11:10:17.828545	149	t
20	12.1.5.7	ALTER WAITING QUEUE	SQL	V12_1.5.7__ALTER_WAITING_QUEUE.sql	1844898518	postgres	2020-02-21 11:10:17.983248	0	t
21	12.1.5.8	ALTER EVENT ADD DISPLAY NAME	SQL	V12_1.5.8__ALTER_EVENT_ADD_DISPLAY_NAME.sql	-925146126	postgres	2020-02-21 11:10:17.988421	1	t
22	12.1.5.9	CREATE PLUGIN CONFIGURATION	SQL	V12_1.5.9__CREATE_PLUGIN_CONFIGURATION.sql	1718746741	postgres	2020-02-21 11:10:17.993647	116	t
23	12.1.5.10	CREATE PLUGIN LOG	SQL	V12_1.5.10__CREATE_PLUGIN_LOG.sql	2080216856	postgres	2020-02-21 11:10:18.115557	65	t
24	12.1.5.11	ALTER TICKETS RESERVATION	SQL	V12_1.5.11__ALTER_TICKETS_RESERVATION.sql	135177719	postgres	2020-02-21 11:10:18.186206	1	t
25	12.1.5.12	ALTER TICKET ADD GENDER	SQL	V12_1.5.12__ALTER_TICKET_ADD_GENDER.sql	-511546992	postgres	2020-02-21 11:10:18.191962	1	t
26	12.1.5.13	ADD LOCALE SELECTION	SQL	V12_1.5.13__ADD_LOCALE_SELECTION.sql	-1603062214	postgres	2020-02-21 11:10:18.19721	290	t
27	12.1.5.14	ALTER FILE BLOB ADD ATTRIBUTES	SQL	V12_1.5.14__ALTER_FILE_BLOB_ADD_ATTRIBUTES.sql	-326843020	postgres	2020-02-21 11:10:18.522618	0	t
28	12.1.5.15	REMOVE DESCRIPTION	SQL	V12_1.5.15__REMOVE_DESCRIPTION.sql	-1976934199	postgres	2020-02-21 11:10:18.528668	27	t
29	12.1.5.16	ADD QUARTZ	SQL	V12_1.5.16__ADD_QUARTZ.sql	-331199594	postgres	2020-02-21 11:10:18.563052	1493	t
30	13.1.6.1	DYNAMIC TICKET FIELDS	SQL	V13_1.6.1__DYNAMIC_TICKET_FIELDS.sql	-1236850531	postgres	2020-02-21 11:10:20.08328	263	t
31	13.1.6.2	ALTER PLUGIN CONFIGURATION	SQL	V13_1.6.2__ALTER_PLUGIN_CONFIGURATION.sql	700424793	postgres	2020-02-21 11:10:20.35243	117	t
32	13.1.6.3	MIGRATE TO DYNAMIC FIELDS	SQL	V13_1.6.3__MIGRATE_TO_DYNAMIC_FIELDS.sql	2039163457	postgres	2020-02-21 11:10:20.480898	82	t
33	13.1.6.4	EMAIL MESSAGE ADD INDICES	SQL	V13_1.6.4__EMAIL_MESSAGE_ADD_INDICES.sql	-935757167	postgres	2020-02-21 11:10:20.569736	40	t
34	13.1.6.5	ADD EXTERNAL EVENTS	SQL	V13_1.6.5__ADD_EXTERNAL_EVENTS.sql	-1162879972	postgres	2020-02-21 11:10:20.61438	156	t
35	13.1.6.6	CREATE SPONSOR SCAN	SQL	V13_1.6.6__CREATE_SPONSOR_SCAN.sql	-1587828157	postgres	2020-02-21 11:10:20.780592	98	t
36	13.1.6.7	MIGRATE NL DYNAMIC FIELDS	SQL	V13_1.6.7__MIGRATE_NL_DYNAMIC_FIELDS.sql	678381694	postgres	2020-02-21 11:10:20.895632	11	t
37	13.1.6.8	ADD DIRECT ASSIGNMENT	SQL	V13_1.6.8__ADD_DIRECT_ASSIGNMENT.sql	657846386	postgres	2020-02-21 11:10:20.91478	182	t
38	13.1.6.9	CREATE DYNAMIC FIELD TEMPLATE	SQL	V13_1.6.9__CREATE_DYNAMIC_FIELD_TEMPLATE.sql	519890030	postgres	2020-02-21 11:10:21.10578	77	t
39	14.1.7.1	ALTER SPONSOR SCAN	SQL	V14_1.7.1__ALTER_SPONSOR_SCAN.sql	-1950303044	postgres	2020-02-21 11:10:21.190929	47	t
40	14.1.7.2	ALTER EMAIL HANDLING	SQL	V14_1.7.2__ALTER_EMAIL_HANDLING.sql	1028791199	postgres	2020-02-21 11:10:21.245885	91	t
41	14.1.7.3	ALTER SPONSOR SCAN	SQL	V14_1.7.3__ALTER_SPONSOR_SCAN.sql	2009763358	postgres	2020-02-21 11:10:21.345263	56	t
42	14.1.7.4	ADD SELECTED CATEGORY ID	SQL	V14_1.7.4__ADD_SELECTED_CATEGORY_ID.sql	1216362928	postgres	2020-02-21 11:10:21.408802	1	t
43	15.1.8.0	REMOVE ORDER CONSTRAINT	SQL	V15_1.8.0__REMOVE_ORDER_CONSTRAINT.sql	36357038	postgres	2020-02-21 11:10:21.414573	1	t
44	15.1.8.1	CREATE ADDITIONAL SERVICE	SQL	V15_1.8.1__CREATE_ADDITIONAL_SERVICE.sql	517301161	postgres	2020-02-21 11:10:21.422511	251	t
45	15.1.8.2	ALTER TICKET FIELD CONF	SQL	V15_1.8.2__ALTER_TICKET_FIELD_CONF.sql	855764629	postgres	2020-02-21 11:10:21.682875	105	t
46	15.1.8.3	DROP CREATE ADD SERVICE ITEM	SQL	V15_1.8.3__DROP_CREATE_ADD_SERVICE_ITEM.sql	1200909884	postgres	2020-02-21 11:10:21.794058	82	t
47	15.1.8.4	ALTER TICKET FIELD CONFIGURATION REMOVE FK	SQL	V15_1.8.4__ALTER_TICKET_FIELD_CONFIGURATION_REMOVE_FK.sql	1414469941	postgres	2020-02-21 11:10:21.883976	128	t
48	15.1.8.5	DROP CREATE ADDITIONAL SERVICE DESCRIPTION	SQL	V15_1.8.5__DROP_CREATE_ADDITIONAL_SERVICE_DESCRIPTION.sql	-447519400	postgres	2020-02-21 11:10:22.020787	96	t
49	15.1.8.6	ADD CATEGORIES TO PROMO CODE	SQL	V15_1.8.6__ADD_CATEGORIES_TO_PROMO_CODE.sql	-232370106	postgres	2020-02-21 11:10:22.12406	27	t
50	15.1.8.7	ADD SRCPRICE VAT FINALPRICE	SQL	V15_1.8.7__ADD_SRCPRICE_VAT_FINALPRICE.sql	-1341782632	postgres	2020-02-21 11:10:22.155369	2054	t
51	15.1.8.8	ADD FIRST LAST NAME	SQL	V15_1.8.8__ADD_FIRST_LAST_NAME.sql	-663511866	postgres	2020-02-21 11:10:24.247426	2	t
52	15.1.8.9	ADD SENT PROMO CODES	SQL	V15_1.8.9__ADD_SENT_PROMO_CODES.sql	2044597324	postgres	2020-02-21 11:10:24.253907	1	t
53	15.1.8.10	ADD EVENT STATUS	SQL	V15_1.8.10__ADD_EVENT_STATUS.sql	1169754971	postgres	2020-02-21 11:10:24.259123	180	t
54	16.1.9.0	ADD RESOURCES	SQL	V16_1.9.0__ADD_RESOURCES.sql	-1764291429	postgres	2020-02-21 11:10:24.448695	196	t
55	17.1.10.0	ADD STATISTICS VIEWS	SQL	V17_1.10.0__ADD_STATISTICS_VIEWS.sql	-1445773461	postgres	2020-02-21 11:10:24.651786	36	t
56	18.1.10.0	ADD INVOICE SUPPORT	SQL	V18_1.10.0__ADD_INVOICE_SUPPORT.sql	-2118286931	postgres	2020-02-21 11:10:24.693016	61	t
57	18.1.10.1	REDO INVOICE SUPPORT	SQL	V18_1.10.1__REDO_INVOICE_SUPPORT.sql	2032506716	postgres	2020-02-21 11:10:24.758324	40	t
58	18.1.10.2	ADD SCAN AUDIT	SQL	V18_1.10.2__ADD_SCAN_AUDIT.sql	-1136413905	postgres	2020-02-21 11:10:24.80435	26	t
59	19.1.11.0	EXPAND CONFIGURATION VALUE	SQL	V19_1.11.0__EXPAND_CONFIGURATION_VALUE.sql	1327417327	postgres	2020-02-21 11:10:24.835444	1	t
60	19.1.11.1	FIX USER CONSTRAINT	SQL	V19_1.11.1__FIX_USER_CONSTRAINT.sql	-1792514578	postgres	2020-02-21 11:10:24.842431	30	t
61	19.1.11.2	ADD EVENT ID FK TO RESERVATION	SQL	V19_1.11.2__ADD_EVENT_ID_FK_TO_RESERVATION.sql	-747800201	postgres	2020-02-21 11:10:24.87673	9	t
62	19.1.11.3	ADD PAYMENT ID TO TRANSACTION	SQL	V19_1.11.3__ADD_PAYMENT_ID_TO_TRANSACTION.sql	-1159401760	postgres	2020-02-21 11:10:24.889936	0	t
63	19.1.11.4	ADD ORG PROMO CODE	SQL	V19_1.11.4__ADD_ORG_PROMO_CODE.sql	-737071865	postgres	2020-02-21 11:10:24.894463	34	t
64	19.1.11.5	ADDITIONAL SERVICES	SQL	V19_1.11.5__ADDITIONAL_SERVICES.sql	687373071	postgres	2020-02-21 11:10:24.932815	30	t
65	19.1.11.6	ADD CC TO EMAIL	SQL	V19_1.11.6__ADD_CC_TO_EMAIL.sql	2032516036	postgres	2020-02-21 11:10:24.967166	0	t
66	19.1.11.7	EU VAT MANAGEMENT	SQL	V19_1.11.7__EU_VAT_MANAGEMENT.sql	164940220	postgres	2020-02-21 11:10:24.971815	115	t
67	19.1.11.8	AUDITING	SQL	V19_1.11.8__AUDITING.sql	-1325096817	postgres	2020-02-21 11:10:25.094367	59	t
68	19.1.11.9	CODE TICKET CATEGORY	SQL	V19_1.11.9__CODE_TICKET_CATEGORY.sql	1956665927	postgres	2020-02-21 11:10:25.157768	38	t
69	20.1.12.0	USER CREATION TIME AND TYPE	SQL	V20_1.12.0__USER_CREATION_TIME_AND_TYPE.sql	-1987687884	postgres	2020-02-21 11:10:25.200266	232	t
70	20.1.12.1	REMOVE GEOAPI SERVER KEY	SQL	V20_1.12.1__REMOVE_GEOAPI_SERVER_KEY.sql	-293519249	postgres	2020-02-21 11:10:25.44376	6	t
71	20.1.12.2	ADD SPRING SESSION	SQL	V20_1.12.2__ADD_SPRING_SESSION.sql	-653652449	postgres	2020-02-21 11:10:25.454981	163	t
72	20.1.12.3	AUDITING EVENT ID LAST TICKET UPDATE	SQL	V20_1.12.3__AUDITING_EVENT_ID_LAST_TICKET_UPDATE.sql	-718215301	postgres	2020-02-21 11:10:25.623212	32	t
73	20.1.12.4	FIX STATISTICS VIEW	SQL	V20_1.12.4__FIX_STATISTICS_VIEW.sql	1100468646	postgres	2020-02-21 11:10:25.661172	5	t
74	20.1.12.5	ADD TICKET WITH RESERVATION AND TRANSACTION	SQL	V20_1.12.5__ADD_TICKET_WITH_RESERVATION_AND_TRANSACTION.sql	1648112566	postgres	2020-02-21 11:10:25.671716	4	t
75	20.1.12.6	CALCULATE AVAILABLE SEATS	SQL	V20_1.12.6__CALCULATE_AVAILABLE_SEATS.sql	-1125568057	postgres	2020-02-21 11:10:25.681096	5	t
76	21.1.13.0	ADD VALID CHECKIN FROM TO TICKET CATEGORY	SQL	V21_1.13.0__ADD_VALID_CHECKIN_FROM_TO_TICKET_CATEGORY.sql	1105664840	postgres	2020-02-21 11:10:25.690767	1	t
77	21.1.13.2	ADD VAT PERCENT IN TICKET	SQL	V21_1.13.2__ADD_VAT_PERCENT_IN_TICKET.sql	-1427376705	postgres	2020-02-21 11:10:25.696746	28	t
78	21.1.13.3	ADD TRANSACTION FEES	SQL	V21_1.13.3__ADD_TRANSACTION_FEES.sql	807645751	postgres	2020-02-21 11:10:25.73093	193	t
79	21.1.13.4	ADD BULK IMPORT	SQL	V21_1.13.4__ADD_BULK_IMPORT.sql	-241095114	postgres	2020-02-21 11:10:25.935113	130	t
80	21.1.13.5	ADD REFERENCE FAILURE CODE	SQL	V21_1.13.5__ADD_REFERENCE_FAILURE_CODE.sql	1575997572	postgres	2020-02-21 11:10:26.070461	25	t
81	21.1.13.6	ADD REFERENCE UNIQUE CONSTRAINT	SQL	V21_1.13.6__ADD_REFERENCE_UNIQUE_CONSTRAINT.sql	1828421959	postgres	2020-02-21 11:10:26.100252	33	t
82	21.1.13.7	ADD TICKET VALIDITY FOR CATEGORY	SQL	V21_1.13.7__ADD_TICKET_VALIDITY_FOR_CATEGORY.sql	-1985661395	postgres	2020-02-21 11:10:26.138829	5	t
83	22.1.14.0	ADD EXTENSION	SQL	V22_1.14.0__ADD_EXTENSION.sql	-1734191664	postgres	2020-02-21 11:10:26.148959	77	t
84	22.1.14.1	ADD EXTENSION LOG	SQL	V22_1.14.1__ADD_EXTENSION_LOG.sql	-1322154658	postgres	2020-02-21 11:10:26.230522	90	t
85	22.1.14.2	ADD EXTENSION CONFIGURATION	SQL	V22_1.14.2__ADD_EXTENSION_CONFIGURATION.sql	1046779659	postgres	2020-02-21 11:10:26.326287	342	t
86	22.1.14.3	ALTER EXTENSION TABLES	SQL	V22_1.14.3__ALTER_EXTENSION_TABLES.sql	-1539257448	postgres	2020-02-21 11:10:26.674506	299	t
87	22.1.14.4	FIX MISSING UNIQUE ORG	SQL	V22_1.14.4__FIX_MISSING_UNIQUE_ORG.sql	1629808487	postgres	2020-02-21 11:10:26.989964	26	t
88	22.1.14.5	FIX EXTENSION EVENT	SQL	V22_1.14.5__FIX_EXTENSION_EVENT.sql	104510803	postgres	2020-02-21 11:10:27.021383	60	t
89	22.1.14.7	ADD EXTENSION DISPLAY NAME	SQL	V22_1.14.7__ADD_EXTENSION_DISPLAY_NAME.sql	220691691	postgres	2020-02-21 11:10:27.090077	184	t
90	22.1.14.8	MigrateMailchimp	SPRING_JDBC	alfio.db.PGSQL.V22_1_14_8__MigrateMailchimp	\N	postgres	2020-02-21 11:10:27.281533	10	t
91	22.1.14.9	REMOVE RESOURCE PRIMARY KEY	SQL	V22_1.14.9__REMOVE_RESOURCE_PRIMARY_KEY.sql	-580585038	postgres	2020-02-21 11:10:27.296676	0	t
92	22.1.14.10	ADD PRIVACY POLICY LINK	SQL	V22_1.14.10__ADD_PRIVACY_POLICY_LINK.sql	1491738136	postgres	2020-02-21 11:10:27.302546	0	t
93	22.1.15.1	FIX EU COUNTRIES LIST	SQL	V22_1.15.1__FIX_EU_COUNTRIES_LIST.sql	2012916662	postgres	2020-02-21 11:10:27.306793	0	t
94	22.1.15.2	ADD CREATION TIMESTAMP	SQL	V22_1.15.2__ADD_CREATION_TIMESTAMP.sql	1708976067	postgres	2020-02-21 11:10:27.311371	2	t
95	22.1.15.3	ADD CUSTOMER REFERENCE	SQL	V22_1.15.3__ADD_CUSTOMER_REFERENCE.sql	-1745559066	postgres	2020-02-21 11:10:27.317103	0	t
96	22.1.15.4.1	LINK FIELD TO CATEGORIES	SQL	V22_1.15.4.1__LINK_FIELD_TO_CATEGORIES.sql	-96571661	postgres	2020-02-21 11:10:27.321835	1	t
97	22.1.15.5.1	CREATE GROUP	SQL	V22_1.15.5.1__CREATE_GROUP.sql	-319675532	postgres	2020-02-21 11:10:27.327529	534	t
98	22.1.15.6	ADD VALID TO USER	SQL	V22_1.15.6__ADD_VALID_TO_USER.sql	1678186024	postgres	2020-02-21 11:10:27.876542	0	t
99	22.1.15.6.1	ADD MAX USAGE PROMO CODE	SQL	V22_1.15.6.1__ADD_MAX_USAGE_PROMO_CODE.sql	-647754288	postgres	2020-02-21 11:10:27.881272	0	t
100	23.2.0.0.0	NEW RESERVATION FLOW	SQL	V23_2.0.0.0__NEW_RESERVATION_FLOW.sql	2108674683	postgres	2020-02-21 11:10:27.885668	12	t
101	23.2.0.0.1	NEW RESERVATION FLOW CHANGES	SQL	V23_2.0.0.1__NEW_RESERVATION_FLOW_CHANGES.sql	801723433	postgres	2020-02-21 11:10:27.903294	1	t
102	23.2.0.0.2	ITALIAN E INVOICING	SQL	V23_2.0.0.2__ITALIAN_E_INVOICING.sql	391885910	postgres	2020-02-21 11:10:27.908881	0	t
103	200.001	DEFAULT PAYMENT PROVIDERS	SQL	V200_001__DEFAULT_PAYMENT_PROVIDERS.sql	-560557969	postgres	2020-02-21 11:10:27.914083	1	t
104	200.002	REMOVE PLUGIN TABLES	SQL	V200_002__REMOVE_PLUGIN_TABLES.sql	249466173	postgres	2020-02-21 11:10:27.920024	3	t
105	201.2.0.0.2	CREATE BILLING DOCUMENT	SQL	V201_2.0.0.2__CREATE_BILLING_DOCUMENT.sql	506372089	postgres	2020-02-21 11:10:27.934513	157	t
106	201.2.0.0.3	ADD ROW SECURITY	SQL	V201_2.0.0.3__ADD_ROW_SECURITY.sql	1788899297	postgres	2020-02-21 11:10:28.107048	396	t
107	201.2.0.0.4	ADD RESERVATION ID IN EMAILS	SQL	V201_2.0.0.4__ADD_RESERVATION_ID_IN_EMAILS.sql	-500349236	postgres	2020-02-21 11:10:28.512326	72	t
108	202.2.0.0.5	ADD TRANSACTION STATUS AND METADATA	SQL	V202_2.0.0.5__ADD_TRANSACTION_STATUS_AND_METADATA.sql	1844685841	postgres	2020-02-21 11:10:28.60353	197	t
109	202.2.0.0.6	ADD COMPANY BILLING DETAILS	SQL	V202_2.0.0.6__ADD_COMPANY_BILLING_DETAILS.sql	-786783469	postgres	2020-02-21 11:10:28.815661	0	t
110	202.2.0.0.7	PROMOCODE DESCRIPTION EMAIL	SQL	V202_2.0.0.7__PROMOCODE_DESCRIPTION_EMAIL.sql	2048766364	postgres	2020-02-21 11:10:28.821138	1	t
111	202.2.0.0.8	ADMIN JOB QUEUE	SQL	V202_2.0.0.8__ADMIN_JOB_QUEUE.sql	-2063459019	postgres	2020-02-21 11:10:28.826803	202	t
112	202.2.0.0.9	REMOVE NOT NULL EVENT LAT LONG	SQL	V202_2.0.0.9__REMOVE_NOT_NULL_EVENT_LAT_LONG.sql	1047990572	postgres	2020-02-21 11:10:29.035321	1	t
113	202.2.0.0.10	ADD PRICE INFORMATION ON RESERVATION	SQL	V202_2.0.0.10__ADD_PRICE_INFORMATION_ON_RESERVATION.sql	301909784	postgres	2020-02-21 11:10:29.041202	745	t
114	202.2.0.0.11	ADD PROMO CODE FOR HIDDEN CATEGORIES	SQL	V202_2.0.0.11__ADD_PROMO_CODE_FOR_HIDDEN_CATEGORIES.sql	503109769	postgres	2020-02-21 11:10:29.803145	369	t
115	202.2.0.0.12	FIX PROMO CODE UNIQUE INDEX	SQL	V202_2.0.0.12__FIX_PROMO_CODE_UNIQUE_INDEX.sql	1587965905	postgres	2020-02-21 11:10:30.189037	2	t
116	202.2.0.0.12.1	ADD EDITABLE FLAG TICKET FIELD	SQL	V202_2.0.0.12.1__ADD_EDITABLE_FLAG_TICKET_FIELD.sql	2072600445	postgres	2020-02-21 11:10:30.195707	190	t
117	202.2.0.0.12.2	ADD TICKET CHECK IN STRATEGY	SQL	V202_2.0.0.12.2__ADD_TICKET_CHECK_IN_STRATEGY.sql	-1189682714	postgres	2020-02-21 11:10:30.393958	225	t
118	202.2.0.0.12.3	ADD SPONSOR SCAN NOTES	SQL	V202_2.0.0.12.3__ADD_SPONSOR_SCAN_NOTES.sql	-1991957194	postgres	2020-02-21 11:10:30.642286	78	t
119	202.2.0.0.13	ADD CURRENCY INFO	SQL	V202_2.0.0.13__ADD_CURRENCY_INFO.sql	2060759209	postgres	2020-02-21 11:10:30.734094	2	t
120	202.2.0.0.14	ADD CATEGORY ORDINAL	SQL	V202_2.0.0.14__ADD_CATEGORY_ORDINAL.sql	35153087	postgres	2020-02-21 11:10:30.740618	212	t
121	202.2.0.0.15	ADD JSONB RECURSIVE MERGE FN	SQL	V202_2.0.0.15__ADD_JSONB_RECURSIVE_MERGE_FN.sql	-252984091	postgres	2020-02-21 11:10:30.961413	3	t
122	202.2.0.0.16	ADD FIXED AND PERCENTAGE FEES	SQL	V202_2.0.0.16__ADD_FIXED_AND_PERCENTAGE_FEES.sql	599289085	postgres	2020-02-21 11:10:30.968314	1	t
\.


--
-- Data for Name: group_link; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.group_link (id, a_group_id_fk, event_id_fk, ticket_category_id_fk, type, match_type, max_allocation, active, organization_id_fk) FROM stdin;
\.


--
-- Data for Name: group_member; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.group_member (id, a_group_id_fk, value, description, active, organization_id_fk) FROM stdin;
\.


--
-- Data for Name: invoice_sequences; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.invoice_sequences (organization_id_fk, invoice_sequence) FROM stdin;
1	1
\.


--
-- Data for Name: j_user_organization; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.j_user_organization (user_id, org_id) FROM stdin;
2	1
3	1
\.


--
-- Data for Name: organization; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.organization (id, name, description, email) FROM stdin;
1	SVConcert	This is a test organization	info@svconcert.com
\.


--
-- Data for Name: promo_code; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.promo_code (id, promo_code, event_id_fk, valid_from, valid_to, discount_amount, discount_type, categories, organization_id_fk, max_usage, description, email_reference, code_type, hidden_category_id) FROM stdin;
1	AQUARIUS	3	2020-03-25 16:35:00-06	2020-04-30 15:00:00-06	500	FIXED_AMOUNT	[3]	1	5	\N	\N	DISCOUNT	\N
\.


--
-- Data for Name: qrtz_blob_triggers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.qrtz_blob_triggers (sched_name, trigger_name, trigger_group, blob_data) FROM stdin;
\.


--
-- Data for Name: qrtz_calendars; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.qrtz_calendars (sched_name, calendar_name, calendar) FROM stdin;
\.


--
-- Data for Name: qrtz_cron_triggers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.qrtz_cron_triggers (sched_name, trigger_name, trigger_group, cron_expression, time_zone_id) FROM stdin;
\.


--
-- Data for Name: qrtz_fired_triggers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.qrtz_fired_triggers (sched_name, entry_id, trigger_name, trigger_group, instance_name, fired_time, sched_time, priority, state, job_name, job_group, is_nonconcurrent, requests_recovery) FROM stdin;
\.


--
-- Data for Name: qrtz_job_details; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.qrtz_job_details (sched_name, job_name, job_group, description, job_class_name, is_durable, is_nonconcurrent, is_update_data, requests_recovery, job_data) FROM stdin;
\.


--
-- Data for Name: qrtz_locks; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.qrtz_locks (sched_name, lock_name) FROM stdin;
\.


--
-- Data for Name: qrtz_paused_trigger_grps; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.qrtz_paused_trigger_grps (sched_name, trigger_group) FROM stdin;
\.


--
-- Data for Name: qrtz_scheduler_state; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.qrtz_scheduler_state (sched_name, instance_name, last_checkin_time, checkin_interval) FROM stdin;
\.


--
-- Data for Name: qrtz_simple_triggers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.qrtz_simple_triggers (sched_name, trigger_name, trigger_group, repeat_count, repeat_interval, times_triggered) FROM stdin;
\.


--
-- Data for Name: qrtz_simprop_triggers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.qrtz_simprop_triggers (sched_name, trigger_name, trigger_group, str_prop_1, str_prop_2, str_prop_3, int_prop_1, int_prop_2, long_prop_1, long_prop_2, dec_prop_1, dec_prop_2, bool_prop_1, bool_prop_2) FROM stdin;
\.


--
-- Data for Name: qrtz_triggers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.qrtz_triggers (sched_name, trigger_name, trigger_group, job_name, job_group, description, next_fire_time, prev_fire_time, priority, trigger_state, trigger_type, start_time, end_time, calendar_name, misfire_instr, job_data) FROM stdin;
\.


--
-- Data for Name: resource_event; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.resource_event (name, organization_id_fk, event_id_fk, content_size, content, content_type, creation_time, attributes) FROM stdin;
\.


--
-- Data for Name: resource_global; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.resource_global (name, content_size, content, content_type, creation_time, attributes) FROM stdin;
\.


--
-- Data for Name: resource_organizer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.resource_organizer (name, organization_id_fk, content_size, content, content_type, creation_time, attributes) FROM stdin;
\.


--
-- Data for Name: scan_audit; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.scan_audit (ticket_uuid, event_id_fk, scan_ts, username, check_in_status, operation, organization_id_fk) FROM stdin;
62695386-d742-4080-bb78-2a60526cc03d	1	2020-02-21 17:33:41.987	eivynd.mendoza	SUCCESS	SCAN	1
62695386-d742-4080-bb78-2a60526cc03d	1	2020-02-21 17:33:50.773	eivynd.mendoza	OK_READY_TO_BE_CHECKED_IN	REVERT	1
d06f56e2-80ee-45a2-ae53-1c39e2eb2819	1	2020-02-22 14:53:03.523	eivynd.mendoza	SUCCESS	SCAN	1
d06f56e2-80ee-45a2-ae53-1c39e2eb2819	1	2020-02-22 14:53:16.729	eivynd.mendoza	OK_READY_TO_BE_CHECKED_IN	REVERT	1
d06f56e2-80ee-45a2-ae53-1c39e2eb2819	1	2020-02-22 15:09:05.077	pablo.aguilar	SUCCESS	SCAN	1
1d18980c-974e-4554-b452-44bb6079e7d3	1	2020-02-22 15:09:06.995	pablo.aguilar	SUCCESS	SCAN	1
1d18980c-974e-4554-b452-44bb6079e7d3	1	2020-02-22 15:13:17.552	pablo.aguilar	OK_READY_TO_BE_CHECKED_IN	REVERT	1
d06f56e2-80ee-45a2-ae53-1c39e2eb2819	1	2020-02-22 15:13:18.836	pablo.aguilar	OK_READY_TO_BE_CHECKED_IN	REVERT	1
1d18980c-974e-4554-b452-44bb6079e7d3	1	2020-03-25 15:32:41.704	admin	SUCCESS	SCAN	1
1d18980c-974e-4554-b452-44bb6079e7d3	1	2020-03-25 15:32:50.498	admin	OK_READY_TO_BE_CHECKED_IN	REVERT	1
\.


--
-- Data for Name: special_price; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.special_price (id, code, price_cts, ticket_category_id, status, session_id, sent_ts, recipient_name, recipient_email, organization_id_fk, access_code_id_fk) FROM stdin;
\.


--
-- Data for Name: sponsor_scan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sponsor_scan (id, creation, event_id, ticket_id, user_id, organization_id_fk, notes) FROM stdin;
\.


--
-- Data for Name: spring_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.spring_session (session_id, creation_time, last_access_time, max_inactive_interval, principal_name) FROM stdin;
7897f7cf-94f3-421c-87ef-73335aeb8062	1584998738792	1585331592860	14400	admin
\.


--
-- Data for Name: spring_session_attributes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.spring_session_attributes (session_id, attribute_name, attribute_bytes) FROM stdin;
7897f7cf-94f3-421c-87ef-73335aeb8062	CSRF_SESSION_ATTRIBUTE	\\xaced0005737200366f72672e737072696e676672616d65776f726b2e73656375726974792e7765622e637372662e44656661756c7443737266546f6b656e5aefb7c82fa2fbd50200034c000a6865616465724e616d657400124c6a6176612f6c616e672f537472696e673b4c000d706172616d657465724e616d6571007e00014c0005746f6b656e71007e0001787074000c582d435352462d544f4b454e7400055f6373726674002434316337376637312d316130622d346364632d613631312d306230396632376163333765
7897f7cf-94f3-421c-87ef-73335aeb8062	SPRING_SECURITY_CONTEXT	\\xaced00057372003d6f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e636f6e746578742e5365637572697479436f6e74657874496d706c00000000000001fe0200014c000e61757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b78707372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001fe0200024c000b63726564656e7469616c737400124c6a6176612f6c616e672f4f626a6563743b4c00097072696e636970616c71007e0004787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c7371007e0004787001737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00067870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000001770400000001737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001fe0200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b787074000a524f4c455f41444d494e7871007e000d737200486f72672e737072696e676672616d65776f726b2e73656375726974792e7765622e61757468656e7469636174696f6e2e57656241757468656e7469636174696f6e44657461696c7300000000000001fe0200024c000d72656d6f74654164647265737371007e000f4c000973657373696f6e496471007e000f787074000f303a303a303a303a303a303a303a3174002462376436343764302d616264322d343862342d613963372d31366562343963373232623270737200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001fe0200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657374000f4c6a6176612f7574696c2f5365743b4c000870617373776f726471007e000f4c0008757365726e616d6571007e000f787001010101737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e000a737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f7200000000000001fe020000787077040000000171007e0010787074000561646d696e
\.


--
-- Data for Name: ticket; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ticket (id, uuid, creation, category_id, event_id, status, original_price_cts, paid_price_cts, tickets_reservation_id, full_name, email_address, special_price_id_fk, locked_assignment, user_language, reminder_sent, src_price_cts, final_price_cts, vat_cts, discount_cts, first_name, last_name, ext_reference, organization_id_fk, currency_code) FROM stdin;
7	2e3fde59-fe1c-419c-82a3-f81db0a92961	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	es	f	1500	1500	0	0	\N	\N	\N	1	USD
10	ba769da1-bc8d-4073-ae30-7bc60d39552d	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
11	e2bb8759-5a4e-437f-b66a-4cef4718d938	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
12	988da067-760f-4076-a303-aeb2766b4668	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
13	8dc99c5d-9d93-4d03-ae63-b123d39b6efa	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
14	454ab0ba-fdbc-4e38-928e-ffe1110a539b	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
15	5b4f7d16-de08-45ab-a678-36d0747676be	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
16	6cb613f6-f859-40d6-b7fe-bcd462b76219	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
17	490fe834-8f21-4227-8e48-5130670ceaa4	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
18	bd3ae33e-a85a-44eb-a1dd-d88c3af54cab	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
19	462a8300-0abd-4d12-8b87-531650a8788a	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
20	dbd0f4c7-4624-49bf-8a48-f2873376d3b5	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
21	2cef9d6b-c5e0-436c-9583-234fa39a04d3	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
22	7f5fe9f0-50a3-4e51-8495-fa3f40d9a10c	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
23	353eb024-ea06-4ed2-a915-60316c933046	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
24	47b6f8a8-0b98-4e3a-afb7-9fef7ba8079a	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
25	0a67b7a2-b66b-4695-a929-257b7526fda9	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
26	763e3f55-7d8b-4519-8a2b-79ba138cd378	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
27	57f55c7f-2057-4756-a8b7-6ccfda48be95	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
28	977689c8-5340-4654-9b2d-da7d36b2cd72	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
29	6a010fde-b762-458d-8434-5136bd197cec	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
30	9baeb615-01e8-4c5e-ac22-984b0f321f67	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
31	68311c27-e6bc-4892-8c48-fc3f452132f9	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
32	5eb79668-bcad-4b82-b4b5-59386be36005	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
33	fcba9a85-07ae-422a-881e-66b6b55522af	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
34	1fd25b6e-c7d5-458c-9fa9-1de02ea53fb8	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
35	e24cd05d-6aa4-4bf5-a5f1-e4d965ab07fe	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
36	024d03b5-af2a-44a1-9cf7-8f8c62c489ad	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
37	3c64896a-35d4-4614-a936-1cf842ed03a6	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
38	1032bb85-d7ad-4210-90e2-d5eaf7f703ac	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
39	4849aaf5-894a-49a6-9383-b27ebc5e2ec5	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
40	e53e4f42-8550-4afc-bff8-5157fce41296	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
41	fb2e1da5-a1e9-414f-a8f6-2658d6b37a3f	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
42	ca0c87c3-f34f-4cd5-b959-ab1bf7497bf6	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
43	2c1a884a-36ed-4222-a90f-ec4b697764c2	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
44	e3f0ab29-2046-496a-b54a-e95cdf6dcc71	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
45	bd4c1910-b111-42aa-80ac-a81b5beb347b	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
46	217fc38d-609d-4d85-a64e-1c1cb72b6c4d	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
47	59fcf190-7ab5-4a71-82ca-988802599f6b	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
48	03b90185-20c2-4596-a093-7f10987866f9	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
49	c65155fc-4618-4d13-91fd-4fe88cfb46e1	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
50	8395ddd3-8514-4bb0-9cbc-230073062b1b	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
51	a9c806b3-3b4b-4a55-ac6b-7e67949537c3	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
52	6191f2b9-0cc3-4f78-8ee9-447eb7e81326	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
53	879be636-4f79-40f7-ace7-c91eefa118d4	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
54	bb03c256-843a-4ba6-be77-93463ceb6f38	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
55	cc8fbffb-11e6-491b-8008-b5fae0963926	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
56	54286722-aef2-4c88-9115-175f6efdfefa	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
57	45efdb36-b832-48b5-b751-8f6e3e9f11bf	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
58	c1ebe597-de04-4090-aa53-2dc831182e31	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
59	fbcee700-a3d2-4414-80c7-3c2e4fdfff19	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
60	ef86efb8-1a89-48b6-b9f3-5f7ce0c6a89c	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
61	4fe616ee-a22f-4217-a601-cae2264923d0	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
62	76d7f68f-e238-449e-9f21-38318677dbd0	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
63	5c8b464e-d39b-4d5c-887d-1e6c77608fe1	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
64	2fd688c8-5202-4733-b73d-eb553ba48286	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
65	424166f5-c17c-4a29-bedc-2c1976d6c197	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
66	8e06092e-45a0-47e8-9478-af22af361f43	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
67	4e48b86c-e273-4b15-b6c3-6e99036cb5cd	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
68	e085d055-39fc-4b17-9b27-f8b5204ce40b	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
69	5cb30e60-96d4-4ad5-aa4a-d47ec74008f1	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
70	3319ce7c-7ade-400e-8b23-4aad0edfba2f	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
71	453bd4ae-89df-4058-b683-c6e7628489b2	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
72	0a607f8c-7aea-413b-955f-f1f2fbb30adb	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
73	1f194a8e-c35d-49fe-be06-b3ca0534e49f	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
74	b7fd7354-bab9-44b2-854c-284dd226c69a	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
75	89a0dc82-0948-44a0-8d48-9546ed9886bd	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
76	445214ae-1c5d-44f5-9905-ecaa41445cab	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
77	380bc847-4afd-47bd-985f-30d624bc1dc5	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
78	e8075ef3-4587-4fe8-ac2e-ec69384f0aea	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
79	ba821ef4-4c8d-4883-893e-9d6185519e4e	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
80	e4a75268-4078-475d-be7b-8cd509244cc3	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
81	93480434-89f7-44f8-bff0-658af7d34b59	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
82	ed75bacf-c33c-4317-ba7e-3d920d34ddc9	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
83	d86a1318-a52a-499f-ad08-66a70ada7a63	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
84	264ca94c-84e4-4dfa-8f26-00c650789864	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
85	c0a4e4ef-e425-46d4-9ab3-9a8669026fb0	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
86	618a2ecc-e78a-4050-a774-7bb1bb07c3e9	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
87	98130abf-60cb-49f4-85f4-af19f66ad93f	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
88	8079efb4-e67f-4a1c-8c79-8978454fcae4	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
89	59bc23fb-899c-41b6-a5a3-7c7a54dff9e7	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
90	fbdca534-46fd-47f3-84e8-42ecb25824f5	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
91	2e7f2105-a722-43ce-833c-c8bdf7ba58c8	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
92	60ef8e69-0eec-4830-bfc8-ce7004879700	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
93	fa088f68-0275-4b42-8423-c8a5c334eea4	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
94	b18d35d6-0cda-4342-843c-98439d83147a	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
95	440a370b-d25b-4bbb-908b-cf2e34b25ac0	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
96	aaeb0dbc-c0b8-4354-9968-7b5f920d3e1a	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
97	22f581b6-ec42-43fd-afd8-4637fe79fe3c	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
98	7fe1faf0-2da0-4a9e-becd-6c448ad99b71	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
99	0f13801b-97f1-4228-964f-cbaeb3d75b6e	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
100	f066b80a-b126-4da9-bb11-87727b3cf663	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
4	789a94c5-6718-4565-8ebf-d3ce368b3a7f	2020-02-21 11:26:20.814-06	1	1	TO_BE_PAID	0	0	11f71518-4ea4-4a8e-813c-85312308602d	Alejandro Castro	alejandro.castro@itproject41.com	\N	f	es	f	1500	1500	0	0	Alejandro	Castro	\N	1	USD
3	311f332c-f834-4c5d-940d-f2a5a9bfe6a2	2020-02-21 11:26:20.814-06	1	1	TO_BE_PAID	0	0	11f71518-4ea4-4a8e-813c-85312308602d	Jose Perez	jose.perez@correo.com	\N	f	es	f	1500	1500	0	0	Jose	Perez	\N	1	USD
5	264b44ea-820f-46de-b5f2-5c3fb89db02e	2020-02-21 11:26:20.814-06	1	1	TO_BE_PAID	0	0	11f71518-4ea4-4a8e-813c-85312308602d	Jorge Ochoa	jandor.ac@gmail.com	\N	f	es	f	1500	1500	0	0	Jorge	Ochoa	\N	1	USD
2	d06f56e2-80ee-45a2-ae53-1c39e2eb2819	2020-02-21 11:26:20.814-06	1	1	ACQUIRED	0	0	fa31ec0a-3f43-4a2c-834c-e0afdeebcce3	Pablo Aguilar	pablo.aguilar@correo.com	\N	t	es	t	1500	1500	0	0	Pablo	Aguilar	\N	1	USD
102	db4117b5-1aba-4977-a313-78249560708a	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
103	88855aa9-602d-45b8-9211-b006e1385f5c	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
104	a832bece-b3cb-40d6-ae7d-6d3297cedcce	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
105	7a2cb857-4b41-4001-ba9a-34ae7c7cb803	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
106	a45bead8-e43b-49eb-b2d6-5d530c438b12	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
107	abadceba-70c7-4961-ae69-2db6a78a8305	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
108	38cd9d51-ab0f-451a-b5c9-4799c34921a3	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
109	7c823cd2-dd57-4734-9a4d-5cafb73946a1	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
110	95b95728-76db-4116-9840-9d4fa62ef770	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
111	9e3af35d-58be-4f60-b44e-cc09a974a49c	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
112	8bde75dd-541b-4e4f-87c4-cca3886ce2c0	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
113	08cac982-0818-4328-8144-a1e834a5a624	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
114	bd028942-af30-4e57-b94e-ddb0586fcb87	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
115	f8034857-902c-4399-9994-c513f4246373	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
116	d9a82f00-2a25-434f-aafa-2bde02f57283	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
117	3dcfa165-be32-4aad-b5d4-6976d6955db6	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
118	2382fc44-a659-43db-a8dc-a087f3701326	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
119	04528c68-50c6-4f51-8ce5-34d8d9a3111c	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
120	88ac317e-1fe9-45e4-bf7f-1416f6a1d22f	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
121	c75ea0d1-ad1e-4a7b-8dec-658df3bbf514	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
122	c3363f77-d13e-4385-bba0-5e7f12dda76e	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
123	3fd3844b-a85e-40cc-9dd0-270615f50af2	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
124	fb103eca-f68d-4cef-bf83-97d4854fb906	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
125	965d11c8-c85a-4bae-b5aa-e64130cbdb23	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
126	94d69380-51c0-45a5-9867-e9a4ce088aa1	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
127	27ff71a2-915e-46e7-ab35-8f3dece794ae	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
128	5ce51b00-6827-4409-96cd-73a49c1a67e9	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
129	176e5bb0-e834-453a-b0ba-fc95576ac869	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
130	4931c1ed-97bf-4241-a029-c0b6fc056573	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
131	5fb9b86e-c4d6-4308-bc48-bf64bd5ae554	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
132	5b86bcb7-30aa-43d5-86de-fded20311cb4	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
133	7ba227b6-882d-4002-b229-17d14a688d5f	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
134	4b544fe3-14b1-4852-a5a9-a2ef63882397	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
135	2cdcd109-fd43-472c-80f9-7aac2ad4930a	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
136	1bf78db7-8c59-49b7-8290-7abf00222972	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
137	1e529e4a-ca24-49b8-9729-e5492c719ac8	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
138	23ad6b37-14d7-448c-9819-d585e9859f84	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
139	aae35e5a-78c3-4925-ba32-425b88f57746	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
140	5b8b7f6e-e512-4a8a-8993-4d90239da0e5	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
141	f790a51e-1056-4c5b-b636-247e7847d568	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
142	7bf7fffc-999a-4da5-ba73-a6ffc2b0c52d	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
143	7ea2104a-dc21-4814-85d1-6ceec924b074	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
144	fc9f0929-31a7-4478-a6ca-506878d8e63f	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
145	edc8226d-c12c-4340-89f2-22375fb0cb7e	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
146	3f414a5a-d6a4-4e8e-a251-a9214aa5f6b2	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
147	c86bba89-9717-48ab-b541-2c75c6fae10e	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
148	af12b91b-7e9f-4466-b043-d82130bdb58f	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
149	70834086-6091-4804-842d-c4a00354d45c	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
150	c1948231-c4ff-411d-a711-366010ae99e6	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
151	1fb65722-d4cd-4bee-876c-5a29da4f2a2c	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
152	628db52c-db66-4c02-8faa-eec352899070	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
153	c00c324b-67f8-4c4a-a29a-32fcff50f2c8	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
154	2ad8d816-c96b-4bae-90f3-33daf0f37c95	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
155	0ad11f1b-aeaf-4392-8e66-c54a34683d76	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
156	155c8cc1-bee8-4e97-92ab-9a3283f6a1c6	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
157	bf20473c-21de-4884-9149-d9973ee06d81	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
158	bc69fb23-ef26-4df5-9fb6-f958243961f0	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
159	1d862f41-4e8c-478d-8eb7-6106efe5b8b4	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
160	b182d8a4-225a-4c40-8aa6-8cb79869bba5	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
161	9c56a74c-7eb6-4308-9203-6e9064e2ab31	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
162	df9cee48-dc19-4bd5-93a6-511d95d67534	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
163	6599c5f7-f129-4721-a4b0-9a86e3d0e33b	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
164	5086db56-4ac6-4f30-ad87-cbee50cf9797	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
165	93447e23-1c28-417f-b543-1fac558dad86	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
166	0fcafe11-9a96-4318-98c4-1f5d1fbb3c6d	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
167	aed9aa64-dd50-4c3c-9e0b-5a37a52727dc	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
168	a4fba934-cad6-47ed-b093-0e506aef9a36	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
169	820eae71-3a6c-4b96-aa9f-82f006d74852	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
170	2eae49e0-5fb6-4516-895a-a070b0d6d7fe	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
171	0f880184-c626-4200-b054-4515eb793713	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
172	79e9d32c-6ecb-4da0-981c-e8c3e83f8c24	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
173	7b0ce5bc-9cdf-4b70-9c9c-e87d96fd1731	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
174	ab75aa85-a978-4d95-943a-d0080dd93ea9	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
175	0ca08550-f387-48da-81bc-c6b5cf953b70	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
176	9d4297e9-4e08-4966-b7eb-04155f316023	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
177	eb162ca5-e9a3-4419-a318-c6f9e0f2df0e	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
178	b6c637e0-b2fe-4476-902c-f40479bf100f	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
179	a61bae84-a65d-4020-a943-33dd09cd90d9	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
180	23e173a3-3d2f-43c6-8124-65997dc16442	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
181	27a3c80c-9a4f-46b1-98e0-fd1982c99551	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
182	20421dd4-cf8f-4fc3-b09c-6147fdb1c103	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
183	e4b13b68-e261-4355-ba21-fefdb2867104	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
184	9bd0aa17-4331-45f6-8331-b3a03ba4d6fa	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
185	9430d651-974a-4f12-9fd1-ca2239a56ffd	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
186	d73c524f-01a6-4f07-a78b-935a3beca7cd	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
187	27a26ced-0636-4e48-83fc-a7875b8a2bec	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
188	b143306e-e838-4172-87f0-59b0cb2037b2	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
189	a3b5fc2d-81ab-43f1-b484-ac1b7267e31a	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
190	e494abcc-fe39-4dee-aa0a-dd4b24963373	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
191	31010c74-34b7-4b03-8f6e-b534e3486e23	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
192	9a64b788-a79b-458f-8926-51a5f79f6eaa	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
193	457bea93-c079-46fe-937d-77d9692d3ccb	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
194	41983c8f-f97b-4834-87a0-2fcd29509e29	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
195	ac67053e-3b7c-46d0-af2d-4bc143703a51	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
196	94d0ac85-82ee-45df-a111-a8462fde8094	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
197	1fbe4104-1988-42ba-acde-7b44fdb58f71	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
198	e947a41c-f179-475c-accd-f4996a7cc707	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
199	a34a7813-3026-4009-9155-a0ccb9842e23	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
200	c33dd138-a2f0-4aba-9020-9e9f480fe7ae	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
6	aadcadef-e89b-488d-b108-019a4ddb5c3e	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	es	f	2000	2000	0	0	\N	\N	\N	1	USD
201	c7dd81c8-9068-4cfd-a15a-f0ec9273c2c8	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
202	a2b68d83-b2c4-4a8d-986f-9259efefe941	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
203	d84350f8-ced4-476f-af90-d76ff19ffff4	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
204	83936580-a2d8-4ef6-b159-5ad115976b57	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
205	633d2ca0-d331-43fc-927a-c15686b764c2	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
206	6844ef9f-16f4-4756-8daa-691b4f9df903	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
207	87faa049-c19f-4d73-a40f-2aa4b99b4ba3	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
208	990f5418-8366-4f1a-9376-ef03ad2b3b07	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
209	8329999d-762b-4277-ace1-d79e6f85caa2	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
210	c3235ee4-7e96-4dc9-a976-f8c29f94e03c	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
211	cb8adc39-c491-4187-97cd-89aa10dcb6e0	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
212	85729a8f-0f19-444c-a826-1ba0c60b4142	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
213	030bdbc7-721c-4ed7-b43e-85f8edaf3ad7	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
101	a0bbb784-743d-44f5-acbb-d968918c5bb4	2020-03-17 15:41:43.374-06	\N	2	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	USD
214	03ecd7bc-b838-4292-89d0-3f12d1071c23	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
215	b5f6f160-fedc-4bac-9230-f6525ea2c2d7	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
216	bec994fe-c41d-401f-be4c-d4b1520a4f57	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
217	c0dc69ca-4707-40d8-ba8b-3bfea58692f2	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
218	1d909132-4545-41e9-b806-e6378b8127fc	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
219	f6c080a1-7420-4a26-8d42-c3f0a7b41640	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
220	7b7d22d9-864c-4a3a-9f9b-43b1b13abb1a	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
221	9ff8692b-fdc6-4fad-a427-7969b9a85343	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
222	68d049d8-6d85-4bb4-8688-4b98c33ad0ae	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
223	bae6b981-1610-4462-86ad-462b6bfeda77	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
224	b6ac8a99-5775-477e-bbbb-a0e45709474b	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
225	0a406367-81b4-4d10-a12f-fe8bb235dcc9	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
226	9aa752e0-8983-4dc2-a91c-64ec9c9c8710	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
227	e32a2ca6-e579-445a-8011-7f6e9c2b6eed	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
228	fd0cdf3c-b9e3-40ae-b143-63c4b9bd5cfa	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
229	7ad11738-7de2-48b4-b5a4-2e2c164c683b	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
230	192d3c2a-7a50-4081-988f-82278be80860	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
231	28216e1f-0c52-4f80-9226-ef53155b31bb	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
232	9ea44b14-740c-46f0-9751-9401af14cef8	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
233	3e12e525-7b41-4dd4-9d00-b2e4a12ef296	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
234	e2584d8a-0554-48a1-9b07-007e1819e3f9	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
235	7f3cec57-d299-4c97-a849-9bc993f12332	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
236	d0d5f8c9-4778-4bda-b98c-ec8f2a2bd6ea	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
237	9b1ebf8b-5684-40d5-94a0-ca837bb533d9	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
238	0ddd256b-7237-45ff-84b4-ad44867174cf	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
239	78f45b63-37b6-47bf-99fc-1667ca59449a	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
240	d5a91ba8-3e34-4ad8-a0c5-94a001d702ad	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
241	a471ad6d-cc8c-4038-916a-810c627af7e7	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
242	3d3f943e-439a-4ad8-88b9-214c5bba541e	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
243	3759f0fb-d72c-4ed1-8b9f-34c618263369	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
244	8da2bfcc-0830-48cb-9ef0-828bb0c5a213	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
245	2ef2cab2-e435-4c1c-ae68-6040df4dce44	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
246	f840d378-bd67-4d64-9398-4bee646fab65	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
247	bcaae09a-32ea-4ab9-9fd2-e39910d0c7f9	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
248	27504a88-9e56-4c81-856b-db03f2bec17c	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
249	d8112fd8-8eb6-4fe6-89a8-a5da4e821ebc	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
250	8e1cc74f-dac4-415e-916f-361bd6070689	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
251	13972bb3-7f79-4278-8ea7-6842a327c9f6	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
252	20ad5039-2cac-41e2-9a63-1475c7c469d8	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
253	2ff69172-6fe7-465a-a6e9-e66b042de4a1	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
254	23edff8d-2da4-4fd7-92b2-960bd5afb2fa	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
255	cfaeb9e5-ccb2-4007-b8f8-bef34235f544	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
256	b691efe3-527f-4993-b052-e90b4a9f2f8d	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
257	9aba5fc4-5f64-46a3-a664-f3a1350f6372	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
258	7b5e5524-59d6-4d77-966b-4c95de877628	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
259	a3ada2ab-aebc-4191-8856-e11b9a8906fb	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
260	32573adc-b3b1-498d-afd4-04e9294dacea	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
261	760658e6-1325-49d4-ad62-ae6cb2b2a9dd	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
262	ef3b2414-548e-4a70-8cab-7f309481511b	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
263	7d7da3b6-6c73-4b62-872c-7ff3455dcdc0	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
264	16ea8378-d3d9-4574-b529-ea081f29037a	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
265	82ffdd14-b0d1-49cd-9f5f-12c1501ccf3d	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
266	1a6be3af-449d-4551-adb1-c9bf3737c966	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
267	e8948943-3d09-4b91-91aa-040f45f85640	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
268	4a4e9b44-8e00-4709-bf02-5e198612cde5	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
269	6af6c068-4393-4884-b583-e146072c4a14	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
270	c71b05f2-ba5e-4489-9bea-5042ce3dfc59	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
271	d98615f3-3456-432c-83b9-c1ee698084c7	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
272	d8f1dbcf-debf-4d3f-9170-845f04b1e238	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
273	6a369741-e2df-4e70-bf14-9d57a061edef	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
274	ef6fbcac-ae35-4495-9165-7a5fea5c6f67	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
275	879c130c-e6b4-43f8-baf8-4fd12e1adafc	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
276	e5ba865e-7b59-4e99-94d1-76ce5c3ca615	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
277	006b044a-b3aa-4ca3-ab6f-afe188b993c2	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
278	cb85b865-94c8-4762-aee0-350d004b513d	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
279	34ba58f6-41e1-4987-97f1-426d13bbbd4f	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
280	33ef28a3-7b5c-4880-ab9f-3e8dc37c20f6	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
281	4c9b5168-399d-4082-8ece-faa21a37ce50	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
282	8ebd0423-c31d-4d07-8d47-5ca20b67f026	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
283	7d015808-eb6e-40fc-9167-25cc84e78750	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
284	58484100-69e8-4ca1-bf0d-6afd8caf0e62	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
285	eb73bf84-fc44-4fa1-b1cc-a192dace11cb	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
286	dd509c4a-d2f4-487c-a265-076a6d38337c	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
287	30109571-e43a-4b9f-b990-157ae1980953	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
288	2336df91-d5d6-40b2-a22f-bf2b96332ed6	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
289	9734e2e5-fdb7-4dd2-82f5-55873e4dcb76	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
290	79a0e1b3-2857-4669-ae42-b52602f01763	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
291	31e243a9-3c8b-42c4-bb2b-de6907c5478f	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
292	a8a333af-9fa9-4590-88c8-8ab384e5f733	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
293	6fc16643-4e08-4fb1-b5fb-72184f0cfd1a	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
294	aa4e0d44-3b4d-4afe-88e4-d6277c97539a	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
295	6cf9f888-146d-4915-aeee-88dd66acb935	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
296	b66022e1-9605-4da7-b2ad-4b99171eb0e6	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
297	31db0e9a-0dec-48b3-b140-f47d58bf8ad3	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
298	2074e7be-9379-44f8-b0d3-1e80676bfe60	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
299	e7d26a99-ecee-4bd8-bdcc-9caecdffb95b	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
300	aedc0210-b511-49f4-a564-62ae1ba65e0f	2020-03-23 15:29:52.928-06	\N	3	FREE	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
1	1d18980c-974e-4554-b452-44bb6079e7d3	2020-02-21 11:26:20.814-06	1	1	ACQUIRED	0	0	1a71e32b-35c3-4089-b249-6e7528ef1a26	Alejandro Castro	jandor.ac@gmail.com	\N	t	es	f	1500	1500	0	0	Alejandro	Castro	\N	1	USD
301	db3d8813-4d19-49c8-b961-cb24e62fdefd	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
302	0fd01e55-0fe3-4871-9673-7d603d99e0b2	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
303	41bfd940-2bfb-494d-b1c4-734d646aa9ed	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
304	22fd679e-7e6d-4386-9a1f-0910ff276f18	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
305	9404bbde-6fb6-40bc-b01c-27d046053da1	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
306	8b29d902-f221-45f8-bdb0-6a3ffbe1adaa	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
307	475c94c0-c0c2-4b52-bf99-6030057ea32f	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
308	b9e333ca-25ee-4597-9eaf-9754ff197a1c	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
309	a8636c47-1bff-41cb-a528-d738ab9cfc47	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
310	96dd8892-cebf-4c4a-a4ef-a1e6cf823955	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
311	3acbffce-0298-4bd5-9e61-016c1a9867b4	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
312	fad8973e-0efc-4d15-b902-0b12acf9f807	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
313	40b40d25-b0e7-4cce-a220-4a454da8a17e	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
314	200da087-1adc-499f-821a-9a1ec9530a37	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
315	b2db4b6a-91cd-4bd7-9d54-df7a19476841	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
316	ae8ab199-c0b2-4246-ae3b-58603479fa9c	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
317	7ac79b90-4803-43a9-8c74-535b5a12d1f2	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
318	23f7be72-46d0-484a-92ba-4f39039393fb	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
319	54dd6fd5-b9ff-4d47-8838-8152f90a2213	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
320	ad60f8be-9363-46ea-b55d-fd20992fc720	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
321	aa6def44-7938-4ab2-957a-5b5220a81f53	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
322	7a2b9ec6-6ac0-4f00-83a5-9bc0658e684f	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
323	fa5b957c-2742-410d-820c-a477ce4820fa	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
324	64fe4899-7e42-477d-bc38-00fd149510ba	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
325	8f6f062b-3062-40bb-bb17-9fbddac893a1	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
326	a9f6de20-4768-4009-a460-cdd63ac62a70	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
327	1de74a77-be92-4165-b546-a27e4a0a33ae	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
328	364f24fc-b94b-42b8-b277-eea1975e4885	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
329	ff6b4153-c7e5-49cb-b130-f17bee926b72	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
330	7e636ebe-144d-4771-8cd1-ea2009e87748	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
331	3c4e010d-367f-4976-8536-d022824db401	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
332	80dc1c31-1aff-45ec-a036-f73aee50eef6	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
333	17234606-254d-4878-8a30-eb0eba357ba4	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
334	8b020027-7829-45bb-91a1-31fdc592ccd0	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
335	568ca2cd-d127-4f82-98f2-a29e3797eeac	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
350	d3b34cb9-eed0-4628-a48d-5d7db6fca9f2	2020-03-25 16:04:57.974-06	4	4	TO_BE_PAID	0	0	00449ca6-fd93-4a4c-857d-09f49315fccf	Alejandro Castro	jandor.ac@gmail.com	\N	f	es	f	1500	1500	0	0	Alejandro	Castro	\N	1	USD
336	d742750e-7b74-4239-9931-0894f93357f7	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
337	4808a41f-071e-42bc-a5e1-b1d5d2bdb57f	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
338	d8396817-3dd0-4d9d-9c1c-577166a77523	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
339	551a832a-e2ff-485f-bae1-beae75da2ebf	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
340	5881526b-a790-43e1-a395-05b723e04c5b	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
341	b19f1ff5-3383-4ac6-98b5-ec70de3453a7	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
342	72876046-6cce-4291-94cf-6b68405dea28	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
343	05bef267-97c9-45a3-b4eb-b658317018a5	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
344	6141e592-91f0-4bf7-ae6d-f1d1e89a2d3b	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
345	ce14a096-37b6-4917-8fb5-ac4b04f9eaac	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
346	59ecf5ca-d3cc-4378-bc4a-e1b7a346709f	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
347	45b98fb2-6ad8-4339-845f-8ce105753b92	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
348	7a020da8-5887-4b35-8996-947ff7a6bfcb	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
349	c6031830-08f2-4139-a92e-4754919e50a2	2020-03-25 16:04:57.974-06	\N	4	INVALIDATED	0	0	\N	\N	\N	\N	f	\N	f	0	0	0	0	\N	\N	\N	1	\N
9	2233dd86-8c67-4b91-9246-76de63eaf1b8	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	es	f	1500	1500	0	0	\N	\N	\N	1	USD
8	dc0fa9b0-255d-4dd4-9df7-ee312718fa13	2020-02-21 11:26:20.814-06	\N	1	FREE	0	0	\N	\N	\N	\N	f	es	f	1500	1500	0	0	\N	\N	\N	1	USD
\.


--
-- Data for Name: ticket_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ticket_category (id, inception, expiration, max_tickets, name, price_cts, access_restricted, tc_status, event_id, bounded, src_price_cts, category_code, valid_checkin_from, valid_checkin_to, ticket_validity_start, ticket_validity_end, organization_id_fk, ticket_checkin_strategy, ordinal) FROM stdin;
2	2020-03-17 15:00:00-06	2020-04-01 15:00:00-06	0	Simphonyc Concert	0	f	ACTIVE	2	f	1200	N1ghtw1sh	\N	\N	\N	\N	1	ONCE_PER_EVENT	1
3	2020-03-23 15:00:00-06	2020-04-30 15:00:00-06	0	Simphonyc Concert	0	f	ACTIVE	3	f	2500	wtt	\N	\N	\N	\N	1	ONCE_PER_EVENT	1
4	2020-03-25 16:00:00-06	2020-04-30 15:00:00-06	0	Indie Rock	0	f	ACTIVE	4	f	1500	0m@m	2020-04-30 16:00:00-06	2020-04-30 19:00:00-06	\N	\N	1	ONCE_PER_EVENT	1
1	2020-02-21 11:00:00-06	2020-03-31 07:00:00-06	0	Simphonyc Concert	0	f	ACTIVE	1	f	1500	3p1c@	\N	\N	\N	\N	1	ONCE_PER_EVENT	1
5	2020-03-29 12:00:00-06	2020-03-31 07:00:00-06	0	VIP	0	f	ACTIVE	1	f	2000	3p1c@-vip	\N	\N	\N	\N	1	ONCE_PER_EVENT	0
\.


--
-- Data for Name: ticket_category_text; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ticket_category_text (ticket_category_id_fk, locale, description, organization_id_fk) FROM stdin;
\.


--
-- Data for Name: ticket_field_configuration; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ticket_field_configuration (id, event_id_fk, field_name, field_order, field_type, field_restricted_values, field_maxlength, field_minlength, field_required, context, additional_service_id, ticket_category_ids, field_disabled_values, organization_id_fk, field_editable) FROM stdin;
2	1	DUI	0	input:text	\N	9	9	t	ATTENDEE	-1	\N	\N	1	t
\.


--
-- Data for Name: ticket_field_description; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ticket_field_description (ticket_field_configuration_id_fk, field_locale, description, organization_id_fk) FROM stdin;
2	es	{"label":"DUI","placeholder":"DUI"}	1
\.


--
-- Data for Name: ticket_field_value; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ticket_field_value (ticket_id_fk, ticket_field_configuration_id_fk, field_value, organization_id_fk) FROM stdin;
1	2	036361094	1
4	2	036361094	1
5	2	036361094	1
3	2	123456478	1
\.


--
-- Data for Name: tickets_reservation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tickets_reservation (id, validity, status, full_name, email_address, billing_address, confirmation_ts, payment_method, offline_payment_reminder_sent, promo_code_id_fk, latest_reminder_ts, automatic, user_language, direct_assignment, first_name, last_name, invoice_number, invoice_model, event_id_fk, vat_status, vat_nr, vat_country, invoice_requested, used_vat_percent, vat_included, creation_ts, customer_reference, billing_address_company, billing_address_line1, billing_address_line2, billing_address_zip, billing_address_city, validated_for_overview, skip_vat_nr, invoicing_additional_information, registration_ts, organization_id_fk, add_company_billing_details, src_price_cts, final_price_cts, vat_cts, discount_cts, currency_code) FROM stdin;
11f71518-4ea4-4a8e-813c-85312308602d	2020-03-08 20:46:17.81-06	COMPLETE	admin	jandor.ac@gmail.com	Carlos Castro	2020-03-08 20:23:25.638-06	ON_SITE	f	\N	\N	f	es	f	Carlos	Castro	\N	\N	1	NOT_INCLUDED	\N	\N	f	0.00	f	2020-03-08 20:21:17.812-06	\N	\N	\N	\N	\N	\N	t	f	\N	2020-03-08 20:23:26.305-06	1	f	7500	7500	0	0	USD
fa31ec0a-3f43-4a2c-834c-e0afdeebcce3	2020-02-23 23:59:00-06	COMPLETE	Pablo Aguilar	pablo.aguilar@correo.com	\N	2020-02-22 08:52:45.23-06	ADMIN	f	\N	\N	f	es	f	Pablo	Aguilar	\N	\N	1	NOT_INCLUDED	\N	\N	f	0.00	f	2020-02-22 08:52:45.156-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	2020-02-22 08:52:45.245-06	1	\N	0	0	0	0	USD
bf6e63df-da4d-4735-964b-46ab6bb6d6f3	2020-02-21 12:07:04.345-06	CANCELLED	Alejandro Castro	alejandro.castro@correo.com	Alejandro Castro	2020-02-21 11:42:51.874-06	ON_SITE	f	\N	\N	f	es	f	Alejandro	Castro	\N	\N	1	NOT_INCLUDED	\N	\N	f	0.00	f	2020-02-21 11:42:04.347-06	\N	\N	\N	\N	\N	\N	t	f	\N	2020-02-21 11:42:51.879-06	1	f	3000	3000	0	0	USD
1a71e32b-35c3-4089-b249-6e7528ef1a26	2020-02-26 08:00:00-06	COMPLETE	Alejandro Castro	jandor.ac@gmail.com	Alejandro Castro	2020-02-21 11:56:28.883-06	OFFLINE	f	\N	\N	f	es	f	Alejandro	Castro	\N	{"originalTotalPrice":{"priceWithVAT":1500,"vat":0,"discount":0,"discountAppliedCount":0,"currencyCode":"USD"},"summary":[{"name":"Simphonyc Concert","price":"15.00","priceBeforeVat":"15.00","amount":1,"subTotal":"15.00","subTotalBeforeVat":"15.00","originalSubTotal":1500,"type":"TICKET","descriptionForPayment":"1 x Simphonyc Concert"}],"free":false,"totalPrice":"15.00","totalVAT":"0","waitingForPayment":false,"deferredPayment":false,"cashPayment":false,"vatPercentage":"0","vatStatus":"NOT_INCLUDED","refundedAmount":null,"singleTicketOrder":true,"descriptionForPayment":"1 x Simphonyc Concert","priceInCents":1500,"ticketAmount":1,"displayVat":true,"totalNetPrice":"15.00","notYetPaid":false,"vatExempt":false}	1	NOT_INCLUDED	\N	\N	f	0.00	f	2020-02-21 11:46:22.169-06	\N	\N	\N	\N	\N	\N	t	f	\N	2020-02-21 11:55:39.572-06	1	f	1500	1500	0	0	USD
56f440a3-03c5-403c-b73a-80cc575cfe89	2020-02-22 23:59:00-06	CANCELLED	Alejandro Castro	alejandro.castro@correo.com	\N	2020-02-21 11:33:28.196-06	ADMIN	f	\N	\N	f	es	f	Alejandro	Castro	\N	{"originalTotalPrice":{"priceWithVAT":1500,"vat":0,"discount":0,"discountAppliedCount":0,"currencyCode":"USD"},"summary":[{"name":"Simphonyc Concert","price":"15.00","priceBeforeVat":"15.00","amount":1,"subTotal":"15.00","subTotalBeforeVat":"15.00","originalSubTotal":1500,"type":"TICKET","descriptionForPayment":"1 x Simphonyc Concert"}],"free":false,"totalPrice":"15.00","totalVAT":"0","waitingForPayment":false,"deferredPayment":false,"cashPayment":false,"vatPercentage":"0","vatStatus":null,"refundedAmount":null,"singleTicketOrder":true,"descriptionForPayment":"1 x Simphonyc Concert","priceInCents":1500,"ticketAmount":1,"displayVat":true,"totalNetPrice":"15.00","notYetPaid":false,"vatExempt":false}	1	NOT_INCLUDED	\N	\N	f	0.00	f	2020-02-21 11:33:28.14-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	2020-02-21 11:33:28.212-06	1	\N	0	0	0	0	USD
00449ca6-fd93-4a4c-857d-09f49315fccf	2020-03-25 16:36:15.98-06	COMPLETE	Alejandro Castro	jandor.ac@gmail.com	Alejandro Castro	2020-03-25 16:11:37.126-06	ON_SITE	f	\N	\N	f	es	f	Alejandro	Castro	\N	\N	4	NOT_INCLUDED	\N	\N	f	0.00	f	2020-03-25 16:11:15.981-06	\N	\N	\N	\N	\N	\N	t	f	\N	2020-03-25 16:11:40.218-06	1	f	1500	1500	0	0	USD
\.


--
-- Data for Name: waiting_queue; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.waiting_queue (id, creation, event_id, status, full_name, email_address, ticket_reservation_id, user_language, subscription_type, selected_category_id, first_name, last_name, organization_id_fk) FROM stdin;
\.


--
-- Data for Name: whitelisted_ticket; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.whitelisted_ticket (group_member_id_fk, group_link_id_fk, ticket_id_fk, requires_unique_value, organization_id_fk) FROM stdin;
\.


--
-- Name: a_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.a_group_id_seq', 1, false);


--
-- Name: additional_service_description_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.additional_service_description_id_seq', 12, true);


--
-- Name: additional_service_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.additional_service_id_seq', 6, true);


--
-- Name: additional_service_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.additional_service_item_id_seq', 27, true);


--
-- Name: admin_job_queue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.admin_job_queue_id_seq', 23516, true);


--
-- Name: admin_reservation_request_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.admin_reservation_request_id_seq', 1, false);


--
-- Name: b_transaction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.b_transaction_id_seq', 1, true);


--
-- Name: ba_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ba_user_id_seq', 3, true);


--
-- Name: billing_document_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.billing_document_id_seq', 3, true);


--
-- Name: configuration_event_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.configuration_event_id_seq', 23, true);


--
-- Name: configuration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.configuration_id_seq', 15, true);


--
-- Name: configuration_organization_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.configuration_organization_id_seq', 1, true);


--
-- Name: configuration_ticket_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.configuration_ticket_category_id_seq', 1, false);


--
-- Name: dynamic_field_template_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.dynamic_field_template_id_seq', 8, true);


--
-- Name: email_message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.email_message_id_seq', 23, true);


--
-- Name: event_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.event_id_seq', 4, true);


--
-- Name: event_migration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.event_migration_id_seq', 1, true);


--
-- Name: extension_configuration_metadata_ecm_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.extension_configuration_metadata_ecm_id_seq', 1, false);


--
-- Name: extension_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.extension_log_id_seq', 1, false);


--
-- Name: extension_support_es_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.extension_support_es_id_seq', 1, false);


--
-- Name: group_link_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.group_link_id_seq', 1, false);


--
-- Name: group_member_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.group_member_id_seq', 1, false);


--
-- Name: organization_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.organization_id_seq', 1, true);


--
-- Name: promo_code_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.promo_code_id_seq', 1, true);


--
-- Name: special_price_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.special_price_id_seq', 1, false);


--
-- Name: sponsor_scan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sponsor_scan_id_seq', 1, false);


--
-- Name: ticket_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ticket_category_id_seq', 5, true);


--
-- Name: ticket_field_configuration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ticket_field_configuration_id_seq', 2, true);


--
-- Name: ticket_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ticket_id_seq', 350, true);


--
-- Name: waiting_queue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.waiting_queue_id_seq', 1, false);


--
-- Name: a_group a_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.a_group
    ADD CONSTRAINT a_group_pkey PRIMARY KEY (id);


--
-- Name: a_group a_group_unique_name_org_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.a_group
    ADD CONSTRAINT a_group_unique_name_org_id UNIQUE (name, organization_id_fk);


--
-- Name: additional_service_description additional_service_description_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.additional_service_description
    ADD CONSTRAINT additional_service_description_pkey PRIMARY KEY (id);


--
-- Name: additional_service_field_value additional_service_field_value_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.additional_service_field_value
    ADD CONSTRAINT additional_service_field_value_pkey PRIMARY KEY (additional_service_id_fk, ticket_field_configuration_id_fk);


--
-- Name: additional_service_item additional_service_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.additional_service_item
    ADD CONSTRAINT additional_service_item_pkey PRIMARY KEY (id);


--
-- Name: additional_service additional_service_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.additional_service
    ADD CONSTRAINT additional_service_pkey PRIMARY KEY (id);


--
-- Name: admin_job_queue admin_job_queue_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.admin_job_queue
    ADD CONSTRAINT admin_job_queue_pkey PRIMARY KEY (id);


--
-- Name: admin_reservation_request admin_reservation_request_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.admin_reservation_request
    ADD CONSTRAINT admin_reservation_request_pkey PRIMARY KEY (id);


--
-- Name: b_transaction b_transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.b_transaction
    ADD CONSTRAINT b_transaction_pkey PRIMARY KEY (id);


--
-- Name: ba_user ba_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ba_user
    ADD CONSTRAINT ba_user_pkey PRIMARY KEY (id);


--
-- Name: billing_document billing_document_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.billing_document
    ADD CONSTRAINT billing_document_pkey PRIMARY KEY (id);


--
-- Name: configuration_event configuration_event_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.configuration_event
    ADD CONSTRAINT configuration_event_pkey PRIMARY KEY (id);


--
-- Name: configuration_organization configuration_organization_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.configuration_organization
    ADD CONSTRAINT configuration_organization_pkey PRIMARY KEY (id);


--
-- Name: configuration configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.configuration
    ADD CONSTRAINT configuration_pkey PRIMARY KEY (id);


--
-- Name: configuration_ticket_category configuration_ticket_category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.configuration_ticket_category
    ADD CONSTRAINT configuration_ticket_category_pkey PRIMARY KEY (id);


--
-- Name: dynamic_field_template dynamic_field_template_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dynamic_field_template
    ADD CONSTRAINT dynamic_field_template_pkey PRIMARY KEY (id);


--
-- Name: email_message email_message_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.email_message
    ADD CONSTRAINT email_message_pkey PRIMARY KEY (id);


--
-- Name: event_description_text event_description_text_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.event_description_text
    ADD CONSTRAINT event_description_text_pkey PRIMARY KEY (event_id_fk, locale, type);


--
-- Name: event_migration event_migration_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.event_migration
    ADD CONSTRAINT event_migration_pkey PRIMARY KEY (id);


--
-- Name: event event_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.event
    ADD CONSTRAINT event_pkey PRIMARY KEY (id);


--
-- Name: extension_configuration_metadata extension_configuration_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.extension_configuration_metadata
    ADD CONSTRAINT extension_configuration_metadata_pkey PRIMARY KEY (ecm_id);


--
-- Name: extension_log extension_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.extension_log
    ADD CONSTRAINT extension_log_pkey PRIMARY KEY (id);


--
-- Name: extension_support extension_support_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.extension_support
    ADD CONSTRAINT extension_support_pkey PRIMARY KEY (es_id);


--
-- Name: file_blob file_blob_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.file_blob
    ADD CONSTRAINT file_blob_pkey PRIMARY KEY (id);


--
-- Name: flyway_schema_history flyway_schema_history_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.flyway_schema_history
    ADD CONSTRAINT flyway_schema_history_pk PRIMARY KEY (installed_rank);


--
-- Name: group_link group_link_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.group_link
    ADD CONSTRAINT group_link_pkey PRIMARY KEY (id);


--
-- Name: group_member group_member_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.group_member
    ADD CONSTRAINT group_member_pkey PRIMARY KEY (id);


--
-- Name: group_member group_member_unique_value; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.group_member
    ADD CONSTRAINT group_member_unique_value UNIQUE (a_group_id_fk, value);


--
-- Name: invoice_sequences invoice_sequences_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.invoice_sequences
    ADD CONSTRAINT invoice_sequences_pkey PRIMARY KEY (organization_id_fk);


--
-- Name: organization organization_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.organization
    ADD CONSTRAINT organization_pkey PRIMARY KEY (id);


--
-- Name: promo_code promo_code_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.promo_code
    ADD CONSTRAINT promo_code_pkey PRIMARY KEY (id);


--
-- Name: qrtz_blob_triggers qrtz_blob_triggers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qrtz_blob_triggers
    ADD CONSTRAINT qrtz_blob_triggers_pkey PRIMARY KEY (sched_name, trigger_name, trigger_group);


--
-- Name: qrtz_calendars qrtz_calendars_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qrtz_calendars
    ADD CONSTRAINT qrtz_calendars_pkey PRIMARY KEY (sched_name, calendar_name);


--
-- Name: qrtz_cron_triggers qrtz_cron_triggers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qrtz_cron_triggers
    ADD CONSTRAINT qrtz_cron_triggers_pkey PRIMARY KEY (sched_name, trigger_name, trigger_group);


--
-- Name: qrtz_fired_triggers qrtz_fired_triggers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qrtz_fired_triggers
    ADD CONSTRAINT qrtz_fired_triggers_pkey PRIMARY KEY (sched_name, entry_id);


--
-- Name: qrtz_job_details qrtz_job_details_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qrtz_job_details
    ADD CONSTRAINT qrtz_job_details_pkey PRIMARY KEY (sched_name, job_name, job_group);


--
-- Name: qrtz_locks qrtz_locks_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qrtz_locks
    ADD CONSTRAINT qrtz_locks_pkey PRIMARY KEY (sched_name, lock_name);


--
-- Name: qrtz_paused_trigger_grps qrtz_paused_trigger_grps_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qrtz_paused_trigger_grps
    ADD CONSTRAINT qrtz_paused_trigger_grps_pkey PRIMARY KEY (sched_name, trigger_group);


--
-- Name: qrtz_scheduler_state qrtz_scheduler_state_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qrtz_scheduler_state
    ADD CONSTRAINT qrtz_scheduler_state_pkey PRIMARY KEY (sched_name, instance_name);


--
-- Name: qrtz_simple_triggers qrtz_simple_triggers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qrtz_simple_triggers
    ADD CONSTRAINT qrtz_simple_triggers_pkey PRIMARY KEY (sched_name, trigger_name, trigger_group);


--
-- Name: qrtz_simprop_triggers qrtz_simprop_triggers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qrtz_simprop_triggers
    ADD CONSTRAINT qrtz_simprop_triggers_pkey PRIMARY KEY (sched_name, trigger_name, trigger_group);


--
-- Name: qrtz_triggers qrtz_triggers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qrtz_triggers
    ADD CONSTRAINT qrtz_triggers_pkey PRIMARY KEY (sched_name, trigger_name, trigger_group);


--
-- Name: resource_global resource_global_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_global
    ADD CONSTRAINT resource_global_pkey PRIMARY KEY (name);


--
-- Name: special_price special_price_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.special_price
    ADD CONSTRAINT special_price_pkey PRIMARY KEY (id);


--
-- Name: sponsor_scan sponsor_scan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sponsor_scan
    ADD CONSTRAINT sponsor_scan_pkey PRIMARY KEY (id);


--
-- Name: spring_session_attributes spring_session_attributes_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.spring_session_attributes
    ADD CONSTRAINT spring_session_attributes_pk PRIMARY KEY (session_id, attribute_name);


--
-- Name: spring_session spring_session_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.spring_session
    ADD CONSTRAINT spring_session_pk PRIMARY KEY (session_id);


--
-- Name: sponsor_scan spsc_unique_ticket; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sponsor_scan
    ADD CONSTRAINT spsc_unique_ticket UNIQUE (event_id, ticket_id, user_id);


--
-- Name: ticket_category ticket_category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_category
    ADD CONSTRAINT ticket_category_pkey PRIMARY KEY (id);


--
-- Name: ticket_category_text ticket_category_text_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_category_text
    ADD CONSTRAINT ticket_category_text_pkey PRIMARY KEY (ticket_category_id_fk, locale);


--
-- Name: ticket_field_configuration ticket_field_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_field_configuration
    ADD CONSTRAINT ticket_field_configuration_pkey PRIMARY KEY (id);


--
-- Name: ticket_field_description ticket_field_description_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_field_description
    ADD CONSTRAINT ticket_field_description_pkey PRIMARY KEY (ticket_field_configuration_id_fk, field_locale);


--
-- Name: ticket_field_value ticket_field_value_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_field_value
    ADD CONSTRAINT ticket_field_value_pkey PRIMARY KEY (ticket_id_fk, ticket_field_configuration_id_fk);


--
-- Name: ticket ticket_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket
    ADD CONSTRAINT ticket_pkey PRIMARY KEY (id);


--
-- Name: tickets_reservation tickets_reservation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tickets_reservation
    ADD CONSTRAINT tickets_reservation_pkey PRIMARY KEY (id);


--
-- Name: additional_service_description unique_asd; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.additional_service_description
    ADD CONSTRAINT unique_asd UNIQUE (additional_service_id_fk, locale, type);


--
-- Name: ticket_category unique_category_code_for_event; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_category
    ADD CONSTRAINT unique_category_code_for_event UNIQUE (event_id, category_code);


--
-- Name: special_price unique_code; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.special_price
    ADD CONSTRAINT unique_code UNIQUE (code);


--
-- Name: configuration unique_configuration_c_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.configuration
    ADD CONSTRAINT unique_configuration_c_key UNIQUE (c_key);


--
-- Name: configuration_event unique_configuration_event; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.configuration_event
    ADD CONSTRAINT unique_configuration_event UNIQUE (organization_id_fk, event_id_fk, c_key);


--
-- Name: configuration_organization unique_configuration_organization; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.configuration_organization
    ADD CONSTRAINT unique_configuration_organization UNIQUE (organization_id_fk, c_key);


--
-- Name: configuration_ticket_category unique_configuration_ticket_category; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.configuration_ticket_category
    ADD CONSTRAINT unique_configuration_ticket_category UNIQUE (organization_id_fk, event_id_fk, ticket_category_id_fk, c_key);


--
-- Name: dynamic_field_template unique_dynamic_template_name; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dynamic_field_template
    ADD CONSTRAINT unique_dynamic_template_name UNIQUE (field_name);


--
-- Name: event_migration unique_event_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.event_migration
    ADD CONSTRAINT unique_event_id UNIQUE (event_id);


--
-- Name: event unique_event_name; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.event
    ADD CONSTRAINT unique_event_name UNIQUE (short_name);


--
-- Name: extension_configuration_metadata unique_extension_configuration_metadata; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.extension_configuration_metadata
    ADD CONSTRAINT unique_extension_configuration_metadata UNIQUE (ecm_es_id_fk, ecm_name, ecm_configuration_level);


--
-- Name: extension_configuration_metadata_value unique_extension_configuration_metadata_value; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.extension_configuration_metadata_value
    ADD CONSTRAINT unique_extension_configuration_metadata_value UNIQUE (fk_ecm_id, conf_path);


--
-- Name: extension_event unique_extension_event; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.extension_event
    ADD CONSTRAINT unique_extension_event UNIQUE (es_id_fk, event);


--
-- Name: extension_support unique_extension_support; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.extension_support
    ADD CONSTRAINT unique_extension_support UNIQUE (path, name);


--
-- Name: admin_job_queue unique_job_schedule; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.admin_job_queue
    ADD CONSTRAINT unique_job_schedule UNIQUE (job_name, request_ts);


--
-- Name: organization unique_org_name; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.organization
    ADD CONSTRAINT unique_org_name UNIQUE (name);


--
-- Name: event unique_private_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.event
    ADD CONSTRAINT unique_private_key UNIQUE (private_key);


--
-- Name: resource_event unique_resource_event; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_event
    ADD CONSTRAINT unique_resource_event UNIQUE (name, organization_id_fk, event_id_fk);


--
-- Name: resource_organizer unique_resource_organizer; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_organizer
    ADD CONSTRAINT unique_resource_organizer UNIQUE (name, organization_id_fk);


--
-- Name: ticket unique_special_price; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket
    ADD CONSTRAINT unique_special_price UNIQUE (special_price_id_fk);


--
-- Name: ticket_field_configuration unique_ticket_field_configuration; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_field_configuration
    ADD CONSTRAINT unique_ticket_field_configuration UNIQUE (event_id_fk, field_name, additional_service_id, context);


--
-- Name: ticket unique_ticket_uuid; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket
    ADD CONSTRAINT unique_ticket_uuid UNIQUE (uuid);


--
-- Name: ba_user unique_username; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ba_user
    ADD CONSTRAINT unique_username UNIQUE (username);


--
-- Name: waiting_queue waiting_queue_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.waiting_queue
    ADD CONSTRAINT waiting_queue_pkey PRIMARY KEY (id);


--
-- Name: whitelisted_ticket whitelisted_ticket_unique_item_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.whitelisted_ticket
    ADD CONSTRAINT whitelisted_ticket_unique_item_id UNIQUE (group_member_id_fk, group_link_id_fk, requires_unique_value);


--
-- Name: waiting_queue wq_unique_email; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.waiting_queue
    ADD CONSTRAINT wq_unique_email UNIQUE (event_id, email_address);


--
-- Name: admin_reservation_request_req_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX admin_reservation_request_req_idx ON public.admin_reservation_request USING btree (request_id);


--
-- Name: admin_reservation_request_res_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX admin_reservation_request_res_idx ON public.admin_reservation_request USING btree (reservation_id);


--
-- Name: admin_reservation_request_sts_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX admin_reservation_request_sts_idx ON public.admin_reservation_request USING btree (status);


--
-- Name: auditing_event_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auditing_event_id_idx ON public.auditing USING btree (event_id);


--
-- Name: auditing_reservation_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auditing_reservation_id_idx ON public.auditing USING btree (reservation_id);


--
-- Name: b_transaction_reservation_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX b_transaction_reservation_id_idx ON public.b_transaction USING btree (reservation_id);


--
-- Name: event_org_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX event_org_id_idx ON public.event USING btree (org_id);


--
-- Name: flyway_schema_history_s_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX flyway_schema_history_s_idx ON public.flyway_schema_history USING btree (success);


--
-- Name: idx_email_event_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_email_event_id ON public.email_message USING btree (event_id);


--
-- Name: idx_email_msg_checksum; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_email_msg_checksum ON public.email_message USING btree (checksum);


--
-- Name: idx_qrtz_ft_inst_job_req_rcvry; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_qrtz_ft_inst_job_req_rcvry ON public.qrtz_fired_triggers USING btree (sched_name, instance_name, requests_recovery);


--
-- Name: idx_qrtz_ft_j_g; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_qrtz_ft_j_g ON public.qrtz_fired_triggers USING btree (sched_name, job_name, job_group);


--
-- Name: idx_qrtz_ft_jg; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_qrtz_ft_jg ON public.qrtz_fired_triggers USING btree (sched_name, job_group);


--
-- Name: idx_qrtz_ft_t_g; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_qrtz_ft_t_g ON public.qrtz_fired_triggers USING btree (sched_name, trigger_name, trigger_group);


--
-- Name: idx_qrtz_ft_tg; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_qrtz_ft_tg ON public.qrtz_fired_triggers USING btree (sched_name, trigger_group);


--
-- Name: idx_qrtz_ft_trig_inst_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_qrtz_ft_trig_inst_name ON public.qrtz_fired_triggers USING btree (sched_name, instance_name);


--
-- Name: idx_qrtz_j_grp; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_qrtz_j_grp ON public.qrtz_job_details USING btree (sched_name, job_group);


--
-- Name: idx_qrtz_j_req_recovery; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_qrtz_j_req_recovery ON public.qrtz_job_details USING btree (sched_name, requests_recovery);


--
-- Name: idx_qrtz_t_c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_qrtz_t_c ON public.qrtz_triggers USING btree (sched_name, calendar_name);


--
-- Name: idx_qrtz_t_g; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_qrtz_t_g ON public.qrtz_triggers USING btree (sched_name, trigger_group);


--
-- Name: idx_qrtz_t_j; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_qrtz_t_j ON public.qrtz_triggers USING btree (sched_name, job_name, job_group);


--
-- Name: idx_qrtz_t_jg; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_qrtz_t_jg ON public.qrtz_triggers USING btree (sched_name, job_group);


--
-- Name: idx_qrtz_t_n_g_state; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_qrtz_t_n_g_state ON public.qrtz_triggers USING btree (sched_name, trigger_group, trigger_state);


--
-- Name: idx_qrtz_t_n_state; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_qrtz_t_n_state ON public.qrtz_triggers USING btree (sched_name, trigger_name, trigger_group, trigger_state);


--
-- Name: idx_qrtz_t_next_fire_time; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_qrtz_t_next_fire_time ON public.qrtz_triggers USING btree (sched_name, next_fire_time);


--
-- Name: idx_qrtz_t_nft_misfire; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_qrtz_t_nft_misfire ON public.qrtz_triggers USING btree (sched_name, misfire_instr, next_fire_time);


--
-- Name: idx_qrtz_t_nft_st; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_qrtz_t_nft_st ON public.qrtz_triggers USING btree (sched_name, trigger_state, next_fire_time);


--
-- Name: idx_qrtz_t_nft_st_misfire; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_qrtz_t_nft_st_misfire ON public.qrtz_triggers USING btree (sched_name, misfire_instr, next_fire_time, trigger_state);


--
-- Name: idx_qrtz_t_nft_st_misfire_grp; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_qrtz_t_nft_st_misfire_grp ON public.qrtz_triggers USING btree (sched_name, misfire_instr, next_fire_time, trigger_group, trigger_state);


--
-- Name: idx_qrtz_t_state; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_qrtz_t_state ON public.qrtz_triggers USING btree (sched_name, trigger_state);


--
-- Name: j_user_organization_org_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX j_user_organization_org_id_idx ON public.j_user_organization USING btree (org_id);


--
-- Name: j_user_organization_user_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX j_user_organization_user_id_idx ON public.j_user_organization USING btree (user_id);


--
-- Name: promo_code_event_id_fk_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX promo_code_event_id_fk_idx ON public.promo_code USING btree (event_id_fk);


--
-- Name: spring_session_attributes_ix1; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX spring_session_attributes_ix1 ON public.spring_session_attributes USING btree (session_id);


--
-- Name: spring_session_ix1; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX spring_session_ix1 ON public.spring_session USING btree (last_access_time);


--
-- Name: ticket_category_event_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ticket_category_event_id_idx ON public.ticket_category USING btree (event_id);


--
-- Name: ticket_category_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ticket_category_id_idx ON public.ticket USING btree (category_id);


--
-- Name: ticket_event_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ticket_event_id_idx ON public.ticket USING btree (event_id);


--
-- Name: ticket_ref_unique; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ticket_ref_unique ON public.ticket USING btree (event_id, ext_reference);


--
-- Name: ticket_tickets_reservation_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ticket_tickets_reservation_id_idx ON public.ticket USING btree (tickets_reservation_id);


--
-- Name: tickets_reservation_promo_code_id_fk_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tickets_reservation_promo_code_id_fk_idx ON public.tickets_reservation USING btree (promo_code_id_fk);


--
-- Name: unique_promo_code_for_event; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX unique_promo_code_for_event ON public.promo_code USING btree (promo_code, event_id_fk) WHERE (event_id_fk IS NOT NULL);


--
-- Name: unique_promo_code_for_org; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX unique_promo_code_for_org ON public.promo_code USING btree (promo_code, organization_id_fk) WHERE (event_id_fk IS NULL);


--
-- Name: additional_service_description additional_service_description_insert_org_id_fk_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER additional_service_description_insert_org_id_fk_trigger BEFORE INSERT ON public.additional_service_description FOR EACH ROW EXECUTE PROCEDURE public.set_organization_id_fk_from_additional_service_id_fk();


--
-- Name: additional_service additional_service_insert_org_id_fk_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER additional_service_insert_org_id_fk_trigger BEFORE INSERT ON public.additional_service FOR EACH ROW EXECUTE PROCEDURE public.set_organization_id_fk_from_event_id_fk();


--
-- Name: additional_service_item additional_service_item_insert_org_id_fk_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER additional_service_item_insert_org_id_fk_trigger BEFORE INSERT ON public.additional_service_item FOR EACH ROW EXECUTE PROCEDURE public.set_organization_id_fk_from_event_id_fk();


--
-- Name: admin_reservation_request admin_reservation_request_insert_org_id_fk_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER admin_reservation_request_insert_org_id_fk_trigger BEFORE INSERT ON public.admin_reservation_request FOR EACH ROW EXECUTE PROCEDURE public.set_organization_id_fk_from_event_id();


--
-- Name: auditing auditing_insert_org_id_fk_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER auditing_insert_org_id_fk_trigger BEFORE INSERT ON public.auditing FOR EACH ROW EXECUTE PROCEDURE public.set_organization_id_fk_from_event_id();


--
-- Name: b_transaction b_transaction_insert_org_id_fk_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER b_transaction_insert_org_id_fk_trigger BEFORE INSERT ON public.b_transaction FOR EACH ROW EXECUTE PROCEDURE public.set_organization_id_fk_from_reservation_id();


--
-- Name: email_message email_message_insert_org_id_fk_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER email_message_insert_org_id_fk_trigger BEFORE INSERT ON public.email_message FOR EACH ROW EXECUTE PROCEDURE public.set_organization_id_fk_from_event_id();


--
-- Name: event_description_text event_description_text_insert_org_id_fk_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER event_description_text_insert_org_id_fk_trigger BEFORE INSERT ON public.event_description_text FOR EACH ROW EXECUTE PROCEDURE public.set_organization_id_fk_from_event_id_fk();


--
-- Name: group_link group_link_insert_org_id_fk_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER group_link_insert_org_id_fk_trigger BEFORE INSERT ON public.group_link FOR EACH ROW EXECUTE PROCEDURE public.set_organization_id_fk_from_event_id_fk();


--
-- Name: group_member group_member_insert_org_id_fk_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER group_member_insert_org_id_fk_trigger BEFORE INSERT ON public.group_member FOR EACH ROW EXECUTE PROCEDURE public.set_organization_id_fk_from_a_group_id_fk();


--
-- Name: scan_audit scan_audit_insert_org_id_fk_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER scan_audit_insert_org_id_fk_trigger BEFORE INSERT ON public.scan_audit FOR EACH ROW EXECUTE PROCEDURE public.set_organization_id_fk_from_event_id_fk();


--
-- Name: special_price special_price_insert_org_id_fk_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER special_price_insert_org_id_fk_trigger BEFORE INSERT ON public.special_price FOR EACH ROW EXECUTE PROCEDURE public.set_organization_id_fk_from_ticket_category_id();


--
-- Name: sponsor_scan sponsor_scan_insert_org_id_fk_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER sponsor_scan_insert_org_id_fk_trigger BEFORE INSERT ON public.sponsor_scan FOR EACH ROW EXECUTE PROCEDURE public.set_organization_id_fk_from_event_id();


--
-- Name: ticket_category ticket_category_insert_org_id_fk_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER ticket_category_insert_org_id_fk_trigger BEFORE INSERT ON public.ticket_category FOR EACH ROW EXECUTE PROCEDURE public.set_organization_id_fk_from_event_id();


--
-- Name: ticket_category_text ticket_category_text_insert_org_id_fk_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER ticket_category_text_insert_org_id_fk_trigger BEFORE INSERT ON public.ticket_category_text FOR EACH ROW EXECUTE PROCEDURE public.set_organization_id_fk_from_ticket_category_id_fk();


--
-- Name: ticket_field_configuration ticket_field_configuration_insert_org_id_fk_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER ticket_field_configuration_insert_org_id_fk_trigger BEFORE INSERT ON public.ticket_field_configuration FOR EACH ROW EXECUTE PROCEDURE public.set_organization_id_fk_from_event_id_fk();


--
-- Name: ticket_field_description ticket_field_description_insert_org_id_fk_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER ticket_field_description_insert_org_id_fk_trigger BEFORE INSERT ON public.ticket_field_description FOR EACH ROW EXECUTE PROCEDURE public.set_organization_id_fk_from_ticket_field_configuration_id_fk();


--
-- Name: ticket_field_value ticket_field_value_insert_org_id_fk_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER ticket_field_value_insert_org_id_fk_trigger BEFORE INSERT ON public.ticket_field_value FOR EACH ROW EXECUTE PROCEDURE public.set_organization_id_fk_from_ticket_field_configuration_id_fk();


--
-- Name: ticket ticket_insert_org_id_fk_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER ticket_insert_org_id_fk_trigger BEFORE INSERT ON public.ticket FOR EACH ROW EXECUTE PROCEDURE public.set_organization_id_fk_from_event_id();


--
-- Name: tickets_reservation tickets_reservation_insert_org_id_fk_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER tickets_reservation_insert_org_id_fk_trigger BEFORE INSERT ON public.tickets_reservation FOR EACH ROW EXECUTE PROCEDURE public.set_organization_id_fk_from_event_id_fk();


--
-- Name: special_price tr_check_access_code_allocation; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER tr_check_access_code_allocation AFTER UPDATE ON public.special_price FOR EACH ROW EXECUTE PROCEDURE public.trf_check_access_code_allocation();


--
-- Name: waiting_queue waiting_queue_insert_org_id_fk_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER waiting_queue_insert_org_id_fk_trigger BEFORE INSERT ON public.waiting_queue FOR EACH ROW EXECUTE PROCEDURE public.set_organization_id_fk_from_event_id();


--
-- Name: whitelisted_ticket whitelisted_ticket_insert_org_id_fk_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER whitelisted_ticket_insert_org_id_fk_trigger BEFORE INSERT ON public.whitelisted_ticket FOR EACH ROW EXECUTE PROCEDURE public.set_organization_id_fk_from_group_member_id_fk();


--
-- Name: a_group a_group_org_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.a_group
    ADD CONSTRAINT a_group_org_id_fk FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: special_price access_code_promo_code_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.special_price
    ADD CONSTRAINT access_code_promo_code_id FOREIGN KEY (access_code_id_fk) REFERENCES public.promo_code(id);


--
-- Name: additional_service_description additional_service_description_additional_service_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.additional_service_description
    ADD CONSTRAINT additional_service_description_additional_service_id_fk_fkey FOREIGN KEY (additional_service_id_fk) REFERENCES public.additional_service(id);


--
-- Name: additional_service_description additional_service_description_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.additional_service_description
    ADD CONSTRAINT additional_service_description_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: additional_service additional_service_event_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.additional_service
    ADD CONSTRAINT additional_service_event_id_fk_fkey FOREIGN KEY (event_id_fk) REFERENCES public.event(id);


--
-- Name: additional_service_field_value additional_service_field_valu_ticket_field_configuration_i_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.additional_service_field_value
    ADD CONSTRAINT additional_service_field_valu_ticket_field_configuration_i_fkey FOREIGN KEY (ticket_field_configuration_id_fk) REFERENCES public.ticket_field_configuration(id);


--
-- Name: additional_service_field_value additional_service_field_value_additional_service_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.additional_service_field_value
    ADD CONSTRAINT additional_service_field_value_additional_service_id_fk_fkey FOREIGN KEY (additional_service_id_fk) REFERENCES public.additional_service(id);


--
-- Name: additional_service_item additional_service_item_additional_service_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.additional_service_item
    ADD CONSTRAINT additional_service_item_additional_service_id_fk_fkey FOREIGN KEY (additional_service_id_fk) REFERENCES public.additional_service(id);


--
-- Name: additional_service_item additional_service_item_event_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.additional_service_item
    ADD CONSTRAINT additional_service_item_event_id_fk_fkey FOREIGN KEY (event_id_fk) REFERENCES public.event(id);


--
-- Name: additional_service_item additional_service_item_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.additional_service_item
    ADD CONSTRAINT additional_service_item_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: additional_service additional_service_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.additional_service
    ADD CONSTRAINT additional_service_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: admin_reservation_request admin_reservation_request_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.admin_reservation_request
    ADD CONSTRAINT admin_reservation_request_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: auditing auditing_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auditing
    ADD CONSTRAINT auditing_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: b_transaction b_transaction_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.b_transaction
    ADD CONSTRAINT b_transaction_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: b_transaction b_transaction_reservation_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.b_transaction
    ADD CONSTRAINT b_transaction_reservation_id_fkey FOREIGN KEY (reservation_id) REFERENCES public.tickets_reservation(id);


--
-- Name: billing_document billing_document_event_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.billing_document
    ADD CONSTRAINT billing_document_event_id_fk FOREIGN KEY (event_id_fk) REFERENCES public.event(id);


--
-- Name: billing_document billing_document_organization_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.billing_document
    ADD CONSTRAINT billing_document_organization_id_fk FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: billing_document billing_document_reservation_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.billing_document
    ADD CONSTRAINT billing_document_reservation_id_fk FOREIGN KEY (reservation_id_fk) REFERENCES public.tickets_reservation(id);


--
-- Name: configuration_event configuration_event_event_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.configuration_event
    ADD CONSTRAINT configuration_event_event_id_fk_fkey FOREIGN KEY (event_id_fk) REFERENCES public.event(id);


--
-- Name: configuration_event configuration_event_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.configuration_event
    ADD CONSTRAINT configuration_event_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: configuration_organization configuration_organization_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.configuration_organization
    ADD CONSTRAINT configuration_organization_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: configuration_ticket_category configuration_ticket_category_event_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.configuration_ticket_category
    ADD CONSTRAINT configuration_ticket_category_event_id_fk_fkey FOREIGN KEY (event_id_fk) REFERENCES public.event(id);


--
-- Name: configuration_ticket_category configuration_ticket_category_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.configuration_ticket_category
    ADD CONSTRAINT configuration_ticket_category_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: configuration_ticket_category configuration_ticket_category_ticket_category_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.configuration_ticket_category
    ADD CONSTRAINT configuration_ticket_category_ticket_category_id_fk_fkey FOREIGN KEY (ticket_category_id_fk) REFERENCES public.ticket_category(id);


--
-- Name: email_message email_message_event_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.email_message
    ADD CONSTRAINT email_message_event_id_fkey FOREIGN KEY (event_id) REFERENCES public.event(id);


--
-- Name: email_message email_message_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.email_message
    ADD CONSTRAINT email_message_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: event_description_text event_description_text_event_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.event_description_text
    ADD CONSTRAINT event_description_text_event_id_fk_fkey FOREIGN KEY (event_id_fk) REFERENCES public.event(id);


--
-- Name: event_description_text event_description_text_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.event_description_text
    ADD CONSTRAINT event_description_text_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: event event_file_blob_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.event
    ADD CONSTRAINT event_file_blob_id_fkey FOREIGN KEY (file_blob_id) REFERENCES public.file_blob(id);


--
-- Name: event_migration event_migration_event_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.event_migration
    ADD CONSTRAINT event_migration_event_id_fkey FOREIGN KEY (event_id) REFERENCES public.event(id);


--
-- Name: event event_org_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.event
    ADD CONSTRAINT event_org_id_fkey FOREIGN KEY (org_id) REFERENCES public.organization(id);


--
-- Name: extension_configuration_metadata extension_configuration_metadata_ecm_es_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.extension_configuration_metadata
    ADD CONSTRAINT extension_configuration_metadata_ecm_es_id_fk_fkey FOREIGN KEY (ecm_es_id_fk) REFERENCES public.extension_support(es_id) ON DELETE CASCADE;


--
-- Name: extension_configuration_metadata_value extension_configuration_metadata_value_fk_ecm_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.extension_configuration_metadata_value
    ADD CONSTRAINT extension_configuration_metadata_value_fk_ecm_id_fkey FOREIGN KEY (fk_ecm_id) REFERENCES public.extension_configuration_metadata(ecm_id) ON DELETE CASCADE;


--
-- Name: extension_event extension_event_es_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.extension_event
    ADD CONSTRAINT extension_event_es_id_fk_fkey FOREIGN KEY (es_id_fk) REFERENCES public.extension_support(es_id);


--
-- Name: group_link group_link_a_group_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.group_link
    ADD CONSTRAINT group_link_a_group_id_fk FOREIGN KEY (a_group_id_fk) REFERENCES public.a_group(id);


--
-- Name: group_link group_link_event_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.group_link
    ADD CONSTRAINT group_link_event_id_fk FOREIGN KEY (event_id_fk) REFERENCES public.event(id);


--
-- Name: group_link group_link_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.group_link
    ADD CONSTRAINT group_link_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: group_link group_link_ticket_category_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.group_link
    ADD CONSTRAINT group_link_ticket_category_id_fk FOREIGN KEY (ticket_category_id_fk) REFERENCES public.ticket_category(id);


--
-- Name: group_member group_member_a_group_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.group_member
    ADD CONSTRAINT group_member_a_group_id_fk FOREIGN KEY (a_group_id_fk) REFERENCES public.a_group(id);


--
-- Name: group_member group_member_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.group_member
    ADD CONSTRAINT group_member_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: invoice_sequences invoice_sequences_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.invoice_sequences
    ADD CONSTRAINT invoice_sequences_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: j_user_organization j_user_organization_org_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.j_user_organization
    ADD CONSTRAINT j_user_organization_org_id_fkey FOREIGN KEY (org_id) REFERENCES public.organization(id);


--
-- Name: j_user_organization j_user_organization_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.j_user_organization
    ADD CONSTRAINT j_user_organization_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.ba_user(id);


--
-- Name: promo_code promo_code_event_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.promo_code
    ADD CONSTRAINT promo_code_event_id_fk_fkey FOREIGN KEY (event_id_fk) REFERENCES public.event(id);


--
-- Name: promo_code promo_code_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.promo_code
    ADD CONSTRAINT promo_code_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: qrtz_blob_triggers qrtz_blob_triggers_sched_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qrtz_blob_triggers
    ADD CONSTRAINT qrtz_blob_triggers_sched_name_fkey FOREIGN KEY (sched_name, trigger_name, trigger_group) REFERENCES public.qrtz_triggers(sched_name, trigger_name, trigger_group);


--
-- Name: qrtz_cron_triggers qrtz_cron_triggers_sched_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qrtz_cron_triggers
    ADD CONSTRAINT qrtz_cron_triggers_sched_name_fkey FOREIGN KEY (sched_name, trigger_name, trigger_group) REFERENCES public.qrtz_triggers(sched_name, trigger_name, trigger_group);


--
-- Name: qrtz_simple_triggers qrtz_simple_triggers_sched_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qrtz_simple_triggers
    ADD CONSTRAINT qrtz_simple_triggers_sched_name_fkey FOREIGN KEY (sched_name, trigger_name, trigger_group) REFERENCES public.qrtz_triggers(sched_name, trigger_name, trigger_group);


--
-- Name: qrtz_simprop_triggers qrtz_simprop_triggers_sched_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qrtz_simprop_triggers
    ADD CONSTRAINT qrtz_simprop_triggers_sched_name_fkey FOREIGN KEY (sched_name, trigger_name, trigger_group) REFERENCES public.qrtz_triggers(sched_name, trigger_name, trigger_group);


--
-- Name: qrtz_triggers qrtz_triggers_sched_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qrtz_triggers
    ADD CONSTRAINT qrtz_triggers_sched_name_fkey FOREIGN KEY (sched_name, job_name, job_group) REFERENCES public.qrtz_job_details(sched_name, job_name, job_group);


--
-- Name: resource_event resource_event_event_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_event
    ADD CONSTRAINT resource_event_event_id_fk_fkey FOREIGN KEY (event_id_fk) REFERENCES public.event(id);


--
-- Name: resource_event resource_event_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_event
    ADD CONSTRAINT resource_event_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: resource_organizer resource_organizer_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_organizer
    ADD CONSTRAINT resource_organizer_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: scan_audit scan_audit_event_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scan_audit
    ADD CONSTRAINT scan_audit_event_id_fk_fkey FOREIGN KEY (event_id_fk) REFERENCES public.event(id);


--
-- Name: scan_audit scan_audit_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scan_audit
    ADD CONSTRAINT scan_audit_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: special_price special_price_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.special_price
    ADD CONSTRAINT special_price_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: sponsor_scan sponsor_scan_event_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sponsor_scan
    ADD CONSTRAINT sponsor_scan_event_id_fkey FOREIGN KEY (event_id) REFERENCES public.event(id);


--
-- Name: sponsor_scan sponsor_scan_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sponsor_scan
    ADD CONSTRAINT sponsor_scan_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: sponsor_scan sponsor_scan_ticket_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sponsor_scan
    ADD CONSTRAINT sponsor_scan_ticket_id_fkey FOREIGN KEY (ticket_id) REFERENCES public.ticket(id);


--
-- Name: spring_session_attributes spring_session_attributes_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.spring_session_attributes
    ADD CONSTRAINT spring_session_attributes_fk FOREIGN KEY (session_id) REFERENCES public.spring_session(session_id) ON DELETE CASCADE;


--
-- Name: ticket_category ticket_category_event_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_category
    ADD CONSTRAINT ticket_category_event_id_fkey FOREIGN KEY (event_id) REFERENCES public.event(id);


--
-- Name: ticket ticket_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket
    ADD CONSTRAINT ticket_category_id_fkey FOREIGN KEY (category_id) REFERENCES public.ticket_category(id);


--
-- Name: ticket_category ticket_category_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_category
    ADD CONSTRAINT ticket_category_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: ticket_category_text ticket_category_text_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_category_text
    ADD CONSTRAINT ticket_category_text_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: ticket_category_text ticket_category_text_ticket_category_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_category_text
    ADD CONSTRAINT ticket_category_text_ticket_category_id_fk_fkey FOREIGN KEY (ticket_category_id_fk) REFERENCES public.ticket_category(id);


--
-- Name: ticket ticket_event_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket
    ADD CONSTRAINT ticket_event_id_fkey FOREIGN KEY (event_id) REFERENCES public.event(id);


--
-- Name: ticket_field_configuration ticket_field_configuration_event_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_field_configuration
    ADD CONSTRAINT ticket_field_configuration_event_id_fk_fkey FOREIGN KEY (event_id_fk) REFERENCES public.event(id);


--
-- Name: ticket_field_configuration ticket_field_configuration_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_field_configuration
    ADD CONSTRAINT ticket_field_configuration_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: ticket_field_description ticket_field_description_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_field_description
    ADD CONSTRAINT ticket_field_description_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: ticket_field_description ticket_field_description_ticket_field_configuration_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_field_description
    ADD CONSTRAINT ticket_field_description_ticket_field_configuration_id_fk_fkey FOREIGN KEY (ticket_field_configuration_id_fk) REFERENCES public.ticket_field_configuration(id);


--
-- Name: ticket_field_value ticket_field_value_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_field_value
    ADD CONSTRAINT ticket_field_value_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: ticket_field_value ticket_field_value_ticket_field_configuration_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_field_value
    ADD CONSTRAINT ticket_field_value_ticket_field_configuration_id_fk_fkey FOREIGN KEY (ticket_field_configuration_id_fk) REFERENCES public.ticket_field_configuration(id);


--
-- Name: ticket_field_value ticket_field_value_ticket_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_field_value
    ADD CONSTRAINT ticket_field_value_ticket_id_fk_fkey FOREIGN KEY (ticket_id_fk) REFERENCES public.ticket(id);


--
-- Name: ticket ticket_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket
    ADD CONSTRAINT ticket_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: ticket ticket_special_price_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket
    ADD CONSTRAINT ticket_special_price_id_fk_fkey FOREIGN KEY (special_price_id_fk) REFERENCES public.special_price(id);


--
-- Name: ticket ticket_tickets_reservation_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket
    ADD CONSTRAINT ticket_tickets_reservation_id_fkey FOREIGN KEY (tickets_reservation_id) REFERENCES public.tickets_reservation(id);


--
-- Name: tickets_reservation tickets_reservation_event_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tickets_reservation
    ADD CONSTRAINT tickets_reservation_event_id_fk_fkey FOREIGN KEY (event_id_fk) REFERENCES public.event(id);


--
-- Name: tickets_reservation tickets_reservation_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tickets_reservation
    ADD CONSTRAINT tickets_reservation_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: tickets_reservation tickets_reservation_promo_code_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tickets_reservation
    ADD CONSTRAINT tickets_reservation_promo_code_id_fk_fkey FOREIGN KEY (promo_code_id_fk) REFERENCES public.promo_code(id);


--
-- Name: waiting_queue waiting_queue_event_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.waiting_queue
    ADD CONSTRAINT waiting_queue_event_id_fkey FOREIGN KEY (event_id) REFERENCES public.event(id);


--
-- Name: waiting_queue waiting_queue_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.waiting_queue
    ADD CONSTRAINT waiting_queue_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: waiting_queue waiting_queue_ticket_reservation_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.waiting_queue
    ADD CONSTRAINT waiting_queue_ticket_reservation_id_fkey FOREIGN KEY (ticket_reservation_id) REFERENCES public.tickets_reservation(id);


--
-- Name: whitelisted_ticket whitelisted_ticket_group_link_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.whitelisted_ticket
    ADD CONSTRAINT whitelisted_ticket_group_link_id_fk FOREIGN KEY (group_link_id_fk) REFERENCES public.group_link(id);


--
-- Name: whitelisted_ticket whitelisted_ticket_group_member_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.whitelisted_ticket
    ADD CONSTRAINT whitelisted_ticket_group_member_id_fk FOREIGN KEY (group_member_id_fk) REFERENCES public.group_member(id);


--
-- Name: whitelisted_ticket whitelisted_ticket_organization_id_fk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.whitelisted_ticket
    ADD CONSTRAINT whitelisted_ticket_organization_id_fk_fkey FOREIGN KEY (organization_id_fk) REFERENCES public.organization(id);


--
-- Name: whitelisted_ticket whitelisted_ticket_ticket_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.whitelisted_ticket
    ADD CONSTRAINT whitelisted_ticket_ticket_id_fk FOREIGN KEY (ticket_id_fk) REFERENCES public.ticket(id);


--
-- Name: a_group; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.a_group ENABLE ROW LEVEL SECURITY;

--
-- Name: a_group a_group_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY a_group_access_policy ON public.a_group USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(organization_id_fk));


--
-- Name: additional_service; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.additional_service ENABLE ROW LEVEL SECURITY;

--
-- Name: additional_service additional_service_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY additional_service_access_policy ON public.additional_service USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(( SELECT event.org_id
   FROM public.event
  WHERE (event.id = additional_service.event_id_fk))));


--
-- Name: additional_service_description; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.additional_service_description ENABLE ROW LEVEL SECURITY;

--
-- Name: additional_service_description additional_service_description_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY additional_service_description_access_policy ON public.additional_service_description USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(( SELECT additional_service.organization_id_fk
   FROM public.additional_service
  WHERE (additional_service.id = additional_service_description.additional_service_id_fk))));


--
-- Name: additional_service_item; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.additional_service_item ENABLE ROW LEVEL SECURITY;

--
-- Name: additional_service_item additional_service_item_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY additional_service_item_access_policy ON public.additional_service_item USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(( SELECT event.org_id
   FROM public.event
  WHERE (event.id = additional_service_item.event_id_fk))));


--
-- Name: admin_reservation_request; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.admin_reservation_request ENABLE ROW LEVEL SECURITY;

--
-- Name: admin_reservation_request admin_reservation_request_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY admin_reservation_request_access_policy ON public.admin_reservation_request USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(( SELECT event.org_id
   FROM public.event
  WHERE (event.id = admin_reservation_request.event_id))));


--
-- Name: auditing; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.auditing ENABLE ROW LEVEL SECURITY;

--
-- Name: auditing auditing_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY auditing_access_policy ON public.auditing USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(( SELECT event.org_id
   FROM public.event
  WHERE (event.id = auditing.event_id))));


--
-- Name: b_transaction; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.b_transaction ENABLE ROW LEVEL SECURITY;

--
-- Name: b_transaction b_transaction_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY b_transaction_access_policy ON public.b_transaction USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(( SELECT tickets_reservation.organization_id_fk
   FROM public.tickets_reservation
  WHERE (tickets_reservation.id = b_transaction.reservation_id))));


--
-- Name: billing_document; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.billing_document ENABLE ROW LEVEL SECURITY;

--
-- Name: billing_document billing_document_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY billing_document_access_policy ON public.billing_document USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(organization_id_fk));


--
-- Name: configuration_event; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.configuration_event ENABLE ROW LEVEL SECURITY;

--
-- Name: configuration_event configuration_event_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY configuration_event_access_policy ON public.configuration_event USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(organization_id_fk));


--
-- Name: configuration_organization; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.configuration_organization ENABLE ROW LEVEL SECURITY;

--
-- Name: configuration_organization configuration_organization_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY configuration_organization_access_policy ON public.configuration_organization USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(organization_id_fk));


--
-- Name: configuration_ticket_category; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.configuration_ticket_category ENABLE ROW LEVEL SECURITY;

--
-- Name: configuration_ticket_category configuration_ticket_category_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY configuration_ticket_category_access_policy ON public.configuration_ticket_category USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(organization_id_fk));


--
-- Name: email_message; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.email_message ENABLE ROW LEVEL SECURITY;

--
-- Name: email_message email_message_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY email_message_access_policy ON public.email_message USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(( SELECT event.org_id
   FROM public.event
  WHERE (event.id = email_message.event_id))));


--
-- Name: event; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.event ENABLE ROW LEVEL SECURITY;

--
-- Name: event event_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY event_access_policy ON public.event USING (public.alfio_check_row_access(org_id)) WITH CHECK (public.alfio_check_row_access(org_id));


--
-- Name: event_description_text; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.event_description_text ENABLE ROW LEVEL SECURITY;

--
-- Name: event_description_text event_description_text_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY event_description_text_access_policy ON public.event_description_text USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(( SELECT event.org_id
   FROM public.event
  WHERE (event.id = event_description_text.event_id_fk))));


--
-- Name: group_link; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.group_link ENABLE ROW LEVEL SECURITY;

--
-- Name: group_link group_link_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY group_link_access_policy ON public.group_link USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(( SELECT event.org_id
   FROM public.event
  WHERE (event.id = group_link.event_id_fk))));


--
-- Name: group_member; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.group_member ENABLE ROW LEVEL SECURITY;

--
-- Name: group_member group_member_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY group_member_access_policy ON public.group_member USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(( SELECT a_group.organization_id_fk
   FROM public.a_group
  WHERE (a_group.id = group_member.a_group_id_fk))));


--
-- Name: invoice_sequences; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.invoice_sequences ENABLE ROW LEVEL SECURITY;

--
-- Name: invoice_sequences invoice_sequences_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY invoice_sequences_access_policy ON public.invoice_sequences USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(organization_id_fk));


--
-- Name: j_user_organization; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.j_user_organization ENABLE ROW LEVEL SECURITY;

--
-- Name: j_user_organization j_user_organization_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY j_user_organization_access_policy ON public.j_user_organization USING (public.alfio_check_row_access(org_id)) WITH CHECK (public.alfio_check_row_access(org_id));


--
-- Name: organization; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.organization ENABLE ROW LEVEL SECURITY;

--
-- Name: organization organization_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY organization_access_policy ON public.organization USING (public.alfio_check_row_access(id)) WITH CHECK (public.alfio_check_row_access(id));


--
-- Name: promo_code; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.promo_code ENABLE ROW LEVEL SECURITY;

--
-- Name: promo_code promo_code_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY promo_code_access_policy ON public.promo_code USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(organization_id_fk));


--
-- Name: resource_event; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.resource_event ENABLE ROW LEVEL SECURITY;

--
-- Name: resource_event resource_event_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY resource_event_access_policy ON public.resource_event USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(organization_id_fk));


--
-- Name: resource_organizer; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.resource_organizer ENABLE ROW LEVEL SECURITY;

--
-- Name: resource_organizer resource_organizer_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY resource_organizer_access_policy ON public.resource_organizer USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(organization_id_fk));


--
-- Name: scan_audit; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.scan_audit ENABLE ROW LEVEL SECURITY;

--
-- Name: scan_audit scan_audit_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY scan_audit_access_policy ON public.scan_audit USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(( SELECT event.org_id
   FROM public.event
  WHERE (event.id = scan_audit.event_id_fk))));


--
-- Name: special_price; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.special_price ENABLE ROW LEVEL SECURITY;

--
-- Name: special_price special_price_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY special_price_access_policy ON public.special_price USING (((organization_id_fk IS NULL) OR public.alfio_check_row_access(organization_id_fk))) WITH CHECK (((ticket_category_id IS NULL) OR public.alfio_check_row_access(( SELECT ticket_category.organization_id_fk
   FROM public.ticket_category
  WHERE (ticket_category.id = special_price.ticket_category_id)))));


--
-- Name: sponsor_scan; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.sponsor_scan ENABLE ROW LEVEL SECURITY;

--
-- Name: sponsor_scan sponsor_scan_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY sponsor_scan_access_policy ON public.sponsor_scan USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(( SELECT event.org_id
   FROM public.event
  WHERE (event.id = sponsor_scan.event_id))));


--
-- Name: ticket; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.ticket ENABLE ROW LEVEL SECURITY;

--
-- Name: ticket ticket_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY ticket_access_policy ON public.ticket USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(( SELECT event.org_id
   FROM public.event
  WHERE (event.id = ticket.event_id))));


--
-- Name: ticket_category; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.ticket_category ENABLE ROW LEVEL SECURITY;

--
-- Name: ticket_category ticket_category_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY ticket_category_access_policy ON public.ticket_category USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(( SELECT event.org_id
   FROM public.event
  WHERE (event.id = ticket_category.event_id))));


--
-- Name: ticket_category_text; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.ticket_category_text ENABLE ROW LEVEL SECURITY;

--
-- Name: ticket_category_text ticket_category_text_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY ticket_category_text_access_policy ON public.ticket_category_text USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(( SELECT ticket_category.organization_id_fk
   FROM public.ticket_category
  WHERE (ticket_category.id = ticket_category_text.ticket_category_id_fk))));


--
-- Name: ticket_field_configuration; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.ticket_field_configuration ENABLE ROW LEVEL SECURITY;

--
-- Name: ticket_field_configuration ticket_field_configuration_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY ticket_field_configuration_access_policy ON public.ticket_field_configuration USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(( SELECT event.org_id
   FROM public.event
  WHERE (event.id = ticket_field_configuration.event_id_fk))));


--
-- Name: ticket_field_description; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.ticket_field_description ENABLE ROW LEVEL SECURITY;

--
-- Name: ticket_field_description ticket_field_description_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY ticket_field_description_access_policy ON public.ticket_field_description USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(( SELECT ticket_field_configuration.organization_id_fk
   FROM public.ticket_field_configuration
  WHERE (ticket_field_configuration.id = ticket_field_description.ticket_field_configuration_id_fk))));


--
-- Name: ticket_field_value; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.ticket_field_value ENABLE ROW LEVEL SECURITY;

--
-- Name: ticket_field_value ticket_field_value_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY ticket_field_value_access_policy ON public.ticket_field_value USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(( SELECT ticket_field_configuration.organization_id_fk
   FROM public.ticket_field_configuration
  WHERE (ticket_field_configuration.id = ticket_field_value.ticket_field_configuration_id_fk))));


--
-- Name: tickets_reservation; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.tickets_reservation ENABLE ROW LEVEL SECURITY;

--
-- Name: tickets_reservation tickets_reservation_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY tickets_reservation_access_policy ON public.tickets_reservation USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(( SELECT event.org_id
   FROM public.event
  WHERE (event.id = tickets_reservation.event_id_fk))));


--
-- Name: waiting_queue; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.waiting_queue ENABLE ROW LEVEL SECURITY;

--
-- Name: waiting_queue waiting_queue_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY waiting_queue_access_policy ON public.waiting_queue USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(( SELECT event.org_id
   FROM public.event
  WHERE (event.id = waiting_queue.event_id))));


--
-- Name: whitelisted_ticket; Type: ROW SECURITY; Schema: public; Owner: postgres
--

ALTER TABLE public.whitelisted_ticket ENABLE ROW LEVEL SECURITY;

--
-- Name: whitelisted_ticket whitelisted_ticket_access_policy; Type: POLICY; Schema: public; Owner: postgres
--

CREATE POLICY whitelisted_ticket_access_policy ON public.whitelisted_ticket USING (public.alfio_check_row_access(organization_id_fk)) WITH CHECK (public.alfio_check_row_access(( SELECT group_member.organization_id_fk
   FROM public.group_member
  WHERE (group_member.id = whitelisted_ticket.group_member_id_fk))));


--
-- PostgreSQL database dump complete
--

